-- Please DON'T change the following 
-- --
--  RobBossMods  	Author: Robert Hartmann
--					Mail  : rbm@keinerspieltmitmir.de
--  Lufti on Nethersturm-EU
--  feel free to change the explanations but email me them because
--  i will distribute the addon on curse.com and wowinterface.com
--  please never remove this comment, many thanks!
local L 
local inquestion = false
local precombat = false
local incombat = false
local lastinstance = nil
local frame = CreateFrame("Frame")
local lastSpoken = {}
local lastboss = nil
local lastbossplot = 0
local lastinfoplot = 0
local lastlootplot = 0
local lastachieveplot = 0
local lastachieveallplot = 0
local lasttargetachievement = 0
local aktiv = 1
local myname = ""
local mytime = 0
local sendbool = 0
local partymember = 0
local inheroic = true
local mynumber = math.random(50000)
local rbm_version = string.format("%.2f" , 2.20)
local rbm_displayversion = tostring(rbm_version).." release"
local update_told = 0
local lastping  = 0
local already_told = {}
local ignoreplayer = {}
local requestamount = {}
local firstrequest = {}
local lang = ""
local ldb
local havetotell = false
local dataobj
local dummy_target1=""
local dummy_target2=""
local dummy_counter = 0
local dummy_bool = false
local whispertarget
local caller = nil
local last_set_dropdown_ID = 0


frame:RegisterEvent("PLAYER_TARGET_CHANGED")
frame:RegisterEvent("CHAT_MSG_PARTY")
frame:RegisterEvent("CHAT_MSG_PARTY_LEADER")
frame:RegisterEvent("CHAT_MSG_WHISPER")
frame:RegisterEvent("CHAT_MSG_ADDON")
--frame:RegisterEvent("GROUP_ROSTER_CHANGED")
frame:RegisterEvent("PLAYER_LOGIN")
frame:RegisterEvent("INSTANCE_LOCK_START")
frame:RegisterEvent("ZONE_CHANGED_NEW_AREA")
frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
frame:RegisterEvent("PLAYER_REGEN_ENABLED")
frame:RegisterEvent("PLAYER_REGEN_DISABLED")
frame:RegisterEvent("CHAT_MSG_INSTANCE_CHAT")
frame:RegisterEvent("CHAT_MSG_INSTANCE_CHAT_LEADER")
SLASH_RBM1, SLASH_RBM2 = '/rbm', '/robbossmods';


	
--[[ 
-- -----------------------
--	HELPER FUNCTIONS 
-- -----------------------
--]]

local five_achieve = {
[68] = {5289},
[76] = {5744},
[77] = {5761},
[302] = {6420,6400},
[321] = {6713},
[303] = {6476},
[246] = {6396,6821},
[558] = {9081},
[559] = {9058},

}


--achievemnts
local all_achieve = {
--cata 5man
[89] = {5366},[90] = {5367},[91] = {5368},[92] = {5369},[93] = {5370},[95] = {5371},[96] = {5503},[98] = {5504},[100] = {5505},[101] = {5285},[104] = {5286},[105] = {5281},
[106] = {5282},[107] = {5283},[109] = {5284},[113] = {5287},[116] = {5288},[118] = {5291},[119] = {5290},[122] = {5292},[124] = {5293},[125] = {5294},[126] = {5296},[130] = {5295},
[131] = {5297},[134] = {5298},[283] = {5995},[323] = {6130},[290] = {6127},[292] = {6070},[341] = {6132},[175] = {5743},[176] = {5762}, [185] = {5759}, [189] = {5858, 5750}, [191] = {5760},
--raids cata
[154] = {5304},[155] = {5305},[156] = {5300},[157] = {4852},[158] = {5311},[167] = {5312},[168] = {5313},[169] = {5307},[170] = {5306},[171] = {5308},[172] = {5309},[173] = {5310},
[174] = {4849},[192] = {5821},[193] = {5810},[194] = {5813},[195] = {5829},[196] = {5830},[197] = {5799},[311] = {6174},[325] = {6129},[324] = {6128},[317] = {6175},[331] = {6084},
[332] = {6105},[318] = {6133},[333] = {6180},

--mop 5man
[335] = {6475, 6671}, [672] = {6460}, [669] = {6420}, [668] = {6089}, [657] = {6477}, [686] = {6471}, [685] = {6472}, [692] = {6485}, [727] = {6822}, 
[738] = {6688}, [671] = {6928}, [674] = {6929}, [660] = {6684}, [654] = {6427}, [665] = {6394}, [663] = {6531}, [690] = {6478}, [698] = {6736}, 


--wod 5man
[889] = {9005,9008},
[893] = {8993},
[965] = {9033},
[967] = {9035},
[968] = {9036},
[1138] = {9024},
[1139] = {9018},
[1140] = {9025},
[1160] = {9026},
[1163] = {9007},
[1186] = {9023},
[1208] = {9493},
[1210] = {9223},
[1214] = {9017},
[1216] = {9551},
[1225] = {9552},
[1226] = {9045},
[1234] = {9057},
[1235] = {9083},
[1238] = {9082},


--wotlk 5man
-- TODO PIT OF SARON: 4525  |? 1865
[603] = {4526},
[608] = {4524},
[615] = {4522},
[616] = {4523},
[641] = {2043},
[643] = {2156,1873},
[644] = {2157,1790},
[638] = {1919},
[605] = {1866},
[606] = {2154},
[607] = {2155},
[597] = {1834},
[598] = {2042},
[600] = {1867},
[628] = {2041},
[632] = {1816},
[631] = {2153},
[625] = {2046,2045,2044,1868,1871},
[618] = {2150},
[621] = {2036},
[619] = {2037},
[594] = {2040},
[596] = {2152,1864},
[592] = {2058},
[590] = {2039},
[589] = {2057},
[588] = {2151},
}
 
function __genOrderedIndex( t )
    local orderedIndex = {}
    for key in pairs(t) do
        table.insert( orderedIndex, key )
    end
    table.sort( orderedIndex )
    return orderedIndex
end

function orderedNext(t, state)
    -- Equivalent of the next function, but returns the keys in the alphabetic
    -- order. We use a temporary ordered key table that is stored in the
    -- table being iterated.

    --print("orderedNext: state = "..tostring(state) )
    if state == nil then
        -- the first time, generate the index
        t.__orderedIndex = __genOrderedIndex( t )
        key = t.__orderedIndex[1]
        return key, t[key]
    end
    -- fetch the next value
    key = nil
    for i = 1,table.getn(t.__orderedIndex) do
        if t.__orderedIndex[i] == state then
            key = t.__orderedIndex[i+1]
        end
    end

    if key then
        return key, t[key]
    end

    -- no more value to return, cleanup
    t.__orderedIndex = nil
    return
end

function orderedPairs(t)
    -- Equivalent of the pairs() function on tables. Allows to iterate
    -- in order
    return orderedNext, t, nil
end


local function prepSpell(tmsg) 
	local repstring = ""
	local lastbegin = 1
	local inSpell = false
	for i=1,#tmsg,1 do
		if tmsg:byte(i) == 93 then
			inSpell = false
			lastbegin = i
		end
		if inSpell then
			if tmsg:byte(i) == 32 then
				repstring = repstring.."_"
			else
				repstring = repstring..string.char(tmsg:byte(i))
			end
		end
		if tmsg:byte(i) == 91 then
			inSpell= true
			repstring = repstring..tmsg:sub(lastbegin,i)
		end
	end
	if lastbegin < #tmsg then
		repstring = repstring..tmsg:sub(lastbegin);
	end
	return repstring
end

local function table_concat(t1, t2)
	for i=1,#t2,1 do
		t1[#t1+1] = t2[i]
	end
	return t1
end
	
local function dig(tabelle)
	local ret_t = {}
	for k,v in pairs(tabelle) do
		if type(v) == "table" then
			table_concat(ret_t,dig(v));
		else
			ret_t[#ret_t+1] = v
		end
	end
	return ret_t
end	

local function spelllinkBreak(line)
	local tag = false
	local nline = ""
	for i = 1, #line do
		local c = line:sub(i,i)
		if c == "[" then
			tag = true
			nline = nline..c
		elseif c == "]" then
			tag = false
			nline = nline..c
		elseif tag and c == " " then
			nline = nline.."_"
		else
			nline = nline..c
		end
	end
	local wordlist = string.gmatch(nline, "%S+")
	local count = 1
	local ret = {}
	for word in wordlist do
		ret[count] = string.gsub(word, "_", " ")
		count = count +1
	end
	return ret
end

local function prepareLinkStrings(toPostString)
	local announceStrings = {""}
	for line in string.gmatch(toPostString,"[^\n]+") do
		local iarr = spelllinkBreak(line)
		for i=1,#iarr,1 do
			local word = iarr[i]
			if strlen(announceStrings[#announceStrings])+1+strlen(word) > 255 then
				tinsert(announceStrings, word)
			else
				if strlen(announceStrings[#announceStrings]) > 0 then
					announceStrings[#announceStrings] = announceStrings[#announceStrings].." "..word
				else
					announceStrings[#announceStrings] = word
				end
			end
		end
		tinsert(announceStrings, "")
	end
	return announceStrings
end

local function GetLocalSpellLink(sid) 
	local r = GetSpellLink(sid)
	if r == nil then 
		return sid
	else 
		return r
	end
end

local function parseSpellID(text)
	local s = text:find("SPELL")
	if s == nil then
		return text
	end
	local e = text:find(")",s)
	local temps = text:sub(s,e)
	local spellid = tonumber(temps:match("%d+"))
	
	---
	local s2 = text:find("SPELL",e)
	if s2 == nil then
		if e+1 > #text then
			return text:sub(1,s-1)..GetLocalSpellLink(spellid)
		else
			return text:sub(1,s-1)..GetLocalSpellLink(spellid)..text:sub(e+1)
		end
	else
		--rekursion
		return text:sub(1,s-1)..GetLocalSpellLink(spellid)..parseSpellID(text:sub(e+1))
	end
end

local function getType(ggx)
	if ggx == "" then
		return nil
	end
	local unitType = string.split("-",ggx);
	if unitType == 'Creature' then	
		return 3
	elseif unitType == 'Vehicle' then	
		return 5
	elseif unitType == 'Player' then 
		return 0
	end
	return -1
end

local function getNPCId(ggg)
	local _,_,_,_,_,npdID = string.split(":",ggg);
	if ggx == "" then
		return nil
	end
	return npdID
end


function GetInstanceDifficulty() 
	_, _, difficultyID = GetInstanceInfo();
	return (difficultyID + 1)
end
	

--[[ 
-----------------------------
-------------   ACTUAL CODE
-----------------------------
--]]

local boss_dead = {}


local function getNextBoss(iID, eID) 
	EJ_SelectInstance(iID)
	for i = 1, 20, 1 do
		local name, _, encounterID = EJ_GetEncounterInfoByIndex(i, iID)
		if encounterID == eID then
			local _,_,ret = EJ_GetEncounterInfoByIndex(i+1, iID)
			return ret
		end
	end
	return eID
end

local function getPrevBoss(iID, eID) 
	EJ_SelectInstance(iID)
	for i = 1, 20, 1 do
		local name, _, encounterID = EJ_GetEncounterInfoByIndex(i, iID)
		if encounterID == eID then
			if i > 1 then
				local _,_,ret = EJ_GetEncounterInfoByIndex(i-1, iID)
				return ret
			else
				return nil
			end
		end
	end
	return eID
end

local addonIndex = { 
	["bfa"] = "Battle for Azeroth",
	["legion"] = "Legion",
	["wod"] = "Warlords of Draenor",
	["mop"] = "Mists of Pandaria",
	["cata"]  = "Cataclysm",
	["wotlk"] = "Wrath of the Lich King",
	["bc"] = "Burning Crusade",
	["vanilla"] = "Classic"
}
	

local vanilla_five = { 63, 226,227,228,229,230,231,232,233,234,236,237,238,239,241,246, 240}
local vanilla_raid = {741,742,743,744}

local bc_five =  { 247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262}
local bc_raid = {745,746,747,748,749,750,751,752}

local wotlk_five = {271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286}
local wotlk_raid = {753,754,755,756,757,758,759,760,761}

local cata_five = { 63, 64, 65, 66, 67, 68, 69, 70, 71,  76, 77,  184, 185, 186 }
local cata_raid = { 72, 73, 74, 75, 78, 187 }

local mop_five = { 302, 303, 311, 312, 313, 316, 321, 246, 324 }
local mop_raid = {317,320,322, 330, 362, 369}

local wod_five = {385,476,536,537,547,556,558,559}
local wod_raid = {457,477,557,669}

local legion_five = {707,716,721,726,727,740,762,767,777,800,900,945}
local legion_raid = {768,786,860,861,875,946}

local bfa_five = {968,1001,1002,1012,1021,1022,1023,1030,1036,1041}
local bfa_raid = {1031}

local current_five = bfa_five;
local current_raid = bfa_raid;

local fortymans = {741,742,744}
local twentyfivemans = {746, 747, 748, 749, 750, 751, 752}
local mythiconly = { 860 }

local alltogether = {} 

local function isIdMythicOnly(id)
	for k,v in ipairs(mythiconly) do
		if id == v then
			return true
		end
	end
	return false
end

local function isIdRaid40(id)
	for k,v in ipairs(fortymans) do
		if id == v then
			return true
		end
	end
	return false
end

local function isIdRaid25(id)
	for k,v in ipairs(twentyfivemans) do
		if id == v then
			return true
		end
	end
	return false
end



local function isIdRaid(id) 
	for k,v in ipairs(current_raid) do
		if id == v then
			return true
		end
	end
	return false
end

table.insert(alltogether, "header.\124cFFFF7F00>> BFA <<\124r");
for k,v in ipairs(bfa_five) do
	table.insert(alltogether,v)
end

for k,v in ipairs(bfa_raid) do
	table.insert(alltogether,v)
end

table.insert(alltogether, "header.\124cFFFF7F00>> LEGION <<\124r");
for k,v in ipairs(legion_five) do
	table.insert(alltogether,v)
end

for k,v in ipairs(legion_raid) do
	table.insert(alltogether,v)
end

table.insert(alltogether, "header.\124cFFFF7F00>> WOD <<\124r");
for k,v in ipairs(wod_five) do
	table.insert(alltogether,v)
end

for k,v in ipairs(wod_raid) do
	table.insert(alltogether,v)
end

table.insert(alltogether, "header.\124cFFFF7F00>> MOP <<\124r");
for k,v in ipairs(mop_five) do
	table.insert(alltogether,v)
end

for k,v in ipairs(mop_raid) do
	table.insert(alltogether,v)
end

table.insert(alltogether, "header.\124cFFFF7F00>> CATA <<\124r");
for k,v in ipairs(cata_five) do
	table.insert(alltogether,v)
end

for k,v in ipairs(cata_raid) do
	table.insert(alltogether,v)
end

table.insert(alltogether, "header.\124cFFFF7F00>> WOTLK <<\124r");
for k,v in ipairs(wotlk_five) do
	table.insert(alltogether,v)
end

table.insert(alltogether, "header.\124cFFFF7F00>> BC <<\124r");
for k,v in ipairs(bc_five) do
	table.insert(alltogether,v)
end

table.insert(alltogether, "header.\124cFFFF7F00>> CLASSIC <<\124r");
for k,v in ipairs(vanilla_five) do
	table.insert(alltogether,v)
end

local boss_dynamic = { 52286, 52271, 23863, 23577, 23574 , 52148, 52053, 52059 , 52151, 52155};

local function check_dynamic_b(tocheck)
	for _,v in ipairs(boss_dynamic) do
		if v == tonumber(tocheck) then
			return true
		end
	end
	return false
end

local function digAddonsForBossid(bossid)
	local addons = { bfa_five, bfa_raid, legion_five, legion_raid, wod_five, wod_raid, mop_five, mop_raid, cata_five, cata_raid, wotlk_five, bc_five, vanilla_five}
	local return_strings = { "bfa", "bfa", "legion", "legion", "wod", "wod" , "mop" , "mop", "cata", "cata","wotlk",  "bc", "vanilla", }
	for index,v in ipairs(addons) do
		for _,v2 in pairs(v) do
			EJ_SelectInstance(v2)
			for ix = 1, 20, 1 do
				local _,_, encounterID = EJ_GetEncounterInfoByIndex(ix, v2)
				if encounterID == bossid then
					return return_strings[index], v2
				end
			end
		end
	end
	return false
end


local currentAddon = "bfa"
RobBossMods = LibStub("AceAddon-3.0"):NewAddon("RobBossMods")
local rbmLDB 
local icon = LibStub("LibDBIcon-1.0")

function RobBossMods:Initialize()

	if rbm_icon == nil then
		rbm_icon ="buch" 
	end
	rbmLDB = LibStub("LibDataBroker-1.1"):NewDataObject("RBM", {
	type = "data source",
	text = "RobBossMods",
	--icon = "Interface\\Icons\\INV_Chest_Cloth_17",
	icon = "Interface\\AddOns\\RobBossMods\\Media\\"..rbm_icon..".tga",
	OnClick = function(self, button) 
		if(button=="LeftButton")then
			if RobBossMods.Anchor:IsVisible() then
				RobBossMods.Anchor:Hide();
			else
				RobBossMods.Anchor:Show();
			end
		else
			InterfaceOptionsFrame_OpenToCategory("RobBossMods");
		end	
	end,
	OnEnter = function(self)
		GameTooltip:SetOwner(self, "ANCHOR_TOPRIGHT");
	--security
	if lastboss ~= nil then
		if lastboss < 0 then
			lastboss = nil
		end
	end
	if lastboss ~= nil and EJ_GetEncounterInfo(lastboss) ~= nil then
		GameTooltip:AddLine("Rob Boss Mods - "..L["CURRENTBOSS"]..": "..EJ_GetEncounterInfo(lastboss));
	else
		lastboss = nil
		GameTooltip:AddLine("Rob Boss Mods - "..L["CURRENTBOSS"]..": "..L["NOBOSS"]);
	end
	GameTooltip:AddLine(L["LEFTCLICK"],1,1,1);
	GameTooltip:AddLine(L["RIGHTCLICK"],1,1,1);
	GameTooltip:AddLine(L["DRAG"],1,1,1);
	GameTooltip:Show();
	end,
	OnLeave = function()
		GameTooltip:Hide()
	end,
})

	-- Obviously you'll need a ## SavedVariables: BunniesDB line in your TOC, duh!
	self.db = LibStub("AceDB-3.0"):New("rbmDB", {
		char = {
			minimap = {
				hide = false,
			},
		},
	})
	if self.db.char.minimap.minimapPos == nil then
		self.db.char.minimap.minimapPos = 250
	end
		
	icon:Register("RobBossMods", rbmLDB, self.db.char.minimap)
end

	

local function checkUpdate()
	SendAddonMessage( "RBMVersion", rbm_version, "GUILD" );
	if IsInGroup() and GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
		SendAddonMessage( "RBMVersion", rbm_version, "PARTY" );
	end
	if IsInGroup() and GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
		SendAddonMessage( "RBMVersion", rbm_version, "INSTANCE_CHAT" );
	end
end

local function resetLang()
	StaticPopupDialogs["RBM_LANGC1"] = {
		text = "RBM: In What language do you want the boss explanation to appear?",
		button1 = "English",
		button2 = "German",
		OnAccept = function()
			rbm_elang = "en"
			StaticPopup_Show ("RBM_LANGCONFIRM")
		end,
		OnCancel = function()
			rbm_elang = "de"
			StaticPopup_Show ("RBM_LANGCONFIRM")
		end,
		timeout = 0,
		whileDead = true,
		hideOnEscape = true,
	}
	StaticPopupDialogs["RBM_LANGCONFIRM"] = {
		text = "The Language change wont take effect until you restart UI. Restart UI now?",
		button1 = "Reload UI",
		button2 = "No, I'll do that later",
		OnAccept = function()
			ReloadUI();
		end,
		timeout = 0,
		whileDead = true,
		hideOnEscape = true,
	}
	StaticPopup_Show ("RBM_LANGC1")
end

local function dynamic_toggle_d()
	for _,v in ipairs(boss_dynamic) do
		RobBossMods.db.global.dynamic["D"..v] = false
	end
end

local function dynamic_toggle_a()
	for _,v in ipairs(boss_dynamic) do
		RobBossMods.db.global.dynamic["D"..v] = true
	end
end



local function getLastDeadBoss() 
	local _,_, encountersTotal,enN = GetInstanceLockTimeRemaining();
	print(enN.."/"..encountersTotal);
	for i= encountersTotal, 1, -1 do
		local bname,asdf,BisKilled = GetInstanceLockTimeRemainingEncounter(i);
		local toprint = i.." "..bname.." "
		if BisKilled then toprint = toprint.."true" else toprint = toprint.."false" end
		if BisKilled then
			if i < encountersTotal then
				return i
			else
				for j=1,encountersTotal,1 do
					_,_,isKilled2 = GetInstanceLockTimeRemainingEncounter(j);
					if not isKilled2 then 
						return (isKilled2-1)
					end
				end
			end
		end
	end
	return 0
end

local function handler(msg, editbox) 
	if msg == "status" or msg == "state" then
		if aktiv == 1 then
			ChatFrame1:AddMessage(L["ENABLED"]);
		else
			ChatFrame1:AddMessage(L["DISABLED"]);
		end
	elseif msg == "off" or msg == "aus" then
		aktiv = 0
	elseif msg == "on" or msg == "ein" then
		aktiv = 1
	elseif msg == "check" then
		checkUpdate();
	elseif msg:find("setzahl") ~= nil then
		local setv = tonumber(msg:sub(9));
		mynumber = setv
	elseif msg == "zahl" or msg == "number" then
		ChatFrame1:AddMessage(mynumber);
	elseif msg == "getlang" then
		ChatFrame1:AddMessage("Language: "..rbm_elang);
	elseif msg == "author" or msg == "autor" then
		ChatFrame1:AddMessage(L["AUTHOR1"]);
		ChatFrame1:AddMessage(L["AUTHOR2"]);
	elseif string.find(msg,"test") ~= nil then
		for i=1,10000 do 
			local x = EJ_GetInstanceInfo(i);
			if x ~= nil then 
				print(i.." "..x);
			end 
		end
				
		--for i=1,10 do
		--	local enc_name, _,  enc_id = EJ_GetEncounterInfoByIndex(i,457)
		--	if enc_name ~= null then
		--		print(i.." "..enc_name);
		--	else
		--		print(i);
		--	end
		--end
	elseif msg == "checks" then
		SendAddonMessage( "RBMCheck", "foo" , "GUILD" );
		
		if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
			SendAddonMessage( "RBMCheck", "foo" , "INSTANCE_CHAT" );
		else
			if IsInRaid() then
				SendAddonMessage( "RBMCheck", "foo" , "RAID" );
			elseif IsInGroup() then
				SendAddonMessage( "RBMCheck", "foo" , "PARTY" );
			end
		end
		
	elseif msg == "resetlang" then
		resetLang();
	elseif msg == "toggle" then 
		if RobBossMods.Anchor:IsVisible() then
			RobBossMods.Anchor:Hide();
		else
			RobBossMods.Anchor:Show();
		end
	elseif msg == "show" then
		RobBossMods.Anchor:Show();
	elseif msg == "hide" then
		RobBossMods.Anchor:Hide();
	elseif msg == "minioff" then
		RobBossMods.db.char.minimap.hide = true
		icon:Hide("RobBossMods")
	elseif msg == "minion" then
		RobBossMods.db.char.minimap.hide = false
		icon:Show("RobBossMods")
	elseif string.find(msg,"ignore") then
		--ignore target?
		if msg == "ignore" or msg == "ignore " then
			local uname = UnitName("target");
			if not uname then
				return
			end
			ignoreplayer[uname] = true
		else
			local uname = string.sub(msg,string.find(msg,"ignore ")+7);
			if not uname then
				return
			end
			ignoreplayer[uname] = true
		end
	else
		print(msg);
		print(string.find(msg,"test"));
		ChatFrame1:AddMessage("RobBossMods Version "..rbm_displayversion);
		ChatFrame1:AddMessage(L["MENU_RESETLANG"]);
		ChatFrame1:AddMessage(L["MENU_AUTHOR"]);
		ChatFrame1:AddMessage(L["MENU_HINT1"]);		
		ChatFrame1:AddMessage(L["MENU_MINIMAP_OFF"]);
		ChatFrame1:AddMessage(L["MENU_MINIMAP_ON"]);
		ChatFrame1:AddMessage(L["MENU_FRAME_ON"]);
		ChatFrame1:AddMessage(L["MENU_FRAME_OFF"]);
		ChatFrame1:AddMessage(L["MENU_FRAME_TOGGLE"]);
	end
end

-- Functions Section 
local bosses_label 


--AchievePost  / AchieveAllPost

function compareAchieveStuff(a,b)
	local x,y
	if a ~= nil then
		local _,_,_,complete = GetAchievementInfo(a)
		if complete == true then
			x = math.random(100)+101
		else
			x = math.random(100)
		end
	else 
		x = 0
	end
	if b ~= nil then
		local _,_,_,completeB = GetAchievementInfo(b)
		if completeB == true then
			y = math.random(100)+101
		else
			y = math.random(100)
		end
	else
		y = -1
	end
	--print(x.." "..y);
	return x < y 
end


local function mapEncounterID2InstanceID(encounterID)
	for i=1,1000,1 do
		if EJ_GetInstanceInfo(i) ~= nil then
			for j=1,20,1 do
				--search every boss and look for our boss
				local _,_,locID = EJ_GetEncounterInfoByIndex(j,i)
				if encounterID == locID then
					return i
				end
			end
		end
	end
end
	

local function doRealAchievePost(toPost, alternative)
	
	local iID = mapEncounterID2InstanceID(lastboss)
	
	
	for i=1, #toPost, 1 do
		local tstring = GetAchievementLink(toPost[i])
		local _,_,_, done = GetAchievementInfo(toPost[i])
		if done then 
			tstring = tstring .." (X)"
		end
		
		if RobBossMods.db.char.postchannel == "PARTY" or RobBossMods.db.char.postchannel == "RAID" or RobBossMods.db.char.postchannel == "INSTANCE_CHAT" then
			if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
				RobBossMods.db.char.postchannel = "INSTANCE_CHAT"
			else
				if IsInRaid() then
					RobBossMods.db.char.postchannel = "RAID"
				elseif IsInGroup() then
					RobBossMods.db.char.postchannel = "PARTY"
				end
			end
		end
		
		if RobBossMods.db.char.postchannel == "MYSELF" and alternative == nil then
			if i == 1 then 
				if caller == "single" then 
					ChatFrame1:AddMessage("Achievements for "..EJ_GetEncounterInfo(lastboss))
				else
					ChatFrame1:AddMessage("Achievements for "..EJ_GetInstanceInfo(iID))
				end
			end
			ChatFrame1:AddMessage(tstring);
		elseif alternative ~= nil then
			if i == 1 then
				if caller == "single" then 
					SendChatMessage("Achievements for "..EJ_GetEncounterInfo(lastboss), "WHISPER" , nil, alternative)
				else
					SendChatMessage("Achievements for "..EJ_GetInstanceInfo(iID), "WHISPER" , nil, alternative)
				end
			end
			SendChatMessage(tstring, "WHISPER", nil, alternative);
		else
			if i == 1 then 
				if caller == "single" then 
					if tonumber(RobBossMods.db.char.postchannel) ~= nil then
						SendChatMessage("Achievements for "..EJ_GetEncounterInfo(lastboss), "CHANNEL" , nil, RobBossMods.db.char.postchannel)
					else
						SendChatMessage("Achievements for "..EJ_GetEncounterInfo(lastboss), RobBossMods.db.char.postchannel , nil, whispertarget)
					end
				else
					if tonumber(RobBossMods.db.char.postchannel) ~= nil then
						SendChatMessage("Achievements for "..EJ_GetInstanceInfo(iID), "CHANNEL" , nil, RobBossMods.db.char.postchannel)
					else
						SendChatMessage("Achievements for "..EJ_GetInstanceInfo(iID), RobBossMods.db.char.postchannel , nil, whispertarget)
					end
				end
			end
			if tonumber(RobBossMods.db.char.postchannel) ~= nil then
				SendChatMessage(tstring, "CHANNEL", nil, RobBossMods.db.char.postchannel);
			else
				SendChatMessage(tstring, RobBossMods.db.char.postchannel, nil, whispertarget);
			end
		end
	end
end

local function doAchievePost(alternative) 
	
	if lastboss == nil then return end
	--IDNumber, Name, Points, Completed, Month, Day, Year, Description, Flags, Image, RewardText = GetAchievementInfo(achievementID or categoryID, index)
	--"Link" = GetAchievementLink(AchievementID)
	local toPost = {}
	if all_achieve[lastboss] == nil then 
		ChatFrame1:AddMessage("\124cFFFF7F00RobBossMods:\124r no Achievements for "..EJ_GetEncounterInfo(lastboss));
		return toPost 
	end
	for k,v in pairs(all_achieve[lastboss]) do
		table.insert(toPost,v)
	end
	table.sort(toPost, compareAchieveStuff)
	caller = "single"
	if alternative ~= "RETURN" then
		doRealAchievePost(toPost,alternative)
	else
		return toPost
	end
end

	
local function doAchieveAllPost(alternative)
	if lastboss == nil then return end
	local toPost = {}
	local iID = mapEncounterID2InstanceID(lastboss)
	EJ_SelectInstance(iID)
	for i=1,20,1 do
		_,_,eID = EJ_GetEncounterInfoByIndex(i, iID)
		if eID ~= nil and all_achieve[eID] ~= nil then
			for _,v in pairs(all_achieve[eID]) do
				table.insert(toPost, v)
			end
		end
	end
	
	if five_achieve[iID] ~= nil then
		for _,v in pairs(five_achieve[iID]) do
			table.insert(toPost,v)
		end
	end
	
	table.sort(toPost, compareAchieveStuff)
	caller = "all"
	
	if alternative ~= "RETURN" then
		doRealAchievePost(toPost,alternative)
	else
		return toPost
	end
end


--LootPost

local function doLootPost(alternative)
	if lastboss == nil then return end
	--we use postchannel to post our stuff
	local myitems = {}
	local mylinks = {}
	local minlevel = 200000
	local maxlevel = 0
	local beute = {}
	--ChatFrame1:AddMessage( lastboss)
	for i=5191, 5192, 1 do
		local itemID, encounterID = EJ_GetLootInfo(i)
		local _,_,_, itemLevel = GetItemInfo(itemID)
		
		  -- if select(1,EJ_GetLootInfo(i)) ~= nil then 
			  -- ChatFrame1:AddMessage( "1 "..select(1,EJ_GetLootInfo(i)))
		  -- end 
		
			  -- if select(2,EJ_GetLootInfo(i)) ~= nil then 
			  -- ChatFrame1:AddMessage( "2 "..select(2,EJ_GetLootInfo(i)))
		  -- end 
		
			  -- if select(3,EJ_GetLootInfo(i)) ~= nil then 
			  -- ChatFrame1:AddMessage( "3 "..select(3,EJ_GetLootInfo(i)))
		  -- end 
		
			  -- if select(4,EJ_GetLootInfo(i)) ~= nil then 
			  -- ChatFrame1:AddMessage( "4 "..select(4,EJ_GetLootInfo(i)))
		  -- end 
		
			  -- if select(5,EJ_GetLootInfo(i)) ~= nil then 
			  -- ChatFrame1:AddMessage( "5 "..select(5,EJ_GetLootInfo(i)))
		  -- end 
		
			  -- if select(6,EJ_GetLootInfo(i)) ~= nil then 
			  -- ChatFrame1:AddMessage( "6 "..select(6,EJ_GetLootInfo(i)))
		  -- end 
		
			  -- if select(7,EJ_GetLootInfo(i)) ~= nil then 
			  -- ChatFrame1:AddMessage( "7 "..select(7,EJ_GetLootInfo(i)))
		  -- end 

		
		if itemID > 0 and encounterID == lastboss and itemLevel ~= nil then
			table.insert(myitems, itemID);
			table.insert(mylinks, link);
			
			if beute[itemLevel] == nil then
				beute[itemLevel] = 1
			else 
				beute[itemLevel] = beute[itemLevel] +1
			end
			
			
			if itemLevel > maxlevel and beute[itemLevel] > 2 then
				maxlevel = itemLevel
			end
			if itemLevel < minlevel and beute[itemLevel] > 2 then
				minlevel = itemLevel
			end
		end
	end
	

	
	local normal = {}
	local heroic = {}
	local misc = {}
	
	if maxlevel > minlevel then
		for i=1, #myitems, 1 do
			local _,_,_, itemLevel = GetItemInfo(myitems[i])
			if itemLevel == maxlevel then
				table.insert(heroic, mylinks[i])
			elseif itemLevel == minlevel then
				table.insert(normal, mylinks[i])
			else
				table.insert(misc, mylinks[i])
			end
		end
	else
		normal = mylinks
	end
	
	local toPostString = ""
	if #normal > 0 then
		toPostString = toPostString..L["LOOTFOR"].." "..EJ_GetEncounterInfo(lastboss).."\n"
	end
	if #normal > 0 and #heroic == 0 then
		toPostString = toPostString.."Normal ("..minlevel..")\n"
		for i=1, #normal, 1 do
			toPostString = toPostString.." "..normal[i]
		end
	elseif #normal > 0 and #heroic > 0 then
		toPostString = toPostString.."Normal ("..minlevel..")\n"
		for i=1, #normal, 1 do
			toPostString = toPostString.." "..normal[i]
		end
		toPostString = toPostString.."\nHero ("..maxlevel..")\n"
		for i=1, #heroic, 1 do
			toPostString = toPostString.." "..heroic[i]
		end
	end
	if #misc > 0 then
		toPostString = toPostString.."\nMisc\n"
		for i=1, #misc, 1 do
			toPostString = toPostString.." "..misc[i]
		end
	end
	
	
	--test
	local announceStrings = prepareLinkStrings(toPostString)
	
	if RobBossMods.db.char.postchannel == "PARTY" or RobBossMods.db.char.postchannel == "RAID" or RobBossMods.db.char.postchannel == "INSTANCE_CHAT" then
		if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
			RobBossMods.db.char.postchannel = "INSTANCE_CHAT"
		else
			if IsInRaid() then
				RobBossMods.db.char.postchannel = "RAID"
			elseif IsInGroup() then
				RobBossMods.db.char.postchannel = "PARTY"
			end
		end
	end
	
	for _, msg in ipairs(announceStrings) do
		if RobBossMods.db.char.postchannel == "MYSELF" and alternative == nil then
			ChatFrame1:AddMessage(msg);
		elseif alternative ~= nil then
			SendChatMessage(msg, "WHISPER", nil, alternative);
		else
			if tonumber(RobBossMods.db.char.postchannel) ~= nil then
				SendChatMessage(msg, "CHANNEL", nil, RobBossMods.db.char.postchannel);
			else
				SendChatMessage(msg, RobBossMods.db.char.postchannel, nil, whispertarget);
			end
		end
	end
	--test
	
end


local function doBossPost(channel,reci) 
	if lastboss == nil then
		return
	end
	--bosses
	
	
	
	local toPostString
	if RobBossMods.db.global.customTactics["T"..tostring(lastboss)] == nil then
		toPostString = L["T"..tostring(lastboss)]
	else
		toPostString = RobBossMods.db.global.customTactics["T"..tostring(lastboss)]
	end
	
	
	if channel == "MYSELF" then
		ChatFrame1:AddMessage(L["TACTICS"]..EJ_GetEncounterInfo(lastboss)..": ");
	else
		SendChatMessage(L["TACTICS"]..EJ_GetEncounterInfo(lastboss)..": ", channel, nil, reci);
	end
	
	toPostString = parseSpellID(toPostString);
	
	--announce an split 
	local announceStrings = {""}
	for line in string.gmatch(toPostString,"[^\n]+") do
		local iarr = spelllinkBreak(line)
		for i=1,#iarr,1 do
			local word = iarr[i]
			if strlen(announceStrings[#announceStrings])+1+strlen(word) > 255 then
				tinsert(announceStrings, word)
			else
				if strlen(announceStrings[#announceStrings]) > 0 then
					announceStrings[#announceStrings] = announceStrings[#announceStrings].." "..word
				else
					announceStrings[#announceStrings] = word
				end
			end
		end
		tinsert(announceStrings, "")
	end
	for _, msg in ipairs(announceStrings) do
		if channel == "MYSELF" then
			ChatFrame1:AddMessage(msg);
		else
			SendChatMessage(msg, channel, nil, reci);
		end
	end
	
end	

local function IsEndboss(bo)
	local gefunden = false
	for i,v in ipairs(final_bosses) do
		if bo == v then
			gefunden = true
		end
	end
	return gefunden
end

--DEV



--ENDDEV
function RBM_tellHint() 
	local lokchan;
	if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
		lokchan = "INSTANCE_CHAT"
	elseif GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
		lokchan = "PARTY"
	end
	if RobBossMods.db.global.tellgrid ~= nil then
		if RobBossMods.db.global.tellgrid["hintonce"] == nil then
			SendChatMessage(L["DONTKNOW"], lokchan,nil,nil)
		else
			SendChatMessage(RobBossMods.db.global.tellgrid["hintonce"], lokchan,nil,nil)
		end
	else
		SendChatMessage(L["DONTKNOW"], lokchan,nil,nil)
	end
end

local info = {}




function frame:OnEvent(event,...)
		-- I log in and greet the guild, depending which time it is, see below
		
		--if (event == "CHAT_MSG_PARTY" or event == "CHAT_MSG_PARTY_LEADER" or event == "CHAT_MSG_INSTANCE_CHAT") and aktiv == 1 then
		--if event == "CHAT_MSG_PARTY" or event == "CHAT_MSG_PARTY_LEADER" or event == "CHAT_MSG_INSTANCE_CHAT" then
		--	print("DRIN");
		--	if aktiv == 1 then
		--		ChatFrame1:AddMessage("AKTIV");
		--	else
		--		ChatFrame1:AddMessage("NICHTAKTIV "..aktiv);
		--	end
		
		if event == "PLAYER_LOGIN" then
			C_ChatInfo.RegisterAddonMessagePrefix("RBMPing")
			C_ChatInfo.RegisterAddonMessagePrefix("RBMPong")
			C_ChatInfo.RegisterAddonMessagePrefix("RBMCheckA")
			C_ChatInfo.RegisterAddonMessagePrefix("RBMVersion")
			C_ChatInfo.RegisterAddonMessagePrefix("RBMCheck")
			
			--UIDropDownMenu_Initialize(RBM_DropDownMenuButton, RBM_DropDownMenu_OnLoad);
			--UIDropDownMenu_Initialize(RBM_DropDownAddonButton, RBM_DropDownAddonButton_OnLoad);
			--UIDropDownMenu_SetText(RBM_DropDownMenuButton, "TEAST");
			
			RobBossMods:Initialize()
			
			--if rbm_tellnextboss == nil then
				RobBossMods.db.char.tellnextboss = false
			--end
			if RobBossMods.db.char.telleverybossparty == nil then
				RobBossMods.db.char.telleverybossparty = false
			end
			if RobBossMods.db.char.telleverybossraid == nil then
				RobBossMods.db.char.telleverybossraid = false
			end
			if RobBossMods.db.char.whispermode == nil then
				RobBossMods.db.char.whispermode = false
			end
			if RobBossMods.db.global.tellgrid == nil then
				RobBossMods.db.global.tellgrid = {}
			end
			if RobBossMods.db.global.dynamic == nil then
				RobBossMods.db.global.dynamic = {}
				dynamic_toggle_a();
			end
			if RobBossMods.db.char.showframe == nil then
				RobBossMods.db.char.showframe = false
			end
			if RobBossMods.db.char.nevershow == nil then
				RobBossMods.db.char.nevershow = false
			end
			L = LibStub("AceLocale-3.0"):GetLocale("RobBossMods")
			
			StaticPopupDialogs["RBM_NOBOSS"] = {
				text = L["NOBOSS"],
				button1 = "okay",
				timeout = 0,
				whileDead = true,
				hideOnEscape = true,
			}
			
			RobBossMods:CreateFrames()
			
			
			RBM_createBlizzOptions();
			ldb = LibStub:GetLibrary("LibDataBroker-1.1")
			dataobj = ldb:NewDataObject("RobBossMods", {
				type = "launcher",
				label = "Rob Boss Mods",
				icon = "Interface\\Icons\\Spell_Nature_StormReach",
				tooltiptext = "RobBossMods",
				OnClick = function(clickedframe, button)
					if button == "LeftButton" then
						if RobBossMods.Anchor:IsVisible() then
							RobBossMods.Anchor:Hide();
						else
							RobBossMods.Anchor:Show();
						end
					else
						InterfaceOptionsFrame_OpenToCategory("RobBossMods");
					end
				end,
			})

			myname = UnitName("player");
			lang = GetLocale();
			if GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
				aktiv = 1
				SendAddonMessage( "RBMPing", mynumber, "PARTY" );
			end
			if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
				aktiv = 1
				SendAddonMessage( "RBMPing", mynumber, "INSTANCE_CHAT" );
			end
			if rbm_elang == nil and lang == "enUS" then
				StaticPopupDialogs["RBM_LANGC"] = {
				text = "RBM: You run the UK or US Client. In What language do you want the boss explanation to appear?",
				button1 = "English",
				button2 = "German",
				OnAccept = function()
					rbm_elang = lang:sub(1,2)
					StaticPopup_Show ("RBM_LANGCONFIRM")
				end,
				OnCancel = function()
					rbm_elang = "de"
					StaticPopup_Show ("RBM_LANGCONFIRM")
				end,
				timeout = 0,
				whileDead = true,
				hideOnEscape = true,
				}
				--hint to reloadui after changed the global var
				StaticPopupDialogs["RBM_LANGCONFIRM"] = {
				text = "The Language change wont take effect until you restart UI. Restart UI now?",
				button1 = "Reload UI",
				button2 = "No, I'll do that later",
				OnAccept = function()
				ReloadUI();
				end,
				timeout = 0,
				whileDead = true,
				hideOnEscape = true,
				}
				StaticPopup_Show ("RBM_LANGC")
				
			elseif rbm_elang == nil and lang == "deDE" then
				rbm_elang = "de" 
			end
		elseif event == "INSTANCE_LOCK_START" then
				--print(getLastDeadBoss());
		elseif event == "PLAYER_REGEN_ENABLED" then
			if RobBossMods.db.char.showframe and (IsInRaid() or IsInGroup()) and precombat ~=nil then
				RobBossMods.Anchor:Show();
			end
			incombat = false
		elseif event == "PLAYER_REGEN_DISABLED" then
			incombat = true
			precombat = RobBossMods.Anchor:IsVisible();
			RobBossMods.Anchor:Hide();
		elseif event == "ZONE_CHANGED_NEW_AREA" then
			SetMapToCurrentZone();
			local whereami = EJ_GetInstanceForMap(C_Map.GetBestMapForUnit("player"));
				
				if EJ_GetInstanceInfo(whereami) == "Pandaria" then
					return;
				end
					
				if whereami > 0 then
					
					local out = ""
					if RobBossMods.db.char.showframe then
						RobBossMods.Anchor:Show();
					end
					
					local bossdown = 0
					for k = 1,GetNumSavedInstances(),1 do
						local name, id, reset, difficulty, locked, extended, instanceIDMostSig, isRaid, maxPlayers, difficultyName, numEncounters, encounterProgress = GetSavedInstanceInfo(k)
						
						if (reset > 0 or locked) and encounterProgress > 0 and name == EJ_GetInstanceInfo(EJ_GetInstanceForMap(C_Map.GetBestMapForUnit("player"))) and encounterProgress < numEncounters then
							bossdown = encounterProgress
							--checkUpdate();
							EJ_SelectInstance(whereami)
							encounterName,_, encounterID = EJ_GetEncounterInfoByIndex(bossdown+1, whereami)
							out = "\124cFFFF7F00RobBossMods:\124r "..L["LOADING"].." "..encounterName.." "..L["ASNEXT"].." \124cFFFF7F00"..name.."\124r ("..encounterProgress.."/"..numEncounters..")";
							lastboss = encounterID
						end
					end
					
					if bossdown == 0 then
						EJ_SelectInstance(whereami)
						encounterName,_, encounterID = EJ_GetEncounterInfoByIndex(1, whereami)
						out = "\124cFFFFF700RobBossMods:\124r "..L["LOADING"].." "..encounterName.." "..L["ASFIRST"].." \124cFFFF7F00"..EJ_GetInstanceInfo(whereami).."\124r... "
						lastboss = encounterID
					end
						
					if lastboss ~= nil then
						RobBossMods.setBossName(lastboss)
					end
					ChatFrame1:AddMessage(out);
					
					--tell undone achievements
					if RobBossMods.db.char.tellachievement and (GetInstanceDifficulty() == 2 or GetInstanceDifficulty() == 1 and GetNumGroupMembers() > 0) then
						local toPost = doAchieveAllPost("RETURN")
						local firstPost = false
						for i=1, #toPost, 1 do
							local _,_,_,compl = GetAchievementInfo(toPost[i])
							if compl == false then
								if firstPost == false then
									ChatFrame1:AddMessage(L["UNDONE_ACHIEVEMENTS"].." "..EJ_GetInstanceInfo(whereami)..":")
									firstPost = true
								end
								ChatFrame1:AddMessage(GetAchievementLink(toPost[i]))
							end
						end
					end
				end
			
		elseif event == "COMBAT_LOG_EVENT_UNFILTERED" then
			local a,b,c,d,e,f,g,h,i,_,_,spell_id,m = CombatLogGetCurrentEventInfo()
			-- Source INHEROIC
			if inheroic and ( getType(d) == 3 or getType(d) == 5 ) then 
				local tempid = getNPCId(d)
				if string.find(b,"SPELL_") then
					--ChatFrame1:AddMessage(tempid.."  "..spell_id.."  "..m);
					local spell_target = i or 0
					local noboss = false
					local bossmsg = ""
					--bosses
					-- first: zul aman
						----akilzon----
					if tempid == 23574 then
						if b == "SPELL_CAST_SUCCESS" and spell_id == 43648 then
							bossmsg = L["D23574_1"]
						elseif b == "SPELL_AURA_REMOVED" and spell_id == 43648 then
							bossmsg = L["D23574_2"]
						end
						----venoxis----
					elseif tempid == 52155 then
						if b == "SPELL_CAST_START" and spell_id == 96509 then
							bossmsg = L["D52155_1"]
						elseif b == "SPELL_CAST_START" and spell_id == 96842 then
							bossmsg = L["D52155_2"]
						elseif b == "SPELL_CAST_SUCCESS" and spell_id == 96477 then
							dummy_counter = (dummy_counter + 1) % 2
							if dummy_counter == 1 then
								dummy_target1 = spell_target
								noboss = true
							elseif dummy_counter == 0 then
								dummy_target2 = spell_target
								bossmsg = L["D52155_3"]
							end
						end
					elseif tempid == 52151 then
						--ChatFrame1:AddMessage("G: "..tempid.." A: "..b.."  SID: "..spell_id.." N: "..m.." T: "..spell_target);
						if b == "SPELL_CAST_SUCCESS" and spell_id == 96776 then
							bossmsg = L["D52151_1"]
						elseif b == "SPELL_AURA_REMOVED" and spell_id == 96776 then
							bossmsg = L["D52151_2"]
						elseif b == "SPELL_CAST_START" and spell_id == 96724 then
							bossmsg = L["D52151_3"]
						elseif b == "SPELL_CAST_START" and spell_id == 96740 then
							bossmsg = L["D52151_4"]
						end
					elseif tempid == 52059 then
						if b == "SPELL_AURA_APPLIED" and spell_id == 96435 then
							bossmsg = L["D52059_1"]
						end
					elseif tempid == 52053 then
						if b == "SPELL_CAST_START" and spell_id == 96316 then
							bossmsg = L["D52053_1"]
						elseif b == "SPELL_CAST_START" and spell_id == 96338 then
							bossmsg = L["D52053_2"]
						elseif b == "SPELL_CAST_START" and spell_id == 96319 then
							bossmsg = L["D52053_3"]
						end
					elseif tempid == 52148 then
						dummy_bool = true
						if b == "SPELL_CAST_SUCCESS" and spell_id == 97170 then
							bossmsg = L["D52148_1"]
						elseif b == "SPELL_CAST_START" and spell_id == 97172 then
							bossmsg = L["D52148_2"]
						end
					elseif tempid == 52730 and dummy_bool then
						if b == "SPELL_AURA_APPLIED" and spell_id == 97597 then
							bossmsg = L["D52730_1"]
						end	
					elseif tempid == 23577 then
						if b == "SPELL_CAST_START" and spell_id == 97500 then
							bossmsg = L["D23577_1"]
						elseif b == "SPELL_CAST_START" and spell_id == 97492 then
							bossmsg = L["D23577_2"]
						end
					elseif tempid == 23863 then
						if b == "SPELL_CAST_SUCCESS" and spell_id == 43095 then
							bossmsg = L["D23863_1"]
						elseif b == "SPELL_AURA_APPLIED" and spell_id == 42402 then
							bossmsg = L["D23863_2"]
						elseif b == "SPELL_CAST_SUCCESS" and spell_id == 43983 then
							bossmsg = L["D23863_3"]
						end
					--test
					elseif tempid == 52638 then
						if b == "SPELL_AURA_APPLIED" and spell_id == 97318 then
							bossmsg = L["D52638_1"]
						end
					elseif tempid == 52271 then
						if b == "SPELL_CAST_SUCCESS" and spell_id == 96658 then
							bossmsg = L["D52271_1"]
						end
					elseif tempid == 52286 then
						if b == "SPELL_CAST_START" and spell_id == 96699 then
							bossmsg = L["D52286_1"]
						end
					elseif spell_id == 96542 then
						ChatFrame1:AddMessage("AGONY!!!!!!!!!!!!!!!!!!!!!");
					else
						noboss = true
					end
					if noboss == false and bossmsg ~= "" and ( (RobBossMods.db.global.dynamic["D"..tempid] and check_dynamic_b(tempid) ) or ( not check_dynamic_b(tempid) )) and aktiv == 1 then
						--ChatFrame1:AddMessage("would send");
						local pre_string = string.gsub(bossmsg, "CUR_SPELL", GetSpellLink(spell_id) );
						pre_string = string.gsub(pre_string, "TAR_SPELL", spell_target );
						pre_string = string.gsub(pre_string, "TAR_DUMM1", dummy_target1 );
						bossmsg = string.gsub(pre_string, "TAR_DUMM2", dummy_target2 );
						SendChatMessage(bossmsg,"SAY",nil,nil);
					end
					--[[local tstring = ""
					if i ~= nil then
						tstring = tstring.."Script Target: "..i
					end
					--ChatFrame1:AddMessage("G: "..tempid.." A: "..b.."  SID: "..spell_id.." N: "..m.." T: "..tstring);]]
				end
			end
			if b == "UNIT_DIED" and EJ_GetInstanceForMap(C_Map.GetBestMapForUnit("player")) >= 0 then
				local nid = getNPCId(h);
				local gefunden = false
				--ChatFrame1:AddMessage(i.." ist gestorben ID "..nid);
				local instance = EJ_GetInstanceForMap(C_Map.GetBestMapForUnit("player"))
				--EJ_SelectInstance(instance)
				--foreach encounter in current instance and each mob per encounter do
				local maxdead = 0
				for ix = 1, 20, 1 do
					xtest,_, encounterID = EJ_GetEncounterInfoByIndex(ix, instance)
					if xtest == nil then
						break
					end
					local j = 1
					local _,tempname = EJ_GetCreatureInfo(j, encounterID)
					while tempname ~= nil do
						if tempname == i then
							maxdead = encounterID
							lastboss = getNextBoss(instance, encounterID)
							if lastboss == nil then
								if aktiv == 1 and RobBossMods.db.char.tellnextboss then
									if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
										SendChatMessage(EJ_GetEncounterInfo(maxdead)..L["LASTBOSS"], "INSTANCE_CHAT", nil, reci);
									elseif GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
										SendChatMessage(EJ_GetEncounterInfo(maxdead)..L["LASTBOSS"], "PARTY", nil, reci);
									end
								end
								--DEBUG
								--ChatFrame1:AddMessage(EJ_GetEncounterInfo(maxdead)..L["LASTBOSS"], "PARTY", nil, reci);
							else 
								RobBossMods.setBossName(lastboss)
								if aktiv == 1 and RobBossMods.db.char.tellnextboss == true then
									if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
										SendChatMessage(EJ_GetEncounterInfo(maxdead)..L["BOSS_DEAD1"]..EJ_GetEncounterInfo(lastboss)..L["BOSS_DEAD2"], "INSTANCE_CHAT", nil, reci);
									elseif GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
										SendChatMessage(EJ_GetEncounterInfo(maxdead)..L["BOSS_DEAD1"]..EJ_GetEncounterInfo(lastboss)..L["BOSS_DEAD2"], "PARTY", nil, reci);
									end
								end
								--DEBUG
							end
								
							gefunden = true
						end
						j = j + 1
						_, tempname = EJ_GetCreatureInfo(j, encounterID)
					end
				end
			end
		elseif event == "PARTY_MEMBERS_CHANGED" or event == "GROUP_ROSTER_CHANGED" then 
			already_told = {}
			if not IsInGroup() then
				boss_dead = {}
				aktiv = 1
				--ChatFrame1:AddMessage("RobBossMods: aktiv")
			else
				aktiv = 1
				--aushandeln
				if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
					SendAddonMessage( "RBMPing", mynumber, "INSTANCE_CHAT" );
					partymember = GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE)
				elseif GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
					SendAddonMessage( "RBMPing", mynumber, "PARTY" );
					partymember = GetNumGroupMembers(LE_PARTY_CATEGORY_HOME)
				end
			end
		elseif event == "CHAT_MSG_ADDON" then
			local pref,msg1,kanal,sender = ...
			--ChatFrame1:AddMessage(pref.." "..msg1.." "..kanal);
			--if sender == "RobBossMods"  then
			if pref == "RBMPing"  then
				lastping = GetTime();
				--ChatFrame1:AddMessage("Empfange Ping mache pong" );
				aktiv = 1
				SendAddonMessage( "RBMPong", mynumber, kanal );
			elseif pref == "RBMPong" then
				--ChatFrame1:AddMessage("Empfange pong von "..sender.." mit Wert: "..tonumber(msg1) );
				--auswerten 
				local rndtime = tonumber(msg1)
				if tonumber(msg1) < mynumber then
					--ChatFrame1:AddMessage("schalte mich ab");
					aktiv = 0
				else
					--ChatFrame1:AddMessage("bleibe aktiv");
				end
			elseif pref == "RBMCheckA" then
				local wer,was = string.split("#",msg1)
				if wer == myname then
					ChatFrame1:AddMessage(sender.."("..kanal.."): "..was);
				end
			elseif pref == "RBMCheck" then
				SendAddonMessage("RBMCheckA", sender.."#"..tostring(rbm_version), kanal);
			--update
			elseif pref == "RBMVersion" and sender ~= myname then
				--message("L: "..rbm_version.."  R: "..tonumber(msg1));
				if tonumber(msg1) > rbm_version and update_told == 0 then
					update_told = 1
					local totext = ""
					totext = L["NEW_VER1"]..rbm_version..L["NEW_VER2"]..tonumber(msg1)
					StaticPopupDialogs["RBM_NEWVER"] = {
					text = totext,
					button1 = "Ok",
					timeout = 0,
					whileDead = true,
					hideOnEscape = true,
					}
					StaticPopup_Show ("RBM_NEWVER")
					--message("Du hast RBM Version "..rbm_version.." installiert. Es gibt eine neue Version: "..tonumber(msg1));
				end
			end
		elseif event == "PLAYER_TARGET_CHANGED" then
			local uname = UnitName("target")
			
			if uname == nil then
				if not RobBossMods.db.char.showframe then
					RobBossMods.Anchor:Hide();
				end
				return
			end
			local guid = UnitGUID("target");
			--ChatFrame1:AddMessage(guid.."  Typ: "..getType(guid).." NPC ID: "..getNPCId(guid));
			local nid = getNPCId(guid);
			if getType(guid) ~=3 and getType(guid) ~= 5 then
				if not RobBossMods.db.char.showframe then
					RobBossMods.Anchor:Hide();
				end
				return 
			end
			--ChatFrame1:AddMessage("NID "..nid);
			
			
			--NEW
			--if mob alive and in my instance then handle the case 
			if UnitHealth("target") > 0 and EJ_GetInstanceForMap(C_Map.GetBestMapForUnit("player")) > 0 then
				--if lastSpoken[nid] == nil then
					--now find out if the mob is a boss
					EJ_SelectInstance(EJ_GetInstanceForMap(C_Map.GetBestMapForUnit("player")))
					for i=1,20,1 do
						local _,_,encIndex = EJ_GetEncounterInfoByIndex(i, EJ_GetInstanceForMap(C_Map.GetBestMapForUnit("player")))
						for j=1,6,1 do
							local cid, cname = EJ_GetCreatureInfo(j, encIndex)
							if cname ~= nil then
								--now check
								if uname == cname then
									--we found our boss now load him up
									RobBossMods.setBossName(encIndex)
									
									if RobBossMods.db.char.tellachievement and (GetInstanceDifficulty() == 2 or GetInstanceDifficulty() == 1 and GetNumGroupMembers() >= 0) and  GetTime()-lasttargetachievement > 240 then
										local toPost = doAchievePost("RETURN")
										local firstPost = false
										for li = 1, #toPost, 1 do
											local _,_,_, compl = GetAchievementInfo(toPost[li])
											if compl == false then
												if firstPost == false then
													ChatFrame1:AddMessage(L["UNDONE_ACHIEVEMENTS"].." \124cFFFFF700("..cname..")\124r");
													firstPost = true
												end
												ChatFrame1:AddMessage(GetAchievementLink(toPost[li]));
											end
										end
										lasttargetachievement = GetTime()
									end
									
									if not incombat and not RobBossMods.db.char.nevershow then
										RobBossMods.Anchor:Show();
									end
									--print(IsInRaid() == 0 and RobBossMods.db.char.telleverybossparty);
									--print(aktiv==1);
									local dumpvar = already_told[lastboss] == nil
									if aktiv == 1 and already_told[lastboss] == nil and
									((IsInRaid() == 1 and RobBossMods.db.char.telleverybossraid) or
									(IsInRaid() == 0 and RobBossMods.db.char.telleverybossparty))
									then
										already_told[lastboss] = true
										
										if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
											tempchan = "INSTANCE_CHAT"
										else
											if IsInRaid() then
												tempchan = "RAID"
											elseif IsInGroup() then
												tempchan = "PARTY"
											end
										end

										local tosend 
										if RobBossMods.db.global.tellgrid ~= nil then
											if RobBossMods.db.global.tellgrid["hintevery"] == nil then
												SendChatMessage(L["DONTKNOW_OLD"],tempchan,nil,nil);
											else
												SendChatMessage(RobBossMods.db.global.tellgrid["hintevery"],tempchan,nil,nil);
											end
										else
											SendChatMessage(L["DONTKNOW_OLD"],tempchan,nil,nil);
										end
									end
								end
							end
						end
					end
					--lastSpoken[nid] = GetTime();
			end
			--old
		elseif (event == "CHAT_MSG_PARTY" or event == "CHAT_MSG_PARTY_LEADER" or event == "CHAT_MSG_INSTANCE_CHAT" or event == "CHAT_MSG_INSTANCE_CHAT_LEADER") and aktiv == 1 then
			local ms,msender = ...
			ms = ms
			msender = msender
			if ms == "!boss" or ms == "!loot" or string.find(ms,"!achieve") or string.find(ms,"!rbminfo") then
				if firstrequest[msender] == nil then
					firstrequest[msender] = GetTime();
				end
				if requestamount[msender] == nil then
					requestamount[msender] = 0
				end
				requestamount[msender] = requestamount[msender] +1
			end
			if ms == "!boss" and (GetTime() - lastbossplot > 25 or RobBossMods.db.char.whispermode) and not ignoreplayer[msender] then
				if inquestion and msender ~=myname then
					return
				end
				if requestamount[msender] >= 10 and (requestamount[msender] / (GetTime()-firstrequest[msender]) > 0.5) then
					--player is probably anoying
					local ja = "yes"
						local nein = "no"
						if rbm_elang == "de" then
							ja = "ja"
							nein = "nein"
						end
						local zahl = GetTime()-firstrequest[msender];
					StaticPopupDialogs["RBM_IGN"] = {
							text = msender..L["IGN1"]..requestamount[msender]..L["IGN2"]..(math.floor(zahl*100)/100)..L["IGN3"],
							button1 = ja,
							button2 = nein,
							OnAccept = function()
								ignoreplayer[msender] = true
								return
							end,
							OnCancel = function(_, reason)
								if reason == "clicked" then
									requestamount[msender] = nil
									firstrequest[msender] = nil
								end;
							end,
							timeout = 0,
							whileDead = true,
							hideOnEscape = true,
						}
						StaticPopup_Show("RBM_IGN");
				else
					if lastboss == nil then
						--nop
					elseif RobBossMods.db.char.whispermode then
						doBossPost("WHISPER",msender);
					else
						if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
							doBossPost("INSTANCE_CHAT");
						elseif GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
							doBossPost("PARTY");
						end
						already_told[lastboss] = 1
					end
					lastbossplot = GetTime();
					if lastboss == nil then
						lastbossplot = 0
					end
				end
			elseif ms == "!loot" and (GetTime() - lastlootplot > 25 or RobBossMods.db.char.whispermode) and not ignoreplayer[msender] then
				if inquestion and msender ~=myname then
					return
				end
				if requestamount[msender] >= 10 and (requestamount[msender] / (GetTime()-firstrequest[msender]) > 0.5) then
					--player is probably anoying
					local ja = "yes"
						local nein = "no"
						if rbm_elang == "de" then
							ja = "ja"
							nein = "nein"
						end
						local zahl = GetTime()-firstrequest[msender];
					StaticPopupDialogs["RBM_IGN"] = {
							text = msender..L["IGN1"]..requestamount[msender]..L["IGN2"]..(math.floor(zahl*100)/100)..L["IGN3"],
							button1 = ja,
							button2 = nein,
							OnAccept = function()
								ignoreplayer[msender] = true
								return
							end,
							OnCancel = function(_, reason)
								if reason == "clicked" then
									requestamount[msender] = nil
									firstrequest[msender] = nil
								end;
							end,
							timeout = 0,
							whileDead = true,
							hideOnEscape = true,
						}
						StaticPopup_Show("RBM_IGN");
				else
					if lastboss == nil then
						--nop
					elseif RobBossMods.db.char.whispermode then
						doLootPost(msender);
					else
						local mem = RobBossMods.db.char.postchannel
						if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
							RobBossMods.db.char.postchannel = "INSTANCE_CHAT"
						elseif GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
							RobBossMods.db.char.postchannel = "PARTY"
						end
						doLootPost();
						RobBossMods.db.char.postchannel = mem
					end
					lastlootplot = GetTime();
					if lastboss == nil then
						lastlootplot = 0
					end
				end
			elseif ms == "!achieve" and (GetTime() - lastachieveplot > 25 or RobBossMods.db.char.whispermode) and not ignoreplayer[msender] then
				if inquestion and msender ~=myname then
					return
				end
				if requestamount[msender] >= 10 and (requestamount[msender] / (GetTime()-firstrequest[msender]) > 0.5) then
					--player is probably anoying
					local ja = "yes"
						local nein = "no"
						if rbm_elang == "de" then
							ja = "ja"
							nein = "nein"
						end
						local zahl = GetTime()-firstrequest[msender];
					StaticPopupDialogs["RBM_IGN"] = {
							text = msender..L["IGN1"]..requestamount[msender]..L["IGN2"]..(math.floor(zahl*100)/100)..L["IGN3"],
							button1 = ja,
							button2 = nein,
							OnAccept = function()
								ignoreplayer[msender] = true
								return
							end,
							OnCancel = function(_, reason)
								if reason == "clicked" then
									requestamount[msender] = nil
									firstrequest[msender] = nil
								end;
							end,
							timeout = 0,
							whileDead = true,
							hideOnEscape = true,
						}
						StaticPopup_Show("RBM_IGN");
				else
					if lastboss == nil then
						--nop
					elseif RobBossMods.db.char.whispermode then
						doAchievePost(msender);
					else
						local mem = RobBossMods.db.char.postchannel
						if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
							RobBossMods.db.char.postchannel = "INSTANCE_CHAT"
						elseif GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
							RobBossMods.db.char.postchannel = "PARTY"
						end
						doAchievePost();
						RobBossMods.db.char.postchannel = mem
					end
					lastachieveplot = GetTime();
					if lastboss == nil then
						lastachieveplot = 0
					end
				end
			elseif ms == "!achieveall" and (GetTime() - lastachieveallplot > 25 or RobBossMods.db.char.whispermode) and not ignoreplayer[msender] then
				if inquestion and msender ~=myname then
					return
				end
				if requestamount[msender] >= 10 and (requestamount[msender] / (GetTime()-firstrequest[msender]) > 0.5) then
					--player is probably anoying
					local ja = "yes"
						local nein = "no"
						if rbm_elang == "de" then
							ja = "ja"
							nein = "nein"
						end
						local zahl = GetTime()-firstrequest[msender];
					StaticPopupDialogs["RBM_IGN"] = {
							text = msender..L["IGN1"]..requestamount[msender]..L["IGN2"]..(math.floor(zahl*100)/100)..L["IGN3"],
							button1 = ja,
							button2 = nein,
							OnAccept = function()
								ignoreplayer[msender] = true
								return
							end,
							OnCancel = function(_, reason)
								if reason == "clicked" then
									requestamount[msender] = nil
									firstrequest[msender] = nil
								end;
							end,
							timeout = 0,
							whileDead = true,
							hideOnEscape = true,
						}
						StaticPopup_Show("RBM_IGN");
				else
					if lastboss == nil then
						--nop
					elseif RobBossMods.db.char.whispermode then
						doAchieveAllPost(msender);
					else
						local mem = RobBossMods.db.char.postchannel
						if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
							RobBossMods.db.char.postchannel = "INSTANCE_CHAT"
						elseif GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
							RobBossMods.db.char.postchannel = "PARTY"
						end
						doAchieveAllPost();
						RobBossMods.db.char.postchannel = mem
					end
					lastachieveallplot = GetTime();
					if lastboss == nil then
						lastachieveallplot = 0
					end
				end
			elseif ms == "!rbminfo" and ((GetTime() - lastinfoplot) > 15) and not ignoreplayer[msender] then 
				if requestamount[msender] >= 10 and (requestamount[msender] / (GetTime()-firstrequest[msender]) > 0.5) then
					--player is probably anoying
					local ja = "yes"
						local nein = "no"
						if rbm_elang == "de" then
							ja = "ja"
							nein = "nein"
						end
						local zahl = GetTime()-firstrequest[msender];
					StaticPopupDialogs["RBM_IGN"] = {
							text = msender..L["IGN1"]..requestamount[msender]..L["IGN2"]..(math.floor(zahl*100)/100)..L["IGN3"],
							button1 = ja,
							button2 = nein,
							OnAccept = function()
								ignoreplayer[msender] = true
								return
							end,
							OnCancel = function(_, reason)
								if reason == "clicked" then
									requestamount[msender] = nil
									firstrequest[msender] = nil
								end;
							end,
							timeout = 0,
							whileDead = true,
							hideOnEscape = true,
						}
						StaticPopup_Show("RBM_IGN");
				else
					lastinfoplot = GetTime();
					if rbm_elang == "de" then
						if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
							SendChatMessage("RobBossMods v"..rbm_displayversion.." Mehr Infos unter www.keinerspieltmitmir.de/rbm ","INSTANCE_CHAT", nil, autor);
						elseif GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
							SendChatMessage("RobBossMods v"..rbm_displayversion.." Mehr Infos unter www.keinerspieltmitmir.de/rbm ","PARTY", nil, autor);
						end
					else
						if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
							SendChatMessage("RobBossMods v"..rbm_displayversion.." More Information on www.keinerspieltmitmir.de/rbm ","INSTANCE_CHAT", nil, autor);
						elseif GetNumGroupMembers(LE_PARTY_CATEGORY_HOME) > 0 then
							SendChatMessage("RobBossMods v"..rbm_displayversion.." More Information on www.keinerspieltmitmir.de/rbm ","PARTY", nil, autor);
						end
					end
				end
			end
		elseif event == "CHAT_MSG_WHISPER" then
			local lmes, author = ...
			if lmes == "!rbminfo" then 
				if rbm_elang == "de" then
					SendChatMessage("RobBossMods v"..rbm_displayversion.." Mehr Infos unter www.keinerspieltmitmir.de/rbm ","WHISPER", nil, author);
				else
					SendChatMessage("RobBossMods v"..rbm_displayversion.." More Information on www.keinerspieltmitmir.de/rbm ","WHISPER", nil, author);
				end
			elseif lmes == "!boss" then
				doBossPost("WHISPER", author);
			elseif lmes == "!loot" then
				doLootPost(author)
			elseif lmes == "!achieve" then
				print("ACH");
				doAchievePost(author)
			elseif lmes == "!achieveall" then
				doAchieveAllPost(author)
			end
		end
end

--Configuration

	local function getgen(info)
		local ns,opt = string.split(".", info.arg)
		if tonumber(opt) == 1 then
			return RobBossMods.db.char.showframe
		elseif tonumber(opt) == 2 then
			return RobBossMods.db.char.telleverybossparty
		elseif tonumber(opt) == 22 then
			return RobBossMods.db.char.telleverybossraid
		elseif tonumber(opt) == 3 then
			return not RobBossMods.db.char.minimap.hide
		elseif tonumber(opt) == 4 then
			return RobBossMods.db.char.whispermode
		elseif tonumber(opt) == 5 then
			return RobBossMods.db.char.nevershow
		elseif tonumber(opt) == 6 then
			if rbm_icon == "rolle" then
				return "scroll"
			elseif rbm_icon == "buch" then
				return "book"
			end
		elseif tonumber(opt) == 7 then
			return RobBossMods.db.char.tellachievement
		end
	end
	
	local function getdyn(info)
		local ns,opt = string.split(".", info.arg)	
		if ns == "dyn" then
			return RobBossMods.db.global.dynamic["D"..opt]
		end
	end
	
	local function setdyn(info, arg1, arg2, arg3, arg4)
		local ns,opt = string.split(".", info.arg)
		if ns == "dyn" then
			if check_dynamic_b(opt) then
				RobBossMods.db.global.dynamic["D"..opt] = not RobBossMods.db.global.dynamic["D"..opt]
			else
				StaticPopupDialogs["RBM_NODYN"] = {
					text = L["MENU_DYN_NO"],
					button1 = "OK",
					timeout = 0,
					whileDead = true,
					hideOnEscape = true,
				}
				StaticPopup_Show("RBM_NODYN");
			end
		end
	end
	
	local function setgen(info, arg1, arg2, arg3, arg4)
		local ns,opt,rest = string.split(".", info.arg)
		if tonumber(opt) == 1 then
			RobBossMods.db.char.showframe = not RobBossMods.db.char.showframe
			if RobBossMods.db.char.showframe then
				RobBossMods.db.char.nevershow = false
			end
		elseif tonumber(opt) == 2 then
			RobBossMods.db.char.telleverybossparty = not RobBossMods.db.char.telleverybossparty
		elseif tonumber(opt) == 22 then
			RobBossMods.db.char.telleverybossraid = not RobBossMods.db.char.telleverybossraid
		elseif tonumber(opt) == 3 then
			RobBossMods.db.char.minimap.hide = not RobBossMods.db.char.minimap.hide
			if RobBossMods.db.char.minimap.hide then
				icon:Hide("RobBossMods")
			else
				icon:Show("RobBossMods")
			end
		elseif tonumber(opt) == 4 then
			RobBossMods.db.char.whispermode = not RobBossMods.db.char.whispermode
		elseif tonumber(opt) == 5 then
			RobBossMods.db.char.nevershow = not RobBossMods.db.char.nevershow
			if RobBossMods.db.char.nevershow then
				RobBossMods.db.char.showframe = false
				RobBossMods.Anchor:Hide();
				StaticPopupDialogs["RBM_ONCE"] = {
					text = L["NEVER_WARNING"],
					button1 = "OK",
					--button2 = "No, I'll do that later",
					timeout = 0,
					whileDead = true,
					hideOnEscape = true,
				}
				StaticPopup_Show ("RBM_ONCE")
			end
		elseif tonumber(opt) == 6 then
			if arg1 == "scroll" then
				rbm_icon = "rolle"
			elseif arg1 == "book" then
				rbm_icon = "buch"
			end
			StaticPopupDialogs["RBM_ONCE_DOS"] = {
					text = L["MUST_RESTART"],
					button1 = "OK",
					--button2 = "No, I'll do that later",
					timeout = 0,
					whileDead = true,
					hideOnEscape = true,
				}
			StaticPopup_Show ("RBM_ONCE_DOS")
		elseif tonumber(opt) == 7 then
			RobBossMods.db.char.tellachievement = not RobBossMods.db.char.tellachievement
		end
	end
			
local function createconfig()	

	local options = {}
	options.type = "group"
	options.name = "Rob Boss Mods"
	options.args = {}
	-- local settings saved in  rbm_customTactics
	if RobBossMods.db.global.customTactics == nil then
		RobBossMods.db.global.customTactics = {}
	end
	
	EJ_SetDifficulty(2);	
		
		local iterator = 0
		
		options.args.a = {
			type = "group",
			name = L["MENU_GENERAL"],
			desc = L["MENU_GENERAL"],
			get = getgen,
			set = setgen,
			order = 1,
			args = {
				nextboss = {
					order = 1,
					type = "toggle",
					name = L["ALWAYS_SHOW_FRAME"],
					desc = L["ALWAYS_SHOW_FRAME_DESC"],
					width = "double",
					arg = "option.1",
				},	
				nevershow = {
					order = 2,
					type = "toggle",
					name = L["NEVER_SHOW_FRAME"],
					desc = L["NEVER_SHOW_FRAME_DESC"],
					width = "double",
					arg = "option.5",
				},				
				targetboss = {
					order = 3,
					type = "toggle",
					name = L["EVERY_BOSS_ANNOUNCEPARTY"],
					desc = L["EVERY_BOSS_ANNOUNCE_DESC"],
					width = "double",
					arg = "option.2",
				},
				targetbossraid = {
					order = 3,
					type = "toggle",
					name = L["EVERY_BOSS_ANNOUNCERAID"],
					desc = L["EVERY_BOSS_ANNOUNCE_DESC"],
					width = "double",
					arg = "option.22",
				},
				minimapopt = {
					order = 4,
					type = "toggle",
					name = L["MINIMAP_SHOW"],
					desc = L["MINIMAP_SHOW"],
					width = "double",
					arg = "option.3",
				},
				whispermode = {
					order = 5,
					type = "toggle",
					name = L["WHISPER_MODE"],
					desc = L["WHISPER_MODE_DESC"],
					width = "double",
					arg = "option.4",
				},
				achievementstuff = {
					order = 5,
					type = "toggle",
					name = L["ACHIEVE_SETTINGS"],
					desc = L["ACHIEVE_SETTINGS_DESC"],
					width = "double",
					arg = "option.7",
				},
				whispericon = {
					order = 5,
					type = "select",
					name = L["SEL_MINIMAP_ICON"],
					desc = "",
					arg = "option.6",
					values = { book = L["BOOK"], scroll = L["SCROLL"] },
				},
				hintonce = {
					order = 6,
					type = "input",
					name = L["HINTONCE"],
					desc = L["HINTONCE_DESC"],
					arg = "hintonce",
					width = "full",
					multiline = true,
					get = function(info)
						if RobBossMods.db.global.tellgrid[info.arg] == nil then
							return L["DONTKNOW"]
						else
							return RobBossMods.db.global.tellgrid[info.arg]
						end
					end,
					set = function(info, value)
						RobBossMods.db.global.tellgrid[info.arg] = value
					end, 
				},
				hintevery = {
					order = 7,
					type = "input",
					name = L["HINTEVERY"],
					desc = L["HINTEVERY_DESC"],
					arg = "hintevery",
					width = "full",
					multiline = true,
					get = function(info)
						if RobBossMods.db.global.tellgrid[info.arg] == nil then
							return L["DONTKNOW_OLD"]
						else
							return RobBossMods.db.global.tellgrid[info.arg]
						end
					end,
					set = function(info, value)
						RobBossMods.db.global.tellgrid[info.arg] = value
					end, 
				},
				--dynamic_toggle = {
				--	order = 8,
				--	type = "execute",
				--	name = L["MENU_DYN_TOGGLE_A"],
				--	desc = L["MENU_DYN_TOGGLE_A_DESC"],
				--	func = function() 
				--		dynamic_toggle_a();
				--	end,
				--},
				--dynamic_toggle2 = {
				--	order = 9,
				--	type = "execute",
				--	name = L["MENU_DYN_TOGGLE_D"],
				--	desc = L["MENU_DYN_TOGGLE_D_DESC"],
				--	func = function()
				--		dynamic_toggle_d();
				--	end,
				--},
				reset = {
					order = 10,
					type = "execute",
					name = L["RESET_CUSTOM"],
					desc = L["RESET_CUSTOM_DESC"],
					func = function()
						RobBossMods.db.global.customTactics = {}
						RobBossMods.db.global.tellgrid = {}
					end,
				},
			},
		}
		
		--merge all instances and raids
		-- mop > cata > wotlk > bc > vanilla,  5 man > raid
		
		
		
		
		for i,v in ipairs(alltogether) do
			local t1,t2 = string.split(".",v)
			if t1 == "header" then
				options.args["boss"..t2] = {
					type = "group",
					name = t2,
					args = {},
					order = 1000-i+2,
				}
			else 
				xname = EJ_GetInstanceInfo(v)
				options.args["boss"..v] = {
					type = "group",
					name = xname,
					desc = xname,
					--set = setdyn,
					--get = getdyn,
					args = {},
					order = 1000 -i +2,
				}
				EJ_SelectInstance(v)
				for j = 1, 20, 1 do
					enc_name, _, encounterID = EJ_GetEncounterInfoByIndex(j, v)
					if enc_name ~= nil then
						options.args["boss"..v].args[tostring(j)] = {
							type = "input",
							order = 2*j +1,
							name = enc_name,
							desc = enc_name,
							arg = encounterID,
							width = "full",
							multiline = true,
							get = function(info)
								
								if RobBossMods.db.global.customTactics["T"..info.arg] == nil then
									return L["T"..info.arg]
								else
									return RobBossMods.db.global.customTactics["T"..info.arg]
								end
							end,
							set = function(info, value)
								RobBossMods.db.global.customTactics["T"..info.arg] = value
							end, 
						}
						--options.args["boss"..v].args[tostring(j).."dyn"] = {
						--	order = 2*j,
						--	type = "toggle",
						--	name = L["MENU_DYNAMIC1"]..enc_name..L["MENU_DYNAMIC2"],
						--	desc = L["MENU_DYNAMIC_DESC"],
						--	width = "double",
						--	arg = "dyn".."."..encounterID,
						--	set = setdyn,
						--	get = getdyn,
						--}
					end
				end
			end
		end
	return options
end

local config = LibStub("AceConfig-3.0")
local dialog = LibStub("AceConfigDialog-3.0")
local registered = false;

local options
function RBM_createBlizzOptions()
	options = createconfig()

	config:RegisterOptionsTable("RBM-Bliz", {
		name = "RobBossMods",
		type = "group",
		args = {
			help = {
				type = "description",
				name = L["MENU_DESCRIPTION"],
			},
		},
	})
	dialog:SetDefaultSize("RBM-Bliz", 600, 400)
	dialog:AddToBlizOptions("RBM-Bliz", "RobBossMods")

    --Options
	
	config:RegisterOptionsTable("RBM-GENERAL", options.args.a)
	dialog:AddToBlizOptions("RBM-GENERAL", options.args.a.name, "RobBossMods")
	
	
	for i,v in ipairs(alltogether) do
		local t1,t2 = string.split(".",v)
		if t1 == "header" then
			config:RegisterOptionsTable("RBM-ZONE-"..tostring(i), options.args["boss"..t2])
			dialog:AddToBlizOptions("RBM-ZONE-"..tostring(i), options.args["boss"..t2].name, "RobBossMods")
		else
			xname = EJ_GetInstanceInfo(v)
			config:RegisterOptionsTable("RBM-ZONE-"..tostring(i), options.args["boss"..v])
			dialog:AddToBlizOptions("RBM-ZONE-"..tostring(i), options.args["boss"..v].name, "RobBossMods")
		end
	end
		
	return blizzPanel
end

function RBM_PrePost() 
	if RobBossMods.db.char.postchannel == "PARTY" or RobBossMods.db.char.postchannel == "RAID"  or RobBossMods.db.char.postchannel == "INSTANCE_CHAT" then

		if GetNumGroupMembers(LE_PARTY_CATEGORY_INSTANCE) > 0 then
			doBossPost("INSTANCE_CHAT")
		else 
			if IsInRaid() then
				doBossPost("RAID")
			elseif IsInGroup() then
				doBossPost("PARTY")
			end
		end
	
	elseif  RobBossMods.db.char.postchannel == "GUILD" then
		doBossPost("GUILD")
	elseif  RobBossMods.db.char.postchannel == "MYSELF" then
		doBossPost("MYSELF")
	elseif RobBossMods.db.char.postchannel == "OFFICER" then
		doBossPost("OFFICER")
	elseif RobBossMods.db.char.postchannel == "WHISPER" then
		doBossPost("WHISPER",whispertarget);
	elseif RobBossMods.db.char.postchannel == "SAY" then
		doBossPost("SAY",whispertarget);
	elseif tonumber(RobBossMods.db.char.postchannel) ~= nil then
		doBossPost("CHANNEL",tonumber(RobBossMods.db.char.postchannel));
	else 
		StaticPopupDialogs["RBM_NOBOSS"] = {
			text = L["NOBOSS"],
			button1 = "okay",
			timeout = 0,
			whileDead = true,
			hideOnEscape = true,
		}
		StaticPopup_Show ("RBM_NOBOSS")
	end
end

function RBM_PrePostWhisper() 
	StaticPopupDialogs["RBM_BOSS_WHISPER"] = {
			text = L["WHISPER_TACTIC"],
			button1 = "ok",
			timeout = 0,
			whileDead = true,
			hideOnEscape = true,
			OnAccept = function (self, data, data2)
				whispertarget = self.editBox:GetText()
				RobBossMods.db.char.postchannel = "WHISPER"
				RobBossMods.postOn:SetText(L["WHISPER"])
			end,
			hasEditBox = true,
		}
	StaticPopup_Show ("RBM_BOSS_WHISPER")
end





local menuFrame5 = CreateFrame("Frame", nil)
menuFrame5.initialize = function(self, level) 
	self.displayMode = "MENU"
		if not level then return end
		wipe(info)
		if level == 1 then
			info.isTitle= nil
			info.text = L["A_SHOW"]
			info.notCheckable = 1
			info.value = "always"
			info.arg1 = "always";
			info.func = RBM_DropDownMenuItem_OnClick;
			
			UIDropDownMenu_AddButton(info,level)
			
			info.isTitle= nil
			info.text = L["P_SHOW"]
			info.notCheckable = 1
			info.value = "partial"
			info.arg1 = "partial";
			info.func = RBM_DropDownMenuItem_OnClick;
						
			UIDropDownMenu_AddButton(info,level)
			
			info.isTitle= nil
			info.text = L["N_SHOW"]
			info.notCheckable = 1
			info.value = "never"
			info.arg1 = "never";
			info.func = RBM_DropDownMenuItem_OnClick;
			
			UIDropDownMenu_AddButton(info,level)
		end
	end
	
	local menuFrame2 = CreateFrame("Frame", nil)
menuFrame2.initialize = function(self, level) 
    if not level then 
		return 
	end
    wipe(info)
	self.displayMode = "MENU"
	--EJ_SetDifficulty(2);
	
    if level == 1 then
        -- Create the title of the menu
     	info.isTitle = 1
		if lastboss == nil then
			info.text = L["CURRENTBOSS"]..": "..L["NONE"]
		else
			info.text = L["CURRENTBOSS"]..": "..EJ_GetEncounterInfo(lastboss)
		end
		info.notCheckable = 1
		UIDropDownMenu_AddButton(info, level)
	
		info.isTitle = 1
        info.text = "5 man instances"
        info.notCheckable = 1
        UIDropDownMenu_AddButton(info, level)

	
		info.disabled     = nil
        info.isTitle      = nil
        info.notCheckable = 1

		info.hasArrow = 1
		
		local temptable = current_five
		table.sort(temptable, function(a,b) return a>b end)
		
		for _,v in orderedPairs(temptable) do
			info.text = EJ_GetInstanceInfo(v)
			info.value = v
			info.func = function()
				local _,_,local_enc_id = EJ_GetEncounterInfoByIndex(1,v)
				RobBossMods.setBossName(local_enc_id)
				CloseDropDownMenus()
			end
			UIDropDownMenu_AddButton(info, level)
		end
		
		
		info.isTitle = 1
        info.text = "Raids"
        info.notCheckable = 1
		info.hasArrow = nil
        UIDropDownMenu_AddButton(info, level)
		
		info.disabled     = nil
        info.isTitle      = nil
        info.notCheckable = 1

		info.hasArrow = 1
		
		local temptable = current_raid
		table.sort(temptable, function(a,b) return a>b end)
		
		for _,v in orderedPairs(temptable) do
			info.text = EJ_GetInstanceInfo(v)
			info.value = v
			info.func = function()
				local _,_,local_enc_id = EJ_GetEncounterInfoByIndex(1,v)
				RobBossMods.setBossName(local_enc_id)
				CloseDropDownMenus()
			end
			UIDropDownMenu_AddButton(info, level)
		end
		

		  -- Close menu item
        info.hasArrow     = nil
        info.value        = nil
        info.notCheckable = 1
        info.text         = CLOSE
        info.func         = self.HideMenu
        UIDropDownMenu_AddButton(info, level)

		
    elseif level == 2 then
		if isIdRaid(UIDROPDOWNMENU_MENU_VALUE) and current_raid == legion_raid and isIdMythicOnly(UIDROPDOWNMENU_MENU_VALUE) then
			EJ_SetDifficulty(23);
		elseif isIdRaid(UIDROPDOWNMENU_MENU_VALUE) and (current_raid == wod_raid or current_raid == legion_raid or current_raid == bfa_raid) then
			EJ_SetDifficulty(14);
		elseif isIdRaid(UIDROPDOWNMENU_MENU_VALUE) and isIdRaid40(UIDROPDOWNMENU_MENU_VALUE) then
			EJ_SetDifficulty(9);
		elseif isIdRaid(UIDROPDOWNMENU_MENU_VALUE) and isIdRaid25(UIDROPDOWNMENU_MENU_VALUE) then
			EJ_SetDifficulty(4);
		elseif isIdRaid(UIDROPDOWNMENU_MENU_VALUE) and current_raid ~= wod_raid then
			EJ_SetDifficulty(3);
		elseif not isIdRaid(UIDROPDOWNMENU_MENU_VALUE) and current_five == vanilla_five then 
			EJ_SetDifficulty(1);
		else 
			EJ_SetDifficulty(2);
		end

		--local last_set_dropdown_ID = 0
		if UIDROPDOWNMENU_MENU_VALUE ~= last_set_dropdown_ID then
			last_set_dropdown_ID = UIDROPDOWNMENU_MENU_VALUE
			EJ_SelectInstance(UIDROPDOWNMENU_MENU_VALUE)
		end

		for i = 1, 40, 1 do
			info.hasArrow = nil
			info.notCheckable = nil
				
			local enc_name, _,  enc_id = EJ_GetEncounterInfoByIndex(i,UIDROPDOWNMENU_MENU_VALUE)
			if enc_name ~= nil then 
				info.text = enc_name

				info.func = function()
					RobBossMods.setBossName(enc_id)
					CloseDropDownMenus()
				end
				UIDropDownMenu_AddButton(info, level)
			end
		end
	end
end

	
function compareAddonStuff(a,b)
	local x
	local y
   if a == "cata" then
		x = 30
	elseif a == "vanilla" then
		x = 0
	elseif a == "bc"  then
		x = 10
	elseif a == "wotlk" then
		x = 20
	elseif a == "mop" then
		x = 40
	elseif a == "wod" then
		x = 50
	else 
		x= 200
	end
	if b == "cata" then
		y = 30
	elseif b == "vanilla" then
		y = 0
	elseif b == "bc"  then
		y = 10
	elseif b == "wotlk" then
		y = 20
	elseif b == "mop" then
		y = 40
	elseif b == "wod" then
		y = 50
	else 
		y= 200
	end
	--print(a.." "..b.." "..x.." "..y);
	return x > y 
end


--menuframe 3 = addonFrame
local menuFrame3 = CreateFrame("Frame", nil)
menuFrame3.displayMode = "MENU"
menuFrame3.initialize = function(self, level) 
	self.displayMode = "MENU"
	if not level then return end
	wipe(info)
	self.displayMode = "MENU"
	if level == 1 then
		local temptable = {}
		for k,v in pairs(addonIndex) do
			table.insert(temptable, k)
		end
		table.sort(temptable, compareAddonStuff)
		for i = 1, #temptable, 1 do
			info.isTitle= nil
			info.text = addonIndex[temptable[i]]
			info.notCheckable = 1
			info.value = temptable[i]
			info.arg1 = temptable[i]
			info.func = RBM_DropDownAddonButton_OnClick;
			UIDropDownMenu_AddButton(info,level)
		end
	end
end

function updatePostChannel(self, id,...)
	local f,s,t = string.split(".",id)
	if f == "OWN" then
		RobBossMods.db.char.postchannel = s
		RobBossMods.postOn:SetText(t)
	else
		RobBossMods.db.char.postchannel = id
		RobBossMods.postOn:SetText(L[id:upper()])
	end
	
	
end


--MenuFrame 4 = list to switch post channel
local menuFrame4 = CreateFrame("Frame", nil)
menuFrame4.initialize = function(self, level) 
	self.displayMode = "MENU"
	if not level then return end
	wipe(info)
	if level == 1 then
		info.isTitle= nil
		info.text = L["PARTY"]
		info.notCheckable = nil
		info.value = "PARTY"
		info.arg1 = "PARTY"
		info.func = updatePostChannel;
		UIDropDownMenu_AddButton(info,level)
		
		info.text = L["MYSELF"]
		info.value = "MYSELF"
		info.arg1 = "MYSELF"
		info.func = updatePostChannel;
		UIDropDownMenu_AddButton(info,level)
		
		info.text = L["SAY"]
		info.value = "SAY"
		info.arg1 = "SAY"
		info.func = updatePostChannel;
		UIDropDownMenu_AddButton(info,level)
		
		info.text = L["GUILD"]
		info.value = "GUILD"
		info.arg1 = "GUILD"
		info.func = updatePostChannel;
		UIDropDownMenu_AddButton(info,level)
		
		info.text = L["OFFICER"]
		info.value = "OFFICER"
		info.arg1 = "OFFICER"
		info.func = updatePostChannel;
		UIDropDownMenu_AddButton(info,level)
		
		if whispertarget ~= nil then
			info.text = L["WHISPER"].." ("..whispertarget..")"
		else
			info.text = L["WHISPER"]
		end
		info.value = "WHISPER"
		info.arg1 = "WHISPER"
		info.func = RBM_PrePostWhisper;
		UIDropDownMenu_AddButton(info,level)
		
		--add other channels 
		
		for i=1,select("#", GetChannelList()), 2 do
			if select(i, GetChannelList()) > 4 then
				info.text = select( (i+1), GetChannelList())
				info.value = "OWN."..select(i, GetChannelList()).."."..select( (i+1), GetChannelList())
				info.arg1 = "OWN."..select(i, GetChannelList()).."."..select( (i+1), GetChannelList())
				info.func = updatePostChannel;
				UIDropDownMenu_AddButton(info,level)
			end
		end
		
	end
end




function RBM_DropDownMenuItem_OnClick(self, id, arg2, checked)
	if id == "always" then
		UIDropDownMenu_SetText(RobBossMods.SettingsDropDown, L["A_SHOW"]);
		
		RobBossMods.db.char.nevershow = false
		RobBossMods.db.char.showframe = true
		
	elseif id == "partial" then
		UIDropDownMenu_SetText(RobBossMods.SettingsDropDown, L["P_SHOW"]);
		
		RobBossMods.db.char.nevershow = false
		RobBossMods.db.char.showframe = false
		
		RobBossMods.Anchor:Hide();
		
	elseif id == "never" then
		UIDropDownMenu_SetText(RobBossMods.SettingsDropDown, L["N_SHOW"]);
		RobBossMods.db.char.nevershow = true
		RobBossMods.db.char.showframe = false
		RobBossMods.Anchor:Hide();
		StaticPopupDialogs["RBM_ONCE"] = {
			text = L["NEVER_WARNING"],
			button1 = "OK",
			--button2 = "No, I'll do that later",
			timeout = 0,
			whileDead = true,
			hideOnEscape = true,
		}
		StaticPopup_Show ("RBM_ONCE")
	end
end

function RBM_DropDownAddonButton_OnClick(self, id, arg2, checked)
	currentAddon = id;
	UIDropDownMenu_SetText(RobBossMods.AddonDropDown, addonIndex[currentAddon]);
	
	if id == "cata" then
		current_five = cata_five
		current_raid = cata_raid
	elseif id == "wotlk" then
		current_five = wotlk_five
		current_raid = wotlk_raid
	elseif id == "bc" then
		current_five = bc_five
		current_raid = bc_raid
	elseif id == "vanilla" then
		current_five = vanilla_five
		current_raid = vanilla_raid
	elseif id == "mop" then
		current_five = mop_five
		current_raid = mop_raid
	elseif id == "wod" then
		current_five = wod_five
		current_raid = wod_raid
	elseif id == "legion" then 
		current_five = legion_five
		current_raid = legion_raid
	elseif id == "bfa" then 
		current_five = bfa_five
		current_raid = bfa_raid
	end
end



--End of Configuration Frame

local function startmoving(self)
	self.IsMovingOrSizing = 1
	self:StartMoving()
end

local function stopmoving(self)
	if self.IsMovingOrSizing then
		self:StopMovingOrSizing()
		self.IsMovingOrSizing = nil
		local a,b,c,d,e = RobBossMods.Anchor:GetPoint();
		RobBossMods.db.char.mainpos.a = a
		RobBossMods.db.char.mainpos.b = b
		RobBossMods.db.char.mainpos.c = c
		RobBossMods.db.char.mainpos.d = d
		RobBossMods.db.char.mainpos.e = e
	end
end

local function startmovingH(self)
	RobBossMods.Anchor.IsMovingOrSizing = 1
	RobBossMods.Anchor:StartMoving()
end

local function stopmovingH(self)
	if RobBossMods.Anchor.IsMovingOrSizing then
		RobBossMods.Anchor:StopMovingOrSizing()
		RobBossMods.Anchor.IsMovingOrSizing = nil
		local a,b,c,d,e = RobBossMods.Anchor:GetPoint();
		RobBossMods.db.char.mainpos.a = a
		RobBossMods.db.char.mainpos.b = b
		RobBossMods.db.char.mainpos.c = c
		RobBossMods.db.char.mainpos.d = d
		RobBossMods.db.char.mainpos.e = e
	end
end

function RobBossMods:CreateFrames()


	-- Create anchor
	self.Anchor = CreateFrame("FRAME", "RBMAnchor", UIParent)
	self.Anchor:SetResizable(false)
	self.Anchor:SetMovable(true)
	
	if RobBossMods.db.char.mainpos == nil then
		RobBossMods.db.char.mainpos = {}
	end
	
	
	if RobBossMods.db.char.mainpos.a == nil then
		self.Anchor:SetPoint("CENTER", UIParent, "CENTER")
	else
		self.Anchor:SetPoint(RobBossMods.db.char.mainpos.a, UIParent, RobBossMods.db.char.mainpos.c, RobBossMods.db.char.mainpos.d, RobBossMods.db.char.mainpos.e);
	end
	self.Anchor:SetWidth(210)
	self.Anchor:SetHeight(300)
	self.Anchor:SetFrameLevel(7)
	self.Anchor:SetBackdrop({
		--bgFile = [[Interface\QuestFrame\UI-QuestItemNameFrame]], 
		edgeFile= [[Interface\DialogFrame\UI-DialogBox-Border]], 
		edgeSize = 4--, 
		--insets = { left = 11, right = 12, top = 12, bottom = 11 }
	})
	
	
	self.Anchor.texture = self.Anchor:CreateTexture(nil, "BACKGROUND");
	self.Anchor.texture:SetColorTexture(0,0,0,0.8);
	self.Anchor.texture:SetAllPoints(self.Anchor);
	
	
	self.Anchor:SetScript("OnMouseDown", startmoving)
	self.Anchor:SetScript("OnMouseUp", stopmoving)
	self.Anchor.oldShow = self.Anchor.Show
	self.Anchor.Show = function(self)
		RobBossMods.updateFrame()
		self:oldShow()
	end
	
	
	-- Create Title
	self.Title = CreateFrame("Button", "RBMTitle", self.Anchor)
	self.Title:SetPoint("TOPLEFT", self.Anchor, "TOPLEFT")
	self.Title:SetPoint("TOPRIGHT", self.Anchor, "TOPRIGHT")
	self.Title:SetHeight(22)
	self.Title:EnableMouse(true)
	self.Title:RegisterForClicks("RightButtonUp")
	self.Title:SetScript("OnMouseDown", startmovingH)
	self.Title:SetScript("OnMouseUp", stopmovingH)
	self.Title.texture = self.Title:CreateTexture(nil);
	self.Title.texture:SetColorTexture(0.592,0.062,0.062,0.9);
		self.Title.texture:SetAllPoints(self.Title);
		
	-- Create Title text
	self.TitleText = self.Title:CreateFontString(nil, nil, "GameFontNormalLarge")
	self.TitleText:SetPoint("TOPLEFT", self.Title, "TOPLEFT", 4, -3)
	self.TitleText:SetJustifyH("LEFT")
	self.TitleText:SetWidth(160);
	self.TitleText:SetTextColor(1, 1, 1, 1)
	self.defaultTitle = "RobBossMods "..rbm_version
	self.TitleText:SetText(self.defaultTitle)
	
	-- DropDown
	self.AddonDropDown = CreateFrame("Button", "RBMAddonDrop", self.Anchor, "UIDropDownMenuTemplate")
	self.AddonDropDown:SetHeight(30)
	self.AddonDropDown:SetWidth(500)
	self.AddonDropDown:SetPoint("TOP", self.Anchor, "TOP", -26, -30)
	UIDropDownMenu_SetWidth(self.AddonDropDown, 180, 0); 
	menuFrame3.displayMode = "MENU"
	UIDropDownMenu_Initialize(self.AddonDropDown, menuFrame3.initialize);
	
	
	--test horizontal line
		
	
	--  Text current boss
	self.currentBossFixed = self.Anchor:CreateFontString(nil,nil, "GameFontNormal")
	self.currentBossFixed:SetPoint("TOPRIGHT", self.Anchor, "TOPRIGHT", -4, -91)
	self.currentBossFixed:SetJustifyH("RIGHT")
	self.currentBossFixed:SetTextColor(1, 0.5, 0, 1)
	self.currentBossFixed:SetText("CURRENT_BOSS")
	
	-- Button Boss Chose
	self.selectBoss = CreateFrame("Button", "RBMselectBossButton", self.Anchor)
	self.selectBoss:SetWidth(40)
	self.selectBoss:SetHeight(40)
	self.selectBoss:SetPoint("TOPLEFT", self.Anchor, "TOPLEFT", 15, -85)
	self.selectBoss:SetScript("OnMouseUp", function(self)
		menuFrame2.displayMode = "MENU"
		ToggleDropDownMenu(1, nil, menuFrame2, self, 0, 0)
		local _,_,_, xOfs, yOfs = self:GetPoint()
		self:SetPoint("TOPLEFT", RobBossMods.Anchor, "TOPLEFT", xOfs-1, yOfs+1)
		self:SetWidth(self:GetWidth()+2)
		self:SetHeight(self:GetHeight()+2)
	end
	)
	self.selectBoss:SetScript("OnMouseDown", function(self)
		local _,_,_, xOfs, yOfs = self:GetPoint()
		self:SetPoint("TOPLEFT", RobBossMods.Anchor, "TOPLEFT", xOfs+1, yOfs-1)
		self:SetWidth(self:GetWidth()-2)
		self:SetHeight(self:GetHeight()-2)
	end)
	self.selectBoss:SetNormalTexture("Interface\\ICONS\\inv_misc_head_dragon_01.blp")
	self.selectBoss:SetHighlightTexture("Interface\\ICONS\\inv_misc_head_dragon_01.blp")
	self.selectBoss:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.selectBoss:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	
	-- font current boss 
	self.currentBoss = self.Anchor:CreateFontString(nil,nil, "GameFontNormal")
	self.currentBoss:SetPoint("TOPRIGHT", self.currentBossFixed, "TOPRIGHT", 0, -15)
	self.currentBoss:SetJustifyH("RIGHT")
	self.currentBoss:SetTextColor(1, 1, 1, 1)
	self.currentBoss:SetText("CURRENT_BOSS_VAR")
	self.currentBoss:SetWidth(130)
	
	--Post button (tactics)
	self.postButton = CreateFrame("Button", "RBMpostButtonButton", self.Anchor)
	self.postButton:SetWidth(48)
	self.postButton:SetHeight(48)
	self.postButton:SetPoint("CENTER", self.Anchor, "CENTER", 0, -32)
	self.postButton:SetScript("OnMouseUp", function(self, button)
		if button == "RightButton" then
			menuFrame4.displayMode = "MENU"
			ToggleDropDownMenu(1,nil, menuFrame4, self, 0, 0)
		elseif button == "LeftButton" then
			if lastboss == nil then
				StaticPopup_Show ("RBM_NOBOSS")
			else 
				RBM_PrePost() 
			end
		end
		self:SetWidth(48)
		self:SetHeight(48)
	end
	)
	self.postButton:SetScript("OnMouseDown", function(self)
		self:SetWidth(46)
		self:SetHeight(46)
	end)
	self.postButton:SetScript("OnEnter", function(self)
		GameTooltip:SetOwner(self, "ANCHOR_TOP");
		GameTooltip:AddLine(L["TOOLTIP_TACTICBUTTON_1"]);
		GameTooltip:AddLine(L["TOOLTIP_TACTICBUTTON_2"]);
		GameTooltip:Show();
	end)
	self.postButton:SetScript("OnLeave", function(self)
		GameTooltip:Hide();
	end)
	
	self.postButton:SetNormalTexture("Interface\\ICONS\\spell_shadow_soothingkiss.blp")
	self.postButton:SetHighlightTexture("Interface\\ICONS\\spell_shadow_soothingkiss.blp")
	self.postButton:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.postButton:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	
	
	--Post button (loot)
	self.lootButton = CreateFrame("Button", "RBMlootButtonButton", self.Anchor)
	self.lootButton:SetWidth(34)
	self.lootButton:SetHeight(34)
	self.lootButton:SetPoint("CENTER", self.Anchor, "CENTER", -65, -31)
	self.lootButton:SetScript("OnMouseUp", function(self)
		if lastboss == nil then
			StaticPopup_Show ("RBM_NOBOSS")
		else 
			doLootPost()
		end
		self:SetWidth(34)
		self:SetHeight(34)
	end
	)
	self.lootButton:SetScript("OnMouseDown", function(self)
		self:SetWidth(32)
		self:SetHeight(32)
	end)
	self.lootButton:SetScript("OnEnter", function(self)
		GameTooltip:SetOwner(self, "ANCHOR_TOP");
		GameTooltip:AddLine(L["TOOLTIP_LOOTBUTTON"]);
		GameTooltip:Show();
	end)
	self.lootButton:SetScript("OnLeave", function(self)
		GameTooltip:Hide();
	end)
	self.lootButton:SetNormalTexture("Interface\\ICONS\\inv_misc_gem_variety_01.blp")
	self.lootButton:SetHighlightTexture("Interface\\ICONS\\inv_misc_gem_variety_01.blp")
	self.lootButton:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.lootButton:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)

	--Post button (achievements)
	self.achieveButton = CreateFrame("Button", "RBMachieveButtonButton", self.Anchor)
	self.achieveButton:SetWidth(34)
	self.achieveButton:SetHeight(34)
	self.achieveButton:SetNormalTexture("Interface\\ICONS\\spell_nature_strength.blp")
	self.achieveButton:SetHighlightTexture("Interface\\ICONS\\spell_nature_strength.blp")
	self.achieveButton:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.achieveButton:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	self.achieveButton:SetPoint("CENTER", self.Anchor, "CENTER", 65, -31)
	self.achieveButton:SetScript("OnMouseUp", function(self, button)
		StaticPopupDialogs["RBM_NOTYET"] = {
			text = "This function is not yet implemented please wait a few more weeks\n\nDiese Funktion ist noch nicht eingebaut, bitte warte einige Tage oder Wochen",
			button1 = "OK",
			timeout = 0,
			whileDead = true,
			hideOnEscape = true,
		}
		--StaticPopup_Show ("RBM_NOTYET")
		if lastboss == nil then
			StaticPopup_Show ("RBM_NOBOSS")
		else 
			if button == "RightButton" then
				doAchieveAllPost()
			elseif button == "LeftButton" then
				doAchievePost()
			end
		end
		self:SetWidth(34)
		self:SetHeight(34)
	end
	)
	self.achieveButton:SetScript("OnMouseDown", function(self)
		self:SetWidth(32)
		self:SetHeight(32)
	end)
	self.achieveButton:SetScript("OnEnter", function(self)
		GameTooltip:SetOwner(self, "ANCHOR_TOP");
		GameTooltip:AddLine(L["TOOLTIP_ACHIEVEBUTTON"]);
		GameTooltip:AddLine(L["TOOLTIP_ACHIEVEBUTTON_2"]);
		GameTooltip:Show();
	end)
	self.achieveButton:SetScript("OnLeave", function(self)
		GameTooltip:Hide();
	end)
	
			
			
	-- Post in TextFrame
	self.postOnFixed = self.Anchor:CreateFontString(nil,nil, "GameFontNormal")
	self.postOnFixed:SetPoint("LEFT", self.Anchor, "LEFT", 5, -80)
	self.postOnFixed:SetJustifyH("LEFT")
	self.postOnFixed:SetTextColor(1, 0.5, 0, 1)
	self.postOnFixed:SetText("POST_ON_FIXED")
	
	
	-- Post in TextFrame Variable
	self.postOn = self.Anchor:CreateFontString(nil,nil, "GameFontNormalLarge")
	self.postOn:SetPoint("RIGHT", self.Anchor, "RIGHT", -5, -80)
	self.postOn:SetJustifyH("RIGHT")
	self.postOn:SetTextColor(1, 1, 1, 1)
	self.postOn:SetText("POST_ON_VAR")
	
	-- Hint post button
	self.hintPostButton = CreateFrame("Button", "RBMhintPostButtonButton", self.Anchor)
	self.hintPostButton:SetWidth(150)
	self.hintPostButton:SetHeight(17)
	self.hintPostButton:SetPoint("BOTTOM", self.Anchor, "BOTTOM", 0, 33)
	self.hintPostButton:SetScript("OnMouseUp", function(self)
		self:SetPoint("BOTTOM", RobBossMods.Anchor, "BOTTOM", 0, 33)
		self:SetWidth(self:GetWidth()+2)
		self:SetHeight(17)
		RBM_tellHint()
	end
	)
	self.hintPostButton:SetScript("OnMouseDown", function(self)
		self:SetPoint("BOTTOM", RobBossMods.Anchor, "BOTTOM", 0, 34)
		self:SetWidth(self:GetWidth()-2)
		self:SetHeight(15)
		
		
	end)
	self.hintPostButton.texture = self.hintPostButton:CreateTexture(nil);
	self.hintPostButton.texture:SetColorTexture(0.2,0.2,0.2,1);
		--self.hintPostButton.texture:SetAllPoints(self.hintPostButton);
	
	self.hintPostButton:SetNormalTexture("self.hintPostButton.texture:GetTexture()")
	self.hintPostButton:SetHighlightTexture("self.hintPostButton.texture:GetTexture()")
	self.hintPostButton:GetNormalTexture():SetVertexColor(1, 0, 1, 1)
	self.hintPostButton:GetHighlightTexture():SetVertexColor(1, 0, 1, 0.5)
	self.hintPostButton.texture:SetColorTexture(0.2,0.2,0.2,1);
	
	-- Hint Button TEXT
	self.hintPostButton.text = self.hintPostButton:CreateFontString(nil, nil, "GameFontNormal")
	self.hintPostButton.text:SetPoint("TOP", 0, -1)
	self.hintPostButton.text:SetText("HINT")
	
	
	
	-- Menu Buttons  [ TITLE    Help Settings Quit]
	self.closeButton = CreateFrame("Button", nil, self.Anchor)
	self.closeButton:SetWidth(25)
	self.closeButton:SetHeight(25)
	
	self.closeButton:SetPoint("TOPRIGHT", self.Anchor, "TOPRIGHT", 2, 1)
	self.closeButton:SetScript("OnMouseUp", function(self)
		RobBossMods.Anchor:Hide()
	end
	)
	self.closeButton:SetScript("OnMouseDown", function(self)
		
	end)
	
	self.closeButton:SetNormalTexture("Interface\\BUTTONS\\ui-panel-minimizebutton-up.blp")
	self.closeButton:SetHighlightTexture("Interface\\BUTTONS\\ui-panel-minimizebutton-up.blp")
	self.closeButton:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.closeButton:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	self.closeButton:SetFrameLevel(self.Title:GetFrameLevel() + 2)
	
	--Zahnrad
	self.configButton = CreateFrame("Button", nil, self.Anchor)
	self.configButton:SetWidth(18)
	self.configButton:SetHeight(18)
	
	self.configButton:SetPoint("TOPRIGHT", self.Anchor, "TOPRIGHT", -19, -2)
	self.configButton:SetScript("OnMouseDown", function(self)
		InterfaceOptionsFrame_OpenToCategory("RobBossMods");
	end)
	
	self.configButton:SetNormalTexture("Interface\\AddOns\\RobBossMods\\Media\\Zahnrad")
	self.configButton:SetHighlightTexture("Interface\\AddOns\\RobBossMods\\Media\\Zahnrad")
	self.configButton:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.configButton:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	self.configButton:SetFrameLevel(self.Title:GetFrameLevel() + 2)

	-- Helperbutton
	
	
	self.helpButton = CreateFrame("Button", nil, self.Anchor)
	self.helpButton:SetWidth(13)
	self.helpButton:SetHeight(13)
	
	self.helpButton:SetPoint("TOPRIGHT", self.Anchor, "TOPRIGHT", -35, -4)
	self.helpButton:SetScript("OnMouseDown", function(self)
		RobBossMods.helperFrame:Show()
	end)
	
	self.helpButton:SetNormalTexture("Interface\\RAIDFRAME\\ReadyCheck-Waiting")
	self.helpButton:SetHighlightTexture("Interface\\RAIDFRAME\\ReadyCheck-Waiting")
	self.helpButton:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.helpButton:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	self.helpButton:SetFrameLevel(self.Title:GetFrameLevel() + 2)
	
	
	-- DropDown Settings
	self.SettingsDropDown = CreateFrame("Button", "RBMSettingsDrop", self.Anchor, "UIDropDownMenuTemplate")
	self.SettingsDropDown:SetHeight(30)
	self.SettingsDropDown:SetWidth(500)
	self.SettingsDropDown:SetPoint("BOTTOM", self.Anchor, "BOTTOM", -26, 0)
	UIDropDownMenu_SetWidth(self.SettingsDropDown, 180, 0); 
	menuFrame5.displayMode = "MENU"
	UIDropDownMenu_Initialize(self.SettingsDropDown, menuFrame5.initialize);
	
	
	self.setBossName = function(eid)
		lastboss = eid
		--test
		_, _, _, _, iconImage = EJ_GetCreatureInfo(1, eid)
		RobBossMods.selectBoss:SetWidth(110)
		RobBossMods.selectBoss:SetHeight(50)
		RobBossMods.selectBoss:SetPoint("TOPLEFT", RobBossMods.Anchor, "TOPLEFT", -4, -82)
			
		RobBossMods.selectBoss:SetNormalTexture(iconImage)
		RobBossMods.selectBoss:SetHighlightTexture(iconImage)
		RobBossMods.selectBoss:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
		RobBossMods.selectBoss:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
		--endtest
		
		--change dropdown
		if eid ~= nil then
			local mynewvalue = digAddonsForBossid(eid)
			if not (eid == false or eid == nil) then
				UIDropDownMenu_SetText(RobBossMods.AddonDropDown, addonIndex[mynewvalue]);
				
				if mynewvalue == "cata" then
					current_five = cata_five
					current_raid = cata_raid
				elseif mynewvalue == "wotlk" then
					current_five = wotlk_five
					current_raid = wotlk_raid
				elseif mynewvalue == "bc" then
					current_five = bc_five
					current_raid = bc_raid
				elseif mynewvalue == "vanilla" then
					current_five = vanilla_five
					current_raid = vanilla_raid
				elseif mynewvalue == "mop" then
					current_five = mop_five
					current_raid = mop_raid
				elseif mynewvalue == "wod" then
					current_five = wod_five
					current_raid = wod_raid
				elseif mynewvalue == "legion" then 
					current_five = legion_five
					current_raid = legion_raid
				elseif mynewvalue == "bfa" then 
					current_five = bfa_five
					current_raid = bfa_raid
				end
			end
		end
		if eid ~= nil and EJ_GetEncounterInfo(eid) ~= nil then
			RobBossMods.currentBoss:SetText(EJ_GetEncounterInfo(eid));
		end
	end
	
	-- arrows
	
	self.nextBossButton = CreateFrame("Button", "RBMnextBossButton", self.Anchor)
	self.nextBossButton:SetWidth(28)
	self.nextBossButton:SetHeight(28)
	self.nextBossButton:SetPoint("TOPRIGHT", self.currentBossFixed, "TOPRIGHT", 0, 30)
	self.nextBossButton:SetNormalTexture("Interface\\BUTTONS\\ui-spellbookicon-nextpage-up.blp")
	self.nextBossButton:SetHighlightTexture("Interface\\BUTTONS\\ui-spellbookicon-nextpage-up.blp")
	self.nextBossButton:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.nextBossButton:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	
	self.nextBossButton:SetScript("OnMouseUp", function(self)
	
		if lastboss == nil then
			
		else
			local _,curInst = digAddonsForBossid(lastboss)
			local nextboss = getNextBoss(curInst, lastboss) 
			if nextboss ~= nil and nextboss ~= lastboss then
				RobBossMods.setBossName(nextboss);
			end
		end
	
		local _,_,_, xOfs, yOfs = self:GetPoint()
		self:SetPoint("TOPRIGHT", RobBossMods.currentBossFixed, "TOPRIGHT", xOfs+1, yOfs+1)
		self:SetWidth(self:GetWidth()+2)
		self:SetHeight(self:GetHeight()+2)
	end
	)
	self.nextBossButton:SetScript("OnMouseDown", function(self)
		local _,_,_, xOfs, yOfs = self:GetPoint()
		self:SetPoint("TOPRIGHT", RobBossMods.currentBossFixed, "TOPRIGHT", xOfs-1, yOfs-1)
		self:SetWidth(self:GetWidth()-2)
		self:SetHeight(self:GetHeight()-2)
	end)
	
	
	self.prevBossButton = CreateFrame("Button", "RBMprevBossButton", self.Anchor)
	self.prevBossButton:SetWidth(28)
	self.prevBossButton:SetHeight(28)
	self.prevBossButton:SetPoint("TOPRIGHT", self.currentBossFixed, "TOPRIGHT", -30, 30)
	self.prevBossButton:SetNormalTexture("Interface\\BUTTONS\\ui-spellbookicon-prevpage-up.blp")
	self.prevBossButton:SetHighlightTexture("Interface\\BUTTONS\\ui-spellbookicon-prevpage-up.blp")
	self.prevBossButton:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.prevBossButton:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	
	self.prevBossButton:SetScript("OnMouseUp", function(self)
	
		if lastboss == nil then
			
		else
			local _,curInst = digAddonsForBossid(lastboss)
			local prevboss = getPrevBoss(curInst, lastboss) 
			if prevboss ~= nil and prevboss ~= lastboss then
				RobBossMods.setBossName(prevboss);
			end
		end
	
		local _,_,_, xOfs, yOfs = self:GetPoint()
		self:SetPoint("TOPRIGHT", RobBossMods.currentBossFixed, "TOPRIGHT", xOfs+1, yOfs+1)
		self:SetWidth(self:GetWidth()+2)
		self:SetHeight(self:GetHeight()+2)
	end
	)
	self.prevBossButton:SetScript("OnMouseDown", function(self)
		local _,_,_, xOfs, yOfs = self:GetPoint()
		self:SetPoint("TOPRIGHT", RobBossMods.currentBossFixed, "TOPRIGHT", xOfs-1, yOfs-1)
		self:SetWidth(self:GetWidth()-2)
		self:SetHeight(self:GetHeight()-2)
	end)
	
	-- HELPER FRAME
	
	self.helperFrame = CreateFrame("FRAME", "RBMHelperFrame", self.Anchor)
	
		
	self.helperFrame:SetResizable(false)
	self.helperFrame:SetMovable(true)
	self.helperFrame:SetPoint("RIGHT", self.Anchor, "RIGHT", self.Anchor:GetWidth()+300,0)
	self.helperFrame:SetWidth(550)
	self.helperFrame:SetHeight(550)
	self.helperFrame:SetFrameLevel(self.Anchor:GetFrameLevel()+5)
	self.helperFrame:SetBackdrop({
		--bgFile = [[Interface\QuestFrame\UI-QuestItemNameFrame]], 
		edgeFile= [[Interface\DialogFrame\UI-DialogBox-Border]], 
		edgeSize = 4--, 
		--insets = { left = 11, right = 12, top = 12, bottom = 11 }
	})
	
	
	self.helperFrame.texture = self.helperFrame:CreateTexture(nil, "BACKGROUND");
	self.helperFrame.texture:SetColorTexture(0,0,0,0.8);
	self.helperFrame.texture:SetAllPoints(self.helperFrame);
	
	
	self.helperFrame:SetScript("OnMouseDown", startmoving)
	self.helperFrame:SetScript("OnMouseUp", stopmoving)
	
	
	
	
	self.helperFrame.head1 = self.helperFrame:CreateFontString(nil, nil, "GameFontNormalLarge")
	self.helperFrame.head1:SetPoint("TOP", 0, -5)
	self.helperFrame.head1:SetText(L["BUTTONS"])
	
		--Post button (tactics)
	self.helperFrame.selectBoss = CreateFrame("Button", nil, self.helperFrame)
	self.helperFrame.selectBoss:SetWidth(35)
	self.helperFrame.selectBoss:SetHeight(35)
	self.helperFrame.selectBoss:SetPoint("TOPLEFT", self.helperFrame, "TOPLEFT", 5, -40)
	
	self.helperFrame.selectBoss:SetNormalTexture("Interface\\ICONS\\inv_misc_head_dragon_01.blp")
	self.helperFrame.selectBoss:SetHighlightTexture("Interface\\ICONS\\inv_misc_head_dragon_01.blp")
	self.helperFrame.selectBoss:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.helperFrame.selectBoss:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	
	--text
	self.helperFrame.selectDesc = self.helperFrame.selectBoss:CreateFontString(nil, nil, "GameFontNormal")
	self.helperFrame.selectDesc:SetPoint("TOPLEFT", 40, 0)
	self.helperFrame.selectDesc:SetWidth(500)
	self.helperFrame.selectDesc:SetJustifyH("LEFT")
	self.helperFrame.selectDesc:SetText(L["HELPER_SELECTBOSS"])
	
	
	self.helperFrame.postButton = CreateFrame("Button", nil, self.helperFrame.selectBoss)
	self.helperFrame.postButton:SetWidth(35)
	self.helperFrame.postButton:SetHeight(35)
	self.helperFrame.postButton:SetPoint("TOPLEFT", self.helperFrame.selectBoss, "TOPLEFT", 0, -60)
	
	self.helperFrame.postButton:SetNormalTexture("Interface\\ICONS\\spell_shadow_soothingkiss.blp")
	self.helperFrame.postButton:SetHighlightTexture("Interface\\ICONS\\spell_shadow_soothingkiss.blp")
	self.helperFrame.postButton:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.helperFrame.postButton:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	
	--text
	self.helperFrame.postDesc = self.helperFrame.postButton:CreateFontString(nil, nil, "GameFontNormal")
	self.helperFrame.postDesc:SetPoint("TOPLEFT", 40, 0)
	self.helperFrame.postDesc:SetWidth(500)
	self.helperFrame.postDesc:SetJustifyH("LEFT")
	self.helperFrame.postDesc:SetText(L["HELPER_BOSSPOST"])
	
	
		--Post button (loot)
	self.helperFrame.postLoot = CreateFrame("Button", nil, self.helperFrame.postButton)
	self.helperFrame.postLoot:SetWidth(35)
	self.helperFrame.postLoot:SetHeight(35)
	self.helperFrame.postLoot:SetPoint("TOPLEFT", self.helperFrame.postButton, "TOPLEFT", 0, -60)
	
	self.helperFrame.postLoot:SetNormalTexture("Interface\\ICONS\\inv_misc_gem_variety_01.blp")
	self.helperFrame.postLoot:SetHighlightTexture("Interface\\ICONS\\inv_misc_gem_variety_01.blp")
	self.helperFrame.postLoot:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.helperFrame.postLoot:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	
	--text
	self.helperFrame.lootDesc = self.helperFrame.postLoot:CreateFontString(nil, nil, "GameFontNormal")
	self.helperFrame.lootDesc:SetPoint("TOPLEFT", 40, 0)
	self.helperFrame.lootDesc:SetWidth(500)
	self.helperFrame.lootDesc:SetJustifyH("LEFT")
	self.helperFrame.lootDesc:SetText(L["HELPER_POSTLOOT"])
	
	
	
		--Post button (achieve)
	self.helperFrame.postAchieve = CreateFrame("Button", nil, self.helperFrame.postLoot)
	self.helperFrame.postAchieve:SetWidth(35)
	self.helperFrame.postAchieve:SetHeight(35)
	self.helperFrame.postAchieve:SetPoint("TOPLEFT", self.helperFrame.postLoot, "TOPLEFT", 0, -60)
	
	self.helperFrame.postAchieve:SetNormalTexture("Interface\\ICONS\\spell_nature_strength.blp")
	self.helperFrame.postAchieve:SetHighlightTexture("Interface\\ICONS\\spell_nature_strength.blp")
	self.helperFrame.postAchieve:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.helperFrame.postAchieve:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	
	--text
	self.helperFrame.achieveDesc = self.helperFrame.postAchieve:CreateFontString(nil, nil, "GameFontNormal")
	self.helperFrame.achieveDesc:SetPoint("TOPLEFT", 40, 0)
	self.helperFrame.achieveDesc:SetWidth(500)
	self.helperFrame.achieveDesc:SetJustifyH("LEFT")
	self.helperFrame.achieveDesc:SetText(L["HELPER_POSTACHIEVEMENT"])
	
	--header2
	self.helperFrame.head2 = self.helperFrame:CreateFontString(nil,nil, "GameFontNormalLarge")
	self.helperFrame.head2:SetPoint("TOP", 0, -270)
	self.helperFrame.head2:SetJustifyH("LEFT")
	self.helperFrame.head2:SetText(L["HEAD_CHATCOMMANDS"])
	
	self.helperFrame.chatcommands = self.helperFrame:CreateFontString(nil, nil, "GameFontNormal")
	self.helperFrame.chatcommands:SetPoint("TOPLEFT", 8, -300)
	self.helperFrame.chatcommands:SetWidth(500)
	self.helperFrame.chatcommands:SetJustifyH("LEFT")
	self.helperFrame.chatcommands:SetText(L["CHATCOMMANDS"])
	
	self.helperFrame.ytlinkbox = CreateFrame("EditBox", nil, self.helperFrame, "InputBoxTemplate")
								
	
	self.helperFrame.ytlinkbox:SetHeight(85)
	self.helperFrame.ytlinkbox:SetWidth(450)
	self.helperFrame.ytlinkbox:SetAutoFocus(false)
	self.helperFrame.ytlinkbox:SetPoint("BOTTOM", self.helperFrame, "BOTTOM", 0, -23)
	self.helperFrame.ytlinkbox:SetText("http://www.youtube.com/watch?v=k6oiYu1T58k");
	self.helperFrame.ytlinkbox:SetScript("OnTextChanged", function(self)
		self:SetText("http://www.youtube.com/watch?v=k6oiYu1T58k")
	end)
	--self.helperFrame.ytlinkbox:SetTextInsets(5, 5, 2, 2)
	
	
	self.closeButton2 = CreateFrame("Button", nil, self.helperFrame)
	self.closeButton2:SetWidth(25)
	self.closeButton2:SetHeight(25)
	
	self.closeButton2:SetPoint("TOPRIGHT", self.helperFrame, "TOPRIGHT", 0, 2)
	self.closeButton2:SetScript("OnMouseUp", function(self)
		RobBossMods.helperFrame:Hide()
	end
	)
	self.closeButton2:SetScript("OnMouseDown", function(self)
		
	end)
	
	self.closeButton2:SetNormalTexture("Interface\\BUTTONS\\ui-panel-minimizebutton-up.blp")
	self.closeButton2:SetHighlightTexture("Interface\\BUTTONS\\ui-panel-minimizebutton-up.blp")
	self.closeButton2:GetNormalTexture():SetVertexColor(1, 1, 1, 1)
	self.closeButton2:GetHighlightTexture():SetVertexColor(1, 1, 1, 0.5)
	--self.closeButton2:SetFrameLevel(self.Title:GetFrameLevel() + 2)
	
	-- CLOSE on HELPFRAME
	
	--INITIALIZE
	RobBossMods.Anchor:Hide()
	RobBossMods.helperFrame:Hide()
	
	if RobBossMods.db.char.postchannel == "WHISPER" then
		RobBossMods.db.char.postchannel = nil
	end
	
	if RobBossMods.db.char.postchannel == nil then
		RobBossMods.db.char.postchannel = "PARTY"
	end
	
	if RobBossMods.db.char.tellachievement == nil then
		RobBossMods.db.char.tellachievement = true
	end
	
	self.updateFrame = function(self)
		if RobBossMods.db.char.nevershow then
			UIDropDownMenu_SetText(RobBossMods.SettingsDropDown, L["N_SHOW"]);
		elseif RobBossMods.db.char.showframe then
			UIDropDownMenu_SetText(RobBossMods.SettingsDropDown, L["A_SHOW"]);
		else  
			UIDropDownMenu_SetText(RobBossMods.SettingsDropDown, L["P_SHOW"]);
		end
	end
	
	RobBossMods.updateFrame()
	
	UIDropDownMenu_SetText(self.AddonDropDown, addonIndex[currentAddon]);
	
	
	RobBossMods.postOn:SetText("no");
	
	
	
	if tonumber(RobBossMods.db.char.postchannel) ~= nil then
		for i=1, select("#", GetChannelList()), 2 do
			if select(i, GetChannelList()) == tonumber(RobBossMods.db.char.postchannel) then
				RobBossMods.postOn:SetText(select(i+1, GetChannelList()));
				break;
			end
		end
	else
		RobBossMods.postOn:SetText(L[RobBossMods.db.char.postchannel])
	end
	
	if RobBossMods.postOn:GetText() == "no" then
		RobBossMods.db.char.postchannel = "PARTY"
		RobBossMods.postOn:SetText(L[RobBossMods.db.char.postchannel])
	end
	
	RobBossMods.currentBoss:SetText(L["NOBOSS"]);
	
	RobBossMods.postOnFixed:SetText(L["POSTON"]);
	RobBossMods.hintPostButton.text:SetText(L["INFORM_PARTY"]);
	RobBossMods.currentBossFixed:SetText(L["CURRENTBOSS"]);
	
	self.hintPostButton:SetWidth(self.hintPostButton.text:GetStringWidth()+15)
	

	self.CreateFrames = nil

end

	
	
	
--- init
--ende init
SlashCmdList["RBM"] = handler;
frame:SetScript("OnEvent", frame.OnEvent)
