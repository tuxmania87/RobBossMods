local fr = CreateFrame("Frame")
fr:RegisterEvent("ADDON_LOADED")

function fr:OnEvent(event,...)
	local arg1 = ...
	if event =="ADDON_LOADED" and arg1 == "RobBossMods" then
		local L
		if rbm_elang == "de" and GetLocale() == "enUS" then
			L = LibStub("AceLocale-3.0"):NewLocale("RobBossMods", "enUS", false)
		else
			L = LibStub("AceLocale-3.0"):NewLocale("RobBossMods", "deDE", false)
		end
		if not L then return end

	-- bosses with their ids as key


--menu entrys
L["5MAN"] = "5er Instanz"
L["ACHIEVE_SETTINGS"] = "Zeige unfertige Erfolge"
L["ACHIEVE_SETTINGS_DESC"] = "Zeige unfertige Erfolge wenn du eine Instanz betrittst oder einen Boss in's Ziel nimmst."
L["ALWAYS_SHOW_FRAME"] = "RBM Fenster immer anzeigen"
L["ALWAYS_SHOW_FRAME_DESC"] = "Immer wenn du in einer Gruppe bist, ist das RobBossMods Fenster aktiv ( nicht im Kampf )."
L["ASFIRST"] = "als ersten Boss von"
L["A_SHOW"] = "Immer anzeigen"
L["ASNEXT"] = "als n\195\164chsten Boss von"
L["AUTHOR1"] = "Autor ist Robert Hartmann, Deutschland ( Lufti auf EU-Nethersturm) Email: robert@keinerspieltmitmir.de u"
L["AUTHOR2"] = "Gehilfen f\195\188r die englische \195\156bersetzung: Stagen@irc.freenode.net und Niadyth@irc.freenode.net"
L["BOOK"] = "Buch"
L["BOSS_DEAD1"] = " ist tot."
L["BOSS_DEAD2"] = " ist als N\195\164chstes dran."
L["BUTTONS"] = "Schaltfl\195\164chen"
L["CHATCOMMANDS"] = "!boss - (im Gruppen/Raidkanal geschrieben) sendet aktuelle Bosstaktiken an den Gruppen/Raidkanal\
\
!loot - (im Gruppen/Raidkanal geschrieben) sendet aktuelle Beuteinformationen an den Gruppen/Raidkanal\
\
!achieve(all) - sendet die Bosserfolge (Instanzenerfolge) an den Gruppen/Raidkanal\
\
!rbminfo sendet einige Informationen \195\188ber das Addon an den Gruppenkanal\
\
/rbm resetlang - wenn du den englischen Client besitzt, aber auf deutsche Taktiken zugreifen m\195\182chtest, kannst du es mit diesem Kommando tun\
\
/rbm toggle - Schaltet das Hauptfenster um (an/aus). Gut benutzbar um eigene Makros zu bauen.\
"
L["CHOICE"] = "Auswahl"
L["CLOSE_OPT"] = "Automatisch verstecken"
L["CLOSE_OPT_DESC"] = "Fenster nur anzeigen, wenn ein Boss im Ziel ist."
L["CURRENT_ADDON"] = "zeige Taktiken f\195\188r"
L["CURRENTBOSS"] = "aktueller Boss"
L["D23574_1"] = "CUR_SPELL unter TAR_SPELL. Alle DORT sammeln!!"
L["D23574_2"] = "CUR_SPELL vorbei: Wieder verteilen!"
L["D23577_1"] = "CUR_SPELL: Boss aus dem Kreis ziehen!"
L["D23577_2"] = "CUR_SPELL: umhauen!"
L["D23863_1"] = "CUR_SPELL: Zauber entfernen / dispellen !!"
L["D23863_2"] = "CUR_SPELL: Zauber entfernen / dispellen !!"
L["D23863_3"] = "CUR_SPELL: Immer in Bewegung bleiben!"
L["D52053_1"] = "Zanzil beschw\195\182rt Berserker: Zum blauen Kessel und Berserker umhauen!"
L["D52053_2"] = "ALLE sofort zum Giftkessel und Schutzbuff holen!"
L["D52053_3"] = "Zanzil beschw\195\182rt Zombies. Roten Buff holen und Tanken!"
L["D52059_1"] = "CUR_SPELL: Boss unterbrechen!"
L["D52148_1"] = "Alle RAUS aus der CUR_SPELL!!"
L["D52148_2"] = "Alle REIN in die Kuppel!!!"
L["D52151_1"] = "CUR_SPELL auf TAR_SPELL: nur minimal heilen!"
L["D52151_2"] = "CUR_SPELL auf TAR_SPELL: verschwunden."
L["D52151_3"] = "CUR_SPELL: Raptor umhauen!"
L["D52151_4"] = "CUR_SPELL: Nicht vor Boss stehen und nicht direkt an ihm dran."
L["D52155_1"] = "CUR_SPELL: Nicht vor ihm stehen! "
L["D52155_2"] = "CUR_SPELL: In Bewegung bleiben und weg von dem gruenen Zeug!"
L["D52155_3"] = "CUR_SPELL: TAR_DUMM1 und TAR_DUMM2 auseinanderlaufen!"
L["D52271_1"] = "T\195\182tet die Geister. Wenn sie einen Spieler erreichen stirbt dieser!"
L["D52286_1"] = "ALLE 15 Meter weg vom Boss !!!"
L["D52638_1"] = "TAR_SPELL wird vom Geier entf\195\188hrt!!"
L["D52730_1"] = "Sprung auf: TAR_SPELL!! An die Ketten stellen"
L["DONTKNOW"] = "Hallo. Ich habe die 'RobBossMods' f\195\188r heroische Instanzen installiert. Vor jedem Boss k\195\182nnt ihr !boss eingeben oder mich mit !boss anwhispern und ihr erhaltet eine Erkl\195\164rung!"
L["DONTKNOW_OLD"] = "Wer den Boss nicht kennt, gibt bitte !boss in den Gruppenchat ein und bekommt eine Erkl\195\164rung."
L["DRAG"] = "Shift + Links-Klick: Minimap Symbol verschieben"
L["EVERY_BOSS_ANNOUNCE_DESC"] = "Wenn ein Boss anvisiert wird, im Gruppenchat den Hinweis geben, dass man mit !boss die Taktik erfragen kann!"
L["EVERY_BOSS_ANNOUNCEPARTY"] = "Hinweis beim Anvisieren (in Gruppe)"
L["EVERY_BOSS_ANNOUNCERAID"] = "Hinweis beim Anvisieren (in Raid)"
L["GUILD"] = "Gilde"
L["HEAD_CHATCOMMANDS"] = "Chat-Kommandos"
L["HELPER_BOSSPOST"] = "Linksklick: Sende die Taktiken f\195\188r den aktuellen Boss zum Kanal, den du vorher eingestellt hast. Pr\195\188fe die Anzeige im Fenster.\\\
Rechtsklick: W\195\164hle den Kanal aus, zu dem du Beute- und Bossinformationen senden m\195\182chtest."
L["HELPER_POSTACHIEVEMENT"] = "Linksklick: Zeige alle Erfolge an, die sich auf den aktuellen Boss beziehen.\
Rechtsklick: Zeige alle Erfolge dieser Instanz an."
L["HELPER_POSTLOOT"] = "Sende Informationen \195\188ber die Beute des aktuellen Bosses an den eingestellten Kanal."
L["HELPER_SELECTBOSS"] = "Du kannst den aktuellen Boss manuell einstellen, aber in den meisten F\195\164llen wird RobBossMods automatisch den korrekten Boss einstellen."
L["HINTEVERY"] = "Hinweis beim Anvisieren"
L["HINTEVERY_DESC"] = "Der Text, der erscheint, wenn du einen Boss ins Visier nimmst ( Option muss dazu aktiviert sein )"
L["HINTONCE"] = "Einmaliger Hinweis"
L["HINTONCE_DESC"] = "Der Text, den du mit Linksklick auf Minimap Symbol posten kannst"
L["IGN1"] = " hat "
L["IGN2"] = " mal in den letzten "
L["IGN3"] = " Sekunden eine !boss Anfrage gestartet. Sollen weitere Anfragen von dem Spieler ignoriert werden?\
\
Hinweis: Du kannst einen Spieler auch manuell ignorieren. Spieler ins Ziel nehmen und /rbm ignore  in den Chat eingeben oder /rbm ignore SPIELERNAME."
L["INFORM_PARTY"] = "Hinweis an Gruppe senden"
L["INSTANCE_CHAT"] = "Gruppe/Raid"
L["LASTBOSS"] = " war der letzte Boss hier. Vielen Dank f\195\188r die Gruppe!"
L["LEFTCLICK"] = "Links-Klick: RobBossMods Fenster anzeigen/verstecken"
L["LOADING"] = "Lade"
L["LOOTFOR"] = "Beute von"
L["MENU_AUTHOR"] = " /rbm autor -- Informationen \195\188ber den Autor abrufen"
L["MENU_DESCRIPTION"] = "RobBossMods zeigt Boss Taktiken f\195\188r Heroische Instanzen an. Wenn du in einer Instanz bist, k\195\182nnen Spieler mit !boss im Gruppenchat die Taktik abfragen. Du kannst die taktiken au\195\159erdem manuell senden und die Bosse ebenso manuell ausw\195\164hlen!"
L["MENU_DYNAMIC1"] = "Ansagen im Kampf von "
L["MENU_DYNAMIC2"] = " aktivieren"
L["MENU_DYNAMIC_DESC"] = "Im Kampfverlauf im /sagen Chat Hinweise geben zum Kampfverlauf."
L["MENU_DYN_NO"] = "Leider gibt es zu diesem Boss noch keine dynamischen Ansagen.\
\
Wende dich doch an /rbm autor um Vorschlaege zu machen!"
L["MENU_DYN_TOGGLE_A"] = "alle Ansagen aktiveren."
L["MENU_DYN_TOGGLE_A_DESC"] = "Alle dynamische Ansagen im Kampf aktiveren. Achtung! Nicht zu allen Bossen existieren Ansagen."
L["MENU_DYN_TOGGLE_D"] = "alle Ansagen deaktiveren."
L["MENU_DYN_TOGGLE_D_DESC"] = "Alle dynamische Ansagen im Kampf deaktiveren."
L["MENU_FRAME_OFF"] = " /rbm hide -- Vestecke RobBossMod Frame"
L["MENU_FRAME_ON"] = " /rbm show -- Zeige RobBossMod Frame"
L["MENU_FRAME_TOGGLE"] = " /rbm toggle -- RobBossMod Frame ein/ausblenden"
L["MENU_GENERAL"] = "Allgemeine Einstellungen"
L["MENU_HINT1"] = " !rbminfo im Gruppenchat .-- Gibt Informationen \195\188ber das Addon. U.a. Link und Namen."
L["MENU_MINIMAP_OFF"] = " /rbm minioff -- Verstecke Minimap Icon"
L["MENU_MINIMAP_ON"] = " /rbm minion -- Zeige Minimap Icon"
L["MENU_RESETLANG"] = " /rbm resetlang -- Spracheinstellungen zur\195\188cksetzen"
L["MENU_STATE"] = " /rbm status -- gibt den Status des Addons zur\195\188ck"
L["MINIMAP_SHOW"] = "Zeige Minimap Symbol"
L["MUST_RESTART"] = "Du musst das Addon neu starten, damit du die \195\132nderungen sehen kannst. Zum Beispiel durch Schreiben von /reload in den Chat."
L["MYSELF"] = "Mich"
L["NEVER_OPT"] = "Nie automatisch anzeigen"
L["NEVER_SHOW_FRAME"] = "RBM Fenster nie automatisch anzeigen"
L["NEVER_SHOW_FRAME_DESC"] = "Das RBM Fenster wird NIEMALS automatisch angezeigt. Nur wenn man /rbm show eingibt ODER auf das Minimap/Broker Symbol dr\195\188ckt."
L["NEVER_WARNING"] = "ACHTUNG! Das Fenster wird dann nur noch erscheinen, wenn du auf das MinimapSymbol / BrokerSymbol dr\195\188ckst oder /rbm show in den Chat eingibst!"
L["NEW_VER1"] = "Du hast RBM Version "
L["NEW_VER2"] = " installiert, aber es gibt bereist Version: "
L["NEXT_BOSS_ANNOUNCE"] = "n\195\164chsten Boss ansagen"
L["NEXT_BOSS_ANNOUNCE_DESC"] = "Wenn der Boss stirbt, im Gruppenchat ansagen und sagen wer der n\195\164chste ist!"
L["NOBOSS"] = "Kein Boss ausgew\195\164hlt!"
L["NONE"] = "keiner"
L["N_SHOW"] = "Niemals automatisch anzeigen"
L["OFFICER"] = "Offiziere"
L["OPTIONS"] = "Optionen"
L["PARTY"] = "Gruppe/Raid"
L["POSTON"] = "Taktik senden"
L["P_SHOW"] = "Beim Boss anzeigen"
L["RAID"] = "Gruppe/Raid"
L["RAIDS"] = "Raids"
L["RESET_CUSTOM"] = "L\195\182schen eigener Taktiken"
L["RESET_CUSTOM_DESC"] = "Eigene Beschreibungen zur\195\188cksetzen und Voreinstellung laden. Achtung ALLE eigenen Taktiken werden gel\195\182scht!"
L["RIGHTCLICK"] = "Rechts-Klick: Einstellungen"
L["SAY"] = "Sagen"
L["SCROLL"] = "Schriftrolle"
L["SEL_MINIMAP_ICON"] = "Minimap-Symbol ausw\195\164hlen"
L["S_GHUR"] = "Ghur'sha ist tot. Ozumat ist als N\195\164chstes dran!"
L["S_HELIX"] = "Helix ist tot. Feindschnitter 5000 ist als N\195\164chstes dran!"
L["S_OZUMAT"] = "Ozumat war der letzte Boss hier. Vielen Dank f\195\188r die Gruppe"
L["T100"] = "Der Tank tankt in einer Ecke, sodass Godfrey in die Ecke schaut. Die Gruppe sollte sich verteilen. Der Boss castet SPELL(93520), was ein beliebiges Gruppenmitglied treffen kann. Sofort aus dem Wirkungsbereich raus!\
Kurz vor SPELL(93520) wirkt er SPELL(93707), einfach AE tanken und umhauen. Entfluchen ALLER Gruppenmitglieder nicht vergessen, wegen SPELL(93629)."
L["T101"] = "SPELL(75722): Blauer Kreis auf dem Boden. Raus da!\
Blaue Wirbelst\195\188rme: Nicht davon erfassen lassen! In Bewegung bleiben sonst ist man bewegugnsunf\195\164hig und bekommt recht viel Schaden.\
SPELL(76008): 2 Sekunden Zauber. Unbedingt unterbrechen.\
PHASE 2 (immer bei 66% und 33%) Sie beschw\195\182rt zwei Naga Hexen (wenn m\195\182glich eine oder beide CC ) und einen dicken Naga. Den dicken entweder Kiten und erst die Adds umhauen oder Hexen CC und Dicken umhauen."
L["T102"] = "SPELL(76047): Verursacht am Einschlagsort Schaden und es \195\182ffnet sich ein Riss. Dieser Riss w\195\164chst stetig, daher muss der Boss gezogen werden (am besten am Rand im Kreis).\
SPELL(95463): Er nimmt eine zuf\195\164lliges Gruppenmitglied und f\195\188gt ihm 6 Sekunden lang Schaden zu. Einfach gegenheilen.\
SPELL(76100): Ab und zu wird der Boss w\195\188tend. Versuchen runterzunehmen wenn m\195\182glich ansonsten gegenheilen."
L["T103"] = "Der Boss mus mit R\195\188cken zur Gruppe getankt werden. Er castet ab und zu SPELL(76170), was dann nur den Tank trifft. Danach castet er sofort SPELL(91412). Unterbrechen!\
Aus den roten Pf\195\188tzen raus gehen. Bei 50% hebt sich das die Brain Qualle - Geistbeuger Ghur'sha - sucht sich einen neuen Wirt.\
SPELL(76234): Gr\195\188ne Pf\195\188tzen. Raus da sonst k\195\182nnt ihr nicht zaubern/angreifen.\
SPELL(76307): Immer wenn man Ghur'sha jetzt angreift, heilt sie sich und f\195\188gt uns Schaden zu, daher: Reinigen, Buff klauen oder mit Hunter runternehmen. Geht das alles nicht: DMG stop!"
L["T104"] = "Phase 1: Die zaubernden Gesichtslosen schnell t\195\182ten. Tank schnappt sich den Dicken. Die kleinen Murlocs regelm\195\164\195\159ig bomben.\
Phase 2: DDs k\195\188mmern sich komplett um die 3 zaubernden Gesichtslosen. Tank kitet die Schattengestalten. Wichtig! nicht ber\195\188hren lassen und aus den schwarzen Pf\195\188tzen raus.\
Phase 3: Alle werden mega gross und machen mega DMG! sofort die Kracke anvisieren /tar ozumat und full dmg alles z\195\188nden was geht :)."
L["T105"] = "SPELL(75272): Kreaturen spawnen unter euch. Lauft einen Schritt zur Seite und versucht die Adds beim Tank abzuladen.\
SPELL(75539): Ihr werdet alle zum zum Boss geportet und m\195\188sst sofort die Ketten zerst\195\182ren. Danach rennen ihr sofort von ihm weg, da er SPELL(95326) castet und dieser euch sonst t\195\182tet."
L["T106"] = "Es werden drei Strahlen SPELL(75610) auf die Adds geworfen. Drei Gruppenmitglieder m\195\188ssen jeweils in einen Strahl, damit dieser nicht permanent auf dem Add ist.\
Der Strahl verursacht einen Debuff, welcher stackt. Sobald der Stack bei 80 ist, lauft ihr aus dem Strahl heraus. Lasst den Debuff austicken und geht wieder hinein.\
Tank und Melee kicken SPELL(75823), da dieser sonst einen Wipe verursachen kann."
L["T107"] = "Alle versammeln sich innerhalb des Kreises, sodass sie nicht auf dem Gitter stehen.\
Karsh ist quasi immun gegen alle Attacken. Um ihn angreifbar zu machen muss ihn der Tank in die Mitte ziehen.\
Dadurch erh\195\164llt Karsh einen Debuff SPELL(75846), welcher ihn angreifbar macht. Wenn der Debuff austickt kommen Adds.\
Ziel ist es den Debuff immer aufrecht zu erhalten. Das tut ihr indem ihr Karsh kurz bevor der Debuff austickt in die Mitte zieht."
L["T108"] = "(normal) Wenn die Adds nicht CC-ed werden k\195\182nnen, m\195\188ssen ihr sie zuerst t\195\182ten. Sonst verursachen sie SPELL(99830) und SPELL(93667), die stark an der Heilung nagen.\
Ansonsten tankt und spankt ihr normal. Wenn ihr das hintere Hund-Add t\195\182tet, bekommt der Boss einen Wutanfall.\
Furchtschutz und Erdsto\195\159totem helfen euch gegen das Fear. Schlie\195\159lich lauft ihr aus dem Feuer heraus."
L["T109"] = "Einer von euch sollte die drei Adds kiten, da sie SPELL(76189) verursachen. Der F\195\188rst selbst kann nicht viel. Nach einer Weile tauscht er den Platz mit einem Add.\
Der Tank muss dann schnell wieder den F\195\188rst herausziehen und die restlichen drei Adds werden von euch weiter gekitet."
L["T110"] = "Phase 1: Er castet rosa Splitter auf den Boden SPELL(86881). Ihr m\195\188sst sofort da HERAUS und (noch w\195\164hrend er castet) AOE darauf zaubern. Die Splitter, welche schl\195\188pfen, explodieren beim Sterben.\
Phase 2: Der Boss ist vergraben. Ihr m\195\188sst aus den Streifen auf dem Boden heraus, Adds tanken und bomben."
L["T111"] = "Phase 1: Er ist am Boden. Heiler und Tank m\195\188ssen in einer Linie sein damit geheilt werden kann. Wenn der Boss SPELL(92265) castet alle hinter die Steine!\
Phase 2: Er ist in der Luft. Einfach den schwarzen Kreisen ausweichen. Ausserdem in beiden Phasen aus dem roten Zeug raus!"
L["T112"] = "Allgemeines: Ozruk wird mit R\195\188cken zur Gruppe getankt.SPELL(95331) und SPELL(95334) reflektieren Zauber.\
SPELL(95338): Er haut vor sich auf dem Boden. 8 Meter um diese Einschlagsstelle bekommt man massiven Schaden. Melee und Tank soweit wie m\195\182glich hinter dem Boss!\
SPELL(95347): Kommt IMMER nach SPELL(92426). Ranged bleibt auf Maxrange. Melee und Tank laufen sofort mind. 15 meter/yards weg!"
L["T1122"] = "no tactics available yet"
L["T1123"] = "no tactics available yet"
L["T1128"] = "no tactics available yet"
L["T113"] = "SPELL(79351) (meistens auf Tank): Unterbrechen! Der macht viel DMG.\
SPELL(79340): Sie castet schwarze Kreise. Raus da und versuchen so dahinter zu stehen, dass die adds da reinlaufen und sterben.\
SPELL(79002): Sie wirft mit Felsen, kann man aber leicht auf dem Boden erkennen, einfach raus da.\
SPELL(92663): Fluch der erlittenen Schaden erh\195\182ht. Unbedingt entfluchen!"
L["T1133"] = "Tank f\195\188hre den Boss zur\195\188ck zur Gruppe um SPELL(161588) und SPELL(162066) zu vermeiden.\
Mitspieler, die mit SPELL(163447) belegt sind, m\195\188ssen in Bewegung bleiben, damit sie nicht von SPELL(162058) getroffen werden."
L["T1138"] = "Die Bosse m\195\188ssen zur gleichen Zeit sterben!! Raketenfunken kann nicht getankt, sondern muss von Fernk\195\164mpfern bearbeitet werden.\
Borka muss getankt und von Nahk\195\164mpfern attackiert werden.\
Tank: Wenn Borka sein Anst\195\188rmen vorbereitet, lenk den Boss so, dass er mit Raketenfunken kollidiert, was seinen Raketenbeschuss stoppt."
L["T1139"] = "Steht nicht auf den lilanen Runen wenn ihr den Boss angreift. Sie zabert SPELL(153240) auf beliebige Spieler. Geht einfach beiseite.\
Sie spawnt ein Add was auf Sie zul\195\164uft. Zerst\195\182rt das Add bevor es den Boss erreicht.\
Sobald sie SPELL(164686) zaubert, m\195\188sst ihr weg von den schwarzen und euch auf die WEI\195\159EN Runen begeben. Verlasst die Runen, wenn der Zauber vorr\195\188ber ist."
L["T114"] = "Es entstehen am Rand kleine Wirbelst\195\188rme, die einen Kreis bilden. Ab und zu ruft der Boss alle diese in die Mitte zu sich. Wer direkt dran steht bekommt Schaden und erh\195\164llt einen Stilleeffekt.\
Seit Patch 4.0.6. ist es nicht mehr m\195\182glich sich direkt in den Boss zu stellen. Daher wird folgende Taktik empfohlen.\
Wenn die St\195\188rme in die Mitte kommen geht man schnell nach aussen. Wartet dort und wenn die St\195\188rme wieder nach Aussen gehen, geht man wieder in die Mitte. "
L["T1140"] = "Wenn ihr einen Wirbel unter euren F\195\188\195\159en seht, geht beiseite! Vermeidet auch die lilanen Dreiecke.\
Sobald der Boss SPELL(153804) beschw\195\182rt, m\195\188sst ihr euch allerdings IN die Wirbel- und Dreieckszonen stellen!\
Bei 50% HP spawnen Adds. Zerst\195\182re sie und wiederhole was du vorher getan hast.\
Heiler seid gefasst darauf, dass die Adds mehr Schaden machen, wenn sie ~50% Leben haben."
L["T1147"] = "no tactics available yet"
L["T1148"] = "no tactics available yet"
L["T115"] = "Wirbelst\195\188rme: Der ganze Platz wird von kleinen Wirbelst\195\188rmen ausgef\195\188llt, die sich geringf\195\188gig bewegen. Ausweichen!\
Wind von Altairus: Der Boss l\195\164sst einen Wind entstehen der in eine Richtung fegt. (Sieht man besser wenn Kamera schr\195\164ger steht). DDs stehen so, dass sie mit dem Wind stehen. Tank umgekehrt.\
SPELL(88308): Der Boss dreht sich zu einem zuf\195\164lligen Spieler und spuckt ihn kegelf\195\182rmig an. Also nicht zu eng stehen sonst trifft das mehrere.\
Dadurch, dass die DDs mit dem Wind stehen erhalten sie SPELL(88282), der sie deutlich schneller casten/angreifen l\195\164sst. Achtet also darauf wie ihr steht."
L["T1153"] = "no tactics available yet"
L["T1154"] = "no tactics available yet"
L["T1155"] = "no tactics available yet"
L["T116"] = "SPELL(87618): Jeder bekommt diesen Debuff. Man ist 18 Sekunden lang gewurzelt. Muss dispellt werden!\
Statisches Dreieck: Asaad castet nach und Nach 3 Punkte auf dem Boden, die er miteinander verbindet. In das entstehende Dreieck m\195\188ssen alle rein, ansonsten sterbt ihr an SPELL(86930).\
SPELL(97496): Nach jeder Dreiecksphase castet er sofort einen Kettenblitzschlag. Versucht daher schnell sternf\195\182rmig auseinander zu gehen!"
L["T1160"] = "Tank den Boss am Rand von der Gruppe weg. Tank weiche aus, wenn er SPELL(154442) castet.\
Wenn der Boss SPELL(175988) zaubert, geht vom Omen weg.\
Nach dem 3. Omen beschw\195\182rt er SPELL(154469). Unterbrecht die Reihe, indem ihr das Skelett t\195\182tet (wie bei Sylvana Ghulen in End Time)."
L["T1161"] = "no tactics available yet"
L["T1162"] = "no tactics available yet"
L["T1163"] = "Phase 1: Haut den Boss auf 60% HP runter.\
Phase 2: Ignoriert den Boss. Positioniert euch so, dass der Boss euch nicht trifft. Versucht zusammen zu bleiben, damit der Tank es einfacher hat die Adds aufzusammeln.\
Damage Dealer: Benutzt SPELL(160702), um die gro\195\159e Kanone zu zerst\195\182ren. Steht nicht in SPELL(166570).\
Phase 3: Greift den Boss an und geht aus den gr\195\188nen Flecken raus."
L["T1168"] = "Geht aus SPELL(152801) und vermeidet den lilanen SPELL(153067)!\
Der Tank muss die Adds einsammeln. Sie k\195\182nnen mit Fl\195\164chenzaubern get\195\182tet werden.\
Wenn er SPELL(152979) beschw\195\182rt, t\195\182tet eure Geister und klickt sie an, damit ihr einen Buff bekommt."
L["T117"] = "Der Boss verteilt auf dem Boden bomben SPELL(91259). Von diesen muss ein Abstand eingehalten werden. Die Bomben k\195\182nnen auch inaktiv werden, dennoch gilt: Abstand! SPELL(91263)\
Manchmal packt der Boss einen Spieler und schleudert ihn gegen eine S\195\164ule. Einfach gegenheilen.\
SPELL(83445): Der Boss bleibt stehen und castet braune Streifen sternenartig von sich. Aus diesen einfach rausgehen.\
Generell gilt: Heiler und Tank m\195\188ssen immer eine Sichtlinie bilden, da der Boss hier stark hin und her gezogen werden muss, ist das nicht immer so einfach."
L["T118"] = "SPELL(81642): Der Boss schmettert regelm\195\164\195\159ig mit seinem Schwanz auf dem Boden. Raus dort!\
SPELL(81690): zuf\195\164lliger Spieler bekommt Debuff und kleine Krokos greifen diesen an. Nicht tankbar. Adds verursachen starken Debuff (Dot) und m\195\188ssen daher schnell get\195\182tet werden.\
Autsch: Ab und zu kommt Autsch und wirbelt ein bisschen rum. Melee und Tank einfach weg vom Wirbler (der nach diesem Kampf sofort erscheint)\
SPELL(81706): Ab 30% wird der Boss w\195\188tend und verursacht mehr DMG. Heldentum/Zeitkr\195\188mmung und Tank CDs z\195\188nden!"
L["T1185"] = "Sch\195\188tzt euch vor SPELL(153480), indem ihr hinter das Schild geht.\
Um sicherzugehen, dass euch der Boss nicht hinter dem Schild folgt, tankt ihn nicht zu nah und nicht zu weit weg."
L["T1186"] = "DD unterbrecht SPELL(154218) und SPELL(154235). \
Heiler vergesst nicht, dass SPELL(153477) f\195\188r Spieler au\195\159erhalb der Schutzzone gro\195\159en Schaden bringt.\
Tank sorg daf\195\188r, dass alle Adds getankt werden."
L["T119"] = "Phase 1: Boss wird getankt und es ist darauf zu achten sofort aus den Lichts\195\164ulen zu gehen. Es erscheint au\195\159erdem ein Ph\195\182nix.\
Ziel ist es den Ph\195\182nix schnell zu t\195\182ten, dann wird dieser zum Ei. Das Ei kann nicht zerst\195\182rt werden und erh\195\164llt immer mehr HP bis es voll ist und der Ph\195\182nix schl\195\188pft.\
W\195\164hrend der Ph\195\182nix ein Ei ist k\195\182nnt ihr also Barim attackieren. Bei 50% kommt ihr in Phase 2.\
Phase 2: Boss ist nicht angreifbar. Raus aus SPELL(88814) das den Boss umgibt. Ein dunkler Ph\195\182nix erscheint, der sofort get\195\182tet werden muss. Erreichen Seelensplitter den Ph\195\182nix haut dieser mehr rein.\
Sobald der dunkle Ph\195\182nix tot ist, geht es mit Phase 1 weiter."
L["T1195"] = "no tactics available yet"
L["T1196"] = "no tactics available yet"
L["T1197"] = "no tactics available yet"
L["T1202"] = "no tactics available yet"
L["T1203"] = "no tactics available yet"
L["T1207"] = "Verwendet Zauberunterbrechung oder CC um SPELL(168041) und SPELL(168105) zu unterbrechen.\
Heiler: Die Gruppe wird zuf\195\164lligen Schaden durch SPELL(168092) und SPELL(168040) erleiden."
L["T1208"] = "Der Boss beschw\195\182rt Feuer, dann Frost und schlie\195\159lich arkane Zauber. Ihr bringt Sie zum \195\132ndern der Spell-Typen indem ihr SPELL(168885) unterbrecht.\
Springt \195\188ber SPELL(166489) um Schaden zu vermeiden. Heiler achtet auf arkane Zauber, die im Team gro\195\159en Schaden anrichten."
L["T1209"] = "T\195\182tet 8 giftige Spinnlinge um den Boss angreifbar zu machen. Vermeidet SPELL(169275) aus. \
Gesch\195\182pfe, die SPELL(169218) erhalten werden mit der Zeit st\195\164rker.\
Heiler entzaubert SPELL(169376). Tank platziere die gifts\195\188chtigen Fahlen in den SPELL(169275), damit sie von ihm getroffen werden und so schneller sterben."
L["T1210"] = "Wenn der Boss SPELL(169613) beschw\195\182rt, tretet auf die Spr\195\182\195\159linge, damit sie nicht wachsen. Vernichtet schnell SPELL(169251) um Kirin Tor am Leben zu erhalten.\
T\195\182tet die Adds, damit ihr nicht \195\188berw\195\164ltigt werdet. Heilt die verb\195\188ndeten Mobs, damit sie weiterk\195\164mpfen.\
Tank: Sammle die Adds ein, die die ganze Zeit spawnen."
L["T1211"] = "no tactics available yet"
L["T1214"] = "F\195\188hrt SPELL(164294) weg von der Gruppe. Vernichtet die Aqu\195\164ussph\195\164re bevor sie Wei\195\159borke erreicht.\
SPELL(164538) heilt AoE-Schaden wenn ein Globule get\195\182tet wurde.\
Tank: Wende den Boss w\195\164hrend SPELL(164357) von der Gruppe ab. Sammle die Adds auf."
L["T1216"] = "K\195\188mmert euch um die Adds, sobald der Boss unangreifbar ist.\
DD steht nicht zu nah an den lodernden Schwindlern, damit ihr SPELL(154018) ausweichen k\195\182nnt. Unterbrecht SPELL(154221) oder t\195\182tet die Beschw\195\182rer, bevor sie die Beschw\195\182rung beenden k\195\182nnen.\
Tank greif die Teufelswache an, da sie schweren Nahkampfschaden machen."
L["T122"] = "Es ist ganz einfach. Bei 90% bekommt der Boss SPELL(84589). Dann nur noch Schaden auf die Adds machen.\
Ads: einfach Tanken und t\195\182ten. Dabei immer aus den gr\195\188nen und grauen Wirbeln auf dem Boden raus gehen. Mehr f\195\164llt mir hier nicht ein."
L["T1225"] = "Bei 75% Leben heilt er sich vollst\195\164ndig und bekommt eine von drei F\195\164higkeiten.\
Vermeidet es in der N\195\164he der anderen zu stehen, wenn SPELL(156921) explodiert. Unterbrecht SPELL(156854) und SPELL(156975) und entzaubert NICHT SPELL(156954)."
L["T1226"] = "Z\195\188ndet sofort Heldentum/Bloodlust und alle schadensverst\195\164rkenden Effekte, um den Boss so weit runter wie m\195\182glich zu bringen.\
Wenn er SPELL(166168) zaubert, verlasst die Plattform und aktiiert die Runenbeh\195\164lter (Tank kann mitmachen, da der Boss in der Phase nicht angreift).\
Vorsicht, denn zwei SPELL(154335) gehen im Raum herum. Vermeidet Schaden durch benutzen von Speed-Buffs."
L["T1227"] = "Tankt den boss von der Gruppe weg. Immer wenn der Boss jemanden verfolgt, unterbrecht ihn!\
T\195\182tet zuerst die zwei Kreaturen und passt auf die Feuerlinien auf, die diese erzeugen. Geht einfach beiseite.\
T\195\182tet den Boss wenn die Kreaturen tot sind und vermeidet Schaden durch die gr\195\188nen Flecken am Boden."
L["T1228"] = "Der Tank muss dem Anst\195\188rmen vom Boss ausweichen. Zieh den Boss immer wieder weg von den Giftflecken und fliegenden \195\132xten, denen generell JEDER ausweichen muss.\
Wenn m\195\182glich zaubert DoTs auf die fliegenden Kreaturen \195\188ber euch.\
Heiler passt auf, denn der Boss macht wesentlich mehr Schaden auf den Tank, wenn der Boss 70% Leben erreicht. Irgendwann steigt der Boss von seinem Mount ab. Benutzt dann unbedingt schadensreduzierende Cooldowns!"
L["T1229"] = "Tank: Tank den Boss ein wenig links oder rechts von der Mitte, so dass es klar sichtbar wird, wenn er beginnt SPELL(155031) zu zaubern. Sammel auch alle Adds ein, die erscheinen wenn der Boss 70% und 40% HP erreicht. Spare die schadensreduzierenden Cooldowns f\195\188r die letzte Phase auf (Wenn der Boss landet).\
Wenn der Boss Flammen spuckt, rennt auf die andere Seite der Br\195\188cke.\
Wenn Adds erscheinen, macht Fl\195\164chenschaden. Wenn der Boss landet positioniert sich die Gruppe (au\195\159er dem Tank) hinter dem Boss."
L["T1234"] = "Kampfrausch/Heldentum sofort z\195\188nden. Weicht dem Wirbelwind aus.\
Wenn der Boss die Plattform verl\195\164sst, tankt die Drachen und passt auf, da sie in einer Linie Feuer spucken.\
Ihr m\195\188sst in Bewegung bleiben, da sich die Drachen immer neu positionieren.\
Wenn die Adds aktiv sind, kommt mehr Schaden auf den Tank rein, passt darauf auf, Heiler!"
L["T1235"] = "Nok'gar kann nicht direkt ins Ziel genommen oder angegriffen werden solange er reitet. Bewegt euch schnell aus der Feuerlinie von SPELL(164632) oder SPELL(164648). \
Boss nicht angreiffen wenn er unter dem Effekt von SPELL(164426) ist.\
Tank f\195\188hre Nok'gar und Dreadfang schnell weg von den Pfeilsalven."
L["T1236"] = "Greift einen Vollstrecker erst an, wenn ihn SPELL(163689) nicht mehr besch\195\188tzt.\
Vermeide SPELL(163390), der Opfer festsetzt und sie so f\195\188r SPELL(163379) angreifbar macht.\
Verteilt euch sobald Neesa SPELL(163376) vorbereitet."
L["T1237"] = "Konzentriert euch aufs Schadenmachen, damit Oshir schnell SPELL(162424) beendet. Seid vorsichtig wenn Oshir SPELL(178124) beschw\195\182rt und achtet auf Adds.\
Fokussiert euch auf das Heilen der SPELL(162418)-Opfer."
L["T1238"] = "Skulloc greift mit heftigen Melee-Treffern zusammen mit dem Kapit\195\164n Koramar und dem ersten Maat Zoggosh in einem mechanisierten Turm an.\
Versteckt euch schnell hinter einer Abdeckung wenn Skulloc mit SPELL(168929) beginnt und k\195\164mpft euch in den Salvenpausen zu ihm vor.\
Verweilt w\195\164hrend SPELL(168929) nicht in der N\195\164he der Schiffsr\195\188ckseite, sonst werden sie euch verbrennen."
L["T124"] = "Blaue Flecken, SPELL(75117),  auf dem Boden: rauslaufen!\
SPELL(74938): Ist dieses Aktiv sollte die Gruppe sich aufteilen. Ein Teil geht links und der andere rechts zum Hebel. Je einer lenkt die Schlangen ab und der andere macht den Hebel.\
Danach wird sich oben gesammelt, schnell die Schlangen get\195\182tet  (machen fiesen Giftdebuff, nicht dispellbar) und dann der Boss, der SPELL(75322) zaubert, unterbrochen."
L["T125"] = "Phase 1: normaler Kampf. Aus dem braunen-grauen Kreisen aus dem Boden raus.\
Phase 2: Boss ist vergraben. adds kommen. Tanken und t\195\182ten das wars. "
L["T126"] = "SPELL(76184): schwarze Kreise auf dem Boden, die nicht mehr verschwinden: sofort raus!\
SPELL(75623): AOE. Einfach gegenheilen."
L["T1262"] = "no tactics available yet"
L["T127"] = "SPELL(74136): \195\164hnlich wie PDC-Eric. Einfach abwenden/umdrehen, wenn sie das castet.\
Split: Sie teilt sich bei 66% in 3 und bei 33% in 2 Kopien ihrer selbst. Jede dieser Kopie hat eine Eigenschaft. T\195\182tet eine Kopie. Die Eigenschaft dieser verschwindet von dem Boss aber verst\195\164rkt die anderen."
L["T128"] = "Boss wird normal getankt. DDs gehen immer SOFORT auf die kleinen gr\195\188nen Knospen die er auf den Boden plaziert.\
Wenn eine Spore erscheint, muss der Tank diese unverz\195\188glich an sich binden und t\195\182ten, sodass der Boss danach in SPELL(75701) steht, der Tank aber nicht.\
Dieser Kampf kann sehr lange dauern, wenn der Boss nicht richtig in die Sporen gestellt wird. Er heilt sich sonst n\195\164mlich deutlich hoch!"
L["T129"] = "Setesh ist nicht tankbar. Der Tank sammelt alle adds ein und kitet sie so gut wie m\195\182glich. Achtung! Boss macht kleine Kreise und grosse Schattenkreise, raus da!\
Setesh l\195\164uft zu einer Ecke und l\195\164sst ein Portal erscheinen. Dieses Muss extrem schnell genuked werden, damit maximal 1 Add und 2 W\195\188rmer austreten. Der Tank sammelt diese ein.\
Die Gruppe macht konsequent Schaden auf den Boss. Achtung er castet kleine AE Kreise zwischendurch, da sofort raus!"
L["T1291"] = "no tactics available yet"
L["T130"] = "SPELL(80352): Unbedingt unterbrechen!\
SPELL(87653): Er visiert einen Spieler an. Unter diesem wird der Boden dunkel. Sofort rauslaufen, da der Boss gleich dort hin springt und Aua macht!\
Ab und zu rennt er in die Mitte und macht einen AE, dann gegenheilen. Au\195\159erdem sind \195\188berall kleine Feuertornados, ausweichen!!"
L["T131"] = "SPELL(74670): Er visiert einen Spieler an und chargt ein paar Sekunden sp\195\164ter an. Weglaufen ist hier die Devise.\
SPELL(74634): Er verursacht direkt vor ihm extremen Schaden. Kurze Reichweite. Melee und Tank gehen hinter den Boss.\
SPELL(74846): Debuff auf dem Tank, der viel Schaden verursacht (DOT). Geht nur weg wenn von dem Tank 90% oder mehr geheilt wurden. Ihr m\195\188sst UNBEDINGT vollheilen!\
Trogs: Unter den Trogs, die erscheinen, ist ein Kranker. Ein ranged DD sollte ihn herausziehen und abseits t\195\182ten.\
Der kranke Trogg darf nicht in der N\195\164he eines Spielers, des Bosses oder anderer NPC sterben, sonst kommt die SPELL(90169)."
L["T132"] = "SPELL(74984): Throngus l\195\164sst kleine AE-Kreise entstehen, sie sehen aus wie Weihe. Dort d\195\188rft ihr nicht stehen!\
Waffenwahl: Zwei Schwerter -> Der Tank bekommt einen Magiedebuff, welcher viel Schaden verursacht. Dieser MUSS gedispellt werden.\
Waffenwahl: SPELL(75007) -> Der Tank muss den Boss kiten. Thronugs wirkt SPELL(90756). Der betroffene Spieler muss leicht geheilt werden.\
Waffenwahl: Schild -> ALLE m\195\188ssen hinter den Boss. Geht am besten direkt an in heran, denn er dreht sich in dieser Phase gern und so k\195\182nnt ihr schnell hinterher."
L["T133"] = "Phase 1: Er beschw\195\182rt Feuerelementare SPELL(75218), die einen verfolgen. Der Verfolgte rennt weg und alle anderen DDs schwenken um auf das Add!\
Phase 2: Ist wie die Phase 1. Dar\195\188berhinaus muss der Tank aus dem Feuerfleck von dem Drachen gehen und die Range DDs sollten Max Range zum dem Boss einhalten. Denn die SPELL(90950) verursachen mehr Schaden, je n\195\164her man daran steht."
L["T134"] = "SPELL(79466): Er macht einen Kreis auf dem Boden aus dem ihr einfach hinausgehen m\195\188sst.\
SPELL(75694): Er macht wieder einen Kreis auf dem Boden, castet aber diesmal lange. Alle m\195\188ssen nun schnell IN den Kreis, sonst bekommt ihr Schaden! Danach verteilt ihr euch wieder.\
Adds: Nach jedem Schattenorkan kommen 2 Ads. Teilt euch auf, denn diese adds m\195\188ssen so schnell wie m\195\182glich down gehen. Sonst heilt sich der Boss wieder voll und es kommen kleine Ads."
L["T139"] = "Zwei Camps mit jeweils gleicher Spieleranzahl bilden, jeweils mit einem Tank. Jeder Heal und DD steht genau AUF/IN dem Tank seines Camps.\
Tank: Abspotten immer nach SPELL(88942) (ca. alle 15 Sek).\
Heals: DOTs (SPELL(88954)) sofort entfernen\
Zwei Zwischen-Phasen mit SPELL(88972): Verteilen und nicht im gelben Feuer stehen! Danach zur\195\188ck zu deinem dir zugewiesenem Tank!"
L["T140"] = "Spread out during this fight but leave an empty space for the raid to collapse on during the eye phase. Be ready to move if the boss channels SPELL(97028) on you. The tanks should always face Occu'thar away from the raid, so that no one other than himself is ever damaged by SPELL(96913). When the Eyes of Occu'thar spawn, everyone needs to group up and AOE down the eyes within 10 seconds or they will explode and damage the whole raid. "
L["T1426"] = "no tactic "
L["T1467"] = "Ihr m\195\188sst oft unterbrechen also stimmt euch ab damit ihr keine Spells verbratet.\
Unterbrecht SPELL(191941) um dann SPELL(191999) unterbrechen zu k\195\182nnen.\
Wenn SPELL(202740) oder SPELL(191823)  nicht unterbrochen wird gibt es viel Fl\195\164chenschaden.\
\
"
L["T1468"] = "Lauft auf die orangenen Flecken wenn n\195\182tig, damit die gr\195\188nen Blubs euch nicht ber\195\188hren.\
Bei Kontakt mit den Lavafl\195\164chen explodieren diese.\
Vermeidet die Kreise auf dem Boden.\
Die Adds k\195\182nnen nicht getankt werden und sollten schnell sterben.\
"
L["T1469"] = "Glazer zaubert SPELL(214893), was von der Wand abprallt zum n\195\164chsten Spieler.\
Benutzt die Spiegel, um den blauen Strahl zum Boss zur\195\188ck zu reflektieren.\
\
"
L["T1470"] = "Vermeidet SPELL(197422) Geister, die unsichtbar sind solange nicht SPELL(197941) gewirkt wird.\
Wenn ihr  SPELL(197941) aufsammelt, benutzt es um SPELL(197422) zu entzaubern.\
Benutzt SPELL(197941) um den Boss zu finden, wenn sie verschwindet.\
"
L["T1479"] = "T\195\182tet alle Gegner um den Boss herum bevor ihr loslegt.\
Entfernt euch von der Gruppe wenn ihr anvisiert werdet von SPELL(191855).\
Stellt euch hinter ihn, wenn er SPELL(192050) zaubert.\
T\195\182tet die Adds und unterbrecht SPELL(191848).\
"
L["T1480"] = "T\195\182tet die Adds schnell und bleibt hinter dem Boss.\
Positioniert einen Gegner zwischen euch und dem roten Pfeil wenn ihr von  SPELL(191975) anvisiert werdet."
L["T1485"] = "Steht hinter dem Boss!\
Weg von  SPELL(193235) wenn er es auf einen Spieler wirft.\
Vermeidet den blauen Blitz der auf dem Boden erscheint nach SPELL(191284).\
\
"
L["T1486"] = "Weg von der Gruppe wenn ihr den debuff SPELL(192048) habt.\
Weg von den gelben Wirbeln die durch SPELL(192307) erscheinen.\
Bleibt in der blauen Blase wenn SPELL(192305) gezaubert wird.\
Tank positioniert den Boss so, dass nur ein NPC ihn kanalisiert.\
"
L["T1487"] = "Nahk\195\164mpfer dicht zusammen stehen. Heiler und Fernk\195\164mpfer 12 Meter weit weg.\
Wenn ihr anvisiert werdet durch SPELL(196838), rennt weg."
L["T1488"] = "Klickt auf das Schild, dass er fallen l\195\164sst um SPELL(193826) zu \195\188berleben.\
Weg von der Gruppe wenn er SPELL(193659) zaubert."
L["T1489"] = "Rennt weg, wenn er SPELL(198263) zaubert.\
Wenn ihr SPELL(197961) habt, rennt zu der Rune, die die gleiche Farbe hat.\
Raus aus den Kreisen am Boden\
Weg von den gl\195\188henden Kugeln die nach SPELL(198077) erscheinen.\
"
L["T1490"] = "Wenn sie beginnt SPELL(19359) zu wirken, lauft auf den H\195\188gel.\
Wenn sie  SPELL(193611) zaubert geht runter von den H\195\188geln.\
Weg von der Gruppe wenn sie  SPELL(193717) zaubert.\
"
L["T1491"] = "Raus aus den Kreisen am Boden.\
Wenn ihr einen Blasenzauber erhaltet geht auf die AoE Fl\195\164chen auf dem Boden um die Blase zu entfernen.\
Heiler nah am Boss stehen um SPELL(193152) zu vermeiden. DDs weg wenn er Shatter castet."
L["T1492"] = "Raus aus den Kreisen am Boden SPELL(192706).\
Nicht vor dem Boss stehen und niemals nah beieinander.\
Fl\195\164chenheilung wenn er SPELL(197165) macht und entzaubert l SPELL(192706) auf der Gruppe.\
Tank immer am Boss bleiben sonst gibt es AoE Schaden."
L["T1497"] = "T1497 Ivanyr"
L["T1498"] = "T1498 Corstalix"
L["T1499"] = "T1499 general xaxal"
L["T1500"] = "T1500 Nal tira"
L["T1501"] = "T1501 Advisor "
L["T1502"] = "Raus aus den Kreisen die unter ihm entstehen wenn er SPELL(193364) wirkt.\
Raus aus den lila Kugeln SPELL(193460).\
Wenn das Mana vom Boss voll ist TANK benutze CDs, HEILER sch\195\182n hochheilen."
L["T1512"] = "Unterbrecht SPELL(194266). Sprecht euch ab um eine Rotation zu machen.\
T\195\182tet alles was erscheint.\
Spieler betroffen von SPELL(194325) brauchen starke Heilung."
L["T1518"] = "Weg von anderen Spielern wenn ihr SPELL(194960) erhaltet.\
Nie dicht beieinander stehen (auch nicht Nahk\195\164mpfer)\
Tank und Heiler: CDs benutzten wenn er  SPELL(194956) zaubert."
L["T154"] = ""
L["T155"] = ""
L["T156"] = ""
L["T157"] = ""
L["T158"] = "Taktik f\195\188r Rat"
L["T1653"] = "Wenn ihr von SPELL(197478) betroffen seid, sammelt euch damit weniger Zeug spawnt.\
Unterbrecht SPELL(200248) und heilt extrem wenn er SPELL(197429) zaubert. Tankt die Adds!"
L["T1654"] = "Tankt den Boss weg von der Gruppe. Niemals vor dem Boss stehen.\
"
L["T1655"] = "Bleibt hinter dem Boss. Wenn er SPELL(204611) zaubert nehmt CDs (heal und tank)."
L["T1656"] = "Wenn der Boss SPELL(199345) zaubert rennt auf ihn zu sonst gibts geschl\195\188pfte Eier. (Mache Eier!).\
Daher versucht ihn gleich in der Mitte des Raumes zu tanken."
L["T1657"] = "Bleibt weg von Spielern die von SPELL(200359) betroffen sind.\
Rennt zu einem Spieler wenn ihr vom Alptraum betroffen seid.\
\
"
L["T1662"] = "T\195\182tet die Adds wenn sie kommen, T\195\182tet sie bevor der Boss SPELL(188114) zaubert.\
Tankt den Boss WEG von der Gruppe oder es gibt AoE Schaden von SPELL(188169).\
Zieht Kampfrausch / Heldentum gleich am Anfang."
L["T1663"] = "Wenn Helya SPELL(227233) wirkt, rennt weg von ihrem Gesicht.\
Killt die Adds schnell und heilt Spieler betroffen von SPELL(197264) schnell hoch.\
"
L["T1664"] = "Stellt euch zwischen den Boss und seinem Ziel, wenn er SPELL(224188) macht, nur wenn das Ziel bereits seinen Debuff hat.\
Wenn er SPELL(198073) wirkt, heilt die Gruppe.\
Der Tank braucht sehr viel Heilung bei SPELL(198245)."
L["T1665"] = "When Ularogg uses SPELL(198496), be ready to use heavy healing on the Tank.\
The longer Ularogg is in the Bellowing Idol, the more damage over time he will increase to your party.\
"
L["T1667"] = "T1667 ursoc"
L["T167"] = ""
L["T1672"] = "Bewegt euch kreisf\195\182rmig an der Kante wenn Dantalionax SPELL(199567) wirkt. T\195\182tet die Adds schnell und heilt wenn SPELL(198635) gezaubert wird besonders gut!"
L["T1673"] = "Einer muss immer nah am boss sein sonst nimmt die Gruppe Schaden durch SPELL(198963).\
Raus aus den gr\195\188nen Kreisen und Gaswolken am Boden..\
Wenn der Boss SPELL(199176) wirkt, heilt den Tank hoch und lauft weg!."
L["T1686"] = "Raus aus SPELL(201121) wenn ihr den Wirbel am Boden seht.\
Killt die Tentakeln wenn sie spawnen.\
Tank und Heiler nehmt CDs wenn er SPELL(201148) zaubert."
L["T1687"] = "Deckung nehmen hinter den Kristallen um Schaden durch die Magmawelle zu verhindern.\
T\195\182tet die Adds schnell weil sie viel AoE Schaden machen!"
L["T1688"] = "Entsch\195\164ft die Eichh\195\182rnchen und t\195\182tet die H\195\188hner.\
Vermeidet die H\195\188hner-Raketen."
L["T169"] = ""
L["T1693"] = "Bleibt hinter dem Boss um seine Kotze zu vermeiden. Killt die Adds die er ausspuckt.\
Bewegt den Boss zu einem gekilltem Add, damit er es isst. (W\195\188rg)\
"
L["T1694"] = "Drache also weg vom Schwanz und Kopf. Entfernt SPELL(201379) vom Tank.\
Wenn er SPELL(201960) zaubert, schnell die Treppen hochrennen. Die Gruppe nimmt trotzdem Schaden also hochheilen!"
L["T1695"] = "Wenn ihr  SPELL(200905) erhaltet hat der n\195\164chset Zauber 10 Sekunden Cooldown. Nehmt also einen unwichtigen Zauber daf\195\188r.\
Unterbrecht SPELL(200905) und t\195\182tet die Adds so schnell wie m\195\182glich.\
Tank: Wenn der Boss bei 70% und 40% schnell Aggro aufbauen bei den Gegnern."
L["T1696"] = "Wenn er DURCHBOHREN wirkt, rennt weg. Killt die Adds die nach SPELL(201863) erscheinen.\
Tank steh nicht vor dem Boss beim DURCHBOHREN.\
Heiler nutzt CDs weil SPELL(202217) die Heilung stark reduziert."
L["T1697"] = "Schaut die Spinnen an, wenn sie euch anvisieren, damit sie nicht n\195\164her kommen.\
."
L["T170"] = ""
L["T1702"] = "DD: CDs erst beim Prinzessin Buff nehmen und loskn\195\188ppeln.\
Heiler: Es wird Gruppenschaden geben f\195\188r nicht-infizierte Spieler.\
Tank: Beweg sie raus aus den Blutflecken wenn sie Blutschwarm castet."
L["T1703"] = "T1703 Nythendra"
L["T1704"] = "T1704 Dragons of Nightmare"
L["T1706"] = "T1706 Skroypon"
L["T171"] = ""
L["T1711"] = "Weg von Spielern die von SPELL(203641) betroffen sind. Wenn du betroffen bist, renn weg.\
Befreit Spieler auf den Plattformen, die der Boss t\195\182ten will.."
L["T1713"] = "T1713 Krosus"
L["T1718"] = "T1718 Patrol Captain"
L["T1719"] = "T1719 Flamewrath"
L["T172"] = ""
L["T1720"] = "T1720 Adviser melandrus"
L["T1725"] = "T1725 Cronomatic anomaly"
L["T1726"] = "T1726 Xavius"
L["T173"] = ""
L["T1731"] = "T1731 Trillax"
L["T1732"] = "T1732 Star augur"
L["T1737"] = "T1737 Guldan"
L["T1738"] = "T1738 Il gynoth"
L["T174"] = ""
L["T1743"] = "T1743 Grand Master Elis"
L["T1744"] = "T1744 Renferal"
L["T175"] = "Es werden Gr\195\188ne Flecken auf dem Boden entstehen, welche sich zu Linien verbinden. Raus dort! Ab und an wird Venoxis einen SPELL(96477) zwischen zwei Spielern erzeugen, welcher konstanten Schaden auf diesen Spielern verursacht.\
Die Verbindung wird gel\195\182st indem die betroffenen Spieler weit auseinander laufen. Lauft dabei nicht in die N\195\164he der anderen Spieler, denn die 2 Spieler explodieren dann und verursachen Schaden im Umkreis.\
Sobald der Boss eine Schlange ist, m\195\188sst ihr sofort aus seinen SPELL(96753) raus gehen. Lauft sofort durch den Boss durch oder lauft aus seiner Sichtweite, sobald er SPELL(96753) zaubert.\
Gegen Ende l\195\164uft Venoxis zu seinem Alter und zaubert SPELL(97099). Vermeidet die Strahlen die einen zuf\195\164lligen Spieler verfolgen. Danach ist er f\195\188r ~5 Sekunden bewegungsunf\195\164hig und beginnt danach wieder von vorn."
L["T1750"] = "T1750 Cenarius"
L["T1751"] = "T1751 Speedblade Aluriel"
L["T176"] = "In der Arena seht ihr mehrere Geister. Wenn ein Gruppenmitglied stirbt, wird es durch den Geist wiederbelebt und erh\195\164llt einen Buff. Man kann den Tot durch Mandokirs TODO nicht entgehen.\
Mandokir wird auf seinem Raptor sitzen. Immer wenn er absteigt muss der Raptor sofort get\195\182tet werden, da er sonst alle Geister t\195\182tet. Mandokir zaubert ausserdem TODO2 direkt vor ihm. Verschwindet daher von seiner Front.\
Wenn Mandokir 20% HP erreicht, geht er Enrage. Sein Cooldown von TODO senkt sich und er macht 100% mehr Schaden! Benutzt HT/KR/ZK um ihn so schnell wie m\195\182gich zu t\195\182ten."
L["T1761"] = "T1761 high botanist"
L["T1762"] = "T1762 Tichondrius"
L["T177"] = "Wenn Gri'lek jemanden verfolgt: weglaufen. Wurzeln dispellen. Au\195\159erdem von dampfenden Wirbel auf dem Boden fernhalten."
L["T178"] = "Seinen \226\128\158Zorn\226\128\156 unbedingt unterbrechen/reflektieren.\
Er bet\195\164ubt vier Spieler mit SPELL(96670) & SPELL(96757). Der eine nicht bet\195\164ubte Spieler MUSS die 4 Adds (3.000 HP) sofort killen, ansonsten sterben diese Spieler durch SPELL(96758)."
L["T179"] = "Renataki zaubert stetig SPELL(96640) und SPELL(96645), eine spezielle Art von SPELL(51723). Rennt aus den Wirbelpfaden bis er fertig ist mit Angreifen und heilt den angegriffenen Spieler."
L["T180"] = "Wushoolay zaubert mit Blitzattacken vor denen ihr wegrennen m\195\188sst. Spieler mit SPELL(96699) m\195\188ssen wegrennen. Vermeidet SPELL(96713) und SPELL(96711)."
L["T181"] = "Bringt Kilnara nicht unter 50% bis ihr alle Panthergruppen im Raum, nach einander, get\195\182tet habt.\
Sie beschw\195\182rt manchmal eine lila Wellenwand SPELL(96460) welche kegelf\195\182rmig vor ihr schaden Macht bis es unterbrochen ist.\
Sie zaubert ausserdem SPELL(96435), was Schattenschaden an nahestehenden Spielern anrichtet. Sollte unterbrochen werden!\
Bei 50% HP wird sie zum Panther und ruft alle restlichen Panther im Raum um Ihr zu helfen. Bei ~25% wird sie kurz verschwinden und wieder auftauchen."
L["T184"] = "Zanzil greift sein Ziel mit Schattenblitzen an. Unterbrechen! Er wird ausserdem SPELL(96914) zaubern, was im Prinzip eine Feuerspur ist, die auf dem Tank und ein paar Meter hinter ihm erscheint. Raus da!\
W\195\164hrend des ganzen Kampfes wird er zuf\195\164llig eine der folgenden drei F\195\164higkeiten nutzen. Wird dem entgegen indem ihr die Kessel in der N\195\164he benutzt:\
SPELL(96319). Zanzil beschw\195\182rt einen Schwarm von non-elite Zombies aus einen der Leichenhaufen. Der Tank muss einen roten Buff aus dem Kessel holen und dann in die Mobs reinspringen. DPS auf Zombies!\
SPELL(96338). Zanzil f\195\188llt alles mit gr\195\188nem Gas, welches schnell hoch tickt. Holt euch den Buff vom gr\195\188nem Kessel SPELL(96328). \
SPELL(96316). Zanzil belebt einen Berserker.Holt euch den Frost Buff vom Frostkessel und t\195\182tet den Berserker sehr schnell bevor Zanzil den n\195\164chsten Zaber wirkt."
L["T185"] = "Geht in die gruenen Blasen, durch die ihr weniger Schaden nehmt und haltet den Boss draussen. Der Tank kann draussen bleiben solange bis Jin'Do seine Spezialattacke zaubert.\
Phase 2 beginnt nach einiger Zeit und Jin'Do ist anders besch\195\164ftigt. Ihr m\195\188sst die Gurubashi Berserker spotten und ihn dazu bringen die Schilde um Hakkars Ketten zu zerst\195\182ren, indem die Gruppe (ohne Tank) auf einer Kette steht.\
Wenn ein Schild zerst\195\182rt ist, t\195\182tet die Kette, aber passt auch auf die Geister auf. Spottet einen n\195\164chsten Berserker und es geht von vorn los.\
Zieht die kleinen Adds zu den Ketten wo der Tank sie halten kann und t\195\182tet sie mit AoE. Vermeidet die schwarzen/lila Dinger, die auf euch zu kommen."
L["T186"] = "Neu ist, dass der Amani Kidnapper, ein Adlerder regelm\195\164\195\159ig ein Gruppenmitglied packt und wirkt SPELL(97318). Die gesamte Gruppe einschliesslich der gepackte Spieler m\195\188ssen den Adler angreifen.\
Akil'zon zaubert  SPELL(97299) auf den Tank und SPELL(97298) auf zuf\195\164llige Spieler, welches stackt aber nicht entfernt werden kann.\
Sobald SPELL(97300) gezaubert wird, geht der getroffene Spieler in die Luft und beschw\195\182rt eine Art Gewitter. Die gesamte Gruppe sollte unter diesen Spieler gehen um Schaden zu vermeiden."
L["T187"] = "In der ersten Phase ( Troll Phase ) zaubert Nalorakk SPELL(42384) auf den Tank.\
Er zaubert ausserdem SPELL(42402) auf den Spieler, der am weitesten von ihm entfernt ist. Es hilft wenn abwechselnd ein Heiler und ein Fern-DD am weitesten entfernt stehen und so das Anst\195\188rmen steuern.\
In Phase 2 verwandelt er sich in einen B\195\164ren. Er zaubert SPELL(97811) auf den Tank und SPELL(42398) auf die Gruppe."
L["T188"] = "Jan'alai zaubert SPELL(97488) in Richtung eines zuf\195\164lligen Spieler. Man kann leicht ausweichen, steht also nicht zu nah an der Hitbox.\
regelm\195\164ssig zaubert Jan'alai  SPELL(42630). Diese orangenen Bomben erscheinen in der Mitte und explodieren wenige Sekunden sp\195\164ter. In die entstandenen L\195\188cken kann man reingehen und weiter k\195\164mpfen.\
Jan'alai wird SPELL(45340), NPCS rennen zu den Eiern auf beiden Seiten und lassen diese schl\195\188fen, bis sie daran gehindert werden (t\195\182ten).\
Die geschl\195\188pften Amani Dragonhawk Hatchling zaubern SPELL(43299) und sollten schnell mit AoE Zaubern get\195\182tet werden. Lasst ein paar rauskommen damit nicht alle auf einmal erscheinen"
L["T189"] = "In der Trollphase l\195\164sst Halazzi SPELL(97499) fallen, was den Boss buffed. T\195\182tet das Totem oder zieht ihn weit davon weg, so dass er nicht weiter geheilt wird. Er zaubert ausserdem SPELL(43139).\
Bei 66% und 33% beschw\195\182rt Halazzi  Geist des Lynx und heilt sich auf 100%. Das Add hat random Aggro, also t\195\182tet es so schnell wie nur irgendm\195\182glich.\
Es zaubert SPELL(43243) und SPELL(43290) was besonders bei Stoffies weh tut. DDs m\195\188ssen das verdorbenen Blitz Totem t\195\182ten, da es ansonsten extremen Schaden anrichtet\
Sobald der Geist tot ist, wird wieder Schaden auf Halazzi gemacht."
L["T190"] = "Er wird unterst\195\188tzt von 2 zuf\195\164lligen Adds. Diese k\195\182nnen CCed und damit ignoriert werden.\
Er wird wechseln zwischen SPELL(43882) auf der Gruppe und SPELL(43501). W\195\164hrend das Feuer weiter l\195\164uft bekommt ihr Stapel von SPELL(44131), die es euch erschweren den Boss zu t\195\182ten.\
Benutzt euren gesunden Menschenverstand wenn ihr mit alle den tempor\195\164ren F\195\164higkeiten umgeht. Unterbrecht Heilung, rennt aus den Spalten raus und entfernt Debuffs.\
Er zaubert zuf\195\164llig Seelenentzug auf ein zuf\195\164lliges Mitglied, welches ihm die Kr\195\164fte des Spielers verleiht ( h\195\164ngt ab von der Klasse )."
L["T191"] = "Er verwandelt sich mehrere Male, was jedesmal einen Aggro-Reset bewirkt. Er ist Spott-Immun aber Dots und Debuffs bleiben auf ihm.\
In der ersten Phase wirkt Daakara SPELL(15576) und SPELL(97639), daher muss der Spieler geheilt werden um den Debuff zu entfernen. Nahk\195\164mpfer passt auf den Wirbelwind auf! Bei 80% und 40% verwandelt Daakara sich in 2 der folgenden 4 M\195\182glichkeiten.\
In SPELL(42606), Daakara erschafft ein SPELL(43983) und zaubert SPELL(43112) vor dem Spieler ausweichen m\195\188ssen. Nicht in die St\195\188rme laufen! T\195\182tet unbedingt SPELL(97930) um den massiven Schaden zu verhinden.\
In SPELL(42594), Daakara zaubert SPELL(43095) dem ein h\195\164sslicher SPELL(42402) folgen kann, genau wie SPELL(43456) auf den Tank. Entfernt den Stun und benutzt Cooldowns auf den Tank.\
In SPELL(42608), Daakara erzeugt eine Feuers\195\164ule und zaubert regelm\195\164\195\159ig SPELL(43208) und SPELL(97855). Alle Spieler m\195\188ssen alarmiert bleiben und das Feuer vermeiden.\
In SPELL(42607), Daakara ruft 2 Adds die einen Spieler schnell zerlegen, wenn sie nicht selbst schnell get\195\182tet werden. Sie benutzen SPELL(97673) und SPELL(97672)."
L["T192"] = "Hier k\195\182nnt ihr selbst Taktiken eintragen."
L["T193"] = "Hier k\195\182nnt ihr selbst Taktiken eintragen."
L["T194"] = "Hier k\195\182nnt ihr selbst Taktiken eintragen."
L["T195"] = "Hier k\195\182nnt ihr selbst Taktiken eintragen."
L["T196"] = "Hier k\195\182nnt ihr selbst Taktiken eintragen."
L["T197"] = "Hier k\195\182nnt ihr selbst Taktiken eintragen."
L["T198"] = "Hier k\195\182nnt ihr selbst Taktiken eintragen."
L["T283"] = "Die Gruppe sollte sich im Halbkreis um ihr aufstellen. Ansonsten den \195\188blichen Sachen ausweichen und sie etwas unterbrechen. Nicht wirklich schwer an der Stelle."
L["T284"] = "keine Taktik derzeit verf\195\188gbar"
L["T285"] = "Weicht SPELL(101339) aus ansonsten werdet ihr zum Eisblock f\195\188r 5 Sekunden.\
Unterbrecht SPELL(101810), eine AoE Attacke. Wichtig: Sie wird eine Bombe in Spielern\195\164he werfen.\
Einer muss die Bombe ausl\195\182sen, indem er einfach drauf rennt. Wenn das nicht nach kurzer Zeit geschieht gibts m\195\164chtig AoE Schaden."
L["T286"] = "keine Taktik derzeit verf\195\188gbar"
L["T289"] = "Tankt den Boss weg von der Gruppe wegen SPELL(102569).\
Im Prinzip wird der Boss AoE Schadensfl\195\164chen erscheinen lassen. Immer raus da. Sind es zuviele Fl\195\164chen benutzt SPELL(101591) indem ihr in der Mitte auf die Sanduhr klickt.\
Dies wird alle Cooldowns entfernen und Mana etc. regenieren."
L["T290"] = "Raus aus den gr\195\188nen Kreisen! Wenn du unsichtbar wirst, halte dich fern von den Augen f\195\188r 40 Sekunden. Der Boss wird w\195\188tend bei 20%."
L["T291"] = "Unterbrecht SPELL(103241) innerhalb von 8 Sekunden. Holt die Spieler aus der Gedankenkontrolle, in dem ihr die Hand t\195\182tet. T\195\182tet den Magi und unterbrecht und bet\195\164ubt wo es nur geht."
L["T292"] = "Bleibt in Bewegung w\195\164hrend SPELL(103888) um Feuer zu vermeiden. Wenn Tyrande bet\195\164ubt wird, t\195\182tet den Schreckenslord um sie zu befreien. Wenn ihr im Lichtstrahl steht, k\195\182nnt ihr sehr gro\195\159en Schaden an den Verdammniswachen ausrichten. \
HT/KR/ZK und andere Cooldowns in Phase 2 benutzen um Mannoroth schnell zu t\195\182ten!"
L["T302"] = "keine Taktik derzeit verf\195\188gbar"
L["T303"] = "keine Taktik derzeit verf\195\188gbar"
L["T311"] = "W\195\164hrend der Kristallphase m\195\188ssen die Tanks abwechselnd abspotten und einige DDs m\195\188ssen den Nahschaden abfangen, der entsteht, wenn die Kristalle beschworen werden. Je n\195\164her du an dem Kristall bist, desto weniger Schaden macht er.\
W\195\164hrend der dunklen Blut-Phase wirst du zum Boss gezogen und musst schnell hinter einen der Pfeiler gehen um nicht im schwarzen Blut zu stehen.\
Der Boss wird ab 20% w\195\188tend und macht dann mehr Schaden auf den Tank."
L["T317"] = "Ersetze diesen Text mit deiner eigenen Taktik f\195\188r "
L["T318"] = "Ersetze diesen Text mit deiner eigenen Taktik f\195\188r "
L["T322"] = "Vermeidet die Eiskreise auf dem Boden. Wenn Thrall gefriert dann holt ihn sofort wieder raus. Der Boss spammt SPELL(102593) auf den Tank, was aber unterbrechbar ist! Bei 30% macht er SPELL(103962) auf alle! Boss so schnell wie m\195\182glich t\195\182ten."
L["T323"] = "SPELL(101401): Sylvanas wird eine gr\195\188ne Fl\195\164che auf dem Boden erscheinen lassen. Raus da!\
Sie wird alle zu sich ranziehe und Ghule beschw\195\182ren die einen Ring um sie bilden. Die Ghule sind verbunden, t\195\182tet SCHNELL genau EINEN Ghul um die Verbindung zu brechen."
L["T324"] = "Ersetze diesen Text mit deiner eigenen Taktik f\195\188r "
L["T325"] = "Ersetze diesen Text mit deiner eigenen Taktik f\195\188r "
L["T331"] = "Ersetze diesen Text mit deiner eigenen Taktik f\195\188r "
L["T332"] = "Ersetze diesen Text mit deiner eigenen Taktik f\195\188r "
L["T333"] = "Ersetze diesen Text mit deiner eigenen Taktik f\195\188r "
L["T335"] = "Boss wechselt zwischen 2 Phasen:\
Phase 1: Verteilt euch und macht Schaden.\
Phase 2: T\195\182tet zuerst das Add des Heilers. T\195\182tet danach so viele Adds wie m\195\182glich bevor P1 wieder startet. Adds sind nicht spottbar!"
L["T339"] = "Taktik f\195\188r Alizabal TODO (f\195\188hle dich frei hier deine eigenen Taktiken einzuf\195\188gen)"
L["T340"] = "Fernk\195\164mpfer verteilen sich auf die Plattformen. Der Boss wird SPELL(101614) auf eine zuf\195\164llige Person, was diese zur\195\188ckschleudert. Nehmt sein geworfenes Totem und schleudert es zur\195\188ck. Baine nimmt dann 50% mehr Schaden"
L["T341"] = "Benedictus wird eine gro\195\159e Welle \195\188ber die gesamte Plattform schicken.Steht nicht vor ihm! Geht aus leuchtenden Eisfl\195\164chen auf dem Boden heraus.\
Er spammt SPELL(104503) und SPELL(104504), was aber unterbrochen werden kann."
L["T342"] = "Asira wird SPELL(102726) auf einen zuf\195\164lligen Caster in der Gruppe zaubern. Diese Person sollte versuchen den Tank oder einen Nahk\195\164mpfer zwischen sich und den Boss zu halten, damit er nicht zum Schweigen gebracht wird.\
Asira sollte aus SPELL(103558) raus bewegt werden. Andere Spieler sollten ebenso nicht darin stehen. "
L["T369"] = "keine Taktik derzeit verf\195\188gbar"
L["T370"] = "keine Taktik derzeit verf\195\188gbar"
L["T371"] = "keine Taktik derzeit verf\195\188gbar"
L["T372"] = "keine Taktik derzeit verf\195\188gbar"
L["T373"] = "keine Taktik derzeit verf\195\188gbar"
L["T374"] = "keine Taktik derzeit verf\195\188gbar"
L["T375"] = "keine Taktik derzeit verf\195\188gbar"
L["T376"] = "keine Taktik derzeit verf\195\188gbar"
L["T377"] = "keine Taktik derzeit verf\195\188gbar"
L["T378"] = "keine Taktik derzeit verf\195\188gbar"
L["T379"] = "keine Taktik derzeit verf\195\188gbar"
L["T380"] = "keine Taktik derzeit verf\195\188gbar"
L["T381"] = "keine Taktik derzeit verf\195\188gbar"
L["T382"] = "keine Taktik derzeit verf\195\188gbar"
L["T383"] = "keine Taktik derzeit verf\195\188gbar"
L["T384"] = "keine Taktik derzeit verf\195\188gbar"
L["T385"] = "keine Taktik derzeit verf\195\188gbar"
L["T386"] = "keine Taktik derzeit verf\195\188gbar"
L["T387"] = "keine Taktik derzeit verf\195\188gbar"
L["T388"] = "keine Taktik derzeit verf\195\188gbar"
L["T389"] = "keine Taktik derzeit verf\195\188gbar"
L["T390"] = "keine Taktik derzeit verf\195\188gbar"
L["T391"] = "keine Taktik derzeit verf\195\188gbar"
L["T392"] = "keine Taktik derzeit verf\195\188gbar"
L["T393"] = "keine Taktik derzeit verf\195\188gbar"
L["T394"] = "keine Taktik derzeit verf\195\188gbar"
L["T395"] = "keine Taktik derzeit verf\195\188gbar"
L["T396"] = "keine Taktik derzeit verf\195\188gbar"
L["T397"] = "keine Taktik derzeit verf\195\188gbar"
L["T398"] = "keine Taktik derzeit verf\195\188gbar"
L["T399"] = "keine Taktik derzeit verf\195\188gbar"
L["T400"] = "keine Taktik derzeit verf\195\188gbar"
L["T401"] = "keine Taktik derzeit verf\195\188gbar"
L["T402"] = "keine Taktik derzeit verf\195\188gbar"
L["T403"] = "keine Taktik derzeit verf\195\188gbar"
L["T404"] = "keine Taktik derzeit verf\195\188gbar"
L["T405"] = "keine Taktik derzeit verf\195\188gbar"
L["T406"] = "keine Taktik derzeit verf\195\188gbar"
L["T407"] = "keine Taktik derzeit verf\195\188gbar"
L["T408"] = "keine Taktik derzeit verf\195\188gbar"
L["T409"] = "keine Taktik derzeit verf\195\188gbar"
L["T410"] = "keine Taktik derzeit verf\195\188gbar"
L["T411"] = "keine Taktik derzeit verf\195\188gbar"
L["T412"] = "keine Taktik derzeit verf\195\188gbar"
L["T413"] = "keine Taktik derzeit verf\195\188gbar"
L["T414"] = "keine Taktik derzeit verf\195\188gbar"
L["T415"] = "keine Taktik derzeit verf\195\188gbar"
L["T416"] = "keine Taktik derzeit verf\195\188gbar"
L["T417"] = "keine Taktik derzeit verf\195\188gbar"
L["T418"] = "keine Taktik derzeit verf\195\188gbar"
L["T419"] = "keine Taktik derzeit verf\195\188gbar"
L["T420"] = "keine Taktik derzeit verf\195\188gbar"
L["T421"] = "keine Taktik derzeit verf\195\188gbar"
L["T422"] = "keine Taktik derzeit verf\195\188gbar"
L["T423"] = "keine Taktik derzeit verf\195\188gbar"
L["T424"] = "keine Taktik derzeit verf\195\188gbar"
L["T425"] = "keine Taktik derzeit verf\195\188gbar"
L["T427"] = "keine Taktik derzeit verf\195\188gbar"
L["T428"] = "keine Taktik derzeit verf\195\188gbar"
L["T429"] = "keine Taktik derzeit verf\195\188gbar"
L["T430"] = "keine Taktik derzeit verf\195\188gbar"
L["T431"] = "keine Taktik derzeit verf\195\188gbar"
L["T432"] = "keine Taktik derzeit verf\195\188gbar"
L["T433"] = "keine Taktik derzeit verf\195\188gbar"
L["T434"] = "keine Taktik derzeit verf\195\188gbar"
L["T435"] = "keine Taktik derzeit verf\195\188gbar"
L["T438"] = "keine Taktik derzeit verf\195\188gbar"
L["T439"] = "keine Taktik derzeit verf\195\188gbar"
L["T440"] = "keine Taktik derzeit verf\195\188gbar"
L["T441"] = "keine Taktik derzeit verf\195\188gbar"
L["T442"] = "keine Taktik derzeit verf\195\188gbar"
L["T443"] = "keine Taktik derzeit verf\195\188gbar"
L["T445"] = "keine Taktik derzeit verf\195\188gbar"
L["T446"] = "keine Taktik derzeit verf\195\188gbar"
L["T448"] = "keine Taktik derzeit verf\195\188gbar"
L["T449"] = "keine Taktik derzeit verf\195\188gbar"
L["T450"] = "keine Taktik derzeit verf\195\188gbar"
L["T451"] = "keine Taktik derzeit verf\195\188gbar"
L["T452"] = "keine Taktik derzeit verf\195\188gbar"
L["T453"] = "keine Taktik derzeit verf\195\188gbar"
L["T454"] = "keine Taktik derzeit verf\195\188gbar"
L["T455"] = "keine Taktik derzeit verf\195\188gbar"
L["T456"] = "keine Taktik derzeit verf\195\188gbar"
L["T457"] = "keine Taktik derzeit verf\195\188gbar"
L["T458"] = "keine Taktik derzeit verf\195\188gbar"
L["T459"] = "keine Taktik derzeit verf\195\188gbar"
L["T463"] = "keine Taktik derzeit verf\195\188gbar"
L["T464"] = "keine Taktik derzeit verf\195\188gbar"
L["T465"] = "keine Taktik derzeit verf\195\188gbar"
L["T466"] = "keine Taktik derzeit verf\195\188gbar"
L["T467"] = "keine Taktik derzeit verf\195\188gbar"
L["T469"] = "keine Taktik derzeit verf\195\188gbar"
L["T470"] = "keine Taktik derzeit verf\195\188gbar"
L["T471"] = "keine Taktik derzeit verf\195\188gbar"
L["T472"] = "keine Taktik derzeit verf\195\188gbar"
L["T473"] = "keine Taktik derzeit verf\195\188gbar"
L["T474"] = "keine Taktik derzeit verf\195\188gbar"
L["T475"] = "keine Taktik derzeit verf\195\188gbar"
L["T476"] = "keine Taktik derzeit verf\195\188gbar"
L["T477"] = "keine Taktik derzeit verf\195\188gbar"
L["T478"] = "keine Taktik derzeit verf\195\188gbar"
L["T479"] = "keine Taktik derzeit verf\195\188gbar"
L["T480"] = "keine Taktik derzeit verf\195\188gbar"
L["T481"] = "keine Taktik derzeit verf\195\188gbar"
L["T482"] = "keine Taktik derzeit verf\195\188gbar"
L["T483"] = "keine Taktik derzeit verf\195\188gbar"
L["T484"] = "keine Taktik derzeit verf\195\188gbar"
L["T485"] = "keine Taktik derzeit verf\195\188gbar"
L["T486"] = "keine Taktik derzeit verf\195\188gbar"
L["T487"] = "keine Taktik derzeit verf\195\188gbar"
L["T489"] = "keine Taktik derzeit verf\195\188gbar"
L["T523"] = "keine Taktik derzeit verf\195\188gbar"
L["T524"] = "keine Taktik derzeit verf\195\188gbar"
L["T527"] = "keine Taktik derzeit verf\195\188gbar"
L["T528"] = "keine Taktik derzeit verf\195\188gbar"
L["T529"] = "keine Taktik derzeit verf\195\188gbar"
L["T530"] = "keine Taktik derzeit verf\195\188gbar"
L["T531"] = "keine Taktik derzeit verf\195\188gbar"
L["T532"] = "keine Taktik derzeit verf\195\188gbar"
L["T533"] = "keine Taktik derzeit verf\195\188gbar"
L["T534"] = "keine Taktik derzeit verf\195\188gbar"
L["T535"] = "keine Taktik derzeit verf\195\188gbar"
L["T536"] = "keine Taktik derzeit verf\195\188gbar"
L["T537"] = "keine Taktik derzeit verf\195\188gbar"
L["T538"] = "keine Taktik derzeit verf\195\188gbar"
L["T539"] = "keine Taktik derzeit verf\195\188gbar"
L["T540"] = "keine Taktik derzeit verf\195\188gbar"
L["T541"] = "keine Taktik derzeit verf\195\188gbar"
L["T542"] = "keine Taktik derzeit verf\195\188gbar"
L["T543"] = "keine Taktik derzeit verf\195\188gbar"
L["T544"] = "keine Taktik derzeit verf\195\188gbar"
L["T545"] = "keine Taktik derzeit verf\195\188gbar"
L["T546"] = "keine Taktik derzeit verf\195\188gbar"
L["T547"] = "keine Taktik derzeit verf\195\188gbar"
L["T548"] = "keine Taktik derzeit verf\195\188gbar"
L["T549"] = "keine Taktik derzeit verf\195\188gbar"
L["T550"] = "no tactic available yet"
L["T551"] = "no tactic available yet"
L["T552"] = "no tactic available yet"
L["T553"] = "no tactic available yet"
L["T554"] = "no tactic available yet"
L["T555"] = "no tactic available yet"
L["T556"] = "no tactic available yet"
L["T557"] = "no tactic available yet"
L["T558"] = "no tactic available yet"
L["T559"] = "no tactic available yet"
L["T560"] = "keine Taktik derzeit verf\195\188gbar"
L["T561"] = "keine Taktik derzeit verf\195\188gbar"
L["T562"] = "keine Taktik derzeit verf\195\188gbar"
L["T563"] = "keine Taktik derzeit verf\195\188gbar"
L["T564"] = "keine Taktik derzeit verf\195\188gbar"
L["T565"] = "keine Taktik derzeit verf\195\188gbar"
L["T566"] = "keine Taktik derzeit verf\195\188gbar"
L["T568"] = "keine Taktik derzeit verf\195\188gbar"
L["T569"] = "keine Taktik derzeit verf\195\188gbar"
L["T570"] = "keine Taktik derzeit verf\195\188gbar"
L["T571"] = "no tactic available yet"
L["T572"] = "no tactic available yet"
L["T573"] = "no tactic available yet"
L["T574"] = "no tactic available yet"
L["T575"] = "no tactic available yet"
L["T576"] = "no tactic available yet"
L["T577"] = "no tactic available yet"
L["T578"] = "no tactic available yet"
L["T579"] = "no tactic available yet"
L["T580"] = "no tactic available yet"
L["T581"] = "no tactic available yet"
L["T582"] = "no tactic available yet"
L["T583"] = "no tactic available yet"
L["T584"] = "no tactic available yet"
L["T585"] = "no tactic available yet"
L["T586"] = "no tactic available yet"
L["T587"] = "no tactic available yet"
L["T588"] = "no tactic available yet"
L["T589"] = "no tactic available yet"
L["T590"] = "no tactic available yet"
L["T591"] = "no tactic available yet"
L["T592"] = "no tactic available yet"
L["T593"] = "no tactic available yet"
L["T594"] = "no tactic available yet"
L["T595"] = "no tactic available yet"
L["T596"] = "no tactic available yet"
L["T597"] = "no tactic available yet"
L["T598"] = "no tactic available yet"
L["T599"] = "no tactic available yet"
L["T600"] = "no tactic available yet"
L["T601"] = "A random player will get SPELL(72452) so heal him or he'll get 16k shadow damage.\
You should disspell SPELL(72426) within 6 seconds. He will cast other healing reducing debuffs so you have to heal through it"
L["T602"] = "Get out of SPELL(72362). When SPELL(72369) is dispelled the not dealt DoT-damage will be spread instantly over the party, so let it tick a while.\
On occasion he will reduce your health by 50% so once again very heal intensive!"
L["T603"] = "very simple in theory. LK will summon some bad guys and you have to kill them ASAP. Don't let the LK touch you and try to not get your healer sliced and focus caster and fat guys."
L["T604"] = "no tactic available yet"
L["T605"] = "no tactic available yet"
L["T606"] = "no tactic available yet"
L["T607"] = "no tactic available yet"
L["T608"] = "Boss casts SPELL(70336) regular and it stacks. He also SPELL(68788) at a random player, go away from the dropping zone.\
SPELL(70336) won't hit you when hiding behind the saronite. Every partymember should try to hide behind them every now and then so that the frost damage won't stack too high."
L["T609"] = "The boss has basically three options which he casts every now and then.\
SPELL(70434): Run away from him especially melee and tank!\
SPELL(69263): Everbody move! Don't stand still to avoid the blue explosions.\
SPELL(68987): The person that is pursuited has to run away from the boss."
L["T610"] = "Rimefang (dragon) on occasion marks an area. Leave that area, because some frost patches will appear there.\
Player who gets SPELL(69172) should stop DPS immediately because otherwhise will bypass DPS to tank.\
If boss gets SPELL(69629) you should try to kite him over the frost patches on the ground so he can't hit you."
L["T611"] = "no tactic available yet"
L["T612"] = "no tactic available yet"
L["T613"] = "no tactic available yet"
L["T614"] = "no tactic available yet"
L["T615"] = "SPELL(68793) is his main attack. He will cast SPELL(68893) on a random player causing him to seperate his soul. Don't let the soul reach the bos or he will get healed!\
When boss reaches 30% health he will SPELL(68872) and randomally Fear a player. Dispell the fear and nuke him down"
L["T616"] = "Try to interrupt her main attack SPELL(70322) as often as possible. When she casts SPELL(69051) stop all of your damage immediately or the debuffed player will die.\
On occasion she casts SPELL(68939) summoning many ghosts. Run away if the damage taken is too high.\
Finally she cats SPELL(68912) creating a wall which is killing you on contact. She rotates 90 degrees so go with her to avoid contact."
L["T617"] = "no tactic available yet"
L["T618"] = "no tactic available yet"
L["T619"] = "no tactic available yet"
L["T620"] = "no tactic available yet"
L["T621"] = "no tactic available yet"
L["T622"] = "no tactic available yet"
L["T623"] = "no tactic available yet"
L["T624"] = "no tactic available yet"
L["T625"] = "no tactic available yet"
L["T626"] = "no tactic available yet"
L["T627"] = "no tactic available yet"
L["T628"] = "no tactic available yet"
L["T629"] = "no tactic available yet"
L["T63"] = "keine Taktik derzeit verf\195\188gbar"
L["T630"] = "no tactic available yet"
L["T631"] = "no tactic available yet"
L["T632"] = "no tactic available yet"
L["T634"] = "no tactic available yet"
L["T635"] = "no tactic available yet"
L["T636"] = "no tactic available yet"
L["T637"] = "no tactic available yet"
L["T638"] = "no tactic available yet"
L["T639"] = "no tactic available yet"
L["T640"] = "no tactic available yet"
L["T641"] = "A very easy tank and spank. When she casts the ritual make sure you kill the 3 adds with AoE otherwise you will get a DoT-Debuff that ticks very high.\
"
L["T642"] = "Boss will activate all 4 adds one after another. The adds are easy tank and spank and don't have any specialty.\
The boss itself cleaves so don't stand in front of him (if you are no tank)."
L["T643"] = "Adds are spawning. Kill them and use the spears when the boss is near the cannon. If you hit him three times he will \"land\".\
Boss will DOT a player with SPELL(59331). Melee: make sure to run away from his SPELL(59322)"
L["T644"] = "Well the boss does many abilities but infact you can just DPS him down and heal your party like crazy.\
It's like most of Wotlk 5-mans. you don't really need tactics at all for most of the bosses."
L["T649"] = "Der Boss ist unangreifbar. Er st\195\188rmt eine T\195\188r an, steht nicht in der Aufschlagszone. DDs gehen in die Kanonen und greifen seine Schwachstelle an. Tank und Heiler bleiben unten und tanken die Adds. Erledigt ab und an die Adds (focus dmg).\
Phase 2: Wenn die Schwachstelle besiegt ist, visiert der Boss einen Spieler an. Dieser muss weglaufen. Passt auf Tornados auf und t\195\182tet den Boss, der 300% mehr Schaden erh\195\164lt."
L["T654"] = "Sammelt euch in der Mitte um SPELL(111218) zu verhindern. Von der Gruppe weg tanken, da der Boss spaltet.\
Manchmal SPELL(82137) er und beschw\195\182rt 2 Adds, diese schnell t\195\182ten. Man muss erst durch ihre R\195\188stung mit 60k Schaden.\
Wenn er rumwirbelt mit SPELL(111216) weich so gut aus wie es geht. Er wird durch den ganzen Raum dabei toben."
L["T655"] = "Er wirft SPELL(107130) auf den Boden. Diese Bomben gehen nicht hoch. Er wirft auch Bomben auf einen Spieler und diese explodieren dann und zwar nach Nord, Ost, S\195\188d, West. Wenn die Explosionsstrahlen andere Bomben treffen explodieren diese ebenso.\
Steht also so, dass ihr andere Bomben ausl\195\182st, sonst werden es zu viele!\
Wenn der Boss 30% oder 70% erreicht jagt er ALLE! Bomben hoch! Es ist also wichtig so zu stehen, dass ihr die Bomben ausl\195\182st bevor der Boss das tut."
L["T656"] = "Boss benutzt SPELL(113691) und SPELL(11366) als Hauptattacken. Unterbrecht ihn so oft wie es geht. Wenn er sich mit SPELL(113682) bufft heilt gegen oder benutzt SPELL(30449) eines Magiers.\
Steht alle hinter dem Boss wegen SPELL(113653). Wenn er dies tut muss auch der Tank im Halbkreis laufen um den Schaden auszuweichen. Fangt die fliegenden B\195\188cher ab oder geht aus den Feuer raus!"
L["T657"] = "Sch\195\188ler: Wenn anvisiert lauft davon, ansonsten t\195\182tet die 2 Sch\195\188ler.\
Boss Phase 1: Geht hinter ihm wenn er SPELL(113656) benutzt und geht weg wenn er SPELL(106433) benutzt.\
Phase 2: Manchmal teilt er sich in 3 Kopien. Weicht SPELL(106470) (blau) aus und t\195\182tet die Kopien.\
Phase 3: Lauft weg, wenn ihr verfolgt werdet und h\195\182rt auf ihn anzugreifen wenn er SPELL(106447) benutzt."
L["T658"] = "Phase 1: Weicht SPELL(106938) aus und entfernt den Feuer DoT vom Tank. (SPELL(106823))\
Phase 2: Der DoT ist nicht mehr entfernbar als heilt den Schaden aus.\
Phase 3: Tankt die Jadeschlange und verteilt euch damit SPELL(107110) wenig Spieler trifft."
L["T659"] = "Phase 1: Boss l\195\164sst eine SPELL(111231) durch den Raum gleiten, also schnell den Boss t\195\182ten! Spieler die SPELL(111610) erhalten m\195\188ssen sich von der Gruppe fern halten. SPELL(111631) springt von Spieler zu Spieler nachdem er einmal getickt hat.\
Phase 2: Boss verschwindet in ihrem Gef\195\164\195\159. T\195\182tet es so schnell wie m\195\182glich!. Fliegende B\195\188cher zaubern Arkane Exlosion oder Feuerbomben. Weicht dem aus!"
L["T660"] = "Boss wird SPELL(114020) und ein Messer werfen. Verteilt euch, weil jeder Spieler in der Flugbahn des Messers SPELL(114056) erhalten wird, was ein stapelbarer Debuff ist. Er springt einen zuf\195\164lligen Spieler an und benutzt SPELL(114056), also entfernt euch!\
Bei 90% 80% 70% und 60% wird er SPELL(114259). DDs auf die Hunde und danach auf den Boss gehen.\
Bei 50% gibt es einen Wutanfall und er macht mehr Schaden als zuvor."
L["T663"] = "Phase 1: SPELL(114062) ist ein frontaler Wirbel der vermieden werden sollte (auch Tank). Sie l\195\164sst SPELL(114035) auf dem Boden entstehen. Diese sind t\195\182dlich!\
Phase 2: (66%, 33% Leben): Sie kopiert sich. T\195\182tet die Kopien eine nach der Anderen. In der Phase gibt es konstanten Arkanschaden. Wenn eine Kopie stirbt explodiert diese, also keine AoE benutzen."
L["T664"] = "Es gibt zwei M\195\182gliche Begegnungen:\
Zao Sonnensucher: T\195\182tet die Sonnen, t\195\182tet die Adds, t\195\182tet den Boss.\
Anger and Strife: Beginnt mit einem der beiden, der daraufhin SPELL(113315) stapelt. Bei 8 Stapeln wechselt zum anderen, damit sich die Stapel mit SPELL(113379) automatisch abbauen."
L["T665"] = "Boss stappelt einen Buff SPELL(113765). Wenn der Schaden zu hoch wird kann der Tank den Boss herumf\195\188hren, da er sehr langsam l\195\164uft.\
Manchmal benutzt er SPELL(113999) was hohen Schaden macht, aber man kann sich mit einem Klick auf den Knochenhaufen sch\195\188tzen (davor).\
Die H\195\164lfte des Raums wird mit Feuer bedeckt sein, also achtet darauf SPELL(114009)."
L["T666"] = "Phase 1: Verteilt euch gut. Boss springt einen Spieler an SPELL(111775) und macht AoE Schaden an der Einschlagszone.Aus dem Feuer raus!.\
Phase 2: Bei 60% trennt sich ihre Seele vom K\195\182rper. SPELL(111642) ist nicht vermeidbar. Sie wird SPELL(115350) und einen Spieler anvisieren. Sie darf diesen Spieler NICHT ber\195\188hren, da sie sonst extrem stark wird.\
Phase 3: Wenn ihre Seele 1% erreicht, muss man beide bek\195\164mpfen. Phasen wie oben erl\195\164utert."
L["T668"] = "Tankt ihn weg von der Gruppe wegen SPELL(106807). Bei 90%, 60% und 30% erh\195\182hen sich Schaden und Angriffsgeschwindigkeit um 15%.\
DDs h\195\188pft auf die F\195\164sser und fahrt in den Boss. Das verursacht SPELL(106648) und er bekommt 15% mehr Schaden."
L["T669"] = "Geht den Gang entlang und pullt alles und lauft zum Bossraum. Immer wenn Adds da sind macht das folgende:\
T\195\182tet die Hammer Hasen zuerst. Nehmt den Hammer und t\195\182tet damit die restlichen Adds. Benutzt den Hammer um die Explosionshasen in die Luft zu schleudern, damit sie dort explodieren.\
Tankt den Boss weg von der Gruppe wegen SPELL(112945). Vermeidet au\195\159erdem Schaden von SPELL(112992)."
L["T670"] = "Er benutzt 3 von 6 M\195\182glichen Attacken.\
SPELL(106546): Verteilt euch und heilt gegen. SPELL(106851): Springt um den Debuff loszuwerden.\
Wenn der Boden voller Bier ist, benutzt die Blasen um zu schweben.\
Wenn er zwei W\195\164nde beschw\195\182rt, springt \195\188ber sie."
L["T671"] = "F\195\188r jede 10% Leben, die der Boss verliert, stapelt er  SPELL(114410) und erh\195\182ht seinen Feuerschaden um 10%. Ab und an springt er auf einen Spieler und macht einen AoE Feuerkreis. Er wird SPELL(114808) nach vorne benutzen. ALLE m\195\188ssen dann weg von ihm.\
Bei 50% hinterl\195\164sst der Boss au\195\159erdem Feuerflecken auf seinem Laufweg."
L["T672"] = "T\195\182tet die 4 Elementare und steht nicht im Wasser. Im Moment ist der Boss unangreifbar\
Phase 2: Lauft im Halbkreis vor  SPELL(106334) weg, was ein gigantischer Strahl ist."
L["T673"] = "Phase 1: Verteilt euch, sodass ihr nicht den AoE Schaden von SPELL(106984) erhaltet. Wenn die Schlange da ist, zieht den Boss aus SPELL(128889) raus.\
Phase 2: Schlangenaction. Sammelt euch unter der Schlange. Wenn der Boss SPELL(107140) benutzt heilt den Spieler schnell und sammelt euch dann mit ihm.\
Phase 3: Panda wird sauer. Ohh ohh. Zieht alle Cooldowns!"
L["T674"] = "Bezwingt zun\195\164chst den Kommandanten. Wenn er stirbt erscheint die Priesterin. Sie wird ihn wiederbeleben und ihr m\195\188sst dann gegen beide k\195\164mpfen.\
T\195\182tet zuerst den Kommandanten, aber unterbrecht SPELL(83968) von der Priesterin, da sie sonst den ganzen Raum wiederbelebt.Versucht au\195\159erdem SPELL(36678) zu unterbrechen."
L["T675"] = "Der Boss benutzt SPELL(107047) und spie\195\159t sein Ziel auf, rediziert das Leben um 50%. Achtung Heiler!\
Bei 70% und 30% fliegt er von einer Kante zu der anderen und hinterl\195\164sst eine riesige Feuerwand. Dabei beschw\195\182rt er Adds. Geht auch aus den gr\195\188nen Fl\195\164chen raus!"
L["T676"] = "Er hinterl\195\164sst gr\195\188ne Flecken SPELL(107091). Zieht ihn aus den Fl\195\164chen raus, da er sonst m\195\164chtiger wird. Nichts und Niemand darf in dem gr\195\188nen Zeug stehen. Zieht ihn dahin wo kein Feuer ist und sammelt und bombt die Adds dort. \
Au\195\159erdem sollte niemand vor ihm stehen, da er spaltet. ( SPELL(107120) )"
L["T677"] = "no tactic available yet"
L["T679"] = "no tactic available yet"
L["T682"] = "no tactic available yet"
L["T683"] = "no tactic available yet"
L["T684"] = "Der Boss benutzt zwei Feuerattacken. Manchmal portet er einen Spieler in einen Lehrraum SPELL(113395). T\195\182tet dort die Adds und kommt wieder zum Boss zur\195\188ck. Wenn Tank oder Heiler geportet sind, zaubert er SPELL(113143). T\195\182tet die Adds und wartet auf den Tank/Heiler."
L["T685"] = "Entfernt SPELL(106872) oder der Spieler wird viel Schaden erleiden. Raus aus den blauen Fl\195\164chen. T\195\182tet die Adds sofort!\
Wenn er SPELL(106827) zaubert, benutzt so viele F\195\164higkeiten wie m\195\182glich. Danach erhaltet ihr n\195\164mlich SPELL(127576).\
Wenn der Boss wenig Leben hat macht er 50% mehr Schaden, Zeit f\195\188r Blutrausch, Heldentum, e.t.c."
L["T686"] = "Wenn euer Hass (neue Leiste) zu voll wird benutzt SPELL(107200) um den Hass zu resetten. Hass f\195\188llt sich immer wenn ihr von einer Bossf\195\164higkeit getroffen werdet.\
Raus aus den SPELL(112933) (lila Zonen, Fernk\195\164mpfer t\195\182ten). Wenn der Boss umringt ist von SPEL(112918) bewegt euch schnell weg.\
Der Tank muss sich dann auch wegbewegen, da er sonst SPELL(114999) kassiert."
L["T687"] = "no tactic available yet"
L["T688"] = "Unterbrecht SPELL(115289) so gut es geht. Manchmal benutzt er SPELL(115139). T\195\182tet Sie so schnell wie m\195\182glich da sie einen DoT hinterlassen.\
Immer mal entrei\195\159t er einem Spieler die Seele SPELL(115297). T\195\182tet die Adds.\
Er beschw\195\182rt einen m\195\164chtigen Geist, der versucht die Leichen der Kreuzfahrer zu erreichen. Lasst das nicht passieren ansonsten verwandelt er sich in einen Zombie. Falls es doch passiert tankt den Zombie und konzentriert Schaden auf den Boss."
L["T689"] = "no tactic available yet"
L["T690"] = "Es geht hier nur um die Reihenfolge.\
1. T\195\182tet Eisenhaut (er reduziert 50% Schaden) dann den Schleicher (bet\195\164ubt Spieler) dann das Orakel (heilt) und dann den letzten (der sheept).\
Der Boss selbst ist jetzt ein Kinderspiel, da er 100% mehr Schaden erleidet."
L["T691"] = "no tactic available yet"
L["T692"] = "Der General wirft seine Klinge auf einen Spieler. Weg von der Einschlagstelle! Heiler entfernt den Debuff von den Spielern.\
Wenn er sein Schild aktiviert benutzt die wurfbaren Bomben um das Schild zu schw\195\164chen. Weicht den anderen Bomben aus, da sie 3 Sekunden nach Aufschlag hoch gehen. "
L["T693"] = "In der Mitte ist eine Pf\195\188tze die stetig w\195\164chst. Wenn ein Blubber die Mitte erreicht w\195\164chst sie au\195\159erdem, also t\195\182tet sie!\
Um die Pf\195\188tze zu schrumpfen geht kurz in sie rein. Manchmal zaubert der Boss SPELL(12001). Je gr\195\182\195\159er die Pf\195\188tze ist, desto gr\195\182\195\159er ist auch der Schaden, also versucht die Pf\195\188tze klein zu halten."
L["T694"] = "no tactic available yet"
L["T695"] = "no tactic available yet"
L["T696"] = "no tactic available yet"
L["T697"] = "no tactic available yet"
L["T698"] = "Tankt den Boss weg von der Gruppe. Wenn er  SPELL(119684) benutzt geht hinter ihn (auch tank!). Spieler unter denen SPELL(17447) ist bewegen sich raus dort.\
Bei 66% aktiviert er Klingenfallen. Achtet auf herumfliegende Klingen an den Seiten.\
Bei 33% aktiviert er Armbr\195\188ste. Ein zuf\195\164lliger Spieler wird Schaden erhalten.\
Weicht zu jeder Zeit den wirbelnden \195\132xten aus!"
L["T708"] = "Ihr m\195\188sst 3 Champions besiegen, nacheinander.\
1. Boss: T\195\182tet den L\195\182wen, da er nicht spottbar ist. Danach auf den Boss. Geht hinter ihn wenn er  SPELL(46968) benutzt.\
2. Boss: Verteilt euch und weicht den Tornado aus, den der Boss beschw\195\182ren kann.\
3. Boss: Heiler achten auf SPELL(123655). Versammelt euch wenn er  SPELL(120196) zaubert um den Schaden zu teilen."
L["T709"] = "no tactic available yet"
L["T713"] = "no tactic available yet"
L["T725"] = "no tactic available yet"
L["T726"] = "no tactic available yet"
L["T727"] = "Heiler achten auf SPELL(121762). Bewegt euch aus SPELL(121443) raus. Wenn der Boss euch mit Harz bedeckt, springt!\
Wenn der Boss an das andere Ende der Br\195\188cke fliegt, erreicht ihn und unterbrecht seinen SPELL(121284)."
L["T728"] = "no tactic available yet"
L["T729"] = "no tactic available yet"
L["T737"] = "no tactic available yet"
L["T738"] = "Phase 1: T\195\182tet die Adds und geht aus Bodeneffekten raus. T\195\182tet die Demolierer mit einem Schadenspunkt. Benutzt das Teer.\
Phase 2: Versurcht SPELL(120778) aufrechtzuerhalten. Passt auf, dass ihr keine Spieler damit trefft. Wenn er SPELL(120760) benutzt, bewegt euch weg von ihm und erneuert den Teer Debuff. Versucht seinem Anst\195\188rmen auszuweichen."
L["T741"] = "no tactic available yet"
L["T742"] = "no tactic available yet"
L["T743"] = "no tactic available yet"
L["T744"] = "no tactic available yet"
L["T745"] = "no tactic available yet"
L["T748"] = "no tactic available yet"
L["T749"] = "no tactic available yet"
L["T816"] = "Council of Elders"
L["T817"] = "Qon"
L["T818"] = "Durumu der Vergessene"
L["T819"] = "Horridon"
L["T820"] = "Primordius"
L["T821"] = "Megaera"
L["T824"] = "Dunkler Animus"
L["T825"] = "Tortos"
L["T827"] = "Jin'rokh der Zerst\195\182rer"
L["T828"] = "Ji-Kun"
L["T829"] = "Twin Consorts"
L["T832"] = "Lei Shen"
L["T846"] = "Tactics Malkorok"
L["T849"] = "Tactics The Fallen Protectors"
L["T850"] = "Tactics General Nazgrim"
L["T851"] = "Tactics Thok the Bloodthirsty"
L["T852"] = "Tactics for Immerseus"
L["T853"] = "Tactics Paragons of the Klaxxi"
L["T856"] = "Tactics Kor'kron Dark Shaman"
L["T857"] = "Chi-Ji, The Red Crane"
L["T858"] = "Yu'lon, The Jade Serpent"
L["T859"] = "Niuzao, The Black Ox"
L["T860"] = "Xuen, The White Tiger"
L["T861"] = "Ordos, Fire-God of the Yaungol"
L["T864"] = "Tactics Iron Juggernaut"
L["T865"] = "Tactics Siegecrafter Blackfuse"
L["T866"] = "Tactics Norushen"
L["T867"] = "Tactics Sha of Pride"
L["T868"] = "Tactics Galakras"
L["T869"] = "Tactics Garrosh Hellscream"
L["T870"] = "Tactics Spoils of Pandaria"
L["T887"] = "Roltall schleudert 3 Brocken (Aufprallzone am Grund sichtbar). Weicht ihnen aus und achtet darauf, dass sie in der gleichen Reihenfolge zur\195\188ckkommen.\
Sobald der 3. Brocken zur\195\188ck ist, beschw\195\182rt Roltall eine Welle, die euch f\195\188r 8 Sekunden zur\195\188ckdr\195\188ckt und in brennende Schlacke schiebt. Der Trick ist neben und nicht vor der Schlacke zu stehen, sobald Roltall sie auswirft, damit man nicht hineingezogen wird.\
Roltall"
L["T888"] = "Bek\195\164mpft feindliche Ads! Unterbrecht SPELL(150759). Heilt befreundete Ads. \
Feindliche Ads konzentrieren sich auf Spieler, die mit SPELL(150745) belegt sind und ignorieren derweil Spott. \
"
L["T889"] = "Tanks: Bleibt auf den Boss konzentriert und unterbrecht so viele SPELL(150677) wie m\195\182glich.\
Heiler: Nach jedem 3. Stack von SPELL(150678) macht der Boss AoE-Schaden. Behaltet eure Cooldowns bis 20%.\
DPS: Vermeidet Flammen (SPELL(150784)) und vernichtet Adds, wenn sie gespawned werden."
L["T89"] = "Phase 1: Boss wirkt SPELL(87859) oder SPELL(87861), teleportiert sich manchmal und hat dabei Aggro Reset.\
Phase 2: Boss bleibt in der Mitte. Es erscheint eine SPELL(91398), welche um den Boss rotiert. ACHTUNG: Immer in Bewegung bleiben. Adds down machen mit AoE Schaden."
L["T893"] = "Immer wenn Adds erscheinen, t\195\182tet diese zuerst und schnell! Unterbrecht SPELL(149997), was die Feuerelementare zaubern.\
Tankt die Steinelementare nicht in der Gruppe, da sie Spieler stunnen k\195\182nnen."
L["T90"] = "Der Boss verteilt mit der Zeit \195\188berall Bomben. Von diesen weg gehen.\
Wenn jemand eine Bombe vom Boss umgeschnallt bekommt SPELL(88352), sollte dieser unter den querliegenden Baumstamm gehen, damit er nicht so hoch geschossen wird.\
Von Zeit zu Zeit packt der Boss einen Spieler und rammt ihn gegen einen Baumstamm. Dieser Spieler ben\195\182tigt davor und danach starke Heilung. "
L["T91"] = "Add: Einer muss mit einer Erntemaschine die adds kontrollieren. Diese spawnen unten an den Schmelzen. Ziel: 2x SPELL(91734) 1xSPELL(91735), damit die adds nicht bis zur Treppe kommen.\
Der Boss selbst wird an/auf der Treppe getankt, damit der Ad Spieler unten genug Platz hat und der Rest der Gruppe keinen Schaden bekommt.\
SPELL(88481): Boss dreht durch und f\195\188gt Spielern in seiner N\195\164he schaden zu. DDs rennen weg. Tank spottet kurz danach.\
SPELL(88495): Boss visiert einen Spieler an und rennt zu ihm. Dieser Spieler rennt weg, NACHDEM unter ihm der rote Kreis entstanden ist.\
Bei 40% verursacht der Boss SPELL(88495): Heldentum/Zeitkr\195\188mmung/Blutrausch und Tank CDs z\195\188nden!"
L["T92"] = "Bossphase: Boss wird getankt und st\195\188rmt manchmal einen Spieler an. In sp\195\164teren Bossphasen sind auch Adds da um die sich gek\195\188mmert werden muss.\
Addphase: Es erscheinen kleine D\195\164mpfe (Ads) die sofort get\195\182tet werden m\195\188ssen. Dr\195\188ckt v dann seht ihr die Balken und somit die adds schneller, denn diese wachsen.\
Je gr\195\182\195\159er die Adds desto mehr Schaden machen sie. Phasen wechseln sich ab, adds kommen aber immer. Bei 10% ruft er extrem viele Ads. Dann Tank CDs z\195\188nden und Boss umhauen und adds ignorieren."
L["T93"] = "Cookie steigt in einen Topf. Er wirft mit gutem Essen ( leuchtet gelb ) und schlechtem Essen ( gr\195\188n ) um sich. Das gute essen um SPELL(92834) zu erhalten und aus dem Schlechten SPELL(92066) rausgehen."
L["T95"] = "Vanessa wird normal getankt. Es erscheinen immer wieder 2 Ads. Diese sollten schnell fokusiert werden. Insbesondere die Blutmagier sind zu unterbrechen.\
Bei 50% verschwinden m\195\182gliche Adds sowie der Boss. Es erscheinen an der Seite des Schiffes 5 Seile. Jeder aus der Gruppe klickt sofort mit Rechtsklick darauf.\
Nach der Tarzan-Einlage geht es wie gewohnt weiter bis 25%, dann kommen wieder Seile und danach kommen keine Adds mehr und sie ist so gut wie tot."
L["T959"] = "no tactics available yet"
L["T96"] = "Vorbemerkungen: HT/TimeWarp aufsparen f\195\188r 25%. Heiler nicht alles vollheilen, nur am Leben lassen mit ca 40-50% Leben.\
SPELL(93581): F\195\188gt einem Spieler Schaden zu. Unterbrechen, da nicht alle Spieler immer voll sein k\195\182nnen!\
SPELL(93713): Entfernt mit Patch 4.0.6.\
SPELL(93423): Er hebt alle Spieler hoch und reduziert deren Leben auf 1% EGAL wie voll sie vorher waren.\
SPELL(93706): Wird sofort nach Katastrophe gecastet. Er heilt AOE ALLES auch sich. H\195\182chtens 2 Ticks durchlassen, dann kicken. Wenn sofort gekickt, dann ist Tank bei 1% HP :).\
Das A und O dieses Kampfes ist, dass ihr euch ausmacht wer was kickt."
L["T965"] = "Bleibt in Bewegung um Schaden zu vermeiden, wenn sie rotieren und sich bewegen.\
Heiler achtet auf den AoE DOT SPELL(165731).\
SPELL(165731) f\195\188gt den Zielen an denen Ranjit vorbeil\195\164uft erheblichen Schaden zu. Dabei bewegt er sich auf einer geraden Linie vor."
L["T966"] = "Verhindert mit SPELL(154149), dass sich der Boss mit dem Sonnenstrahl heilt, indem ihr euch zwischen beide stell.\
Heiler: SPELL(154135) macht mehr Schaden je \195\182fter man ihn einsetzt.\
Tank: Vermeide SPELL(154132), der mit dem rechten oder linken Arm beschworen wird."
L["T967"] = "Entfernt euch von den Sonnenv\195\182geln, sonst beschw\195\182ren sie SPELL(153828). T\195\182tet die V\195\182gel entfernt von Aschehaufen und SPELL(169810)  sonst werden sie wiederbelebt.\
Der Tank muss im Melee-Bereich des Bosses bleiben, da er sonst SPELL(153898) beschw\195\182rt. Behaltet die Cooldowns des Tanks f\195\188r SPELL(153794)."
L["T968"] = "Besiegt schnell die Zeloten bevor sie SPELL(153954) beschw\195\182ren, indem ihr sie \195\188ber den Rand fallen lasst.\
Vernichtet so schnell wie m\195\182glich die Schildkonstrukte.\
Entfernt euch von SPELL(154043) und f\195\188hrt den Destruktionsstrahl nicht durch eure Gruppe. \
Unterbrecht SPELL(154396)."
L["T97"] = "Einfacher Kampf. Ab und zu kommen Worgen. Mittanken und umhauen. Entfluchen nicht vergessen."
L["T971"] = "no tactics available yet"
L["T98"] = "Tank muss immer aus SPELL(93687) raus und SEHR schnell reagieren, wenn die 2 Adds kommen sofort an sich binden und umhauen.\
Alternativ: Umnuken und 2 Adds CCen, weitere Adds nur an sich binden und weiter Boss nuken! Die Adds wirken SPELL(93844) auf den Boss.\
3. Methode: alles bis zum Boss cleraen, der Tank pullt den Boss und rennt dann runter zum Hof. Dort werden keine Ads mehr spawnen und man hat mehr Platz."
L["T99"] = "SPELL(93617): Alle in Bewegung bleiben! SPELL(93689): Alle stehen bleiben!\
SPELL(93527): Schauen das man nicht drin steht."
L["TACTICS"] = "Taktik f\195\188r "
L["TOOLTIP_ACHIEVEBUTTON"] = "Erfolge anzeigen"
L["TOOLTIP_ACHIEVEBUTTON_2"] = "Linksklick: Erfolge f\195\188r aktuellen Boss\
Rightklick: Erfolge f\195\188r aktuelle Instanz"
L["TOOLTIP_LOOTBUTTON"] = "Beute anzeigen"
L["TOOLTIP_TACTICBUTTON_1"] = "Taktiken senden"
L["TOOLTIP_TACTICBUTTON_2"] = "Linksklick: Taktiken an Kanal senden\
Rechtsklick: Kanal einstellen"
L["UNDONE_ACHIEVEMENTS"] = "nicht erledigte Erfolge"
L["WHISPER"] = "Fl\195\188stern"
L["WHISPER_MENU"] = "Fl\195\188stern"
L["WHISPER_MODE"] = "Fl\195\188stermodus"
L["WHISPER_MODE_DESC"] = "Wenn aktiviert und jemand schreibt im Gruppenchat !boss, dann wirst du ihm die Taktik zufl\195\188stern, anstatt sie im Gruppenchat zu posten."
L["WHISPER_TACTIC"] = "Taktik senden an ..."


	end
end
fr:SetScript("OnEvent", fr.OnEvent)
