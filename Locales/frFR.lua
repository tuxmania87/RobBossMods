local L = LibStub("AceLocale-3.0"):NewLocale("RobBossMods", "frFR", false)
if not L then return end

L["5MAN"] = "5 man dungeons"
L["ACHIEVE_SETTINGS"] = "Show unfinished achievements"
L["ACHIEVE_SETTINGS_DESC"] = "Show unfinished Achievements when entering an instance or set boss into target."
L["ALWAYS_SHOW_FRAME"] = "Toujours afficher la fen\195\170tre RBM"
L["ALWAYS_SHOW_FRAME_DESC"] = "La fen\195\170tre reste toujours visible (en dehors des combats)."
L["ASFIRST"] = "as first boss of"
L["A_SHOW"] = "toujours afficher"
L["ASNEXT"] = "as next boss for"
L["AUTHOR1"] = "Auteur : Robert Hartmann, Allemagne (JBalandar, Skum, Nomuerto, Lest\195\161 tous sur le serveur EU-Thrall). Email : robert@keinerspieltmitmir.de"
L["AUTHOR2"] = "Aide pour la traduction en Anglais : Stagen@irc.freenode.net and Niadyth@irc.freenode"
L["BOOK"] = "Book"
L["BOSS_DEAD1"] = "a \195\169t\195\169 vaincu."
L["BOSS_DEAD2"] = "est le suivant."
L["BUTTONS"] = "Buttons"
L["CHATCOMMANDS"] = "!boss - (typed in party/raid channel from anybody) will post boss tactics of the current boss in party/raid channel\
\
!loot - (typed in party/raid cannel from anybody) will post loot of the current boss in party/raid channel\
\
!achieve(all) will post achievements related to the boss (related to the instance) in party/raid channel\
\
!rbminfo will post some information about the addon in the party channel\
\
/rbm resetlang - if you use english client but want to access the german tactics you can switch settings with this command\
\
/rbm toggle - Toggle the Main Window. You can use this for building macros"
L["CHOICE"] = "Choix"
L["CLOSE_OPT"] = "Cacher"
L["CLOSE_OPT_DESC"] = "Afficher la fen\195\170tre uniquement quand un boss est cibl\195\169."
L["CURRENT_ADDON"] = "show tactics for "
L["CURRENTBOSS"] = "Boss actuel"
L["D23574_1"] = "CUR_SPELL under TAR_SPELL. Meet THERE!!!"
L["D23574_2"] = "CUR_SPELL is over: Spread again!"
L["D23577_1"] = "CUR_SPELL: Get the boss out of the green circle!"
L["D23577_2"] = "CUR_SPELL: spawned. Kill it!"
L["D23863_1"] = "CUR_SPELL: Remove the debuff !!"
L["D23863_2"] = "CUR_SPELL: Remove the debuff !!"
L["D23863_3"] = "CUR_SPELL: Don't stand still!"
L["D52053_1"] = "Zanzil summons Berserk: Go to blue cauldron and use buff on the big one!"
L["D52053_2"] = "MOVE ALL to the green Cauldron and get poison buff!"
L["D52053_3"] = "Zanzil summons zombies. Get red cauldron debuff and tank the adds!"
L["D52059_1"] = "CUR_SPELL: Interrupt the boss!!"
L["D52148_1"] = "All OUT of the CUR_SPELL!!"
L["D52148_2"] = "All IN the bubble!!!"
L["D52151_1"] = "CUR_SPELL on TAR_SPELL: only heal minimal!"
L["D52151_2"] = "CUR_SPELL on TAR_SPELL: is over."
L["D52151_3"] = "CUR_SPELL: Kill the raptor!"
L["D52151_4"] = "CUR_SPELL: Don't stand in front of the boss or near him."
L["D52155_1"] = "CUR_SPELL: Don't stand in front of the boss"
L["D52155_2"] = "CUR_SPELL: Stay in movement and go away from the green stuff following you!"
L["D52155_3"] = "CUR_SPELL: TAR_DUMM1 and TAR_DUMM2 walk away from each other!"
L["D52271_1"] = "Kill all the ghosts. If they reach a player they will kill you!!"
L["D52286_1"] = "Get all 15 yards away from boss !!!"
L["D52638_1"] = "TAR_SPELL is captured by bird!!"
L["D52730_1"] = "Leap on: TAR_SPELL!! Go to the chains"
L["DONTKNOW"] = "Si l'un d'entre vous ne connait pas la rencontre, qu'il tape !boss sur le canal de groupe pour avoir quelques explications."
L["DONTKNOW_OLD"] = "Si quelqu'un ne connait pas le combat de ce boss, qu'il tape !boss sur le canal de groupe pour obtenir quelques explications."
L["DRAG"] = "Shift + clic-gauche pour d\195\169placer"
L["EVERY_BOSS_ANNOUNCE_DESC"] = "D\195\168s qu'un boss est cibl\195\169, un message s'affiche sur le canal de groupe/raid pour inviter les joueurs \195\160 utiliser la commande !boss afin de connaitre la strat\195\169gie du combat."
L["EVERY_BOSS_ANNOUNCEPARTY"] = "Afficher quand cibl\195\169"
L["EVERY_BOSS_ANNOUNCERAID"] = "Afficher quand cibl\195\169 (en raid)"
L["GUILD"] = "Guilde"
L["HEAD_CHATCOMMANDS"] = "Chat Commands"
L["HELPER_BOSSPOST"] = "left-click: Post the Boss tactics in the channel you set up before. Check the Label below.\\ \
right-click: Select the channel where you want to post your tactics/loot information."
L["HELPER_POSTACHIEVEMENT"] = "Leftclick: Post achievements that are related to the current boss\
Rightclick: Post achievements that are related to the current instance"
L["HELPER_POSTLOOT"] = "Post the loot that the boss will drop when killed."
L["HELPER_SELECTBOSS"] = "You can manually select the current Boss but in most cases RobBossMods should automatically chose the correct Boss."
L["HINTEVERY"] = "Afficher une note quand cibl\195\169"
L["HINTEVERY_DESC"] = "Invite les joueurs \195\160 utiliser la commande !boss sur le canal de groupe quand le boss est cibl\195\169 (l'option doit \195\170tre activ\195\169e)."
L["HINTONCE"] = "Note de groupe"
L["HINTONCE_DESC"] = "Informations que vous pouvez envoyer sur le canal de groupe en utilisant le menu du clic-gauche."
L["IGN1"] = " wanted you to post tactics "
L["IGN2"] = " times in the last "
L["IGN3"] = " seconds. Do you want to ignore further requests from that person?\
\
Hint: You can also ignore a player by targeting that player and type /rbm ignore  or type /rbm ignore PLAYERNAME."
L["INFORM_PARTY"] = "Envoyer une note au groupe"
L["INSTANCE_CHAT"] = "Groupe/Raid"
L["LASTBOSS"] = "C'\195\169tait la derni\195\168re rencontre du donjon. Merci pour votre participation et bon jeu \195\160 tous."
L["LEFTCLICK"] = "Clic-gauche pour afficher la strat\195\169gie"
L["LOADING"] = "Loading"
L["LOOTFOR"] = "loot of "
L["MENU_AUTHOR"] = "/rbm author - D\195\169tails sur l'auteur"
L["MENU_DESCRIPTION"] = "RobBossMods affiche les strat\195\169gies pour les donjons h\195\169ro\195\175ques et les raids (en mode Normal et H\195\169ro\195\175que). Quand vous \195\170tes dans une instance, n'importe quel joueur peut prendre connaissance de la strat\195\169gie des boss en utilisant la commande !boss. Vous pouvez \195\169galement choisir manuellement le boss pour lequel vous souhaitez afficher la strat\195\169gie."
L["MENU_DYNAMIC1"] = "Activer les conseils en combat pour"
L["MENU_DYNAMIC2"] = ""
L["MENU_DYNAMIC_DESC"] = "Donner des conseils pour le combat en cours via le canal /dire "
L["MENU_DYN_NO"] = "Aucun conseil dynamique pour ce boss.\
Merci d'informer l'auteur de RBM de vos suggestions. Peut-\195\170tre pourront-elle \195\170tre impl\195\169ment\195\169es bient\195\180t ;)"
L["MENU_DYN_TOGGLE_A"] = "Activer les conseils."
L["MENU_DYN_TOGGLE_A_DESC"] = "Enable all dynamic hints infight. This is not available for all bosses yet."
L["MENU_DYN_TOGGLE_D"] = "D\195\169sactiver tous les conseils."
L["MENU_DYN_TOGGLE_D_DESC"] = "Disable all dynamic hints infight."
L["MENU_FRAME_OFF"] = "/rbm hide -- Fermer la fen\195\170tre RBM"
L["MENU_FRAME_ON"] = "/rbm show -- Ouvrir la fen\195\170tre RBM"
L["MENU_FRAME_TOGGLE"] = "/rbm toggle - Afficher/Cacher la fen\195\170tre RBM"
L["MENU_GENERAL"] = "Options g\195\169n\195\169rales"
L["MENU_HINT1"] = "!rbminfo sur le canal de groupe - Fourni les informations de l'addon aux membres du groupe"
L["MENU_MINIMAP_OFF"] = "/rbm minioff - Retire l'ic\195\180ne de la mini-carte"
L["MENU_MINIMAP_ON"] = "/rbm minion - Affiche l'ic\195\180ne sur la mini-carte"
L["MENU_RESETLANG"] = "/rbm resetlang - R\195\169initialise le choix de la langue"
L["MENU_STATE"] = "/rbm state - Statut de l'addon"
L["MINIMAP_SHOW"] = "Afficher le bouton sur la mini-carte"
L["MUST_RESTART"] = "You have to restart the addon to see the changes. You can do so by typing /reload in the chat."
L["MYSELF"] = "Moi-m\195\170me"
L["NEVER_OPT"] = "Ne jamais afficher automatiquement"
L["NEVER_SHOW_FRAME"] = "La fen\195\170tre RBM ne s'affichera pas automatiquement"
L["NEVER_SHOW_FRAME_DESC"] = "La nouvelle fen\195\170tre principale de RBM ne s'affichera JAMAIS automatiquement. Elle ne s'affichera que si vous tapez /rbm show ou en cliquant sur l'ic\195\180ne de la mini-carte."
L["NEVER_WARNING"] = "ATTENTION ! La fen\195\170tre principale de RBM ne s'affichera d\195\169sormais QUE lorsque vous cliquez sur l'icone de la mini-carte ou si vous tapez /rbm show dans la fen\195\170tre de dialogue."
L["NEW_VER1"] = "Vous avez RBM version"
L["NEW_VER2"] = "mais une nouvelle version est disponible : "
L["NEXT_BOSS_ANNOUNCE"] = "Afficher le boss suivant"
L["NEXT_BOSS_ANNOUNCE_DESC"] = "Annoncer au groupe quand le boss meurt et pr\195\169ciser lequel sera le prochain."
L["NOBOSS"] = "Aucun boss de s\195\169lectionn\195\169 !"
L["NONE"] = "Aucun"
L["N_SHOW"] = "ne jamais afficher automatiquement"
L["OFFICER"] = "Officier"
L["OPTIONS"] = "Options"
L["PARTY"] = "Groupe/Raid"
L["POSTON"] = "Afficher au..."
L["P_SHOW"] = "afficher lorsqu'un boss est cibl\195\169"
L["RAID"] = "Groupe/Raid"
L["RAIDS"] = "Raids"
L["RESET_CUSTOM"] = "R\195\169initialiser toutes le strat\195\169gies personnalis\195\169es."
L["RESET_CUSTOM_DESC"] = "R\195\169initialiser toutes les options. ATTENTION : La totalit\195\169 des configurations sera effac\195\169e."
L["RIGHTCLICK"] = "Clic-droit pour choisir un boss manuellement"
L["SAY"] = "Dire"
L["SCROLL"] = "Scroll"
L["SEL_MINIMAP_ICON"] = "select the minimap icon"
L["S_GHUR"] = "Ghu'sha a \195\169t\195\169 vaincu. Ozumat est le suivant."
L["S_HELIX"] = "Helix a \195\169t\195\169 vaincu. Le faucheur 5000 est le suivant."
L["S_OZUMAT"] = "Ozumat \195\169tait le dernier boss de l'instance. Merci \195\160 tous."
L["T100"] = "Le tank garde Godfrey dans un coin face au mur. Quand le boss lance SPELL(93520) le tank s'ecarte du cone d'effet du sort. \
Juste avant SPELL(93520) il SPELL(93707). Tankez les et dps pendant que le boss lance son SPELL(93520) dans le coin. \
Important: Dissipez TOUS LES MEMBRES rapidement a cause de SPELL(93629)."
L["T101"] = "SPELL(75722) : Cercle bleu au sol. Ecartez vous ! \
Tornades bleues: Restez bien a l'ecart sinon vous prendrez des degats et serez envoy\195\169 sur le cot\195\169 de la salle. \
SPELL(81477) : 2 secondes d'incantation. A INTERROMPRE sinon le tank risque de se faire tuer.\
Phase 2 (a 66% et 33%) : Elle invoque 2 sorcieres et un guerrier Nagas. \
DPS le CaC. Controler les sorcieres."
L["T102"] = "SPELL(76047) : Le boss frappe le sol du pied. Le tank doit se d\195\169placer pendant l'incantation du sort sous peine de se faire tuer. Les cercles s'aggrandissent au fur et a mesure. \
Le tank doit placer le boss sur les cotes de la salle et se deplacer en grand cercle. \
SPELL(95463) : Le boss attrape un membre du groupe au hasard et le serre pendant 6 secondes. Soignez le ! \
SPELL(76100) : Le boss enrage, il est conseill\195\169 au tank de rester a distance et de le kiter."
L["T103"] = "Tanker le boss dos au groupe. Il lance SPELL(76170) qui ne doit toucher que le tank. \
Peu apres il lance SPELL(91412) qui doit etre interrompu ! Ne restez pas dans les AoE de terre ! \
Quand Erunak passe sous 50% de sa vie, le Torve-esprit Ghur'sha choisi une nouvelle victime a controller. \
DPS le joueur controll\195\169, a hauteur de 50% de sa vie pour que le Torve-esprit le lache. \
SPELL(76234) : Flaque verte. Sortez en sinon vous ne pourrez attaquer/lancer des sorts. \
SPELL(76307) : Toute attaque magique contre Quand Ghur'sha le soigne. \
Utilisez Vol de sort, Expiation... Ou arretez de faire des degats magiques tant que le bouclier est actif !"
L["T104"] = "Phase 1 : Tuez le Sapeur sans visage rapidement. Le tank recupere l'elite pendant que le groupe le tue. AoE les murlocs. \
Phase 2 : Trois sans visage apparaissent. Tuez les pendant que le tank recupere les betes d'ombre et les kites. \
Le tank doit bien rester a l'ecart et faire en sorte qu'elles soient en dehors des flaques noires. \
Phase 3: Les membres du groupe recoivent un buff qui les fait grandir pendant qu'Ozumat fait des degats dans toute la salle. \
Tuez vite les adds restants, puis ciblez Ozumat pour le tuer au plus vite !"
L["T105"] = "SPELL(75272) : Des minions vont apparaitre sur votre position.\
Vous devez les kiter jusqu au tank de facon a ce qu'il les aoe.\
SPELL(75539) : Le groupe va etre teleport\195\169 aux pieds du boss. Detruisez les chaines au plus vite ! \
Ecartez-vous vite du boss car il va utiliser SPELL(95326)."
L["T106"] = "3 rayons SPELL(75610) sont canalis\195\169s sur les adds qui se tiennent pres du boss.\
Vous devez rester entre chaque add et le rayon qui le frappe \
de facon a ce que le minion ne soit pas touch\195\169 par le rayon \
(un distance peut le faire et quand meme DPS). \
Le rayon cause un debuff qui peut s'accumuler jusqu'a 100.\
Le joueur qui subit le rayon doit faire attention au compteur du debuff. \
Quand le nombre de debuffs atteint 80, le joueur doit s'ecater pour ne plus etre touch\195\169 par le rayon et le laisser canaliser sur l'add. \
Laissez le debuff se dissiper puis interposez-vous de nouveau. \
Le tank doit interrompre le sort SPELL(75823) qui effraye un joueur \
risquant de les faire partir du rayon."
L["T107"] = "Tout le groupe doit se placer sur le cercle interne de facon a ce que personne ne se trouve sur la grille en metal.\
Karsh est presque totalement immunis\195\169 contre tout grace a SPELL(75842). \
Afin de le rendre vulnerable le tank doit l'amener dans le pillier de feu au centre. \
Tant qu'il est au contact du feu il accumule un debuff toutes les 2 secondes qui augmente les degats subits. En contre partie il fait une AoE de feu. \
Le boss ne doit jamais perdre le debuff ! Si tel est le cas des adds apparaitront. Quelques secondes avant la fin du debuff, \
le tank doit ramener le boss sous le pillier de feu pour raffraichir la dur\195\169e du debuff."
L["T108"] = "Si les adds ne peuvent \195\170tre controll\195\169s (CC) ils doivent \195\170tre tu\195\169s en priorit\195\169 car il posent SPELL(99830). \
Notez que si l'add \"Nabot\" est tu\195\169 le boss enrage. \
Un anti-fear est tres utile. Ne restez pas dans le feu !"
L["T109"] = "Un DPS doit kiter les Ombres qui ne subissent aucun degats, mais \
lancent SPELL(76189) sur leur cible actuelle qui reduit les soins re\195\167us si le boss est proche. \
Les Ombres ne doivent pas \195\170tre a proximit\195\169 du tank ! Le boss en lui meme est facile. \
Obsidius va reguli\195\168rement posseder une de ses Ombres, prendre sa place et remettre l'aggro a zero."
L["T110"] = "Phase 1 : Corborus lances des \195\169clats violets au sol : SPELL(86881). Ecartez-vous en ! \
Les eclats font apparaitre des cristaux volants qui infligent de gros degats lorsqu'ils explosent. \
DPS \195\160 l'AoE les \195\169clats et cristaux rapidement. \
Phase 2 : Le boss s'enterre. Ecartez vous des remous du sol et tuez les minions \195\160 l'AoE."
L["T111"] = "Phase 1 : Peau-de-pierre est au sol. Le tank et le soigneur doivent etre en ligne de mire. \
Quand il lance SPELL(92265) tout le monde doit se cacher derriere un des rochers au sol. \
Phase 2 : Le Boss est en vol. Evitez les rochers qui tombent et les flaques de lave."
L["T112"] = "Ozruk doit etre tank\195\169 dos au groupe. SPELL(95331) et SPELL(95334) reflechissent les sort. \
SPELL (95338) : Ozruk frappe le sol devant lui causant de gros degats. \
Tank et CaC doivent se positionner derriere lui quand il lance ce sort. \
Il lance SPELL(95347) juste apres SPELL(92426). Les DPS distance s'\195\169loignent. \
Le tank et les CaC doivent rester a 15m du boss pour eviter son AoE."
L["T1122"] = "no tactics available yet"
L["T1123"] = "no tactics available yet"
L["T1128"] = "no tactics available yet"
L["T113"] = "SPELL(79351) est lanc\195\169 sur le joueur qui a l'aggro, idealement le tank : Ce sort fait de gros degats, il doit etre interrompu ! \
SPELL(79340) : Fait apparaitre des puits sombres sous un joueur au hasard. \
S'en ecarter au plus vite et essayer de creer un \"mur\" de puits d'ombre pour y pieger les minions. \
SPELL(79002) : Lance de gros rochers. La zone d'impact est visible au sol (tremblements). Sortez de la zone. \
SPELL(92663) : Lance une malediction qui augmente les dommages re\195\167us. Doit \195\170tre dissip\195\169e."
L["T1133"] = "Tank re-position the boss for the party to avoid SPELL(161588) and SPELL(162066).\
The ally marked with SPELL(163447) has to stay on the move to get not hit by SPELL(162058)"
L["T1138"] = "The bosses have to die at the same time. Rocketspark is un-tankable and should be ranged down.\
Borka should be tanked and attacked by melee DD. \
Tank: When Borka casts SPELL(161089) let him collide with Rocketspark interrupting his cast."
L["T1139"] = "Don't stand on purple runes when engaging boss. She uses SPELL(153240) on random player. Just move away.\
She spawns an add to her SPELL(153153). Nuke down the add.\
When she casts SPELL(164686) move away from the black and stand on WHITE runes. Leave that runes when the cast is over."
L["T114"] = "De petits cyclones vont apparaitre en cercle concentrique autour du boss. \
Ils viendront de temps en temps aur le boss. \
Tous ceux qui seront touch\195\169 par un cyclone subiront un effet de \"silence\" et des degats importants. \
Le plus simple est que tout le monde se tienne au cac et dps le boss a fond. \
Autrement, tous s'\195\169cartent du boss quand les cyclones se rapprochent."
L["T1140"] = "If you see a swirly under you get out! Try to avoid being in purple triangles as well.\
Once the boss casts SPELL(153804) get INTO the bad stuff you avoided earlier!\
At 50% HPhe spawns adds. Focus them down and do everything you did before.\
Healers watch out when adds have ~50% health they do more damage."
L["T1147"] = "no tactics available yet"
L["T1148"] = "no tactics available yet"
L["T115"] = "Tornades : Toute la salle est remplie de petites tornades qui se deplacent lentement. Evitez les ! \
Vent d'Altarius : Le boss cr\195\169e un vent dans une direction specifique (facile a voir si on regarde la scene d'en haut). \
Les DPS doivent se tenir au vent (vent dans le dos), \
SPELL(88308) : Le boss cible un joueur au hasard et souffle dans sa direction. Ne restez pas cote a cote \
sous peine de recevoir des degats indirectes du souffle.\
Quand les DPS se placent correctement ils beneficieront de SPELL(88282) qui augmente la hate."
L["T1153"] = "no tactics available yet"
L["T1154"] = "no tactics available yet"
L["T1155"] = "no tactics available yet"
L["T116"] = "SPELL(87618): Tout le monde recoit se debuff, enracin\195\169 pendant 18s. \
Ce debuff doit etre dissip\195\169 pour tous les joueurs. \
Triangle champ electique: Asaad lance trois boules d'\195\169clair au sol et les connecte en triangle. \
Tous les joueurs doivent se tenir dans le triangle sous peine de mourir par SPELL(86930). \
Chaine d'eclair: Essayez de ne pas tous rester proche."
L["T1160"] = "Tank the boss on the the edge not facing the group. Tank step aside when he casts SPELL(154442).\
If boss casts SPELL(175988) on you get away from the omen.\
After the 3rd omen he casts SPELL(154469). Break the chain by killing a skeleton (like Sylvana ghouls in End Time)."
L["T1161"] = "no tactics available yet"
L["T1162"] = "no tactics available yet"
L["T1163"] = "1st phase: Take boss down to 60% HP.\
2nd phase: Boss is untankable. Position so the boss cannot hit you with his gun. Make sure to cuddle making it easier for the tank to pick up adds.\
Damage Dealer: Use  SPELL(160702) to destroy the Assault Cannon. Avoid standing in SPELL(166570).\
3rd phase: Attack the boss and avoid red patches on the ground."
L["T1168"] = "Move out of the SPELL(152801) and get out of purple SPELL(153067) ASAP!\
Tank has to pick up the adds. They can be aoed down.\
When he casts SPELL(152979) kill your spirit and click it to receive a buff."
L["T117"] = "Le boss pose des SPELL(91259) au sol. Ecartez vous! \
Les mines peuvent devenir inactives (arretent de briller) mais restent dangereuse. \
SPELL(91263), De temps en temps le boss attrape et lance un joueur contre un mur. Soignez le! \
SPELL(83445): le boss arrete de bouger et des lignes de tremblement apparaissent au sol. Ecartez vous! \
Le soigneur doit rester en vue du tank, pas evident car le tank doit emener le boss a l'ecart des mines."
L["T118"] = "SPELL(81642) : Frappe le sol avec sa queue. Que les CaC restent sur ses flancs. \
SPELL(81690) : Cause des degats de saignement qui attire les crocodiles proches. \
Apportez les au tank et tuez les. \
Augh apparait et fait des degats sous forme d'un petit deluge. Evitez-le ! \
SPELL(81706) : Il enrage a 30% de saz vie. Annulez l'enrag\195\169 ou utilisez vos CD pour le tuer rapidement !"
L["T1185"] = "When he hurled SPELL(153480) protect yourself by getting behind it quickly.\
Don't tank boss too close or too far from the hurled Shield to ensure that the boss does not follow you behind the shield and you still can get behind it."
L["T1186"] = "DD interrupt SPELL(154218) and SPELL(154235). \
Healer keep in mind that SPELL(153477) does much damage to players standing outside the protective zone.\
Tank make sure to tank all adds."
L["T119"] = "Phase 1 : Tankez le boss a l'\195\169cart des pilliers de lumiere. \
Un phoenix apparait qui doit \195\169tre \195\169cart\195\169 du groupe par un DPS et kit\195\169. \
Phase 2 : a 50% de sa vie, le boss n'est plus vulnerable. \
Sortez de SPELL(88814) autour de lui. Un phoenix d'ombre apparait, il doit \195\170tre tank\195\169 hors de l'aoe autour du boss et vite tu\195\169. \
La phase 1 recommence quand le phoenix d'ombre meurt."
L["T1195"] = "no tactics available yet"
L["T1196"] = "no tactics available yet"
L["T1197"] = "no tactics available yet"
L["T1202"] = "no tactics available yet"
L["T1203"] = "no tactics available yet"
L["T1207"] = "Use spell interrupts or crowd control to prevent SPELL(168041) and SPELL(168105).\
Healer: Group will receive random damage through SPELL(168092) and SPELL(168040).\
"
L["T1208"] = "Boss casts fire then frost finally arcane spells. By interrupting SPELL(168885) you make her switch magic school.\
Jump over SPELL(166489) to avoid damage. Healers beware of arcane spells doing much damage to the team."
L["T1209"] = "Kill 8 Toxic Spiderlings to make boss attackable. Avoid SPELL(169275). \
Creatures that gain SPELL(169218) will become stronger over time.\
Healer dispell SPELL(169376). Tank position Venom-Crazed Pale Ones to be struck by bosses SPELL(169275) so that they die quicker."
L["T1210"] = "When boss casts SPELL(169613) step on any sprouting to prevent them from growing. Quickly destroy SPELL(169251) to keep kirin'tor alive.\
Kill adds so that you don't get overwhelme. Heal friendly mobs to keep them in the fight.\
Tank make sure to pick up adds that are spawning all the time."
L["T1211"] = "no tactics available yet"
L["T1214"] = "Lead SPELL(164294) away from group. Destroy Aqueous Globules before they can reach Whiterbark.\
SPELL(164538) deals AoE damage when a globule is killed.\
Tank: During SPELL(164357) face boss away from group. Prepare to pick up adds.)"
L["T1216"] = "When the boss is untargetable cleanup the adds ASAP.\
DD don't stand close to Blazing Tricksters to avoid SPELL(154018). Interrupt SPELL(154221) or kill casters before they can finish casting.\
Tank pick up the Felguards since they do heavy melee damage."
L["T122"] = "Quand le combat commence, Siamat re\195\167oit SPELL(84589). Tuez les minions et \195\169vitez les aoe (grises et vertes) au sol. \
Les minions cac doivent \195\170tre tank\195\169s. Une fois le bouclier de boss disparu, DPS boss. Le tank aggro les minions qui apparaissent."
L["T1225"] = "At 75% health he heals full and gets one of three abilities.\
Avoid standing near others when SPELL(156921) detonates. Interrupt SPELL(156854) and SPELL(156975) and DON'T dispell SPELL(156954)"
L["T1226"] = "Start with all damage cooldowns inclusive Bloodlust/Heroism and burst boss down as far as possible.\
When he casts SPELL(166168) leave the plattform and deactivate the rune conduits (Tank can also do this since the boss does not attack anyone in that phase).\
Be carefull because two spawned SPELL(154335) will rotate around the room. Use speed buffs/spells to avoid damage. "
L["T1227"] = "Face the boss away from the group. Whenever the boss chases someone (SPELL(161199)) interrupt that spell!\
Focus damage on the two adds and be careful to avoid the fire lines they create. Just step aside.\
Kill the boss when the adds are down and try to dispell SPELL(161203)."
L["T1228"] = "Tank side step the bosses charge and pull the boss away from flying axes and poison spots on the ground that have to be avoided from everyone.\
If possible do DoT Damage to the flying whelps above you.\
Healer be careful since the boss does significantly more damage when he reaches 70% health. At some time the boss dismounts. Save your damage reduction and bloodlust/heroism cooldowns for when that happens."
L["T1229"] = "Tank: Tank the boss a bit left or right from the center so it is obvious when he starts SPELL(155031). Make also sure to pick up all the adds when the boss flies above you (at 70% and 40% HP). Save damage reduction CD for when the boss lands at the end.\
Everyone run to the opposite site of the bridge when the boss spits fire i.e. SPELL(155031).\
AoE the Whelps down at 70% and 40% HP. When the boss lands at the end of the fight, stay BEHIND her."
L["T1234"] = "Bloodlust/Heroism at the start of the fight. Avoid her Whirlwind and get out of fire.\
When she leaves the platform take care of dragons that spite fire in a linear direction. \
You have to move all the time to avoid getting burned.\
While adds are active there is much damage income towards the tank. Healer be prepared!"
L["T1235"] = "Nok'gar cannot be directly targeted or attacked while mounted. Move quickly to get out of the line of fire of SPELL(164632) or SPELL(164648). \
Stop attacking boss when he is under the effect of SPELL(164426).\
Tank move Nok'gar and Dreadfang quickly away from the arrow barrages."
L["T1236"] = "Avoid damaging an Enforcer that is protected by SPELL(163689) until the effect fades.\
Avoid SPELL(163390) which immobilize victims and make them vulnerable to SPELL(163379).\
Spread out when Neesa prepares to fire her SPELL(163376)."
L["T1237"] = "Focus damage to quickly break Oshir out of SPELL(162424). Be alert when Oshir casts SPELL(178124) and focus adds.\
Focus healing on the victim of SPELL(162424)."
L["T1238"] = "Skulloc attacks with fierce melee strikes alongside the warrior Koramar and First Mate Zoggosh in a mechanized turret.\
Quickly hide behind cover when Skulloc begins SPELL(168929) working your way towards him during lulls between volleys.\
Do not linger near the rear of the ship during SPELL(168929) will incinerate you.\
"
L["T124"] = "Flammes bleues au sol SPELL(75117). Ecartez vous ! \
SPELL (74938): Le boss devient invulnerable. Sautez tous a gauche du boss dans la fosse. \
Le tank recupere les serpents, un joueur active le levier (10s). \
Le tank et le groupe se deplacent vers le second levier (a droite dans la fosse), la encore un joueur active le levier. \
Remontez et DPS les serpents qui restent. Interrompre SPELL(75322) et recommencez a tanker et DPS le boss. \
La phase des leviers dans la fosse se fait 2 fois."
L["T125"] = "Phase 1 : evitez les zones d'AoE grises. Le boss est tank\195\169 normalement. Phase 2 : Le boss est enterr\195\169, tanerk et DPS les minions qui apparaissent."
L["T126"] = "SPELL(76184) : Des cercles noirs et violets apparaissent au sol, evitez les ! \
SPELL(75623) : Cause des degats de groupe. Packez-vous pour faciliter les soins de groupe."
L["T1262"] = "no tactics available yet"
L["T127"] = "SPELL(74136) : Tournez-vous quand le boss lance le sort pour \195\169viter les degats et ne pas \195\170tre desorient\195\169. \
A 66% de sa vie, le boss se divise en 3 copies. \
A chaque copie correspond une comp\195\169tence du boss. Si la copie est tu\195\169 elle ne lui sera plus disponible. \
Choisissez la copie SPELL(89883) en premier, puis SPELL(74362) et finissez par SPELL(90760)."
L["T128"] = "Le boss se tank normalement. Un des DPS assign\195\169 s'occupe des pousses vertes qui apparaissent au sol continuellement. \
Elles soignent le boss et \195\169closent en minion. \
Si une spore apparait, le tank doit la r\195\169. Quand la spore meurt elle explose et pose SPELL(75701) au sol. \
Le tank positionne le boss dans la marque au sol en restant lui meme hors de la marque de facon \
a reduire les soins que le boss re\195\167oit, sans quoi le combat durera tres longtemps."
L["T129"] = "Setesh ne se tank pas. Le tank recup\195\168re les minions et les kite le plus longtemps possible. \
Attention : Le boss fait des cercles de degats au sol. Evitez les ! \
Setesh se deplace et cr\195\169e un portail. Tous les DPS doivent detruire le portail rapidement. \
Seulement 1 add et 2 creatures de mana doivent en sortir et etre recuper\195\169s par le tank. \
Les DPS se focalise sur les portails et le boss entre temps. \
Les minions ont un buff qui reduit les degats subits et ne doivent pas etre attaqu\195\169s."
L["T1291"] = "no tactics available yet"
L["T130"] = "SPELL(80352): Doit etre interrompu ! SPELL(87653) : Le boss cible un joueur. Sous le joueur cibl\195\169 le sol devient sombre. \
Ecartez vous car le boss va vous sauter dessus. De temps en temps il va se place au centre et faire des degats de zone. \
Bien soigner pendant ces degats de zones ! \
Le boss est aussi plus vulnerable pendant cette phase, c'est donc le moment d'utiliser Heroisme/Furie et autres buffs."
L["T131"] = "SPELL(74670) : Le boss va cibler un joueur puis apres 2 seconde le charger. \
Le joueur cibl\195\169 et ceux a proximit\195\169 ont 2 secondes pour s'\195\169carter rapidement sous peine de se faire tuer en un coup ! \
Il est judicieux sur ce combat que les joueurs s'ecartent les uns des autres. \
SPELL(74634) : Cause des degats en AoE autour du boss. Le tank et les joueurs CaC doivent s'\195\169carter. \
SPELL(74846) : Pose un debuff sur le tank et lui inflige de gros degats sur la dur\195\169e, \
ou jusqu'a que le tank soit soign\195\169 au dessus de 90% de sa vie. Le tank doit etre \"overheal\" pour toute la dur\195\169e du debuff. \
Troggs : 4 Troggs apparaissent. Un joueur DPS devrait les \195\169loigner du boss et les tuer. \
S'ils meurent proche du boss ce dernier enragera : SPELL(09169)."
L["T132"] = "SPELL(74884) : Throngus cr\195\169e de petits cercles a zone d'effet. Ne restez pas dedans ! \
S'il choisit 2 \195\169p\195\169es, le tank recoit un debuff magique qui lui inflige de gros degats. Le tank doit avoir de gros soins. \
S'il choisit la masse SPELL(90756), sa vitesse de deplacement est reduite de 70%, mais les degats qu'il inflige au tank sont augment\195\169s d'autant.\
Le tank doit kiter le boss et eviter de se faire toucher en gardant l'aggro ! \
Throngus lance aussi SPELL(90756) sur un joueur au hasard et lui inflige de lourds degats. \
Ce joueur devra etre bien soign\195\169 alors que le tank est encore en phase de kite...\
Si Throngus choisit le bouclier tous les DPS doivent se placer derriere lui car il absorbe 99% des degats subit de face \
et crache des flammes magiques."
L["T133"] = "Phase 1 : Le boss invoque un elementaire de feu : SPELL(75218) qui va cibler et marquer un joueur (ligne de feu).\
Le joueur cibl\195\169 doit rester a distance de l'elementaire sans se faire toucher ! \
Tous les autres DPS doivent tuer l'elementaire au plus vite sinon il explosera au contact de sa cible. \
Phase 2 : Les flammes du dragon vont couvrir la moiti\195\169 de la salle. Placez-vous derriere lui des qu'il incante."
L["T134"] = "SPELL(79466) : Cible un joueur au hasard et draine sa vie sur la dur\195\169e. \
Le joueur cibl\195\169 et ceux a proximit\195\169 doivent s'\195\169carter sous peine d'\195\170tre immobilis\195\169 et prendre des degats. \
SPELL(75694) : Cr\195\169e un cercle d'ombre au sol. \
Tous kes joueurs se placent au centre sous peine de prendre les degats de la tempete d'ombre. \
Pendant qu'il fait sa tempete d'ombre, faire un maximum de DPS. \
Apres la tempete d'Ombre, 2 adds apparaissent a l'entr\195\169e de la piece. Les minions doivent etre tu\195\169s au plus vite \
Diviser les DPS au besoin, les minions peuvent \195\170tre ralentis mais pas assom\195\169s. \
S'ils arrivent jusqu'aux oeufs ils soigneront le boss et feront apparaitre d'autres petits minions."
L["T139"] = "Tactiques sur Argaloth A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T140"] = "Spread out during this fight but leave an empty space for the raid to collapse on during the eye phase. Be ready to move if the boss channels SPELL(97028) on you. The tanks should always face Occu'thar away from the raid, so that no one other than himself is ever damaged by SPELL(96913). When the Eyes of Occu'thar spawn, everyone needs to group up and AOE down the eyes within 10 seconds or they will explode and damage the whole raid. "
L["T1426"] = "no tactic "
L["T1467"] = "Tactics for Tirathon Saltheril T1467"
L["T1468"] = "T1468 ash Golom"
L["T1469"] = "T1469 Glazer"
L["T1470"] = "T1470 Cordana Felsong"
L["T1479"] = "T1479 Serpantix"
L["T1480"] = "T1480 Warlord Patjesh"
L["T1485"] = "T1485 Hymdall"
L["T1486"] = "T1486 Hyrjia"
L["T1487"] = "T1487 Fenryr"
L["T1488"] = "T1488 God king"
L["T1489"] = "T1489 Odyn"
L["T1490"] = "T1490 Lady Hatecoil"
L["T1491"] = "T1491 King Deepbeard"
L["T1492"] = "T1492 Wrath of Azshara"
L["T1497"] = "T1497 Ivanyr"
L["T1498"] = "T1498 Corstalix"
L["T1499"] = "T1499 general xaxal"
L["T1500"] = "T1500 Nal tira"
L["T1501"] = "T1501 Advisor "
L["T1502"] = "T1502 Ymirion"
L["T1512"] = "T1512 Harbaron"
L["T1518"] = "T1518 Amalgam of souls"
L["T154"] = "Tactiques sur Conclave du Vent A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T155"] = "Tactiques sur Al'Akir A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T156"] = "Tactiques sur Halfus Brise-Wyrm A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T157"] = "Tactiques sur Valiona A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T158"] = "Tactiques sur le Conseil Ascendant A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T1653"] = "T1653 Ravencrest"
L["T1654"] = "T1654 Glaidalis"
L["T1655"] = "T1655 Oakheart"
L["T1656"] = "T1656 Dresaron"
L["T1657"] = "T1657 Shade of Xavius"
L["T1662"] = "T1662 Rokmora"
L["T1663"] = "T1663 helya"
L["T1664"] = "T1664 Smash"
L["T1665"] = "T1665 Ularogg"
L["T1667"] = "T1667 ursoc"
L["T167"] = "Tactiques sur Cho'gall A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T1672"] = "T1672 lord Kur"
L["T1673"] = "T1673 Naraxas"
L["T1686"] = "T1686 Mindflayer"
L["T1687"] = "T1687 Dragul"
L["T1688"] = "T1688 Manastorm"
L["T169"] = "Tactiques sur Syst\195\168me de D\195\169fense Omnitron A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T1693"] = "Stay behind Festerface to avoid his toxic vomit. Kill the Goo that he spawns after he vomits.\
Try to move the boss to a killed Goo so that he eats it." -- Requires localization
L["T1694"] = "T1694 Shivermaw"
L["T1695"] = "T1695 . Inquisitor Tormentorum"
L["T1696"] = "T1696 Anub esset"
L["T1697"] = "T1697 Sael orn"
L["T170"] = "Tactiques sur Magmagueule A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T1702"] = "T1702 BLood Princess"
L["T1703"] = "T1703 Nythendra"
L["T1704"] = "T1704 Dragons of Nightmare"
L["T1706"] = "T1706 Skroypon"
L["T171"] = "Tactiques sur Atramedes A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T1711"] = "T1711 Betrug"
L["T1713"] = "T1713 Krosus"
L["T1718"] = "T1718 Patrol Captain"
L["T1719"] = "T1719 Flamewrath"
L["T172"] = "Tactiques sur Chimaeron A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T1720"] = "T1720 Adviser melandrus"
L["T1725"] = "T1725 Cronomatic anomaly"
L["T1726"] = "T1726 Xavius"
L["T173"] = "Tactiques sur Maloriak A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T1731"] = "T1731 Trillax"
L["T1732"] = "T1732 Star augur"
L["T1737"] = "T1737 Guldan"
L["T1738"] = "T1738 Il gynoth"
L["T174"] = "Tactiques sur Nefarian A FAIRE (vous pouvez ajouter vos propres tactiques ici)"
L["T1743"] = "T1743 Grand Master Elis"
L["T1744"] = "T1744 Renferal"
L["T175"] = "Juste avant d'engager le boss prenez le buff vert dans le chaudron \195\160 l'ext\195\169rieur de la pi\195\168ce. Lorsque le combat commence, vous ne pouvez plus rafra\195\174chir le buff.\
Des t\195\162ches vertes de poison appara\195\174tront sur le sol, \195\169vitez de marcher dedans et sautez par dessus elles si vous n'avez pas le choix. Par moment, Venoxis cr\195\169e un SPELL(96477) entre deux joueurs causant un dommage constant aux deux joueurs et \195\160 ceux qui sont \195\160 cot\195\169. Cassez le lien en vous \195\169loignant l'un de l'autre, mais essayez de courir dans une direction o\195\185 il n'y a personne, car il y aura une explosion AOE lorsque le lien va casser. \195\137vitez \195\160 tout prix les flaques de venin sur le sol.\
Lorsque le boss se transforme en serpent, vous devez faire attention \195\160 ses fr\195\169quentes attaques de vomis SPELL(96753). Passez imm\195\169diatement derri\195\168re lui en le traversant ou sortez de son champ de vision (y compris le tank) quand vous voyez qu'il lance cette attaque.\
A un moment, Venoxis reculera vers son autel, en commen\195\167ant \195\160 caster SPELL(97099). \195\137vitez les vrilles de poison qu'il cr\195\169\195\169 au hasard en direction de joueurs. Apr\195\168s cela, il sera \195\169puis\195\169 pendant ~5 secondes et recommencera un nouveau cycle."
L["T1750"] = "T1750 Cenarius"
L["T1751"] = "T1751 Speedblade Aluriel"
L["T176"] = "Dans l\226\128\153ar\195\168ne, vous pouvez voir des esprits li\195\169s. Quand un membre du groupe meurt \195\160 cause de AFAIRE, cet esprit ressuscitera cette personne + vous donnera un buff. Vous ne pouvez pas \195\169viter la mort caus\195\169e par AFAIRE.\
Mandokir montera son raptor. D\195\168s que Mandokir descend de sa monture, il faut DPS le raptor aussi fort que possible car il essaie de tuer les esprits. Mandokir lance \195\169galement un sort en face de lui. C'est annonc\195\169 alors ne restez pas en face de lui!\
Quand Mandokir atteint 20%, il enrage, ce qui r\195\169duit son CD sur AFAIRE et augmente ses dommages de 100%. Lancez BL/TW/HR pour le tuer alors aussi vite que possible."
L["T1761"] = "T1761 high botanist"
L["T1762"] = "T1762 Tichondrius"
L["T177"] = "Gri'lek, un guerrier troll, est un mob typique de CAC qui cible r\195\169guli\195\168rement un joueur. Quand cela arrive, il grandit, provoque plus de dommages, et se d\195\169place relativement moins vite.\
Il dit une expression lorsqu'il cible un joueur, ce qui laisse le temps \195\160 tout le monde de bouger. \195\137vitez ses tremblements de terre et dissipez ses enracinements."
L["T178"] = "AFAIRE: tactiques pour l'event du Tranchant de la Folie"
L["T179"] = "Renataki, qui est par ailleurs un combat de tank, envoie r\195\169guli\195\168rement SPELL(96640) et SPELL(96645), une version de SPELL(51723) sur les st\195\169ro\195\175des. Sortez imm\195\169diatement de son trajet de cyclone jusqu'\195\160 ce qu'il ait fini d'attaquer et soignez la cible vis\195\169e."
L["T180"] = "Wushoolay, un chaman troll \195\169l\195\169mentaire, envoie divers sorts de foudre dont vous devez vous \195\169loignez. Les joueurs proches du SPELL(96699) doivent s'\195\169loigner rapidement pour ne pas \195\170tre tu\195\169s. \195\137vitez \195\169galement le SPELL(96713) et le SPELL(96711)."
L["T181"] = "Nettoyez l'int\195\169rieur du temple de tous les packs de trash, autrement ils interviendront dans le combat contre le boss. Utiliser les contr\195\180les (CC) sur les pr\195\170tres (oui c'est dur).\
Pendant que vous combattez Kilnara, ne la descendez pas en dessous de 50% de vie tant que vous n'avez tu\195\169 un par un tous les packs de panth\195\168res de sa pi\195\168ce. Par moment, elle incantera un mur de vague violet SPELL(96460) qui balaiera tous les joueurs devant lui \195\160 moins d'\195\170tre contourn\195\169.\
Elle envoie \195\169galement SPELL(96435), qui cause des dommages d'ombre aux joueurs proches. Il faudra l'interrompre.\
A 50%, elle se transforme et appelle \195\160 l'aide toutes les panth\195\168res que vous avez laiss\195\169es en vie. A ~25% elle dispara\195\174tra bri\195\168vement et r\195\169appara\195\174tra ensuite."
L["T184"] = "Zanzil attaquera sa cible avec des traits de l'ombre qui peuvent et doivent \195\170tre interrompus. Il lancera \195\169galement une capacit\195\169 d\195\169nomm\195\169e SPELL(96914) qui consiste essentiellement en un chemin de feu qui appara\195\174tra sous le tank et quelques m\195\168tres derri\195\168re lui. \195\137loignez vous en.\
Pendant la totalit\195\169 du combat, il utilisera au hasard une de ces trois capacit\195\169s qui peuvent \195\170tre contr\195\169es par les chaudrons situ\195\169s \195\160 proximit\195\169: \
\
SPELL(96319). Zanzil soul\195\168vera une nu\195\169e de zombies non-\195\169lites \195\160 partir de l'une des piles de corps \195\160 proximit\195\169. Le tank doit prendre un buff de feu d'enfer dans le chaudron de Sang Br\195\187lant et sauter dans les flaques nouvellement apparues.\
Les DPS doivent passer sur les zombies et les descendre.\
SPELL(96338). Zanzil remplira la zone de combat de gaz toxiques qui causent chaque seconde des dommages de Nature de 5/10% du total de vie. Obtenir le buff de SPELL(96328) aide \195\160 limiter les dommages.\
SPELL(96316). Zanzil va ranimer un des guerriers-fauve suspendus dans les coins de la zone. Comme pour les packs de trash avant le boss, le guerrier-fauve doit \195\170tre gel\195\169 en utilisant le chaudron de Formule de Br\195\187lure du Givre. Les DPS doivent ensuite passer du boss sur le guerrier-fauve et le tuer rapidement."
L["T185"] = "Le combat consiste essentiellement en un jeu d'aggro et de DPS (tank-and-spank) avec quelque mouvements. Allez dans la bulle protectrice violette de Jin'do, et laissez le boss \195\160 l'ext\195\169rieur. Le tank peut rester \195\160 l'ext\195\169rieur aussi longtemps que Jin'do encha\195\174ne son gros sort.\
La phase 2 commencera apr\195\168s quelque temps (peut-\195\170tre une dur\195\169e fixe?) et Jin'do sera occup\195\169 autrement. Vous devez aggro l'Esprit de Gurubashi et lui faire d\195\169truire les boucliers autour des Cha\195\174nes de Hakkar et groupant vos membres du groupe (sauf le tank) sur une cha\195\174ne et en ayant le troll qui saute sur eux.\
Lorsque le bouclier est d\195\169truit, tuez l'esprit, puis concentrez votre feu et d\195\169truisez la cha\195\174ne. Il faut ensuite aggro un nouvel esprit pour la cha\195\174ne suivante, et ainsi de suite. Attirez les petits adds vers les cha\195\174nes afin que le tank les tiennent, et avec un peu d'AOE ils devraient bien descendre. \195\137vitez les taches noires/violettes qui flottent entre vous."
L["T186"] = "Une nouveaut\195\169 dans cet affrontement: le kidnappeur Amani. Il s'agit d'un aigle qui saisi de temps en temps un membre du groupe, en envoyant SPELL(97318). Le groupe, y inclus le joueur saisi, doit switcher sur le kidnappeur Amani et lib\195\169rer le joueur.\
Akil'zon envoie \195\160 nouveau SPELL(97299) sur le tank et SPELL(97298) sur des joueurs au hasard, qui se stackent mais peuvent \195\170tre enlev\195\169s.\
Quand SPELL(97300) est envoy\195\169, le joueur cibl\195\169 sera suspendu dans les airs et enverra un orage \195\169lectrique \195\160 travers toute la plateforme. Le groupe doit se r\195\169unir sous le joueur cibl\195\169 pour \195\169viter les dommages. "
L["T187"] = "En phase 1, la phase du troll, Nalorakk envoie SPELL(42384) sur le tank.\
Il utilise SPELL(42402) sur le joueur le plus \195\169loign\195\169 de lui. Cela aide d'avoir un DPS distant et un heal qui alternent dans cette pouss\195\169e.\
En phase 2, il se transforme en ours. Il envoie SPELL(97811) sur le tank et SPELL(42398) sur le groupe."
L["T188"] = "Jan'alai envoie SPELL(97488) en direction d'un joueur au hasard. C'est facilement \195\169vitable, en particulier si vous restez proche de sa zone de CAC (hitbox).\
Par moments, Jan'alai invoquera SPELL(42630). Ces bombes oranges vont appara\195\174tre al\195\169atoirement au centre de la pi\195\168ce et exploser quelques secondes plus tard, laissant de grands espaces dans lesquels les joueurs peuvent se mettre \195\160 l'abri et continuer de combattre.\
Jan'alai enverra SPELL(45340), des mobs vont courir vers les tas d'oeufs de chaque c\195\180t\195\169 de l'autel et commencer \195\160 les faire \195\169clore jusqu'\195\160 ce qu'ils soient tu\195\169s.\
\
Les jeunes faucons-dragons Amani \195\169clos, qui envoie des SPELL(43299), doivent \195\170tre descendu en AOE et le DPS entra\195\174nera de l'aggro. Il est conseill\195\169 d'avoir plusieurs vagues de jeunes faucons-dragons plut\195\180t que de tous les faire appara\195\174tre en m\195\170me temps."
L["T189"] = "Dans la phase troll, Halazzi pose SPELL(97499) qui buff le boss. Ce totem doit \195\170tre tu\195\169; toutefois, vous pouvez toujours \195\169carter  le boss du cercle de gu\195\169rison vert et continuez de focus en DPS. Il envoie \195\169galement SPELL(43139).\
A 66% et 33%, Halazzi invoquera l'Esprit du Lynx et gu\195\169rira \195\160 100%. Les DPS doivent switcher sur l'Esprit du Lynx et se grouper afin de maximiser le DPS, dans la mesure o\195\185 l'Esprit du Lynx n'a pas l'aggro traditionnelle et attaquera les joueurs au hasard.\
Il enverra \195\169galement SPELL(43243) et SPELL(43290) qui peuvent \195\170tre particuli\195\168rement durs pour les DPS tissus. Les DPS doivent switcher pour tuer les Totems de Foudre Corrompus avant qu'il ne fasse de tr\195\168s lourds montant de dommages.\
Une fois que l'Esprit du Lynx est mort, les DPS retournent sur Halazzi."
L["T190"] = "Il est escort\195\169 par deux adds au hasard, qui peuvent \195\170tre contr\195\180l\195\169s et ignor\195\169s pendant la dur\195\169e du combat.\
Il va alterner entre des lancers de SPELL(43882) au groupe et une canalisation de SPELL(43501). Plus le combat se prolonge, plus vous aurez de stacks de SPELL(44131), qui rendront Malacrass difficile \195\160 tuer.\
Usez de votre bon sens lorsque vous utilisez toutes les capacit\195\169s temporaires des joueurs. Interrompez les soins, \195\169cartez vous des tourbillons de lames, et supprimez les debuffs.\
Il lance drain d'\195\162me au hasard sur un membre du raid, absorbant ses comp\195\169tences en amplifiant leur puissance."
L["T191"] = "Il se transforme plusieurs fois, ce qui constitue une menace de wipe, et est immunis\195\169 contre SPELL(355) mais les debuffs (et DOT) resteront sur lui.\
Dans la premi\195\168re phase, Daakara envoie SPELL(15576) et SPELL(97639), qui n\195\169cessitent que le joueur soit soign\195\169 pour les enlever. Les CAC qui se prendront un cyclone pendant qu'ils seront affect\195\169s par le Lancer Effroyable pourront vite se retrouver morts. A 80% et 40%, Daakara se transforme au hasard en deux parmi les quatre formes animales restantes.\
En SPELL(42606), Daakara cr\195\169\195\169 un SPELL(43983) et envoie SPELL(43112) que les joueurs doivent esquiver. Les joueurs qui percutent les cyclones seront touch\195\169s par SPELL(97646). Soyez certains de tuer le SPELL(97930) afin d'\195\169viter que des dommages suppl\195\169mentaires n'apparaissent.\
En SPELL(42594), Daakara envoie SPELL(43095) qui peut \195\170tre suivi d'un d\195\169sagr\195\169able SPELL(42402), ainsi que SPELL(43456) sur le tank. Enlever le stun et utilisez des cd sur le tank.\
En SPELL(42608), Daakara cr\195\169\195\169 une Colonne de Feu et envoie de temps en temps SPELL(43208) et SPELL(97855). Tous les joueurs doivent rester sur le qui-vive et constamment \195\169viter le feu.\
En SPELL(42607), Daakara envoie deux adds qui peuvent rapidement tuer un joueur s'ils ne sont pas tu\195\169s rapidement, entre SPELL(97673) et SPELL(97672)."
L["T192"] = "TODO: Tactics on Beth'tilac. Feel free to edit it for yourself!"
L["T193"] = "TODO: Tactics on Rhyolith. Feel free to edit it for yourself!"
L["T194"] = "TODO: Tactics on Alysrazor. Feel free to edit it for yourself!"
L["T195"] = "TODO: Tactics on Shannox. Feel free to edit it for yourself!"
L["T196"] = "TODO: Tactics on Baleroc. Feel free to edit it for yourself!"
L["T197"] = "TODO: Tactics on Majordomo Stagehelm. Feel free to edit it for yourself!"
L["T198"] = "TODO: Tactics on Ragnaros. Feel free to edit it for yourself!"
L["T283"] = "The challenge to this fight is avoiding as many spell effects as possible. Standing inside the purple zone will slow casting by 50%, but casters outside the zone will need to move more often to avoid harmful spells.\
Tyrande will cast SPELL(102173) which should be interrupted by the tank and melee. She casts SPELL(102151) which splits into three beams after traveling a short distance. She also summons SPELL(102605) which travels around the circle and silences anyone it hits.\
She buffs herself to cast faster at 80% and 55% HP, and at 30% she casts SPELL(102241) causing arcane damage to rain down making it more difficult to stay alive."
L["T284"] = "no tactic available yet"
L["T285"] = "Jaina will frequently use SPELL(101927) to throw a growing ember on the ground which will explode for massive damage unless someone steps on it, so everyone should keep an eye out for these. She will occasionally teleport away from the tank and cast SPELL(101339) toward him and then cast SPELL(101810) three times, which should be interrupted if possible."
L["T286"] = "no tactic available yet"
L["T289"] = "Murozond has a frontal SPELL(102569) and a rear SPELL(108589), so the tank should be careful about positioning the boss. Slowly kiting the boss in a circle around the hourglass while the dps attack from the side should work well.\
When the hourglass in the center of the area is clicked, all HP and mana will be restored to full, all dead players will be revived, and all cooldowns will be reset, so use everything you have to nuke down his large heath pool.\
The longer the fight lasts, the faster the boss will cast SPELL(101984) which will become more difficult to avoid.\
"
L["T290"] = "The boss casts SPELL(108141) on the ground periodically, creating a circle of fire that you must avoid. He will cast SPELL(104905) causing a large amount of damage and triggering a phase where Illidan helps you hide.\
During this phase you should move far away from the center and group up for heals, and then move together avoid the eyes that search for you. If a player is detected, the boss will stun and attack that person.\
The boss enrages at low HP causing additional damage to the tank."
L["T291"] = "Interrupt Azshara's SPELL(103241) within 8 seconds. Break players out of mind control by killing the hand. Kill the magi and stun/interrupt if you can."
L["T292"] = "Keep moving during SPELL(103888) to avoid fire. When Tyrande gets stunned,  kill the Dreadlord Debilitators to free her. If you stand in the beam of light you can do big damage to the Doomguards. Pop cooldowns and burn Mannoroth in phase 2."
L["T302"] = "no tactic available yet"
L["T303"] = "no tactic available yet"
L["T311"] = "During the Crystal Phase, the tanks need to regularly taunt the boss off of each other and a few DPS players need to soak the proximity damage done by crystals that the boss summons. The closer you are to the crystal, the less damage it will do.\
During the Black Blood phase, you'll be pulled in toward the boss and must run behind one of the nearby pillars to avoid standing in the black blood.\
The boss enrages at 20% causing additional tank damage."
L["T317"] = "Replace this text with your own tactics for Hagara the Stormbinder"
L["T318"] = "Replace this text with your own tactics for Spine of Deathwing"
L["T322"] = "Avoid ice circles on the ground. If Thrall gets frozen in ice then break him out. He spams an interruptable SPELL(102593) on the tank.\
At 30% health he will channel SPELL(103962) dealing AoE frost damage to everyone until he is killed. "
L["T323"] = "Players should spread out to avoid AoE damage from SPELL(101404). Sylvanas will pull everyone toward her and then summon a ring of ghouls linked by purple beams.\
You must single target a ghoul and kill it in order to escape the ring before it collapses because touching a beam will kill you instantly. \
It is helpful to use a raid marker to indicate which ghoul to kill."
L["T324"] = "Replace this text with your own tactics for Warlord Zon'ozz"
L["T325"] = "Replace this text with your own tactics for Yor'sahj the Unsleeping."
L["T331"] = "Replace this text with your own tactics for Ultraxion"
L["T332"] = "Replace this text with your own tactics for Warmaster Blackhorn"
L["T333"] = "Replace this text with your own tactics for the Madness of Deathwing"
L["T335"] = "Boss rotates in two phases:\
Phase 1: Spread out and DPS.\
Phase 2: First kill the healers add! Then try to kill as much adds as possible before switching to phase 1 again.\
Adds are not tauntable"
L["T339"] = "Tactics for Alizabal TODO (feel free to add your own tactic here)"
L["T340"] = "Ranged spread out to different platforms. The boss will SPELL(101614) at a random person knocking you back. You can pick up the totem and throw it back at him increasing his damage taken by 50%. "
L["T341"] = "Benedictus will summon a large wave that moves across the platform. Don't stand in front of it. Avoid glowing circles on the ground.\
He spams SPELL(104503) and SPELL(104504) which can be interrupted."
L["T342"] = "Asira will cast SPELL(102726) on a random caster in the group. This person should try to keep the tank or a melee DPS between them and the boss to avoid being silenced.\
Asira should be moved out of SPELL(103558) and other players should move out too."
L["T369"] = "no tactic available yet"
L["T370"] = "no tactic available yet"
L["T371"] = "no tactic available yet"
L["T372"] = "no tactic available yet"
L["T373"] = "no tactic available yet"
L["T374"] = "no tactic available yet"
L["T375"] = "no tactic available yet"
L["T376"] = "no tactic available yet"
L["T377"] = "no tactic available yet"
L["T378"] = "no tactic available yet"
L["T379"] = "no tactic available yet"
L["T380"] = "no tactic available yet"
L["T381"] = "no tactic available yet"
L["T382"] = "no tactic available yet"
L["T383"] = "no tactic available yet"
L["T384"] = "no tactic available yet"
L["T385"] = "no tactic available yet"
L["T386"] = "no tactic available yet"
L["T387"] = "no tactic available yet"
L["T388"] = "no tactic available yet"
L["T389"] = "no tactic available yet"
L["T390"] = "no tactic available yet"
L["T391"] = "no tactic available yet"
L["T392"] = "no tactic available yet"
L["T393"] = "no tactic available yet"
L["T394"] = "no tactic available yet"
L["T395"] = "no tactic available yet"
L["T396"] = "no tactic available yet"
L["T397"] = "no tactic available yet"
L["T398"] = "no tactic available yet"
L["T399"] = "no tactic available yet"
L["T400"] = "no tactic available yet"
L["T401"] = "no tactic available yet"
L["T402"] = "no tactic available yet"
L["T403"] = "no tactic available yet"
L["T404"] = "no tactic available yet"
L["T405"] = "no tactic available yet"
L["T406"] = "no tactic available yet"
L["T407"] = "no tactic available yet"
L["T408"] = "no tactic available yet"
L["T409"] = "no tactic available yet"
L["T410"] = "no tactic available yet"
L["T411"] = "no tactic available yet"
L["T412"] = "no tactic available yet"
L["T413"] = "no tactic available yet"
L["T414"] = "no tactic available yet"
L["T415"] = "no tactic available yet"
L["T416"] = "no tactic available yet"
L["T417"] = "no tactic available yet"
L["T418"] = "no tactic available yet"
L["T419"] = "no tactic available yet"
L["T420"] = "no tactic available yet"
L["T421"] = "no tactic available yet"
L["T422"] = "no tactic available yet"
L["T423"] = "no tactic available yet"
L["T424"] = "no tactic available yet"
L["T425"] = "no tactic available yet"
L["T427"] = "no tactic available yet"
L["T428"] = "no tactic available yet"
L["T429"] = "no tactic available yet"
L["T430"] = "no tactic available yet"
L["T431"] = "no tactic available yet"
L["T432"] = "no tactic available yet"
L["T433"] = "no tactic available yet"
L["T434"] = "no tactic available yet"
L["T435"] = "no tactic available yet"
L["T438"] = "no tactic available yet"
L["T439"] = "no tactic available yet"
L["T440"] = "no tactic available yet"
L["T441"] = "no tactic available yet"
L["T442"] = "no tactic available yet"
L["T443"] = "no tactic available yet"
L["T445"] = "no tactic available yet"
L["T446"] = "no tactic available yet"
L["T448"] = "no tactic available yet"
L["T449"] = "no tactic available yet"
L["T450"] = "no tactic available yet"
L["T451"] = "no tactic available yet"
L["T452"] = "no tactic available yet"
L["T453"] = "no tactic available yet"
L["T454"] = "no tactic available yet"
L["T455"] = "no tactic available yet"
L["T456"] = "no tactic available yet"
L["T457"] = "no tactic available yet"
L["T458"] = "no tactic available yet"
L["T459"] = "no tactic available yet"
L["T463"] = "no tactic available yet"
L["T464"] = "no tactic available yet"
L["T465"] = "no tactic available yet"
L["T466"] = "no tactic available yet"
L["T467"] = "no tactic available yet"
L["T469"] = "no tactic available yet"
L["T470"] = "no tactic available yet"
L["T471"] = "no tactic available yet"
L["T472"] = "no tactic available yet"
L["T473"] = "no tactic available yet"
L["T474"] = "no tactic available yet"
L["T475"] = "no tactic available yet"
L["T476"] = "no tactic available yet"
L["T477"] = "no tactic available yet"
L["T478"] = "no tactic available yet"
L["T479"] = "no tactic available yet"
L["T480"] = "no tactic available yet"
L["T481"] = "no tactic available yet"
L["T482"] = "no tactic available yet"
L["T483"] = "no tactic available yet"
L["T484"] = "no tactic available yet"
L["T485"] = "no tactic available yet"
L["T486"] = "no tactic available yet"
L["T487"] = "no tactic available yet"
L["T489"] = "no tactic available yet"
L["T523"] = "no tactic available yet"
L["T524"] = "no tactic available yet"
L["T527"] = "no tactic available yet"
L["T528"] = "no tactic available yet"
L["T529"] = "no tactic available yet"
L["T530"] = "no tactic available yet"
L["T531"] = "no tactic available yet"
L["T532"] = "no tactic available yet"
L["T533"] = "no tactic available yet"
L["T534"] = "no tactic available yet"
L["T535"] = "no tactic available yet"
L["T536"] = "no tactic available yet"
L["T537"] = "no tactic available yet"
L["T538"] = "no tactic available yet"
L["T539"] = "no tactic available yet"
L["T540"] = "no tactic available yet"
L["T541"] = "no tactic available yet"
L["T542"] = "no tactic available yet"
L["T543"] = "no tactic available yet"
L["T544"] = "no tactic available yet"
L["T545"] = "no tactic available yet"
L["T546"] = "no tactic available yet"
L["T547"] = "no tactic available yet"
L["T548"] = "no tactic available yet"
L["T549"] = "no tactic available yet"
L["T550"] = "no tactic available yet"
L["T551"] = "no tactic available yet"
L["T552"] = "no tactic available yet"
L["T553"] = "no tactic available yet"
L["T554"] = "no tactic available yet"
L["T555"] = "no tactic available yet"
L["T556"] = "no tactic available yet"
L["T557"] = "no tactic available yet"
L["T558"] = "no tactic available yet"
L["T559"] = "no tactic available yet"
L["T560"] = "no tactic available yet"
L["T561"] = "no tactic available yet"
L["T562"] = "no tactic available yet"
L["T563"] = "no tactic available yet"
L["T564"] = "no tactic available yet"
L["T565"] = "no tactic available yet"
L["T566"] = "no tactic available yet"
L["T568"] = "no tactic available yet"
L["T569"] = "no tactic available yet"
L["T570"] = "no tactic available yet"
L["T571"] = "no tactic available yet"
L["T572"] = "no tactic available yet"
L["T573"] = "no tactic available yet"
L["T574"] = "no tactic available yet"
L["T575"] = "no tactic available yet"
L["T576"] = "no tactic available yet"
L["T577"] = "no tactic available yet"
L["T578"] = "no tactic available yet"
L["T579"] = "no tactic available yet"
L["T580"] = "no tactic available yet"
L["T581"] = "no tactic available yet"
L["T582"] = "no tactic available yet"
L["T583"] = "no tactic available yet"
L["T584"] = "no tactic available yet"
L["T585"] = "no tactic available yet"
L["T586"] = "no tactic available yet"
L["T587"] = "no tactic available yet"
L["T588"] = "no tactic available yet"
L["T589"] = "no tactic available yet"
L["T590"] = "no tactic available yet"
L["T591"] = "no tactic available yet"
L["T592"] = "no tactic available yet"
L["T593"] = "no tactic available yet"
L["T594"] = "no tactic available yet"
L["T595"] = "no tactic available yet"
L["T596"] = "no tactic available yet"
L["T597"] = "no tactic available yet"
L["T598"] = "no tactic available yet"
L["T599"] = "no tactic available yet"
L["T600"] = "no tactic available yet"
L["T601"] = "A random player will get SPELL(72452) so heal him or he'll get 16k shadow damage.\
You should disspell SPELL(72426) within 6 seconds. He will cast other healing reducing debuffs so you have to heal through it"
L["T602"] = "Get out of SPELL(72362). When SPELL(72369) is dispelled the not dealt DoT-damage will be spread instantly over the party, so let it tick a while.\
On occasion he will reduce your health by 50% so once again very heal intensive!"
L["T603"] = "very simple in theory. LK will summon some bad guys and you have to kill them ASAP. Don't let the LK touch you and try to not get your healer sliced and focus caster and fat guys."
L["T604"] = "no tactic available yet"
L["T605"] = "no tactic available yet"
L["T606"] = "no tactic available yet"
L["T607"] = "no tactic available yet"
L["T608"] = "Boss casts SPELL(70336) regular and it stacks. He also SPELL(68788) at a random player, go away from the dropping zone.\
SPELL(70336) won't hit you when hiding behind the saronite. Every partymember should try to hide behind them every now and then so that the frost damage won't stack too high."
L["T609"] = "The boss has basically three options which he casts every now and then.\
SPELL(70434): Run away from him especially melee and tank!\
SPELL(69263): Everbody move! Don't stand still to avoid the blue explosions.\
SPELL(68987): The person that is pursuited has to run away from the boss."
L["T610"] = "Rimefang (dragon) on occasion marks an area. Leave that area, because some frost patches will appear there.\
Player who gets SPELL(69172) should stop DPS immediately because otherwhise will bypass DPS to tank.\
If boss gets SPELL(69629) you should try to kite him over the frost patches on the ground so he can't hit you."
L["T611"] = "no tactic available yet"
L["T612"] = "no tactic available yet"
L["T613"] = "no tactic available yet"
L["T614"] = "no tactic available yet"
L["T615"] = "SPELL(68793) is his main attack. He will cast SPELL(68893) on a random player causing him to seperate his soul. Don't let the soul reach the bos or he will get healed!\
When boss reaches 30% health he will SPELL(68872) and randomally Fear a player. Dispell the fear and nuke him down"
L["T616"] = "Try to interrupt her main attack SPELL(70322) as often as possible. When she casts SPELL(69051) stop all of your damage immediately or the debuffed player will die.\
On occasion she casts SPELL(68939) summoning many ghosts. Run away if the damage taken is too high.\
Finally she cats SPELL(68912) creating a wall which is killing you on contact. She rotates 90 degrees so go with her to avoid contact."
L["T617"] = "no tactic available yet"
L["T618"] = "no tactic available yet"
L["T619"] = "no tactic available yet"
L["T620"] = "no tactic available yet"
L["T621"] = "no tactic available yet"
L["T622"] = "no tactic available yet"
L["T623"] = "no tactic available yet"
L["T624"] = "no tactic available yet"
L["T625"] = "no tactic available yet"
L["T626"] = "no tactic available yet"
L["T627"] = "no tactic available yet"
L["T628"] = "no tactic available yet"
L["T629"] = "no tactic available yet"
L["T63"] = "no tactic available yet"
L["T630"] = "no tactic available yet"
L["T631"] = "no tactic available yet"
L["T632"] = "no tactic available yet"
L["T634"] = "no tactic available yet"
L["T635"] = "no tactic available yet"
L["T636"] = "no tactic available yet"
L["T637"] = "no tactic available yet"
L["T638"] = "no tactic available yet"
L["T639"] = "no tactic available yet"
L["T640"] = "no tactic available yet"
L["T641"] = "A very easy tank and spank. When she casts the ritual make sure you kill the 3 adds with AoE otherwise you will get a DoT-Debuff that ticks very high.\
"
L["T642"] = "Boss will activate all 4 adds one after another. The adds are easy tank and spank and don't have any specialty.\
The boss itself cleaves so don't stand in front of him (if you are no tank)."
L["T643"] = "Adds are spawning. Kill them and use the spears when the boss is near the cannon. If you hit him three times he will \"land\".\
Boss will DOT a player with SPELL(59331). Melee: make sure to run away from his SPELL(59322)"
L["T644"] = "Well the boss does many abilities but infact you can just DPS him down and heal your party like crazy.\
It's like most of Wotlk 5-mans. you don't really need tactics at all for most of the bosses."
L["T649"] = "At the beginning the boss ins nearly invincible. He charges towards a door, don't be close to the impact zone. DPS have to go into the cannons so that they attack is Vulnerability. Tank and heal stay tanking the adds that will spawn. Every now and then focus (not AoE) the adds down.\
Phase 2: If the vulnerability is down boss will fixate a player. Just run if fixated. avoid the tornadoes and nuke the boss that is getting 300% extra damage."
L["T654"] = "Assemble yourself in the middle to prevent his SPELL(111218). Tank him with his back pointing to the party because he cleaves.\
On occassion he SPELL(82137) and two adds will appear. Focus the adds down ASAP. you have to break their armor first by doing 60k damage.\
When he starts to whirl around with SPELL(111216) make sure to not get hit by it. He will move very quick around the whole room."
L["T655"] = "He throws SPELL(107130) on the ground. These explosions won't explode yet. He also will throw explosives at the back of a player but those will explode into all 4 main directions. If that explosion hits other explosions the also will explode (in the 4 main directions).\
So make sure to stand crosswise to the inactive explosive so that they will be triggered! its important that you trigger the explosives.\
If boss reaches 70% and 30% he will detonate ALL remaining explosives on the ground, again, its important to stand crosswise so that you will trigger the inactive explosives when the explosive thrown at you explodes in 4 directions like a cross."
L["T656"] = "Boss will cast SPELL(113691) and SPELL(11366) as main attacks. Try to interrupt that spells as often as possible. If he buffs himself with SPELL(113682) outheal the extra damage or spellsteal it as mage.\
Make sure you stand behind him because he casts SPELL(113653). If so the tank has to move in a circle around him so the breath won't hit him. Intercept the fyling books or if not, avoid the fire patches on the ground."
L["T657"] = "Students: run away when fixated and just kill the two students.\
Boss Phase 1: Get behind him when he uses SPELL(113656) and get away when he uses SPELL(106433).\
Phase 2: On occassion he will split himself into 3. Avoid the SPELL(106470) (blue ones) and kill the copies.\
Phase 3: Get Away when chased by the boss and stop attacking the boss when uses SPELL(106447)."
L["T658"] = "Phase 1: Avoid SPELL(106938) and remove the fire DoT from the tank. (SPELL(106823))\
Phase 2: The DoT no longer is removeable so outheal the damage and the heal absorb.\
Phase 3: Tank the Jade Serpent, spread out and outheal his SPELL(107110)"
L["T659"] = "Phase 1: Boss will sweep an SPELL(111231) through the room so have to kill her very fast! Players who get debuffed with SPELL(111610) should stay away from the party. SPELL(111631) will jump from player to player after doing 1 ticks of damage\
Phase 2: Boss will esacpe to her phylactery. Kill it as fast as possible!. Flying books will do arcane bomb or fire damage. Avoid them as good as you can."
L["T660"] = "Boss will do SPELL(114020) and throw a knife at a player. Spread out and don't stand close to eachother because everybody in  flying path of the knive will get SPELL(114056) a dot that stacks. He also leaps on random player and does AoE which also generates SPELL(114056) so get away from that.\
At 90% 80% 70% and 60% he will SPELL(114259). DPS switch to the dog and then back to the boss otherwise the dogs will overwhelm you!\
At 50% he soft-enrages doing more damage and attacking faster."
L["T663"] = "Phase 1: SPELL(114062) is a frontal cone you should really avoid (including the tank!). She puts SPELL(114035) on the ground those are also very deadly so avoid them\
Phase 2: (66%, 33% of health): She copies herself. Just kill the copies  one after another. You will constantly get arcande dmg in phase 2. If you kill the wrong copy you also get damage so don't AoE all the copies."
L["T664"] = "There are two possible encounter.\
Zao Sunseeker: Kill the Suns, aoe the adds, kill the boss\
Anger and Strife: Start with Anger and stop DPS when he stacks 8 times switching to Strife and vice versa, so the SPELL(113315) will be consumed by SPELL(113379)\
"
L["T665"] = "Boss constantly will stack SPELL(113765) so if the damage gets too high the tank can kite him around because the buff reduces is walk speed.\
On occasion he will cast SPELL(113999) which can really hurt, but you can protect yourself with a buff gained by clicking on the bone piles.\
Also you have to stay out of any fire effect which will cover a half of the room SPELL(114009)."
L["T666"] = "Phase 1: Don't stand together (spread your party). Boss will leap on a random player SPELL(111775) and causes AoE Damage around impact zone. Also stay out of the fire patches she will put on the ground SPELL(111585).\
Phase 2: When boss reaches 60% her soul is separated from her body. SPELL(111642) is a AoE you cannot avoid. She will SPELL(115350) and fixate a player. That player has to run away because touching her would increase her damage massively.\
Phase 3: When her souls reaches 1% you have to fight both her soul and her body."
L["T668"] = "Tank him away from the group because of SPELL(106807). At 90%, 60% and 30% of health he will increase damage and attack speed by 15%.\
DPS hop on a barrel and crash into the boss giving him SPELL(106648) increasing his damage taken by 15%."
L["T669"] = "Pull everything and just walk to the boss room. Every time you face the adds do the following:\
Kill the hammer bunnies first. Take the hammer. Use the hammer to kill the rest AND use it to bump the explosive bunnies so that they'll explode in the air  not at the ground.\
Tank the boss away from group cause off his SPELL(112945). Also avoid damage from his SPELL(112992)."
L["T670"] = "He will use 3 out of 6 possible abilities.\
SPELL(106546): Spread out and outheal damage. SPELL(106851): Jump to get rid of the debuff.\
When the ground is full of beer use the bubbles to float and avoid the damage.\
When he casts two moving walls jump to get over them"
L["T671"] = "For each 10% health lost the boss stacks SPELL(114410) increasing his fire damage by 10% each. Every now and then he leaps on a player doing fire damage as AoE. Get out of that area. He will cast SPELL(114808) in front of him. Everybody (also tank) has to get away from him.\
If the boss reaches 50% he also will leave fire patches as a trail of fire as he moves."
L["T672"] = "Kill the 4 elementals and dont stand in the water. Now the boss is vulnerable.\
Phase 2: make sure to move away from his SPELL(106334) which is basically a beam that rotates."
L["T673"] = "Phase 1: Spread out to not get the aoe damage of SPELL(106984). When the Serpent arrived pull the boss out of the SPELL(128889).\
Phase 2: Face the Serpent. Group up UNDER the serpent (except tank). When boss casts SPELL(107140) overheal that player fast and then group up because that player then will AoE Heal you.\
Phase 3: Panda enrages! Pop BL, TW, HR to kill him ASAP."
L["T674"] = "First you have to fight the commander, he does nothing special just nuke him down. When he dies Whitemane appears. She will resurrect him and then you have to fight them both.\
Focus on the commander again but you have to interrupt SPELL(83968) of Whitemane because otherwise the whole room will be revived. Try also to interrupt her SPELL(36678) as well."
L["T675"] = "The boss will do SPELL(107047) impaling his target and reduce the targets health by 50% of total health (important for HEALER). \
When he reaches 70% and 30% he will fly to an endpoint to the opposite one and leaving a huge firewall trail. While does that he will summon adds, just off tank them. Also get out of the green poison patches on the ground"
L["T676"] = "He will spawn green patches on the ground so called SPELL(107091). Pull him away from that patches because they will buff his damage! Make sure no one stands in the green patches at any time. Kite him all the way long where no fire is and collect and bomb the adds meanwhile. \
Make also sure that no one stands in front of him because he cleaves ( SPELL(107120) )"
L["T677"] = "no tactic available yet"
L["T679"] = "no tactic available yet"
L["T682"] = "no tactic available yet"
L["T683"] = "no tactic available yet"
L["T684"] = "Boss wil attack with two fire spells. On occassion he will teleport a random player  in a studyroom teaching him/her a SPELL(113395). Fight your way back to the main room ASAP. If the healer or tank is teleported he will cast SPELL(113143). Focus on the adds until the tank/heal comes back."
L["T685"] = "Dispell the SPELL(106872) or a player will receive heavy dot damage. If a blue circle appears get out of it. Kill the spawned Volatile Energy asap.\
When he casts SPELL(106827) use as much attacks or spells as possible. Once he finished that you receive SPELL(127576).\
When the boss is at low health he will do 50% more damage so pop TW, BL or HR here."
L["T686"] = "If your hatred (new bar) fills too much use SPELL(107200) (new button) to clear your hatred. Hatred fills when you get hit by any of bosses abilities.\
Get out of the SPELL(112933) (purple zones,range can focus them down). If the boss is surrounded by SPEL(112918) get away from him. \
While his purple ring is active tank has to get away too because boss then hits with SPELL(114999)"
L["T687"] = "no tactic available yet"
L["T688"] = "Try to interrupt SPELL(115289) as often as possible. On occasion he SPELL(115139). Kill them ASAP because they will leave a dot on their target.\
Every now and then he rips  the soul of a random player SPELL(115297). Kill the spawned adds.\
He summons an empowering Spirit which tries to reach a corpse of a fallen crusader killed before. Don't let the spirit reach its target or the spirit turns into a zombie. Off-Tank the zombie if that happens."
L["T689"] = "no tactic available yet"
L["T690"] = "It's all about the add kill order.\
1st kill ironhide (he reduces 50% damage) then kill the sneaker (stuns a player for 5 sec) then the oracle (heals) and finally the last one (who polymorphs).\
The boss itself is no problem now because he receives 100% more damage."
L["T691"] = "no tactic available yet"
L["T692"] = "The general will throw his blade at a random player. Get away from that area! Healers remove the tempest debuff from any player.\
When he calls reinforcements make sure to use the bombs to lower his shield or dps the adds. Beware: If armed the bombs will explode within 3 seconds.\
You only can throw 1 of 2 types of bombs so move away from the other type.\
"
L["T693"] = "You can see a puddle in the middle which will grow in time. Also when a blob reaches the middle it'll grow so kill the blobs fast.\
You can decrease the size of the puddle by moving into it (short). On Occassion the boss casts SPELL(12001) on the puddle. The bigger the puddle is the more damage you will receive so make sure to shrink it from time to time."
L["T694"] = "no tactic available yet"
L["T695"] = "no tactic available yet"
L["T696"] = "no tactic available yet"
L["T697"] = "no tactic available yet"
L["T698"] = "Tank the boss away from the party. If he casts SPELL(119684) then get behind him. Players who suffer of SPELL(17447) should get out of the circle\
When boss reaches 66% he activates blade trap. Watch out for flying blades from the sides.\
When boss reaches 33% he activates cross bows. A random player will get damage so heal it out\
At every point in the fight avoid the whirling axes."
L["T708"] = "You have to face three Champions to win the fight\
1st boss: Kill the add first because it's not tauntable. Then kill the boss and get behind him when he casts SPELL(46968).\
2nd boss: Spread out and avoid the tornado which is being cast.\
3rd boss: Healer watch out for his SPELL(123655). Group up when he casts SPELL(120196) to share the damage."
L["T709"] = "no tactic available yet"
L["T713"] = "no tactic available yet"
L["T725"] = "no tactic available yet"
L["T726"] = "no tactic available yet"
L["T727"] = "Healers watch out for SPELL(121762). Move out of SPELL(121443). If the boss caught you in resin just jump to break free.\
When the boss flies towards the end of the bridge make sure to reach him and interrupt his SPELL(121284)."
L["T728"] = "no tactic available yet"
L["T729"] = "no tactic available yet"
L["T737"] = "no tactic available yet"
L["T738"] = "Phase 1: kill the adds and simply avoid bad patches on the ground. Kill the demolisher by just hitting them. Use the tar for help.\
Phase 2: Try to apply SPELL(120778) but dont hit your own party with the tar. When he casts SPELL(120760) move away from him and reapply the tar debuff afterwards. Try to evade his charge ability for it hits you hard."
L["T741"] = "no tactic available yet"
L["T742"] = "no tactic available yet"
L["T743"] = "no tactic available yet"
L["T744"] = "no tactic available yet"
L["T745"] = "no tactic available yet"
L["T748"] = "no tactic available yet"
L["T749"] = "no tactic available yet"
L["T816"] = "Council of Elders"
L["T817"] = "Qon"
L["T818"] = "Durumu the Forgotten"
L["T819"] = "Horridon"
L["T820"] = "Primordius"
L["T821"] = "Megaera"
L["T824"] = "Dark Animus"
L["T825"] = "Tortos"
L["T827"] = "Jin'rokh the Breaker"
L["T828"] = "Ji-Kun"
L["T829"] = "Twin Consorts"
L["T832"] = "Lei Shen"
L["T846"] = "Tactics Malkorok"
L["T849"] = "Tactics The Fallen Protectors"
L["T850"] = "Tactics General Nazgrim"
L["T851"] = "Tactics Thok the Bloodthirsty"
L["T852"] = "Tactics for Immerseus"
L["T853"] = "Tactics Paragons of the Klaxxi"
L["T856"] = "Tactics Kor'kron Dark Shaman"
L["T857"] = "Chi-Ji, The Red Crane"
L["T858"] = "Yu'lon, The Jade Serpent"
L["T859"] = "Niuzao, The Black Ox"
L["T860"] = "Xuen, The White Tiger"
L["T861"] = "Ordos, Fire-God of the Yaungol"
L["T864"] = "Tactics Iron Juggernaut"
L["T865"] = "Tactics Siegecrafter Blackfuse"
L["T866"] = "Tactics Norushen"
L["T867"] = "Tactics Sha of Pride"
L["T868"] = "Tactics Galakras"
L["T869"] = "Tactics Garrosh Hellscream"
L["T870"] = "Tactics Spoils of Pandaria"
L["T887"] = "Roltall launches 3 boulders (impact zone visible on ground). Avoid them and be aware that those boulders will come back in the same order.\
When the 3rd boulder has returned Roltall summons a wave that pushes you away for 8 seconds.\
The wave skill would only be a nuisance if Roltall had not thrown some burning slags before. The trick here is to stay aside a slag and never in front of one so as not to be repelled into them."
L["T888"] = "Priorize hostile adds! Interrupt SPELL(150759). Heal friendly adds. \
Hostile ads will focus on allies who have been struck by SPELL(150745) ignoring taunts during that time."
L["T889"] = "Tanks: Stay focused on boss and interrupt as many SPELL(150677) as possible.\
Healer: After every 3rd stack of SPELL(150678) boss does AoE damage. Save Cooldowns for last 20%\
DPS: Avoid flames (SPELL(150784)) and nuke down add when it spawned"
L["T89"] = "Phase 1: Boss fait SPELL(87859) or SPELL(87861). De temps en temps il fait un transfer et remets l'aggro a zero.\
Rien de special sur cette phase. \
Phase 2: Boss reste au milieu de la salle. Un SPELL(91398) apparait et tourne autour du boss. \
ATTENTION: Toujours s'\195\169carter avant pour ne pas etre touch\195\169 et eviter en meme temps les cercles bleus au sol.\
DPS les minions a l'aoe."
L["T893"] = "Whenever adds spawn, kill them as fast as possible. Fire Elementals cast SPELL(149997). Interrupt that! \
Tank has to tank the rock elementals not facing the group."
L["T90"] = "Helix lance des bombes au hasard sur le sol. Ne pas les toucher. \
Si un joueur recoit une SPELL(88352), qu'il se mette sous le tronc d'arbre de facon a \195\169viter des degats supplementaires. \
De temps a autre, le boss prend un joueur et l'emmene contre le mur et aura besoin de soins supplementaires. \
Quand le gros meurt, Helix lui meme va sauter sur un des joueurs. \
Continuez a l'attaquer et a soigner le joueur affect\195\169."
L["T91"] = "Un DPS doir controller une moissoneuse pour tuer les minions qui apparaissent a la forge. \
Le cycle de DPS avec la moissoneuse est : 2x SPELL(91734) 1x SPELL(91735) pour que les minions n'arrivent pas a la rampe. \
Le boss est tank\195\169 sur la rampe de facon a laisser assez de place au joueur dans la moissoneuse. \
Le reste du groupe reste en haut de la rampe. SPELL(88481) : Le boss devient fou et fait des degats a \
tout joueur se trouvant pres de lui. Il faut donc s'ecarter. Le tank le recurpere apres la surcharge. \
SPELL(88495) : Le boss cible un joueur et cr\195\169e un cercle rouge sous lui. \
Le joueur et ceux a proximit\195\169 doivent s'ecarter des que le cercle est visible. \
Quand le boss passe en dessous des 30% de sa vie, il recoit SPELL(91720) augmentant ses degats. \
Utilisez vos CD et Heroisme/Furie..."
L["T92"] = "Phase du boss: tank\195\169 normalement, il charge un joueur de temps en temps. \
Phase des adds: de petites vapeurs apparaissent. Tuez les vite! \
(utilisez la touche \"v\" pour voir leur vie et les tuer plus vite). \
Les vapeurs vont grandir. Le plus grand elles deviennent le plus de degats elles font. \
Quand le boss passe sous les 10% de ses pdv encore plus de vapeurs apparaissent, ignorez les et DPS boss vite!!!"
L["T93"] = "Macaron saute dans un gros pot et lance des plats au sol. \
Il y a de bons plats (jaunes, buff de hate) et de mauvais plats (verts, debuff et d\195\169gats). \
Mangez les bons plats et \195\169vitez les plats verts."
L["T95"] = "Vanessa est tank\195\169 normalement. 2 minion vont apparaitre tout le temps et doivent \195\170tre tuer rapidement. \
Sp\195\169cifiquement, il faut interrompre les sorts des mages de sang. \
Quand Vanessa descend en dessou de 50% pdv tous les minions disparaissent avec elle. \
Sur un cote du bateau 5 cordes vont apparaitre. \
Chacun des membre du groupe doit tirer sur une des cordes (clic droit). \
Apres avoir fait le Tarzan, on repasse en phase boss et adds. \
A 25% les cordes reapparaissent. Encore plus d'adds apparaitront ensuite, focus Vanessa. \
Quand elle meurt, il faut utiliser les cordes une derniere fois."
L["T959"] = "no tactics available yet"
L["T96"] = "Combat difficile. Resevez furie/heroisme pour les derniers 25% de pdv du boss. \
Le soigneur ne doit pas essayer de garder les pdv au maximum (voir sort ci-apres) tout en gardant les joueurs en vie. \
SPELL(93581): Sort soutenu qui fait des degats a un membre du groupe. A interrompre! \
SPELL(93713): Soigne une grande partie de ses pdv. A interrompre absoluement! \
SPELL(93706): Lanc\195\169 juste apres l'axphyxie. Il soigne tout le monde y compris lui. \
Laissez un ou deux tick pour soigner et garder le tank en vie. \
La cl\195\169 de ce combat est de bien assigner un joueur pour un quel sort il doit interrompre. "
L["T965"] = "Stay on the move to avoid zone damage as they rotate and move.\
Healers beware of SPELL(165731) doing AoE dot.\
SPELL(165731) inflicts significant damage on all targets that Ranjit passes through as he charges forward in a straight line."
L["T966"] = "Don't allow boss to reach solar beam healing him with SPELL(154149) by standing in between.\
Healer: SPELL(154135) does more damage each time it is cast.\
Tank: Avoid SPELL(154132) wich strikes with left or right arm."
L["T967"] = "Move away from Solar Flares or they trigger SPELL(153828). Defat Flares away from Piles of Ash or SPELL(169810) will revive them.\
Tank has to stay in melee range of boss or he will SPELL(153898). Save Tank Cooldowns for SPELL(153794)"
L["T968"] = "Defeat Zealots quickly before they SPELL(153954) by dropping them off the edge of Skyreach.\
Defeat Shield Constructs as fast as possible.\
Move away from SPELL(154043) and avoid leading the destructive beam through your allies.\
Interrupt SPELL(154396)\
"
L["T97"] = "Combat facile. Des worgens vont apparaitre, tuez les vite avant de retourner sur le boss. Dissiper les maledictions."
L["T971"] = "no tactics available yet"
L["T98"] = "Le tank doit sortir de SPELL(93687). Tuez toujours les minions car ils SPELL(93844) le boss. \
Le tank doit reagir vite car d'autres minions vont essayer de tuer le soigneur. \
Strategies optionelles : Controler les 2 minions et tuez le boss au plus vite \
ou bien descendre le boss dans la cour, ce qui empeche les minions d'apparaitre."
L["T99"] = "SPELL(93617): Tous les membres doivent se deplacer. \
SPELL(93689): Tous les joueurs doivent etre immobiles! \
SPELL(93527): continuez a vous deplacer et ecartez vous des zones bleues."
L["TACTICS"] = "Strategies activ\195\169es"
L["TOOLTIP_ACHIEVEBUTTON"] = "Show Achievements"
L["TOOLTIP_ACHIEVEBUTTON_2"] = "Leftclick: post achievements related to boss\
Rightclick: post achievements related to Instance"
L["TOOLTIP_LOOTBUTTON"] = "Show Loot"
L["TOOLTIP_TACTICBUTTON_1"] = "Post Tactics"
L["TOOLTIP_TACTICBUTTON_2"] = "Leftclick: post to channel\
RightClick: select Channel"
L["UNDONE_ACHIEVEMENTS"] = "Achievements not yet done:"
L["WHISPER"] = "whisper"
L["WHISPER_MENU"] = "Chuchoter"
L["WHISPER_MODE"] = "Mode Chuchotement"
L["WHISPER_MODE_DESC"] = "Si activ\195\169, lorsqu'un joueur utilise la commande !boss sur le canal de groupe, vous lui chuchoterez la strat\195\169gie plut\195\180t que de l'afficher sur le canal de groupe."
L["WHISPER_TACTIC"] = "Envoyer la strat\195\169gie \195\160 ..."

