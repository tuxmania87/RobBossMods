local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local L = LibStub("AceLocale-3.0"):NewLocale("RobBossMods", "enUS", false, debug)
if GetLocale() ~= "deDE" and GetLocale() ~= "enUS" and GetLocale() ~= "enGB" then
	L = LibStub("AceLocale-3.0"):NewLocale("RobBossMods", GetLocale(), false, debug)
end
if not L then return end

L["5MAN"] = "5 man dungeons"
L["A_SHOW"] = "always show"
L["ACHIEVE_SETTINGS"] = "Show unfinished achievements"
L["ACHIEVE_SETTINGS_DESC"] = "Show unfinished Achievements when entering an instance or set boss into target."
L["ALWAYS_SHOW_FRAME"] = "Always show RBM Frame"
L["ALWAYS_SHOW_FRAME_DESC"] = "When you are in a party the RobBossMods Frame will always be visivle ( except for combat )"
L["ASFIRST"] = "as first boss of"
L["ASNEXT"] = "as next boss for"
L["AUTHOR1"] = "Author is Robert Hartmann, Germany (  Lest\195\131\194\161 on EU-Thrall  ) Email: robert@keinerspieltmitmir.de "
L["AUTHOR2"] = "Helper for English translation: Stagen@irc.freenode.net and Niadyth@irc.freenode.net"
L["BOOK"] = "Book"
L["BOSS_DEAD1"] = " is dead. "
L["BOSS_DEAD2"] = " is the next one!"
L["BUTTONS"] = "Buttons"
L["CHATCOMMANDS"] = [=[!boss - (typed in party/raid channel from anybody) will post boss tactics of the current boss in party/raid channel

!loot - (typed in party/raid channel from anybody) will post loot of the current boss in party/raid channel

!achieve(all) will post achievements related to the current boss in party/raid channel

!rbminfo will post some information about the addon in the party channel

/rbm resetlang - if you use English client but want to access the German tactics you can switch settings with this command

/rbm toggle - Toggle the Main Window. You can use this for building macros]=]
L["CHOICE"] = "Choice"
L["CLOSE_OPT"] = "Hide"
L["CLOSE_OPT_DESC"] = "Only show window when targeting a boss"
L["CURRENT_ADDON"] = "show tactics for "
L["CURRENTBOSS"] = "Current Boss"
L["D23574_1"] = "CUR_SPELL under TAR_SPELL. Meet THERE!!!"
L["D23574_2"] = "CUR_SPELL is over: Spread again!"
L["D23577_1"] = "CUR_SPELL: Get the boss out of the green circle!"
L["D23577_2"] = "CUR_SPELL: spawned. Kill it!"
L["D23863_1"] = "CUR_SPELL: Remove the debuff !!"
L["D23863_2"] = "CUR_SPELL: Remove the debuff !!"
L["D23863_3"] = "CUR_SPELL: Don't stand still!"
L["D52053_1"] = "Zanzil summons Berserk: Go to blue cauldron and use buff on the big one!"
L["D52053_2"] = "MOVE ALL to the green Cauldron and get poison buff!"
L["D52053_3"] = "Zanzil summons zombies. Get red cauldron debuff and tank the adds!"
L["D52059_1"] = "CUR_SPELL: Interrupt the boss!!"
L["D52148_1"] = "All OUT of the CUR_SPELL!!"
L["D52148_2"] = "All IN the bubble!!!"
L["D52151_1"] = "CUR_SPELL on TAR_SPELL: only heal minimal!"
L["D52151_2"] = "CUR_SPELL on TAR_SPELL: is over."
L["D52151_3"] = "CUR_SPELL: Kill the raptor!"
L["D52151_4"] = "CUR_SPELL: Don't stand in front of the boss or near him."
L["D52155_1"] = "CUR_SPELL: Don't stand in front of the boss"
L["D52155_2"] = "CUR_SPELL: Stay in movement and go away from the green stuff following you!"
L["D52155_3"] = "CUR_SPELL: TAR_DUMM1 and TAR_DUMM2 walk away from each other!"
L["D52271_1"] = "Kill all the ghosts. If they reach a player they will kill you!!"
L["D52286_1"] = "Everyone get 15 yards away from boss !!!"
L["D52638_1"] = "TAR_SPELL is captured by bird!!"
L["D52730_1"] = "Leap on: TAR_SPELL!! Go to the chains"
L["DONTKNOW"] = "Hi. I installed RobBossMods for heroic instances. Right before a boss you can type !boss in the party chat or whisper me !boss and you'll get an explanation."
L["DONTKNOW_OLD"] = "If somebody doesn't know the boss, please type !boss in partychat and you will get an explanation!"
L["DRAG"] = "Shift + Left-Click : Drag"
L["EVERY_BOSS_ANNOUNCE_DESC"] = "When targeting a boss, post in the party/raid channel that everyone can say !boss to receive Boss Tactics"
L["EVERY_BOSS_ANNOUNCEPARTY"] = "Post when targeting (in party)"
L["EVERY_BOSS_ANNOUNCERAID"] = "Post when targeting (in raid)"
L["GUILD"] = "Guild"
L["HEAD_CHATCOMMANDS"] = "Chat Commands"
L["HELPER_BOSSPOST"] = [=[left-click: Post the Boss tactics in the channel you set up before. Check the Label below.\ 
right-click: Select the channel where you want to post your tactics/loot information.]=]
L["HELPER_POSTACHIEVEMENT"] = [=[Leftclick: Post achievements that are related to the current boss
Rightclick: Post achievements that are related to the current instance]=]
L["HELPER_POSTLOOT"] = "Post the loot that the boss will drop when killed."
L["HELPER_SELECTBOSS"] = "You can manually select the current Boss but in most cases RobBossMods should automatically chose the correct Boss."
L["HINTEVERY"] = "Note when targeting"
L["HINTEVERY_DESC"] = "The text that is send to party chat when targeting a boss ( option has to be enabled )"
L["HINTONCE"] = "Party Note"
L["HINTONCE_DESC"] = "The text you can send to party chat via left-click menu"
L["IGN1"] = " wanted you to post tactics "
L["IGN2"] = " times in the last "
L["IGN3"] = [=[ seconds. Do you want to ignore further requests from that person?

Hint: You can also ignore a player by targeting that player and type /rbm ignore  or type /rbm ignore PLAYERNAME.]=]
L["INFORM_PARTY"] = "Send Party Note"
L["INSTANCE_CHAT"] = "Party/Raid"
L["LASTBOSS"] = " was the last boss here. Many thanks for the group!"
L["LEFTCLICK"] = "Left-click to show/hide RobBossMods Frame"
L["LOADING"] = "Loading"
L["LOOTFOR"] = "loot of "
L["MENU_AUTHOR"] = " /rbm author -- get information about the author"
L["MENU_DESCRIPTION"] = "RobBossMods shows Boss Tactics for bosses in heroic 5-mans and (normal and heroic) raids. When you are in an instance people can receive boss tactics by typing !boss in party chat. You can also announce Boss Tactics and set the current boss manually!"
L["MENU_DYN_NO"] = [=[unfortunately there are no dynamic tips for this boss.

Please inform the /rbm author about your ideas! Maybe i will implement your ideas soon ;)]=]
L["MENU_DYN_TOGGLE_A"] = "enable all tips."
L["MENU_DYN_TOGGLE_A_DESC"] = "Enable all dynamic hints infight. This is not available for all bosses yet."
L["MENU_DYN_TOGGLE_D"] = "disable all tips."
L["MENU_DYN_TOGGLE_D_DESC"] = "Disable all dynamic hints infight."
L["MENU_DYNAMIC_DESC"] = "Give dynamic hints infight in /say chat about combat."
L["MENU_DYNAMIC1"] = "Activate hint infight for "
L["MENU_DYNAMIC2"] = ""
L["MENU_FRAME_OFF"] = " /rbm hide -- Hide RobBossMod Frame"
L["MENU_FRAME_ON"] = " /rbm show -- Show RobBossMod Frame"
L["MENU_FRAME_TOGGLE"] = " /rbm toggle -- RobBossMod Frame hide/show"
L["MENU_GENERAL"] = "General Settings"
L["MENU_HINT1"] = " !rbminfo in partychat  -  Gives Information about the addon in Partychat e.g. Link for download"
L["MENU_MINIMAP_OFF"] = " /rbm minioff -- Hide Minimap Icon"
L["MENU_MINIMAP_ON"] = " /rbm minion -- Show minimap icon"
L["MENU_RESETLANG"] = " /rbm resetlang -- reset language settings"
L["MENU_STATE"] = " /rbm state -- returns the status of the addon"
L["MINIMAP_SHOW"] = "Show Minimap Icon"
L["MUST_RESTART"] = "You have to restart the addon to see the changes. You can do so by typing /reload in the chat."
L["MYSELF"] = "myself"
L["N_SHOW"] = "never pop-up"
L["NEVER_OPT"] = "Never pop up"
L["NEVER_SHOW_FRAME"] = "RBM Window will never pop-up automatically"
L["NEVER_SHOW_FRAME_DESC"] = "The new RBM Main Window will NEVER pop up. It will only show by typing /rbm show or by clicking on minimap/broker symbol."
L["NEVER_WARNING"] = "WARNING! The RBM MainWindow will now ONLY appear when you click on MinimapIcon/BrokerIcon or you type /rbm show in Chat"
L["NEW_VER1"] = "You are running RBM Version "
L["NEW_VER2"] = " but there is a new version: "
L["NEXT_BOSS_ANNOUNCE"] = "Post next Boss"
L["NEXT_BOSS_ANNOUNCE_DESC"] = "Announce boss death as well as the next in line in party chat upon boss death."
L["NOBOSS"] = "No boss selected"
L["NONE"] = "none"
L["OFFICER"] = "Officer"
L["OPTIONS"] = "Options"
L["P_SHOW"] = "show when targeting boss"
L["PARTY"] = "Party/Raid"
L["POSTON"] = "Post tactics"
L["RAID"] = "Party/Raid"
L["RAIDS"] = "Raids"
L["RESET_CUSTOM"] = "Reset custom tactics and load default"
L["RESET_CUSTOM_DESC"] = "Reset custom tactics and load default ones. Attention ALL custom tactics will be reset!"
L["RIGHTCLICK"] = "Right-Click: Settings"
L["S_GHUR"] = "Ghur'sha is dead. Ozumat is the next one!"
L["S_HELIX"] = "Helix is dead. Foe Reaper 5000 is the next one!"
L["S_OZUMAT"] = "Ozumat was the last boss here. Many thanks for the group!"
L["SAY"] = "say"
L["SCROLL"] = "Scroll"
L["SEL_MINIMAP_ICON"] = "select the minimap icon"
L["T100"] = [=[Tank Godfrey in a corner so that he looks in that direction. Move away when he casts SPELL(93520)!
Shortly before SPELL(93520), he'll SPELL(93707). Tank them and nuke them down while SPELL(93520) fires into the corner.
Important: Quickly decurse ANYONE hit by SPELL(93629).]=]
L["T101"] = [=[SPELL(75722): Periodically, she will SPELL(75722). Get out of Blue Circles on the ground to avoid them!
Both her Geysers & SPELL(75683) cause knockback damage.
Her SPELL(76008) has a 3-second cast. Interrupt it or your tank will almost die.
PHASE 2 (at 66% and 33%): She summons two Naga Witches and a big Naga. CC one or both casters & nuke the big one or kite the big one & nuke the casters.]=]
L["T102"] = [=[Tank the boss with your back to a wall.
When he begins to cast SPELL(76047), MOVE AWAY quickly, as a direct hit will one-shot an appropriately geared tank.
The fissure will grow in time, so the tank needs to kite him to another wall.
The boss will SPELL(95463) a random player for 6 seconds. Just heal the poor guy getting squeezed :)
Sometimes he casts SPELL(76100), but it does not cause much damage. Can be removed or just ignored because tank runs away from him most of the time.]=]
L["T103"] = [=[Tank the boss with back pointing to the group. He casts SPELL(76170) which should only hit the tank. Shortly afterwards he casts SPELL(91412). Try to interrupt the Bolt!\
 Get out of the red circles. When Erunak reaches 50% the Mindbender chooses a new victim's brain to eat :)
Avoid the SPELL(76234) green puddles, otherwise you won't be able to attack/cast.
Stop attacking Ghur'sha, when he gains the buff from SPELL(76307). Otherwise, every attack heals him and damages the praty!
 Use: purge, spellsteal, or hunter's SPELL(19801) to remove his buff.]=]
L["T104"] = [=[Phase 1: Kill the faceless caster quickly. Tank the big one and bomb these nasty little murlocs.
 Phase 2: DD nuke the three caster adds. Tank should kite the huge shadow creatures. Important! Dont get to close to them and stay out of the black puddles.
 Phase 3: Everyone becomes uber big and their damage increases A LOT! Kill remaining shadow creatures and instantly nuke down the giant octupus, Ozumat. Use Everything. You won't ever do that much damage again. :)]=]
L["T105"] = [=[SPELL(75272): Kite creatures which spawn on your position to the tank, so he can AoE them.
SPELL(75539): Whole Party will be teleported to the boss. Target & destroy the chains instantly! Afterwards, run away from the boss because he casts SPELL(95326), which will kill you if you are in range.]=]
L["T106"] = [=[There will be 3 beams of SPELL(75610) on the adds near the boss. Players need to stand in between each beam so that it will not hit the add. (Choose ranged players, if available)
 Players should step out of the beam at 80 stacks of the debuff, until they drop off (beams will hit the adds again), then step back in.
 SPELL(75823) MUST be interrupted, or its Fear will chase players out of the beams, causing a wipe.]=]
L["T107"] = [=[The whole group has to come into the inner circle so that you don't stand on the steel grid.
 Karsh is almost immune against everything SPELL(75842). To make him vulnerable the tank has to drag him through the pillar of fire in the middle.
 This will apply a debuff SPELL(75846) on him, which makes him vulnerable. The more stacks the more damage he takes but it also increases the AoE damage it causes to the party.
 The debuff must not go off him, because if it does then adds will spawn. So just a few seconds before the debuff is about to run out, the tank must pull him into the pillar of fire for just a moment to refresh the debuff.]=]
L["T108"] = [=[(normal) If the adds can't be CC'ed then they must be killed first, as they drop SPELL(99830) and cast SPELL(93667) which can be tough to outheal.
 Other than that, it's straight up tank and spank. Note that if the add 'Runty' is killed then Beauty will Enrage.
 Fear Ward on the tank or a Tremor Totem is useful for the fear. Move out of the fire!]=]
L["T109"] = [=[Someone should kite the Shadows who will take no damage but reduce the healing taken to their present target if the boss is nearby. SPELL(76189) 
That means they should NOT be near the tank or have aggro on him! The boss itself is quite easy. Obsidius will periodically 'posses' one of his Shadows, switching places and resetting aggro with it.]=]
L["T110"] = [=[Phase 1: Corborus casts purple shards on the ground SPELL(86881). Get out there (even while the boss casts) because these shards will spawn into flying crystals which must be killed with AoE. Heavy Damage when they explode
 Phase 2: Boss is submerged. Get out of the ground effects and get down the adds that spawn with AoE. (on heroic you must watch careful on the ground for a ground effect and get out of it before the boss one shots you)]=]
L["T111"] = [=[Phase 1: Slabhide is on the ground. Healer and tank have to be in a line of sight. When boss casts SPELL(92265) everyone will have to hide behind the rocks on the ground.
 Phase 2: Boss is in the air. Avoid the black circles on the ground. Also get out of the red circles on the ground!]=]
L["T112"] = [=[General: Ozruk has to be tanked with the back pointing to the group. SPELL(95331) and SPELL(95334) will reflect spells.
SPELL(95338): He hits the ground in front of him. 8 yards around this impact there will be much damage. Melee and tank get behind the boss!
SPELL(95347): Comes direclty after SPELL(92426). Ranged dps stays in max range. Melee and tank run away from him. 15 meters ranged!]=]
L["T1122"] = "no tactics available yet"
L["T1123"] = "no tactics available yet"
L["T1128"] = "no tactics available yet"
L["T113"] = [=[SPELL(79351) (mostly cast on tank): Interrupt! Does heavy damage.
SPELL(79340): She casts black wells under a random player. Get out there and try to create a wall of these circles to force the adds to run into them.
SPELL(79002): She throws rocks. Can be easily seen on the ground. Just get out.
SPELL(92663): Curse which increases taken damage. Got to be decursed!]=]
L["T1133"] = [=[Tank re-position the boss for the party to avoid SPELL(161588) and SPELL(162066).
The ally marked with SPELL(163447) has to stay on the move to get not hit by SPELL(162058)]=]
L["T1138"] = [=[The bosses have to die at the same time. Rocketspark is un-tankable and should be ranged down.
Borka should be tanked and attacked by melee DD. 
Tank: When Borka casts SPELL(161089) let him collide with Rocketspark interrupting his cast.]=]
L["T1139"] = [=[Don't stand on purple runes when engaging boss. She uses SPELL(153240) on random player. Just move away.
She spawns an add to her SPELL(153153). Nuke down the add.
When she casts SPELL(164686) move away from the black and stand on WHITE runes. Leave that runes when the cast is over.]=]
L["T114"] = [=[Little Whirlwinds will spawn and they will create an outer circle. From time to time the whirlwinds will come to the middle. Everyone who stands IN one of the whirlwinds will suffer a silence effect and get a lot of damage.
Since Patch 4.0.6. you can't stand IN the boss anymore. So you have to do the following:
Everytime the tornados reach the middle, the party goes to the outer circle.]=]
L["T1140"] = [=[If you see a swirly under you get out! Try to avoid being in purple triangles as well.
Once the boss casts SPELL(153804) get INTO the bad stuff you avoided earlier!
At 50% HPhe spawns adds. Focus them down and do everything you did before.
Healers watch out when adds have ~50% health they do more damage.]=]
L["T1147"] = "no tactics available yet"
L["T1148"] = "no tactics available yet"
L["T115"] = [=[Tornadoes: The whole place is full of little tornadoes which slightly move. Dodge them!
 Wind of Altairus: The boss creates a wind that has a certain direction. (Much better seen if you zoom out and look from above). DPS stand with the wind. Tank the other way around.
SPELL(88308): Boss targets a random player and casts a breath. Don't stand together because then it'll hit more players which means more work for the healer.
 When DPS stands right they will get SPELL(88282) which increases cast and attack speeds. Make sure you stand right so that you can dps the boss down as fast as possible.]=]
L["T1153"] = "no tactics available yet"
L["T1154"] = "no tactics available yet"
L["T1155"] = "no tactics available yet"
L["T116"] = [=[SPELL(87618): Everyone gets this debuff. You are rooted for 18 seconds. Has to be dispelled!
 Static Trianlge/Groundfield: Asaad casts three points on the ground and connects them to a triangle. Stand in that triangle otherwise you will die from his SPELL(86930).
 Chain Lighting: Try not to stand close to each other!]=]
L["T1160"] = [=[Tank the boss on the the edge not facing the group. Tank step aside when he casts SPELL(154442).
If boss casts SPELL(175988) on you get away from the omen.
After the 3rd omen he casts SPELL(154469). Break the chain by killing a skeleton (like Sylvana ghouls in End Time).]=]
L["T1161"] = "no tactics available yet"
L["T1162"] = "no tactics available yet"
L["T1163"] = [=[1st phase: Take boss down to 60% HP.
2nd phase: Boss is untankable. Position so the boss cannot hit you with his gun. Make sure to cuddle making it easier for the tank to pick up adds.
Damage Dealer: Use  SPELL(160702) to destroy the Assault Cannon. Avoid standing in SPELL(166570).
3rd phase: Attack the boss and avoid red patches on the ground.]=]
L["T1168"] = [=[Move out of the SPELL(152801) and get out of purple SPELL(153067) ASAP!
Tank has to pick up the adds. They can be aoed down.
When he casts SPELL(152979) kill your spirit and click it to receive a buff.]=]
L["T117"] = [=[Boss puts little mines on the ground SPELL(91259). Get away from them. The mines can get inactive (stop glowing) but are still dangerous then. SPELL(91263)
 Sometimes the boss grabs a player and throws him against the wall. Heal that player.
SPELL(83445): Boss stops moving and brown lines appear on the ground. Just get out of them.
 In general: Heal and tank have to be in a line of sight which is not that easy because the tank has to keep moving to avoid the mines.]=]
L["T118"] = [=[SPELL(81642): Periodically smashes the ground with his tail. Melee dont stand behind him, instead stand at his side.
SPELL(81690): Deals 1850 to 2150 bleeding damage every 2 seconds. Nearby hungry crocolisks can smell your blood! You are a prime target for attack. Get to the Tank or kite them while the others dps them down.
 Augh: Augh appears and does a little whirlwind. Just avoid him.
SPELL(81706): 30% Enrage. Remove it or activate trinkets and cooldown abilities.]=]
L["T1185"] = [=[When he hurled SPELL(153480) protect yourself by getting behind it quickly.
Don't tank boss too close or too far from the hurled Shield to ensure that the boss does not follow you behind the shield and you still can get behind it.]=]
L["T1186"] = [=[DD interrupt SPELL(154218) and SPELL(154235). 
Healer keep in mind that SPELL(153477) does much damage to players standing outside the protective zone.
Tank make sure to tank all adds.]=]
L["T119"] = [=[Phase 1: Boss is tanked. Move away from the pillars of light. A phoenix will appear (like in Kael'thas fight, Tempest Keep).
 Just like in BC name someone who will pull the bird out of the group and kill him. Phoenix will become an egg.
 While phoenix is an egg the named DD dps the boss. When the boss reaches 50% phase 2 will start.
 Phase 2: Boss is not vulnerable. Get out of SPELL(88814) around him. A dark phoenix appears. Just dps him down as fast as possible dont kite anything. Soul Fragments will reach the phoenix make him doing more damage so hurry!
 When dark phoenix dies phase 1 starts again until the high prophet bites the dust.]=]
L["T1195"] = "no tactics available yet"
L["T1196"] = "no tactics available yet"
L["T1197"] = "no tactics available yet"
L["T1202"] = "no tactics available yet"
L["T1203"] = "no tactics available yet"
L["T1207"] = [=[Use spell interrupts or crowd control to prevent SPELL(168041) and SPELL(168105).
Healer: Group will receive random damage through SPELL(168092) and SPELL(168040).
]=]
L["T1208"] = [=[Boss casts fire then frost finally arcane spells. By interrupting SPELL(168885) you make her switch magic school.
Jump over SPELL(166489) to avoid damage. Healers beware of arcane spells doing much damage to the team.]=]
L["T1209"] = [=[Kill 8 Toxic Spiderlings to make boss attackable. Avoid SPELL(169275). 
Creatures that gain SPELL(169218) will become stronger over time.
Healer dispell SPELL(169376). Tank position Venom-Crazed Pale Ones to be struck by bosses SPELL(169275) so that they die quicker.]=]
L["T1210"] = [=[When boss casts SPELL(169613) step on any sprouting to prevent them from growing. Quickly destroy SPELL(169251) to keep kirin'tor alive.
Kill adds so that you don't get overwhelme. Heal friendly mobs to keep them in the fight.
Tank make sure to pick up adds that are spawning all the time.]=]
L["T1211"] = "no tactics available yet"
L["T1214"] = [=[Lead SPELL(164294) away from group. Destroy Aqueous Globules before they can reach Whiterbark.
SPELL(164538) deals AoE damage when a globule is killed.
Tank: During SPELL(164357) face boss away from group. Prepare to pick up adds.)]=]
L["T1216"] = [=[When the boss is untargetable cleanup the adds ASAP.
DD don't stand close to Blazing Tricksters to avoid SPELL(154018). Interrupt SPELL(154221) or kill casters before they can finish casting.
Tank pick up the Felguards since they do heavy melee damage.]=]
L["T122"] = [=[Its quite easy! Immediately that the fight starts, the boss will get SPELL(84589). Focus the adds then until he drops his shield.
 Adds: Just tank and spank. Get out of the ground effects (nasty green and grey circles). Thats it :) feel free to correct me.]=]
L["T1225"] = [=[At 75% health he heals full and gets one of three abilities.
Avoid standing near others when SPELL(156921) detonates. Interrupt SPELL(156854) and SPELL(156975) and DON'T dispell SPELL(156954)]=]
L["T1226"] = [=[Start with all damage cooldowns inclusive Bloodlust/Heroism and burst boss down as far as possible.
When he casts SPELL(166168) leave the plattform and deactivate the rune conduits (Tank can also do this since the boss does not attack anyone in that phase).
Be carefull because two spawned SPELL(154335) will rotate around the room. Use speed buffs/spells to avoid damage. ]=]
L["T1227"] = [=[Face the boss away from the group. Whenever the boss chases someone (SPELL(161199)) interrupt that spell!
Focus damage on the two adds and be careful to avoid the fire lines they create. Just step aside.
Kill the boss when the adds are down and try to dispell SPELL(161203).]=]
L["T1228"] = [=[Tank side step the bosses charge and pull the boss away from flying axes and poison spots on the ground that have to be avoided from everyone.
If possible do DoT Damage to the flying whelps above you.
Healer be careful since the boss does significantly more damage when he reaches 70% health. At some time the boss dismounts. Save your damage reduction and bloodlust/heroism cooldowns for when that happens.]=]
L["T1229"] = [=[Tank: Tank the boss a bit left or right from the center so it is obvious when he starts SPELL(155031). Make also sure to pick up all the adds when the boss flies above you (at 70% and 40% HP). Save damage reduction CD for when the boss lands at the end.
Everyone run to the opposite site of the bridge when the boss spits fire i.e. SPELL(155031).
AoE the Whelps down at 70% and 40% HP. When the boss lands at the end of the fight, stay BEHIND her.]=]
L["T1234"] = [=[Bloodlust/Heroism at the start of the fight. Avoid her Whirlwind and get out of fire.
When she leaves the platform take care of dragons that spite fire in a linear direction. 
You have to move all the time to avoid getting burned.
While adds are active there is much damage income towards the tank. Healer be prepared!]=]
L["T1235"] = [=[Nok'gar cannot be directly targeted or attacked while mounted. Move quickly to get out of the line of fire of SPELL(164632) or SPELL(164648). 
Stop attacking boss when he is under the effect of SPELL(164426).
Tank move Nok'gar and Dreadfang quickly away from the arrow barrages.]=]
L["T1236"] = [=[Avoid damaging an Enforcer that is protected by SPELL(163689) until the effect fades.
Avoid SPELL(163390) which immobilize victims and make them vulnerable to SPELL(163379).
Spread out when Neesa prepares to fire her SPELL(163376).]=]
L["T1237"] = [=[Focus damage to quickly break Oshir out of SPELL(162424). Be alert when Oshir casts SPELL(178124) and focus adds.
Focus healing on the victim of SPELL(162424).]=]
L["T1238"] = [=[Skulloc attacks with fierce melee strikes alongside the warrior Koramar and First Mate Zoggosh in a mechanized turret.
Quickly hide behind cover when Skulloc begins SPELL(168929) working your way towards him during lulls between volleys.
Do not linger near the rear of the ship during SPELL(168929) will incinerate you.
]=]
L["T124"] = [=[Blue stuff on the ground SPELL(75117): Get out ! 
SPELL(74938): Boss is invulnerable while his shield is up. Split your party. Each team has to get to one lever. One of each team has to distract the snakes while the other activates the lever.
 Afterwards assemble upstairs quickly, kill any remaining snakes (they do a nasty poison debuff which isn't removeable) and interrupt the boss who casts SPELL(75322). This lever phase will occur two times.]=]
L["T125"] = [=[Phase 1: normal fight. Get out of the grey ground effect.
 Phase 2: Boss is submerged. Tank and dps the adds down. Thats it. Achievemtent when completing the fight on a camel.]=]
L["T126"] = [=[SPELL(76184): black/purple circles on the ground that dont disappear, get out of them!
SPELL(75623): AoE. Just heal it.]=]
L["T1262"] = "no tactics available yet"
L["T127"] = [=[SPELL(74136): like in Trial of the Champion (Eradic the Pure). Just turn around when she casts supernova, to avoid taking damage and being disorientated.
 Split: Boss splits at 66% in 3 copies and at 33% in 2 copies. Each copy represents one of the bosses abilities and killing that add removes the corresponding ability.
 Removing one ability, however, increases the effect of the remaining ones, so choice of which to leave up to the end is down to your group composition.]=]
L["T128"] = [=[Boss is tanked normaly. DD instantly change target to the green flower things on the ground which will be constantly spawning. You dont want them to hatch. While they are alive they place a HoT effect on the boss and increase his damage done to the tank.
 If there is a spore the tank has to bind it. When the spore dies it'll leave SPELL(75701) on the ground. Get the boss into that stain but yourself out so that the boss wont heal himself that much.
 This fight might take long if the boss is not properly positioned. He will heal himself very much otherwise.]=]
L["T129"] = [=[Setesh is not tankable. Tank collects the adds and kites them as good as possible ( use slow and stun effects ). Attention! Boss does damage circles on the ground, get out of them!
 Setesh goes to a corner and creates a portal. ALL dd have to dps that portal quick. You only want one add and two mana creatures to come out, not more! Tank collects them to kite them around.
 Party does permanent damage on boss except when portals appear. Ignore the Adds, as they have a reduced damage taken buff.]=]
L["T1291"] = "no tactics available yet"
L["T130"] = [=[SPELL(80352): Got to be interrupted! 
SPELL(87653): Boss targets a player. The ground under that player becomes dark. Get out of that area because the boss is going to jumo onto you which will cause 'augh' :)
Occasionally he will go to the middle doing AoE damage. Heal through it! During this phase the boss takes additional damage, so it's a good time to pop heroism/bloodlust/time warp.]=]
L["T131"] = [=[SPELL(74670): Boss targets a player and charges after 2 seconds. Run away from your position otherwise he could one shot you. Don't stand together.
SPELL(74634): AoE damage in the area around the boss. Melee and tank will have a short period to get away from Umbriss. 
SPELL(74846): Debuff on the tank, which does incredible damage over time. Only passes if tank gets 90% or more health. The Tank need to be overhealed until the debuff fades! 
 Trogs: 4 trogs spawn. One DPS should kite them away from the boss and kill them. If they die near Umbriss, then he will Frenzy SPELL(90169) and make the fight so much harder.]=]
L["T132"] = [=[SPELL(74984): Throngus creates litle AE circles. (looks like Consecration) Don't stand there! 
 If he picks the 2 Swords then the Tank gets a magic debuff which does huge damage. It either has to be dispelled or the tank should receive heavy heals to compensate for the loss of health! Use all cooldowns if necessary. 
 If he picks the mace SPELL(75007), then the tank has to kite the boss while staying out of melee range as best as possible. Throngus will also SPELL(90756) a random party member which will have to recieve a bit extra healing. 
 If he picks a shield, then all DPS needs to get behind him. Use AoE heals, since the party will be taking more damage than the tank.]=]
L["T133"] = [=[Phase 1: Boss summons a fire elemental SPELL(75218) which will follow a random party member. The haunted has to run away, all the others should dps the add down as fast as possible. If the fire elemental reaches it target it will detonate - don't let that happen. 
 Phase 2: The flames of the dragon SPELL(90950) will cover half the room, so dps should stand at max range to reduce the damage taken.]=]
L["T134"] = [=[SPELL(79466):  Targets a random player and drains life over time. You will need to move otherwise you get damage and you will be rooted.
SPELL(75694): Again a circle on the ground. Get in there, everyone. While he casts the shadow gale you do much more damage on the boss. Adds: After a Shadow Gale a Faceless Corruptor (2 in heroic) will spawn. Split your dps up because you have to get the Adds down as fast as possible. Adds can be slowed but not stunned. If they reach the eggs, then they will heal the boss and release little adds.]]=]
L["T139"] = "Tactics on Argaloth TODO (feel free to add your own tactic here)"
L["T140"] = "Spread out during this fight but leave an empty space for the raid to collapse on during the eye phase. Be ready to move if the boss channels SPELL(97028) on you. The tanks should always face Occu'thar away from the raid, so that no one other than himself is ever damaged by SPELL(96913). When the Eyes of Occu'thar spawn, everyone needs to group up and AoE down the eyes within 10 seconds or they will explode and damage the whole raid. "
L["T1426"] = "no tactic "
L["T1467"] = [=[This fight requires a lot of interrupts, so coordinate with your group so interrupts don't get wasted.
You must interrupt SPELL(191941) to be able to interrupt SPELL(191999).
If SPELL(202740) or SPELL(191823)  isn't interrupted, be prepared to use AoE heals.

]=]
L["T1468"] = [=[Walk on the moving orange lava patches, if necessary, to avoid letting the Ember adds touch.
Ember adds will explode if they touch lava patches.
Avoid the circles on the ground if you are at range.
Adds cannot be tanked and should die ASAP.
]=]
L["T1469"] = [=[Glazer will cast SPELL(214893), which bounce off the walls towards the nearest player.
Use the Mirrors along the edge of the platform to reflect the blue Focused beam onto Glazer's back.
Tank: Help direct the mirrors when he focuses his beam.
]=]
L["T1470"] = [=[Avoid SPELL(197422) spirits, which are invisible unless revealed by SPELL(197941).
If you pick up SPELL(197941), use the ability that appears as a button on the screen to dispel SPELL(197422).
Use SPELL(197941) to remove the SPELL(218533) spreading patch that forms on the ground.
Use SPELL(197941) to reveal her when she disappears.  She goes to a random location around the edge of the platform.
]=]
L["T1479"] = [=[Kill all the enemies around Serpentix before attacking.
Move away from other players if you are targeted with SPELL(191855).
Stand behind him when he casts SPELL(192050).
Kill adds and interrupt SPELL(191848).
]=]
L["T1480"] = [=[Kill and tank adds ASAP and stay BEHIND the boss all the time.
Position an enemy between yourself and the red arrow if you are targeted with SPELL(191975).]=]
L["T1485"] = [=[Stand behind him!
Stay away from SPELL(193235) when he throws it at a random player.
Avoid the blue lightning that appears on the ground after he uses the SPELL(191284).

]=]
L["T1486"] = [=[Stay away from other players when you are afflicted with SPELL(192048)..
Avoid the yellow swirls after SPELL(192307) is cast.
Stay inside the blue bubble that appears when SPELL(192305) is being cast.
Tank move the boss so that only one NPC is channeling her.
]=]
L["T1487"] = [=[Melee stand close to each other. Range and Heal stay at least 12 yards away.
If you are targeted by SPELL(196838) run away.]=]
L["T1488"] = [=[Click the Aegis of Aggramar shield that he drops on the ground to survive SPELL(193826).
Stay away from allies when Skovald casts SPELL(193659).]=]
L["T1489"] = [=[Run away when he starts casting SPELL(198263).
When you are afflicted by SPELL(197961), run to the rune on the floor that matches the rune floating above your head.
Avoid the circles on the ground.
Avoid the glowing orbs that appear after he casts SPELL(198077).
]=]
L["T1490"] = [=[Move onto the mounds when she begins to cast SPELL(19359).
Move away from mounds when she casts SPELL(193611), so they won't be destroyed.
Move away from the group if you get the SPELL(193717) debuff.
]=]
L["T1491"] = [=[Move out of the circles that appear on the ground.
If you get a bubble cast on from Gaseous Bubbles, stand in the AoE damage spots on the ground to remove the bubble.
Healers stand close to avoid SPELL(193152) and DPS get away when he casts SPELL(193051)]=]
L["T1492"] = [=[Move away from the circle on the ground for SPELL(192706).
Don't stand in front of the boss and never stand close to each other.
AoE Heal when SPELL(197165) and dispell SPELL(192706) on allies.
Tank stay on top of the boss otherwise group will suffer AoE damage.]=]
L["T1497"] = "T1497 Ivanyr"
L["T1498"] = "T1498 Corstalix"
L["T1499"] = "T1499 general xaxal"
L["T1500"] = "T1500 Nal tira"
L["T1501"] = "T1501 Advisor "
L["T1502"] = [=[Avoid the circle that appears beneath him when he starts casting SPELL(193364).
Avoid the purple orbs after he casts SPELL(193460).
Tank be prepared to use cooldowns when Ymiron's mana bar fills up.]=]
L["T1512"] = [=[Interrupt SPELL(194266). Establish an interrupt order with other players, if you need to.
Kill Shackled Servitors and Destroy Soul Fragments as they appear.
Players with SPELL(194325) cast on them will need lots of healing.]=]
L["T1518"] = [=[Move away from other players if you become afflicted by SPELL(194960).
Try not standing next to each other (melee as well).
Tank and Heal: Use your CDs when it casts SPELL(194956).]=]
L["T154"] = "Tactics on Conclave of Wind  TODO (feel free to add your own tactic here)"
L["T155"] = "Tactics on Al'Akir TODO (feel free to add your own tactic here)"
L["T156"] = "Tactics on Halfus Wyrmbreaker  TODO (feel free to add your own tactic here)"
L["T157"] = "Tactics on Valiona  TODO (feel free to add your own tactic here)"
L["T158"] = "Tactics on Ascendant Council  TODO (feel free to add your own tactic here)"
L["T1653"] = [=[If targeted by SPELL(197478), gather with other players so less Felblazed Ground appears.
Interrupt SPELL(200248) and heal big time when it does SPELL(197429). Tank the adds.]=]
L["T1654"] = "Tank face the boss away from the group. Heal and DPS never stand in front of him! That's it."
L["T1655"] = "Stay behind the boss. When it casts SPELL(204611) prepare to mitigate damage and overheal."
L["T1656"] = [=[Run towards the boss when he uses SPELL(199345) or you will hatch eggs.
Therefore also try to tank him in the center of the room.]=]
L["T1657"] = [=[Stay away from allies if you are afflicted by SPELL(200359).
Move next to an ally if you are afflicted by Waking Nightmare.
]=]
L["T1662"] = [=[Kill the Blightshard Skitters as they spawn. Be sure to kill them before Rokmora casts SPELL(188114).
Make sure to face Rokmora away from the party, or they will take damage from SPELL(188169).
Use Bloodlust/Heroism at the begin of this encounter.]=]
L["T1663"] = [=[When Helya starts to cast SPELL(227233), quickly move away from the area she's facing.
Quickly kill Destructor and Grasping Tentacles as the appear.
Healer: Players afflicted with SPELL(197264) will take damage.
]=]
L["T1664"] = [=[Stand between Smashspite and his target for SPELL(224188), if the target has the debuff from intercepting already.
When it does SPELL(198073) do AoE healing. Also tank eats a lot of damage after SPELL(198245).]=]
L["T1665"] = [=[When Ularogg uses SPELL(198496), be ready to use heavy healing on the Tank.
The longer Ularogg is in the Bellowing Idol, the more damage over time he will increase to your party.
]=]
L["T1667"] = "T1667 ursoc"
L["T167"] = "Tactics on Cho'gall TODO (feel free to add your own tactic here)"
L["T1672"] = "Move along the edges in a circular motion when Dantalionax casts SPELL(199567). Kill adds ASAP and prepare big heals when SPELL(198635) is cast."
L["T1673"] = [=[Make sure at least 1 group member in melee range, if possible, or the group will take damage from SPELL(198963).
Avoid the green circles and green gas on the ground.
When Naraxas uses SPELL(199176) heal tank up and move away as far as you can.]=]
L["T1686"] = [=[Move out of SPELL(201121) when you see the swirl on the ground.
Kill Faceless Tendrils when they spawn from Eternal Darkness.
Tank and Heal use your CD for when it casts SPELL(201148).]=]
L["T1687"] = [=[Take cover behind Crystal Spikes to avoid taking damage from Magma Wave.
Kill adds ASAP or they do heavy AoE damage!]=]
L["T1688"] = [=[Disarm Elementium Squirrel Bombs as they appear and kill the Rocket Chickens ASAP.
Avoid the robot chicken rockets.
]=]
L["T169"] = "Tactics on Omnitron Defense System  TODO (feel free to add your own tactic here)"
L["T1693"] = [=[Stay behind Festerface to avoid his toxic vomit. Kill the Goo that he spawns after he vomits.
Try to move the boss to a killed Goo so that he eats it.]=]
L["T1694"] = [=[Dragon so Stay away from head or tail. Remove SPELL(201379) from tank.
When it casts SPELL(201960) move upstairs ASAP. The Group will suffer damage so heal up.]=]
L["T1695"] = [=[If you get afflicted by SPELL(200905), the next ability you use will have its cooldown increased by 10 seconds.
Interrupt SPELL(200905) and kill adds ASAP.
Tank: Gain threat on the enemies he releases when he is at 70% and 40% health.]=]
L["T1696"] = [=[When it casts IMPALE on you move away! Kill the scarabs that appear after SPELL(201863).
Tank don't stand in front of the boss when he impales ppl. 
Healer use CD as SPELL(202217) reduces Healing on Tank.]=]
L["T1697"] = "Turn to face Phase Spiders that are targeting you with Creeping Slaughter to stop them from approaching."
L["T170"] = "Tactics on Magmaw: TODO (feel free to add your own tactic here)"
L["T1702"] = [=[DPS: save your CD for the princess buff.
Heal: Prepare for group heal and intensive heal for non infected players.
Tank: Move her out of blood pool when she casts Blood Swarm]=]
L["T1703"] = "T1703 Nythendra"
L["T1704"] = "T1704 Dragons of Nightmare"
L["T1706"] = "T1706 Skroypon"
L["T171"] = "Tactics on Atramedes  TODO (feel free to add your own tactic here)"
L["T1711"] = [=[Avoid players targeted by SPELL(203641). If you are affected by it run away.
Free players that are being executed by the boss.]=]
L["T1713"] = "T1713 Krosus"
L["T1718"] = "T1718 Patrol Captain"
L["T1719"] = "T1719 Flamewrath"
L["T172"] = "Tactics on Chimaeron TODO (feel free to add your own tactic here)"
L["T1720"] = "T1720 Adviser melandrus"
L["T1725"] = "T1725 Cronomatic anomaly"
L["T1726"] = "T1726 Xavius"
L["T173"] = "Tactics on Maloriak  TODO (feel free to add your own tactic here)"
L["T1731"] = "T1731 Trillax"
L["T1732"] = "T1732 Star augur"
L["T1737"] = "T1737 Guldan"
L["T1738"] = "T1738 Il gynoth"
L["T174"] = "Tactics on Nefarian  TODO (feel free to add your own tactic here)"
L["T1743"] = "T1743 Grand Master Elis"
L["T1744"] = "T1744 Renferal"
L["T175"] = [=[Right before pulling the boss get the green buff from the cauldron outside the room. Once the fight began you can't refresh the buff.
Green patches of poison will be drawn in a pattern on the ground, avoid stepping in those and jump over them if you have to. Occasionally Venoxis creates a SPELL(96477) between two players which deals constant damage to those two and nearby players. Break the link by running away from each other, but try to run in a direction where no one else is, as an AoE explosion will occur as a result of breaking the link. Avoid the venom puddles on the ground at all costs.
Once the boss transforms in a snake, you need to be aware of his frequent spew attacks SPELL(96753). Immediately walk through him or get out of his sight (including the tank) when you see him casting it.
At a point Venoxis will walk back to his altar, beginning to cast SPELL(97099). Avoid the created poisonous tendrils that follow after random players. After this, he will be exhausted for ~5 seconds and begin the circle anew.]=]
L["T1750"] = "T1750 Cenarius"
L["T1751"] = "T1751 Speedblade Aluriel"
L["T176"] = [=[In the arena you can see some linked spirits. When a partymember dies, such a spirit will resurrect that person + give you a buff. You can't avoid the death that comes with his charge.
Mandokir will ride his raptor. Whenever Mandokir dismounts you have to DPS the raptor as hard as possible because he tries to kill the spirits. Mandokir also casts a spell in front of him. This is announced so get away from his front!
When Mandokir hits 20% he enrages, lowering his cooldown on TODO and increase his damage by 100%. Hit BL/TW/HR to kill him very quick then.]=]
L["T1761"] = "T1761 high botanist"
L["T1762"] = "T1762 Tichondrius"
L["T177"] = [=[Gri'lek, a troll berserker, is a typical melee mob that periodically targets a player. When this happens, he grows in size, deals more damage, and moves noticeably slower.
He does an emote when targeting a player, so everyone has time to move. Avoid his tremor and dispel roots]=]
L["T178"] = "Hazza'rah periodically summons SPELL(96670). Adds target a player and stun them with SPELL(96757). If the add reaches the targeted player, it will use SPELL(96758) and kill the player."
L["T179"] = "Renataki, otherwise a tank-and-spank, periodically casts SPELL(96640) and SPELL(96645), a version of SPELL(51723) on steroids. Run out of his whirlwinding path until he is done attacking and heal the ambushed target."
L["T180"] = "Wushoolay, a troll elemental shaman, casts several lightning abilities you must move away from Players close to SPELL(96699) must run away to avoid being killed. Also avoid the SPELL(96713) and SPELL(96711)."
L["T181"] = [=[While fighting Kilnara, don't bring her down below 50% health until you have killed all panther packs in her room one by one. Occasionally she will summon a purple wave wall SPELL(96460) that will sweep players in front of it away unless circumvented.
Also casts SPELL(96435), dealing shadow damage to nearby players. Should be interrupted.
At 50% she will transform and call any panthers you have left alive to her aid. At ~25% she will briefly disappear and then reappear.]=]
L["T184"] = [=[Zanzil will attack his target with Shadowbolts, which can and must be interrupted. He will also cast an ability called SPELL(96914), which is essentially a fire trail that spawns on the tank and a few yards behind him. Move out of it.
Through the whole fight, he will randomly perform one of the three abilities, which can be countered by the cauldrons located in the vicinity:
SPELL(96319). Zanzil will raize a swarm of non-elite zombies from one of the nearby corpse piles. The tank must grab a Hellfire buff from the Burning Blood cauldron and jump into the newly-spawned pack. 
The DPS must switch to the zombies and bring them down.
SPELL(96338). Zanzil will fill the fighting area with toxic gas, which deals 5/10% of maximum health as Nature damage every second. Getting the buff from SPELL(96328) helps mitigate the damage.
SPELL(96316). Zanzil will revive one of the Berserkers, suspended in the corners of the area. Similar to the trash pack before the boss, the Berserker must be frozen using the Frostburn Formula cauldron. The DPS then must switch from the boss to the Berserker and kill him quickly.]=]
L["T185"] = [=[Get inside Jin'do's damage-reducing green bubble, and keep the boss outside. Tank can stay outside for as long as until Jin'do channels his big spell.
Phase 2 will begin after some time, and Jin'do will be occupied otherwise. You need to taunt the Gurubashi Berserks and make him destroy the shields around Hakkar's Chains by having your party members (except tank) stack on a chain, and have the troll leap at them.
When a shield is destroyed ,focus on the chain. Taunt a new berserk for the next chain, and so on. Drag the minor adds to the chains where the tank can hold them, and with some AoE they should go down as well. Avoid black/purple specks floating towards you.]=]
L["T186"] = [=[New to the encounter is the Amani Kidnapper, an eagle that will periodically seize a party member, casting SPELL(97318). The party, including the seized player, should switch to kill the Amani Kidnapper and free the player.
Akil'zon once again casts SPELL(97299) on the tank and SPELL(97298) on random players, which stacks but can be removed.
When SPELL(97300) is cast, the targeted player will be suspended in the air and shoot an electrical storm across the entire platform. The party should group up underneath the targeted player to avoid taking damage.]=]
L["T187"] = [=[In phase 1, the troll phase, Nalorakk casts SPELL(42384) on the tank.
He also uses SPELL(42402) on the player the farthest away from him. It helps to have a ranged dps and healer alternate the surges.
In phase 2, he turns into a bear. He casts SPELL(97811) on the tank and SPELL(42398) on the party.]=]
L["T188"] = [=[Jan'alai casts SPELL(97488) in the direction of a random player. This is easily avoidable, especially if you stay close to his hit box.
Periodically, Jan'alai will cast SPELL(42630). These orange bombs will spawn haphazardly from the center of the room and explode several seconds later, leaving large gaps where players can safely stand and continue to fight.
Jan'alai will SPELL(45340), NPCs which will run to the sets of eggs at either side of the altar and begin to hatch them until they are killed. 
The hatched Amani Dragonhawk Hatchling, which cast SPELL(43299), should be AoE'd down and dps should watch aggro. It is advisable to have several waves of Hatchlings instead of letting them all spawn at once.]=]
L["T189"] = [=[In the troll phase, Halazzi drops SPELL(97499) which buffs the boss. This totem can be killed; however, you can also move the boss away from the green healing circle and continue to focus dps. He also casts SPELL(43139). 
At 66% and 33%, Halazzi will summon Spirit of the Lynx and heal to 100%. DPS should switch to the Spirit of the Lynx and group up to maximize dps, as the Spirit of the Lynx does not have traditional aggro and will attack random players. 
It will also cast SPELL(43243) and SPELL(43290) which can be especially rough on cloth dps. DPS must switch to kill the Corrupted Lightning Totem before it does extreme amounts of damage. 
Once the Spirit of the Lynx has died, the DPS will go back to Halazzi.]=]
L["T190"] = [=[He is flanked by two random adds, which can be CCed and ignored for the duration of the fight. 
He will alternate between hurling SPELL(43882) at the party and channeling SPELL(43501). As the fight drags on, you will get high stacks of SPELL(44131) , making Malacrassharder to kill.
Use common sense when dealing with all of the temporary player abilities. Interrupt heals, run away from cleaves, and remove debuffs.
Randomly casts soul drain on a raid member, absorbing the character's abilities and amplifying their power. ]=]
L["T191"] = [=[He transforms several times, which is a threat wipe, and he is immune to SPELL(355) but debuffs (and DoTs) will remain on him.
In the first phase, Daakara casts SPELL(15576) and SPELL(97639), for which the player must be healed in order to be removed. Melee dps that eat a whirlwind while afflicted with Grievous Throw may find themselves dead quickly. At 80% and 40%, Daakara transforms into two of the four remaining avatars at random.
In SPELL(42606), Daakara creates an SPELL(43983) and casts SPELL(43112) that players must dodge. Players that run into cyclones will be hit by SPELL(97646). Be sure to kill the SPELL(97930) to prevent additional damage from going out.
In SPELL(42594), Daakara casts SPELL(43095) which can be followed up with a nasty SPELL(42402), as well as SPELL(43456) on the tank. Remove the stun and use cooldowns on the tank.
In SPELL(42608), Daakara creates Column of Fire and periodically casts SPELL(43208) and SPELL(97855). All players must remain alert and constantly avoid fire.
In SPELL(42607), Daakara spawns two adds that can quickly destroy a player if they are not killed quickly, between SPELL(97673) and SPELL(97672).]=]
L["T192"] = "TODO: Tactics on Beth'tilac. Feel free to edit it for yourself!"
L["T193"] = "TODO: Tactics on Rhyolith. Feel free to edit it for yourself!"
L["T194"] = "TODO: Tactics on Alysrazor. Feel free to edit it for yourself!"
L["T195"] = "TODO: Tactics on Shannox. Feel free to edit it for yourself!"
L["T196"] = "TODO: Tactics on Baleroc. Feel free to edit it for yourself!"
L["T197"] = "TODO: Tactics on Majordomo Stagehelm. Feel free to edit it for yourself!"
L["T198"] = "TODO: Tactics on Ragnaros. Feel free to edit it for yourself!"
L["T283"] = [=[The challenge to this fight is avoiding as many spell effects as possible. Standing inside the purple zone will slow casting by 50%, but casters outside the zone will need to move more often to avoid harmful spells.
Tyrande will cast SPELL(102173) which should be interrupted by the tank and melee. She casts SPELL(102151) which splits into three beams after traveling a short distance. She also summons SPELL(102605) which travels around the circle and silences anyone it hits.
She buffs herself to cast faster at 80% and 55% HP, and at 30% she casts SPELL(102241) causing arcane damage to rain down making it more difficult to stay alive.]=]
L["T284"] = "no tactic available yet"
L["T285"] = "Jaina will frequently use SPELL(101927) to throw a growing ember on the ground which will explode for massive damage unless someone steps on it, so everyone should keep an eye out for these. She will occasionally teleport away from the tank and cast SPELL(101339) toward him and then cast SPELL(101810) three times, which should be interrupted if possible."
L["T286"] = "no tactic available yet"
L["T289"] = [=[Murozond has a frontal SPELL(102569) and a rear SPELL(108589), so the tank should be careful about positioning the boss. Slowly kiting the boss in a circle around the hourglass while the dps attack from the side should work well.
When the hourglass in the center of the area is clicked, all HP and mana will be restored to full, all dead players will be revived, and all cooldowns will be reset, so use everything you have to nuke down his large heath pool.
The longer the fight lasts, the faster the boss will cast SPELL(101984) which will become more difficult to avoid.]=]
L["T290"] = [=[The boss casts SPELL(108141) on the ground periodically, creating a circle of fire that you must avoid. He will cast SPELL(104905) causing a large amount of damage and triggering a phase where Illidan helps you hide.
During this phase you should move far away from the center and group up for heals, and then move together avoid the eyes that search for you. If a player is detected, the boss will stun and attack that person.
The boss enrages at low HP causing additional damage to the tank.]=]
L["T291"] = "Interrupt Azshara's SPELL(103241) within 8 seconds. Break players out of mind control by killing the hand. Kill the magi and stun/interrupt if you can."
L["T292"] = "Keep moving during SPELL(103888) to avoid fire. When Tyrande gets stunned,  kill the Dreadlord Debilitators to free her. If you stand in the beam of light you can do big damage to the Doomguards. Pop cooldowns and burn Mannoroth in phase 2."
L["T302"] = "no tactic available yet"
L["T303"] = "no tactic available yet"
L["T311"] = [=[During the Crystal Phase, the tanks need to regularly taunt the boss off of each other and a few DPS players need to soak the proximity damage done by crystals that the boss summons. The closer you are to the crystal, the less damage it will do.
During the Black Blood phase, you'll be pulled in toward the boss and must run behind one of the nearby pillars to avoid standing in the black blood.
The boss enrages at 20% causing additional tank damage.]=]
L["T317"] = "Replace this text with your own tactics for Hagara the Stormbinder"
L["T318"] = "Replace this text with your own tactics for Spine of Deathwing"
L["T322"] = [=[Avoid ice circles on the ground. If Thrall gets frozen in ice then break him out. He spams an interruptable SPELL(102593) on the tank.
At 30% health he will channel SPELL(103962) dealing AoE frost damage to everyone until he is killed. ]=]
L["T323"] = [=[Players should spread out to avoid AoE damage from SPELL(101404). Sylvanas will pull everyone toward her and then summon a ring of ghouls linked by purple beams.
You must single target a ghoul and kill it in order to escape the ring before it collapses because touching a beam will kill you instantly. 
It is helpful to use a raid marker to indicate which ghoul to kill.]=]
L["T324"] = "Replace this text with your own tactics for Warlord Zon'ozz"
L["T325"] = "Replace this text with your own tactics for Yor'sahj the Unsleeping."
L["T331"] = "Replace this text with your own tactics for Ultraxion"
L["T332"] = "Replace this text with your own tactics for Warmaster Blackhorn"
L["T333"] = "Replace this text with your own tactics for the Madness of Deathwing"
L["T335"] = [=[This fight rotates in two phases and is very straightforward:
Phase 1: Spread out to limit SPELL(106113) (which should be dispelled). Boss also casts SPELL(106736) on 2 players.
Phase 2: Boss casts SPELL(117665) creating a (non-tauntable) Figment of Doubt copy of each player.
All stack on the tank to maximize damage--killing the healer's add First!
Then kill as many of the others before they explode, healing the boss for 10%.]=]
L["T339"] = "Tactics for Alizabal TODO (feel free to add your own tactic here)"
L["T340"] = "Ranged spread out to different platforms. The boss will SPELL(101614) at a random person knocking you back. You can pick up the totem and throw it back at him increasing his damage taken by 50%. "
L["T341"] = [=[Benedictus will summon a large wave that moves across the platform. Don't stand in front of it. Avoid glowing circles on the ground.
He spams SPELL(104503) and SPELL(104504) which can be interrupted.]=]
L["T342"] = [=[Asira will cast SPELL(102726) on a random caster in the group. This person should try to keep the tank or a melee DPS between them and the boss to avoid being silenced.
Asira should be moved out of SPELL(103558) and other players should move out too.]=]
L["T369"] = "no tactic available yet"
L["T370"] = "no tactic available yet"
L["T371"] = "no tactic available yet"
L["T372"] = "no tactic available yet"
L["T373"] = "no tactic available yet"
L["T374"] = "no tactic available yet"
L["T375"] = "no tactic available yet"
L["T376"] = "no tactic available yet"
L["T377"] = "no tactic available yet"
L["T378"] = "no tactic available yet"
L["T379"] = "no tactic available yet"
L["T380"] = "no tactic available yet"
L["T381"] = "no tactic available yet"
L["T382"] = "no tactic available yet"
L["T383"] = "no tactic available yet"
L["T384"] = "no tactic available yet"
L["T385"] = "no tactic available yet"
L["T386"] = "no tactic available yet"
L["T387"] = "no tactic available yet"
L["T388"] = "no tactic available yet"
L["T389"] = "no tactic available yet"
L["T390"] = "no tactic available yet"
L["T391"] = "no tactic available yet"
L["T392"] = "no tactic available yet"
L["T393"] = "no tactic available yet"
L["T394"] = "no tactic available yet"
L["T395"] = "no tactic available yet"
L["T396"] = "no tactic available yet"
L["T397"] = "no tactic available yet"
L["T398"] = "no tactic available yet"
L["T399"] = "no tactic available yet"
L["T400"] = "no tactic available yet"
L["T401"] = "no tactic available yet"
L["T402"] = "no tactic available yet"
L["T403"] = "no tactic available yet"
L["T404"] = "no tactic available yet"
L["T405"] = "no tactic available yet"
L["T406"] = "no tactic available yet"
L["T407"] = "no tactic available yet"
L["T408"] = "no tactic available yet"
L["T409"] = "no tactic available yet"
L["T410"] = "no tactic available yet"
L["T411"] = "no tactic available yet"
L["T412"] = "no tactic available yet"
L["T413"] = "no tactic available yet"
L["T414"] = "no tactic available yet"
L["T415"] = "no tactic available yet"
L["T416"] = "no tactic available yet"
L["T417"] = "no tactic available yet"
L["T418"] = "no tactic available yet"
L["T419"] = "no tactic available yet"
L["T420"] = "no tactic available yet"
L["T421"] = "no tactic available yet"
L["T422"] = "no tactic available yet"
L["T423"] = "no tactic available yet"
L["T424"] = "no tactic available yet"
L["T425"] = "no tactic available yet"
L["T427"] = "no tactic available yet"
L["T428"] = "no tactic available yet"
L["T429"] = "no tactic available yet"
L["T430"] = "no tactic available yet"
L["T431"] = "no tactic available yet"
L["T432"] = "no tactic available yet"
L["T433"] = "no tactic available yet"
L["T434"] = "no tactic available yet"
L["T435"] = "no tactic available yet"
L["T438"] = "no tactic available yet"
L["T439"] = "no tactic available yet"
L["T440"] = "no tactic available yet"
L["T441"] = "no tactic available yet"
L["T442"] = "no tactic available yet"
L["T443"] = "no tactic available yet"
L["T445"] = "no tactic available yet"
L["T446"] = "no tactic available yet"
L["T448"] = "no tactic available yet"
L["T449"] = "no tactic available yet"
L["T450"] = "no tactic available yet"
L["T451"] = "no tactic available yet"
L["T452"] = "no tactic available yet"
L["T453"] = "no tactic available yet"
L["T454"] = "no tactic available yet"
L["T455"] = "no tactic available yet"
L["T456"] = "no tactic available yet"
L["T457"] = "no tactic available yet"
L["T458"] = "no tactic available yet"
L["T459"] = "no tactic available yet"
L["T463"] = "no tactic available yet"
L["T464"] = "no tactic available yet"
L["T465"] = "no tactic available yet"
L["T466"] = "no tactic available yet"
L["T467"] = "no tactic available yet"
L["T469"] = "no tactic available yet"
L["T470"] = "no tactic available yet"
L["T471"] = "no tactic available yet"
L["T472"] = "no tactic available yet"
L["T473"] = "no tactic available yet"
L["T474"] = "no tactic available yet"
L["T475"] = "no tactic available yet"
L["T476"] = "no tactic available yet"
L["T477"] = "no tactic available yet"
L["T478"] = "no tactic available yet"
L["T479"] = "no tactic available yet"
L["T480"] = "no tactic available yet"
L["T481"] = "no tactic available yet"
L["T482"] = "no tactic available yet"
L["T483"] = "no tactic available yet"
L["T484"] = "no tactic available yet"
L["T485"] = "no tactic available yet"
L["T486"] = "no tactic available yet"
L["T487"] = "no tactic available yet"
L["T489"] = "no tactic available yet"
L["T523"] = "no tactic available yet"
L["T524"] = "no tactic available yet"
L["T527"] = "no tactic available yet"
L["T528"] = "no tactic available yet"
L["T529"] = "no tactic available yet"
L["T530"] = "no tactic available yet"
L["T531"] = "no tactic available yet"
L["T532"] = "no tactic available yet"
L["T533"] = "no tactic available yet"
L["T534"] = "no tactic available yet"
L["T535"] = "no tactic available yet"
L["T536"] = "no tactic available yet"
L["T537"] = "no tactic available yet"
L["T538"] = "no tactic available yet"
L["T539"] = "no tactic available yet"
L["T540"] = "no tactic available yet"
L["T541"] = "no tactic available yet"
L["T542"] = "no tactic available yet"
L["T543"] = "no tactic available yet"
L["T544"] = "no tactic available yet"
L["T545"] = "no tactic available yet"
L["T546"] = "no tactic available yet"
L["T547"] = "no tactic available yet"
L["T548"] = "no tactic available yet"
L["T549"] = "no tactic available yet"
L["T550"] = "no tactic available yet"
L["T551"] = "no tactic available yet"
L["T552"] = "no tactic available yet"
L["T553"] = "no tactic available yet"
L["T554"] = "no tactic available yet"
L["T555"] = "no tactic available yet"
L["T556"] = "no tactic available yet"
L["T557"] = "no tactic available yet"
L["T558"] = "no tactic available yet"
L["T559"] = "no tactic available yet"
L["T560"] = "no tactic available yet"
L["T561"] = "no tactic available yet"
L["T562"] = "no tactic available yet"
L["T563"] = "no tactic available yet"
L["T564"] = "no tactic available yet"
L["T565"] = "no tactic available yet"
L["T566"] = "no tactic available yet"
L["T568"] = "no tactic available yet"
L["T569"] = "no tactic available yet"
L["T570"] = "no tactic available yet"
L["T571"] = "no tactic available yet"
L["T572"] = "no tactic available yet"
L["T573"] = "no tactic available yet"
L["T574"] = "no tactic available yet"
L["T575"] = "no tactic available yet"
L["T576"] = "no tactic available yet"
L["T577"] = "no tactic available yet"
L["T578"] = "no tactic available yet"
L["T579"] = "no tactic available yet"
L["T580"] = "no tactic available yet"
L["T581"] = "no tactic available yet"
L["T582"] = "no tactic available yet"
L["T583"] = "no tactic available yet"
L["T584"] = "no tactic available yet"
L["T585"] = "no tactic available yet"
L["T586"] = "no tactic available yet"
L["T587"] = "no tactic available yet"
L["T588"] = "no tactic available yet"
L["T589"] = "no tactic available yet"
L["T590"] = "no tactic available yet"
L["T591"] = "no tactic available yet"
L["T592"] = "no tactic available yet"
L["T593"] = "no tactic available yet"
L["T594"] = "no tactic available yet"
L["T595"] = "no tactic available yet"
L["T596"] = "no tactic available yet"
L["T597"] = "no tactic available yet"
L["T598"] = "no tactic available yet"
L["T599"] = "no tactic available yet"
L["T600"] = "no tactic available yet"
L["T601"] = [=[A random player will get SPELL(72452) so heal him or he'll get 16k shadow damage.
You should disspell SPELL(72426) within 6 seconds. He will cast other healing reducing debuffs so you have to heal through it]=]
L["T602"] = [=[Get out of SPELL(72362). When SPELL(72369) is dispelled the not dealt DoT-damage will be spread instantly over the party, so let it tick a while.
On occasion he will reduce your health by 50% so once again very heal intensive!]=]
L["T603"] = "very simple in theory. LK will summon some bad guys and you have to kill them ASAP. Don't let the LK touch you and try to not get your healer sliced and focus caster and fat guys."
L["T604"] = "no tactic available yet"
L["T605"] = "no tactic available yet"
L["T606"] = "no tactic available yet"
L["T607"] = "no tactic available yet"
L["T608"] = [=[Boss casts SPELL(70336) regular and it stacks. He also SPELL(68788) at a random player, go away from the dropping zone.
SPELL(70336) won't hit you when hiding behind the saronite. Every partymember should try to hide behind them every now and then so that the frost damage won't stack too high.]=]
L["T609"] = [=[The boss has basically three options which he casts every now and then.
SPELL(70434): Run away from him especially melee and tank!
SPELL(69263): Everbody move! Don't stand still to avoid the blue explosions.
SPELL(68987): The person that is pursuited has to run away from the boss.]=]
L["T610"] = [=[Rimefang (dragon) on occasion marks an area. Leave that area, because some frost patches will appear there.
Player who gets SPELL(69172) should stop DPS immediately because otherwise will bypass DPS to tank.
If boss gets SPELL(69629) you should try to kite him over the frost patches on the ground so he can't hit you.]=]
L["T611"] = "no tactic available yet"
L["T612"] = "no tactic available yet"
L["T613"] = "no tactic available yet"
L["T614"] = "no tactic available yet"
L["T615"] = [=[SPELL(68793) is his main attack. He will cast SPELL(68893) on a random player causing him to seperate his soul. Don't let the soul reach the bos or he will get healed!
When boss reaches 30% health he will SPELL(68872) and randomally Fear a player. Dispell the fear and nuke him down]=]
L["T616"] = [=[Try to interrupt her main attack SPELL(70322) as often as possible. When she casts SPELL(69051) stop all of your damage immediately or the debuffed player will die.
On occasion she casts SPELL(68939) summoning many ghosts. Run away if the damage taken is too high.
Finally she cats SPELL(68912) creating a wall which is killing you on contact. She rotates 90 degrees so go with her to avoid contact.]=]
L["T617"] = "no tactic available yet"
L["T618"] = "no tactic available yet"
L["T619"] = "no tactic available yet"
L["T620"] = "no tactic available yet"
L["T621"] = "no tactic available yet"
L["T622"] = "no tactic available yet"
L["T623"] = "no tactic available yet"
L["T624"] = "no tactic available yet"
L["T625"] = "no tactic available yet"
L["T626"] = "no tactic available yet"
L["T627"] = "no tactic available yet"
L["T628"] = "no tactic available yet"
L["T629"] = "no tactic available yet"
L["T63"] = "no tactic available yet"
L["T630"] = "no tactic available yet"
L["T631"] = "no tactic available yet"
L["T632"] = "no tactic available yet"
L["T634"] = "no tactic available yet"
L["T635"] = "no tactic available yet"
L["T636"] = "no tactic available yet"
L["T637"] = "no tactic available yet"
L["T638"] = "no tactic available yet"
L["T639"] = "no tactic available yet"
L["T640"] = "no tactic available yet"
L["T641"] = "A very easy tank and spank. When she casts the ritual make sure you kill the 3 adds with AoE otherwise you will get a DoT-Debuff that ticks very high."
L["T642"] = [=[Boss will activate all 4 adds, one after another. The adds are easy tank and spank and don't have any specialty.
The boss itself cleaves so don't stand in front of him (if you are not tank).]=]
L["T643"] = [=[Adds are spawning. Kill them and use the spears when the boss is near the cannon. If you hit him three times he will "land".
Boss will DOT a player with SPELL(59331). Melee: make sure to run away from his SPELL(59322)]=]
L["T644"] = [=[Well the boss does many abilities but infact you can just DPS him down and heal your party like crazy.
It's like most of Wotlk 5-mans. you don't really need tactics at all for most of the bosses.]=]
L["T649"] = [=[Phase 1: Boss is nearly invulnerable.  When he charges the doors, stay away from the impact zone.
DPS: 'mount up' in cannons around the courtyard and break through his carapace (use: /tar Weak Spot ).
Tank & Healer: remain on the ground, killing adds.  Swarm Bringers are top priority, due to SPELL(111600).
Kill Protectorates next by EVENLY damaging all 8.  This minimizes their SPELL(107324), which they cast at 20%.  Finally kill the Engulfers, if possible.
Phase 2: Adds no longer spawn. Boss is now vulnerable to attacks and will SPELL(111723) on random players.  They should use its speed buff to kite him away from the party, minimizing his SPELL(111728).
This boss doesn't need to be tanked.  Finish him off and any remaining adds from Phase 1.]=]
L["T654"] = [=[Tank the boss near a wall, away from the room's center (or up top by the entrance).  Face him away from the party to minimize his SPELL(111217). Healers should dispell his SPELL(111221).
To win this fight, you MUST avoid his SPELL(111216) or they will suck you in and kill you!
He always casts it right after performing a SPELL(111218) into the center.  He then spins along the same path around the room, but not always in the same direction.
Run up the stairs (or stay up top) when he begins to spin and jump down as he approaches.  Then run up the stairs behind him, as he returns to the center.
Pairs of adds can appear during the fight.  They have an armor buff but can be ignored, if you're not taking too much damage. (They can't survive his whirlwind, either.)]=]
L["T655"] = [=[The boss continuously throws bombs around the room, which detonate when hit by a fire trail.
Exploding bombs leave fire trails only in the 4 cardinal directions.  So players should always stand where there are no bombs directly to the north, south, east, or west of them.
The boss also casts SPELL(107268) on random players, who should quickly move behind pillars to avoid a chain reaction.
At 70% and 30% the boss detonates all bombs currently in the room.]=]
L["T656"] = [=[Interrupt his SPELL(113691) and SPELL(113690), unless he casts SPELL(113682) first.
Move in a circle behind him to avoid his SPELL(113653).
Try to intercept his SPELL(113364) missiles or just stay out of their large fiery zones.]=]
L["T657"] = [=[Intro: defeat the waves of Novices and the 2 Advanced students.  Run away when they fixate.
Phase 1: Get behind the boss when he uses SPELL(106853) and move away when he uses SPELL(106434).
Phase 2: At 50%, he splits into 3 parts.  Avoid his SPELL(106533) and kill the copies.
Phase 3: Run away when fixated and stop attacking when he uses SPELL(106454).]=]
L["T658"] = [=[Phase 1: Avoid her SPELL(106938) and remove the tank's fire DoT from SPELL(106823).
Phase 2: Avoid her SPELL(107053). The DoT from SPELL(106841) is not removeable, so outheal the damage and the absorb.
Phase 3: Tank the Jade Serpent, spread out and outheal its SPELL(107110).]=]
L["T659"] = [=[Phase 1: Boss sweeps an SPELL(111231) through the room.  So kill her ASAP!  She casts SPELL(111606) on herself and SPELL(111631), which jumps from player to player.
Stay away from the party if debuffed by SPELL(111610).
Phase 2: Boss esacpes to her Phylactery, casting SPELL(113809) at random players.  Again, kill her quickly!
Avoid SPELL(113859) and SPELL(120027) damage from the flying books.]=]
L["T660"] = [=[Spread out at least 5 yards to minimize damage.
Boss targets random players with his SPELL(114242) AoE and a SPELL(114020), which hits everyone in its path.  Both cause a SPELL(114056) stacking debuff.
At 90% 80% 70% and 60% he will SPELL(114259). DPS: switch to each dog to avoid getting overwhelmed!
At 50% he enters a SPELL(116140), doing more damage and attacking faster.]=]
L["T663"] = [=[Phase 1: Don't stand in front of her SPELL(114062). Avoid her SPELL(114038). It has a purple ground effect.
Phase 2: At 66% and 33% she copies herself.  Kill her EXACT copy (same hair color, earrings, etc.) to end the phase.
Tab through copies, looking at their portraits (or mark her with a Skull in Phase 1).
Wrong copies inflict SPELL(113866) damage when destroyed, so don't AoE them.]=]
L["T664"] = [=[There are two possible encounter.
Zao Sunseeker: Kill the Suns, AoE the adds, kill the boss.
Peril and Strife: Start by attacking Peril until he reaches 8-9 stacks, then switch to Strife, and vice versa, so their SPELL(113315) buff will be consumed by SPELL(113379).]=]
L["T665"] = [=[Everyone MUST maintain a SPELL(113996) buff.  Click on bone piles around the room to get it.
The buff protects against one SPELL(113999) attack and lasts 5 minutes. So refresh your Armor if Spiked, but avoid bone piles engulfed in blue SPELL(114009).
Boss gets a SPELL(113765) buff each time he hits in melee. This stacks 99 times, but only lasts 15 secs.  If his damage gets too high, kite him around to remove it. (It also gives him a speed debuff.)]=]
L["T666"] = [=[Phase 1: Spread out.  Lilian leaps on random players, using a SPELL(111775) AoE in the impact zone.
She also grabs everyone in her SPELL(111570), inflicting an 8-second SPELL(111585) debuff.  All players must immediately move away, avoiding each others' flame trails.
Phase 2: At 60% the fight switches to her soul, which casts SPELL(115350) on a random player.  Kite the soul around when fixated to avoid being hit.
Successful hits trigger SPELL(111649), reducing the soul's speed (making it easier to kite), but also inflicting its SPELL(111642) AoE on everyone.
Healers: these both cause a LOT of damage!
Phase 3: At 1% the soul becomes invulnerable and Lilian re-enters the battle.
Both retain all abilities from Phases 1 & 2.  Spread out and continue to kite the soul, even when running out Dark Blaze.  The fight ends when Lilian reaches 1%.]=]
L["T668"] = [=[Kill 40/40 Hozen to make boss appear.  Tank him facing away from the group because of his SPELL(106807).
DPS: hop on barrels and crash into the boss giving him a SPELL(106648) debuff.
At 90% 60% and 30% boss casts SPELL(106651), stacking all three times.]=]
L["T669"] = [=[Pull everything and just walk to the boss room.  Every time you face the adds do the following:
Kill the Boppers first and take their hammers.  Each hammer stacks +3 charges to your extra action button.
Use hammers to kill remaining Hoplings AND bump explosive Hoppers into the air, so they don't detonate on the ground.
Tank the boss facing away from the group because of his SPELL(112945). Also avoid damage from his SPELL(112992).]=]
L["T670"] = [=[He will use 3 out of 6 possible abilities.
SPELL(106546): Spread out and outheal damage. SPELL(106851): Jump to get rid of the debuff.
Bubble Shields: Destroy all 8.  SPELL(114451): stand in its stream to block the healing.
If the ground is full of SPELL(115003) use the SPELL(114459) to float and avoid the damage.
If he casts SPELL(114466) jump high as they move past.]=]
L["T671"] = [=[For each 10% health lost, the boss stacks SPELL(114410) increasing fire damage by +10%.
Move away from his front when he channels SPELL(114808) (including tank).
Move away from his landing zone when he casts SPELL(113764).
At 50% health, he will also leave a trail of fire from SPELL(114460).]=]
L["T672"] = [=[Phase 1: Move to back of the room, don't stand in the water, and kill the 4 elementals.
Phase 2: Boss is now vulnerable.  Avoid his SPELL(106334) which is basically a beam that rotates.]=]
L["T673"] = [=[Phase 1: Spread out to avoid AoE damage from SPELL(106984). When the Serpent arrives, pull the boss out of the SPELL(128889).
Phase 2: Group up UNDER the Serpent to attack it (except tank). When boss casts SPELL(107140) overheal that player fast and then stack because player will AoE heal everyone.
Phase 3: Serpent fails & Cloudstrike enrages! Pop BL, TW, HR to kill him ASAP.]=]
L["T674"] = [=[Phase 1: Tank and spank Commander Durand, avoiding his SPELL(115739).
Phase 2: Whitemane casts SPELL(9256) on party and SPELL(9232) on Durand.
Focus on the Commander again, but you MUST interrupt Whitemane's SPELL(113134) or the whole room will revive and you'll probably wipe.
Once he's down, finish her off, interrupting SPELL(12039) if possible.]=]
L["T675"] = [=[The boss casts SPELL(107047) and SPELL(106933), both require intense healing.
Players need to avoid the green poison patches from Mantid SPELL(115458) attacks.
At 70% and 30% the boss makes a double SPELL(116297) (first west to east, then north to south), engulfing the platform in flames and isolating the remaining areas.
Party should stick together when avoiding the flames, to kill the adds that appear.]=]
L["T676"] = [=[Kite this boss up one side of the ramp (and down the other, if needed).  Avoid green poison patches of SPELL(107091).
Standing in them gives the boss a 10% attack buff & reduces his damage taken.  This stacks up to 5 times.
DPS: kill any adds that join the fight, but avoid standing in front of the boss due to his SPELL(107120).  Healers: watch out for the adds' SPELL(116633) attacks.]=]
L["T677"] = [=[Tanks: separate the bosses, facing them toward stairs, to avoid overlapping their Devastating Combos (random 180-degree SPELL(116835) & 12-yard SPELL(132425)) attacks.
Blue arcs in air show each strike's direction. Avoid all 10 combo strikes (5 in lfr) to gain SPELL(116809) (extra action button, lasts 3 seconds).
DPS: Adds are your Priority!  If they accumulate, healers will be overwhelmed.
Emperor's Courage: fixates on a tank and is the Highest Priority.  Kill from behind, before it reaches target.
Emperor's Strength: can be tanked by any melee. Avoid their SPELL(116550) by running behind them.
Emperor's Rage: are weak and spawn in pairs, fixating on non-tanks.  These are lowest priority adds.
During SPELL(116803) move near healers and use defensive cooldowns.  Powerful raid-wide healing is required, after.]=]
L["T679"] = [=[Healers: don't let tanks die to SPELL(125206).
Ranged: hang out on the carpet.  All: avoid SPELL(130774) and Cobalt Mines (unless protected by SPELL(115861)).
If you get SPELL(130395) stack with the other player.  If SPELL(116038) is up, spread out to break chains with reduced damage.]=]
L["T682"] = [=[Enter Spirit World to kill Minions or they'll pile up.
To Cross Over, stand near a Spirit Totem and DPS it down. Players affected by SPELL(122151) cannot Cross Over.
In Normal mode, get SPELL(120717) and punch the SPELL(120715) button within 30 seconds to avoid death.
Access to the Spirit World is blocked, once Gara'jal reaches 20% & starts SPELL(117752).]=]
L["T683"] = "no tactic available yet"
L["T684"] = [=[Boss casts SPELL(113136) at the tank and SPELL(113141) at random players.  He'll also channels SPELL(113143).
DPS: focus on the adds while he's channeling, then back on him when he's through.
He occasionally teleports a random player away to teach them a SPELL(113395) at the hands of many adds.  For Tanks & DPS, just kill the adds quickly and return to the main room.
For Healers, kite the Expired adds to the back of the room and dispell SPELL(113312) from the Fresh adds there.]=]
L["T685"] = [=[Dispell the SPELL(106872) or tank will get heavy damage and confused.  Move away when the ground begins to vibrate to avoid SPELL(106871) attacks. Kill spawned Volatile Energy asap.
When the boss casts SPELL(106827), use as many attacks or spells as possible. Once it's finished, you'll receive SPELL(127576).
Sha will cast SPELL(38166) when its health is low, so pop TW, BL, or HR here.]=]
L["T686"] = [=[Your new Hatred bar fills when you get hit by any of boss' abilities.  Use SPELL(107200) (new button) to clear it.
When boss casts SPELL(115002) quickly DPS them down, before everyone gets sucked in.
Remain inside (or stay out of) his SPELL(131521).  Healer: boss targets the tank with SPELL(114999) when Ring of Malice is active.]=]
L["T687"] = [=[At the pull and while Qiang is active, STACK on the tank to minimize SPELL(117920).
Run out of SPELL(117948) and dodge SPELL(117910).  Re-stack on the tank after these.
For Meng's SPELL(117708), stack up and use non-DoT AoE.  Do not DoT your fellow players or use SPELL(50842), etc.
Spread out for Subetai, to minimize his SPELL(118122).  Dodge SPELL(118094) and run away from SPELL(118047).
If targeted by Zian's SPELL(117506), get clear of the raid while everyone blasts it down.]=]
L["T688"] = [=[Try to interrupt SPELL(115289) as often as possible. Healers will need to dispell SPELL(115297).
When the boss casts SPELL(115139), kill the Crusader(s) ASAP, before boss can SPELL(115147) to possess them.
Don't let the Spirit reach its target or it'll turn into an Empowered Zombie. Off-Tank the Zombie if that happens.]=]
L["T689"] = [=[Phase 1: (Statue: Fist) when boss casts SPELL(116040), stack up at a distance under tank's SPELL(115817).
Ranged: be ready to dodge SPELL(116295).
Phase 2: (66%, Spear) For SPELL(116784), drop fire patches at room's edges. During SPELL(116711), they flow to boss.
Phase 3: (33%, Staff) STACK on boss during SPELL(116364) for reduced damage (use SPELL(115817) if available.)
If you get SPELL(116417) stay away from party, but closer during Arcane Velocity, without causing more damage.
Phase 4: (Shield, Heroic only) Stack up away from SPELL(118071) to AoE adds quickly (using Nullification Barrier on it despawns them). Heal through SPELL(118783).
In Heroic mode, phases change every 25% health (instead of 33%). Phase order is determined by nearest statue. Kite boss to: Staff, Fist, Shield, then Spear.]=]
L["T690"] = [=[It's all about kill order. (Plus, CC's & interrupts!)
1st kill Ironhide (reduces allies' damage taken 50%), next either Skulker (5 second stuns) or Oracle (heals allies),
and finally kill Hexxer (reduces casting speed 50%).
Then Gekkan is no problem because he receives 100% more damage from 4 stacks of SPELL(118988).
In Heroic mode, Pillager spawns.  Burn him down for extra loot.]=]
L["T691"] = "no tactic available yet"
L["T692"] = [=[The General will cast SPELL(124317) at a random player.  Get away from that area!
Healers remove the SPELL(119875) debuff from any player.
When he calls in reinforcements, use the bombs to lower his shield or to kill the adds.
You only can throw 1 of the 2 types of bombs.  Move away from the other type.
Beware: If armed, the bombs will explode within 3 seconds.]=]
L["T693"] = [=[Central sap puddle grows over time. Stand in it briefly to make it shrink.
When boss casts SPELL(120001) on the puddle, the bigger it is, the more damage it causes.
Kill blobs before they reach puddle or it'll grow faster.]=]
L["T694"] = "no tactic available yet"
L["T695"] = "no tactic available yet"
L["T696"] = "no tactic available yet"
L["T697"] = "no tactic available yet"
L["T698"] = [=[Tank boss away from the party--all the way to entry alcove, if possible.
Avoid Whirlwind Axes throughout the fight.  Get behind boss when he casts SPELL(119684).
Get out of his staff's Circle of Flame before it completes.
At 66%, he activates Blade Trap.  Watch out for flying blades from the sides.
At 33%, he targets all crossbows at a random player, which must be healed through.]=]
L["T708"] = [=[You have to face three Champions to win the fight.
Right boss: Kill his Quilen first because it's not tauntable.  Get behind boss when he casts SPELL(119922).
Middle boss: Tank and spank.  Spread out and avoid his SPELL(119971).
Left boss: Group up when he casts SPELL(120195) to share the damage.  Healers: watch out for his SPELL(123655).]=]
L["T709"] = "no tactic available yet"
L["T713"] = [=[STAY OUT OF THE PURPLE CIRCLE under Garalon or trigger a SPELL(122774), causing heavy raid-wide damage!
Avoid the growing SPELL(123120).  If you get SPELL(123092) run to the tanks and pass it to them.
Tanks: use Pheromones to kite the boss around arena and swap them when SPELL(123081) stacks too high.
Also, keep at least two people in Garalon's SPELL(122735) cone, or he gains a SPELL(122754) stack.
DPS: Get in blue dashed circle for SPELL(123428) buff and keep 4 legs broken to stack SPELL(122786) debuff.  Attack body, until he casts SPELL(123495).
In Heroic mode, he casts SPELL(122774) every 35 seconds!  In Normal mode (only), Pheromone swaps trigger a SPELL(122774).]=]
L["T725"] = "no tactic available yet"
L["T726"] = [=[First 3 Phases cycle twice: 1-2-3, 1-2-3, & 4.
Phase 1: Inner Ring stacks SPELL(117878) debuff on players. Move to Outer Ring often, to drop it. (LFR: NO debuff.)
Tanks: face boss away (SPELL(117960)) & swap regularly to dump Overcharged.  All: switch to Celestial Protector(s).
Off-tank: kite add near Inner/Outer border (to drop Overcharged).  Keep it within Inner Ring, until below 25%.
In Heroic Mode, add's SPELL(129711) deathcry MUST be soaked by at least 3 (in 25-man) or 1 (in 10-man) players OR the raid wipes from SPELL(132256)!
Phase 2: (boss at 85%, then 50%) Destroy Energy Charge waves.  Each re-spawn, stacks permanent debuff on boss.  Move to Outer Ring before floor vanishes OR DIE!
Phase 3: (floor gone!) Avoid Energy curtains.  Attack Empyreal Foci in opposing pairs (LFR: any order) to respawn floor .
Phase 4: beat Enrage timer! Use HR, BL, TW now.]=]
L["T727"] = [=[Move out of SPELL(121443). If caught in SPELL(121447), just jump to break free.  Healers: watch out for SPELL(121762).
When the boss flies toward the end of the bridge, make sure to reach him and interrupt his SPELL(121284).]=]
L["T728"] = "no tactic available yet"
L["T729"] = "no tactic available yet"
L["T737"] = [=[Phase 1: Kill small adds, avoid ground effects, always DPS Mutated Construct players to just below 20% health.
Constructs: cast SPELL(122395) to self-interrupt SPELL(122398) (AoE auto-cast) and use SPELL(123060) before Willpower runs out OR DIE!
Also, use SPELL(123156) on SPELL(122504) pools to regenerate Willpower, but save most pools for final phase.
Phase 2: At 70%, boss casts SPELL(122540).  DPS: focus on Amber Monstrosity (& Constructs' 20% health).  All: avoid 15-yard SPELL(122408) range.
Phase 3: (Monstrosity dead) boss casts SPELL(122556). Constructs: consume SPELL(122504) pools as needed and remember to SPELL(123060).
In Heroic mode, kite (fixating) Amber Globules together to despawn them & avoid 30-yard explosion killzones!]=]
L["T738"] = [=[Phase 1: Throw SPELL(120778) kegs into Mantids' path, but don't hit your teammates.
Kill any adds that get through and simply avoid bad patches on the ground.
Kill the Demolishers by just hitting them with tar. Ranged should target Amber Wing when it attacks.
Phase 2: Move away when boss casts SPELL(120760) and reapply the tar debuff afterward.
Try to evade his SPELL(120800).]=]
L["T741"] = [=[Spread out to minimize SPELL(121896) and SPELL(121885) traps (right-click to free trapped players).
Immediately move toward walls, if debuffed with SPELL(122064). Don't fill center of the room with void zones.
At 75%, boss casts SPELL(131813).  Bombs do not despawn and must be avoided throughout fight.
Adds Enrage on pull.  Same types share health pools.  Boss gains SPELL(122354) when a type is eliminated.
Tank: pull boss and a few adds.  Off-Tank(s): pull remaining adds for AoE.  DPS: adds are THE priority.
Crowd Control adds, up to limit: none with 3 adds, 2 with 6 adds, 3 with 9 adds (4 with 9, in Normal mode).
In Heroic mode, killed groups respawn after 45 secs.]=]
L["T742"] = "Tsulong"
L["T743"] = [=[First 2 phases alternate, until boss at 30%.
Tanks: always swap at 4 stacks of SPELL(123707) or become SPELL(123713), probably wiping.
Phase 1: (2.5 mins vs. boss) Spread out 5+ yards, unless standing in a SPELL(123184).  Ensure both Fields are not destroyed at the same time:
Players with SPELL(123792) debuff: stand in SAME Field, until it's almost gone, or debuff fades, or health gets low.
Healers: those in Fields cannot be healed. Focus on exiting players, especially if Field is about to SPELL(123504).
Phase 2: (2.5 mins vs. adds) boss fades, 8 adds spawn. They gain SPELL(125422) buff, if within 8 yards one another.
Small adds leave SPELL(124748) pools, which attach on contact. Stepping in 2nd pool removes debuff, but also merges them. 4 merged pools become an SPELL(125826).
Tanks: split up adds: 3 Windblades & 1 Reaver, each.  Kill small adds first.  Amber Traps work on Reavers.
Phase 3: (Burn phase vs. boss) Use TW, HR, BL. Spread out 8+ yards to avoid SPELL(124868) fear debuff. Tanks: face boss away and swap at 4 stacks, again.]=]
L["T744"] = [=[Phase 1: Finish this boss ASAP.  Throughout fight, he stacks soft Enrages and continually creates damaging tornadoes.
Tank: kite boss away from large collections of tornadoes (which don't despawn) and swap on SPELL(123474).
Ranged & Healers: spread out 8+ yards and avoid tornadoes, but be prepared to STACK on target of SPELL(122994).
In Heroic mode, boss casts SPELL(125312) on the entire raid, every 60 seconds.  Run away while he's spinning.
Phase 2: At 20% boss whisks party to one end of the hallway, while he flies to the other.
Use SPELL(34844), SPELL(36589), SPELL(77764), etc. to avoid oncoming tornadoes or be transported back to starting point.
Boss stacks SPELL(123471) (soft Enrage) every 10 secs and deals moderate raid-wide Nature damage every second!
At 10% he will fly to the opposite end of the hall, while all tornadoes reverse direction.  Avoid them, once again, to reach him.]=]
L["T745"] = [=[Boss casts SPELL(122852) & channels SPELL(122760) all throughout. He also channels SPELL(123791) when not in melee.
Phase 1: Zor'lok switches platforms each time his health drops by 20%.  Each platform grants a specific ability:
Pl-1: At 100%, SPELL(122718): Distribute party evenly among the yellow shields while he's channeling.
Pl-2: At 80%, SPELL(123721): Circle boss in the direction of Rings.  Avoid the straight-moving Sonic Pulses.
Pl-3: At 60%, SPELL(122740): Attack mind-controlled players to 50% & dispell any debuffs they apply.
Beware, SPELL(123812) gas on ground!
When boss vacates Pl-1 & Pl-2, he leaves behind a copy, which only uses that specific ability & Song of the Empress.
Split the party (possibly twice) when the boss switches platforms and kill copies before phase ends or face a wipe!
Phase 2: At 40%, boss flies to center and absorbs gas for 10% Damage/20% Haste buff, then pulls everyone to him.
He uses ALL platform-specific abilities and creates another Pl-2 copy in the center. Save HR, BL, & TW for this!
A tank & healer can kite copy to far corner of the room, although healing through Force & Verve may be a problem.
In Normal mode & LFR, NO copies are created, Attenuation lacks Sonic Pulses, and each yellow shield protects slightly more (LFR: unlimited) players.]=]
L["T748"] = "Obsidian Sentinel"
L["T749"] = "Commander Malor"
L["T816"] = "Council of Elders"
L["T817"] = "Qon"
L["T818"] = "Durumu the Forgotten"
L["T819"] = "Horridon"
L["T820"] = "Primordius"
L["T821"] = "Megaera"
L["T824"] = "Dark Animus"
L["T825"] = "Tortos"
L["T827"] = "Jin'rokh the Breaker"
L["T828"] = "Ji-Kun"
L["T829"] = "Twin Consorts"
L["T832"] = "Lei Shen"
L["T846"] = "Tactics Malkorok"
L["T849"] = "Tactics The Fallen Protectors"
L["T850"] = "Tactics General Nazgrim"
L["T851"] = "Tactics Thok the Bloodthirsty"
L["T852"] = "Tactics for Immerseus"
L["T853"] = "Tactics Paragons of the Klaxxi"
L["T856"] = "Tactics Kor'kron Dark Shaman"
L["T857"] = "Chi-Ji, The Red Crane"
L["T858"] = "Yu'lon, The Jade Serpent"
L["T859"] = "Niuzao, The Black Ox"
L["T860"] = "Xuen, The White Tiger"
L["T861"] = "Ordos, Fire-God of the Yaungol"
L["T864"] = "Tactics Iron Juggernaut"
L["T865"] = "Tactics Siegecrafter Blackfuse"
L["T866"] = "Tactics Norushen"
L["T867"] = "Tactics Sha of Pride"
L["T868"] = "Tactics Galakras"
L["T869"] = "Tactics Garrosh Hellscream"
L["T870"] = "Tactics Spoils of Pandaria"
L["T887"] = [=[Roltall launches 3 boulders (impact zone visible on ground). Avoid them and be aware that those boulders will come back in the same order.
When the 3rd boulder has returned Roltall summons a wave that pushes you away for 8 seconds.
The wave skill would only be a nuisance if Roltall had not thrown some burning slags before. The trick here is to stay aside a slag and never in front of one so as not to be repelled into them.]=]
L["T888"] = [=[Priorize hostile adds! Interrupt SPELL(150759). Heal friendly adds. 
Hostile ads will focus on allies who have been struck by SPELL(150745) ignoring taunts during that time.]=]
L["T889"] = [=[Tanks: Stay focused on boss and interrupt as many SPELL(150677) as possible.
Healer: After every 3rd stack of SPELL(150678) boss does AoE damage. Save Cooldowns for last 20%
DPS: Avoid flames (SPELL(150784)) and nuke down add when it spawned]=]
L["T89"] = [=[Phase 1: Boss does SPELL(87859) or SPELL(87861). Sometimes he blinks with aggro reset. nothing special about that phase.
 Phase 2: Boss stays in the middle. A SPELL(91398) appears which will rotate around the boss. ATTENTION: Always stay in move. Get out of the blue circles and AoE the adds down.]=]
L["T893"] = [=[Whenever adds spawn, kill them as fast as possible. Fire Elementals cast SPELL(149997). Interrupt that! 
Tank has to tank the rock elementals not facing the group.]=]
L["T90"] = [=[Helix plants bombs at random on the ground. Naturally, you shouldn't touch these.
 If a player gets a SPELL(88352) to him, try to get below the tree trunk so that you can avoid getting too much full damage.
 Sometimes the boss takes a player and runs against the wall. That player needs some extra heal after the crash.
 When the big guy dies, Helix himself will pester the players by jumping on their backs. Dps him down.]=]
L["T91"] = [=[Add: One has to control the harvest machine to dps the adds that will spawn at the forge. Objective: 2x SPELL(91734) 1x SPELL(91735) so that the adds wont reach the 'stairs'.
 Boss itself is tanked on the stairs so that the player who controls the add has enough room and the rest of the party does not suffer the fire damage from the adds.
SPELL(88481): Boss gets crazy and does damage to every player who stands near him. DD run away. Tank taunts him back after the overdrive.
SPELL(88495): Boss targets a player and creates a red circle under him. That player has to run away AFTER the red circle is visible. All the other melee stand back ( even tank ).
 When Boss HP reaches 40% he gets SPELL(91720) doing much more damage. Use Cooldowns and heroism/bloodlust/timewarp then.]=]
L["T92"] = [=[Bossphase: Boss is tanked normally. He sometimes charges a player. In later bossphases there are also adds but not in the first one.
 Addphase: Little vapors appear. Finish them quickly! Push the v  key so that you can see their life bars so that you can nuke them easier. The vapors will grow.
 The taller they are the more damage they make. The two phases switch and adds won't stop spawn. When boss reaches 10% he calles more vapors. Ignore them and dps the bown down.]=]
L["T93"] = "Cookie gets in a big pot. He throws with food. There is good food ( shines yellow ) and bad one ( shines green ). Eat the good food to get haste buff SPELL(92834) and avoid the green SPELL(92066) ( does little AoE damage )"
L["T95"] = [=[Vanessa is tanked normally. Two adds will spawn all the time. DPS them down quickly. Especially interrupt the blood mages.
 When Vanessa reaces 50% health all adds disappear (and her). On the side of the ship 5 cords will spawn. Every party member has to right click one of the cords.
 fter the tarzan-swing everything keeps going like before until she hits 25% health. Again cords will spawn. Afterwards no more adds will spawn just focus her down. When she dies you have to use the cords one last time.]=]
L["T959"] = "no tactics available yet"
L["T96"] = [=[Preliminary: Save Bloodlust/Heroism/Time Warp for 25% Boss HP. Healer dont try to heal everyone to full HP. Just keep all alive with around 40%-50%
SPELL(93581): Channeled spell wich does damage to on party member. Interrupt because no player will have full HP.
SPELL(93713): Removed with Patch 4.0.6.
SPELL(93423): He lifts everyone up and reduces their HP to 1%.
SPELL(93706): Instantly casts after Asphyxiate. He heals everyone including himself. Let One or Two Ticks go through because otherwise your tank would be a one hit.
 The Key to that fight is to name the persons that kick the special casts.]=]
L["T965"] = [=[Stay on the move to avoid zone damage as they rotate and move.
Healers beware of SPELL(165731) doing AoE dot.
SPELL(165731) inflicts significant damage on all targets that Ranjit passes through as he charges forward in a straight line.]=]
L["T966"] = [=[Don't allow boss to reach solar beam healing him with SPELL(154149) by standing in between.
Healer: SPELL(154135) does more damage each time it is cast.
Tank: Avoid SPELL(154132) wich strikes with left or right arm.]=]
L["T967"] = [=[Move away from Solar Flares or they trigger SPELL(153828). Defat Flares away from Piles of Ash or SPELL(169810) will revive them.
Tank has to stay in melee range of boss or he will SPELL(153898). Save Tank Cooldowns for SPELL(153794)]=]
L["T968"] = [=[Defeat Zealots quickly before they SPELL(153954) by dropping them off the edge of Skyreach.
Defeat Shield Constructs as fast as possible.
Move away from SPELL(154043) and avoid leading the destructive beam through your allies.
Interrupt SPELL(154396)
]=]
L["T97"] = "Easy Fight. Worgen adds will spawn, dps them down and change back to the boss. Decurse the party and that's it."
L["T971"] = "no tactics available yet"
L["T98"] = [=[Tank has to get out of SPELL(93687). Always kill the adds fast cause they will SPELL(93844) the boss. Tank has to react quickly cause new adds will spawn and try to slice your healer.
 Optional tactic: CC the two Adds and nuke down the boss as hard as possible ( trinkets, cooldowns, BL/HR/TW )
 Alternative method: Clear everything up to the boss, then have your tank pull him and use any available run speed increase to run him all the way down into the courtyard.
 This gives you a lot more space to move him around and out of the death and decay effects and also stops any adds spawning after the first two.]=]
L["T99"] = [=[SPELL(93617): ALL stay in motion! SPELL(93689): ALL stand still!  
SPELL(93527): stay in motion and don't get into the blue zones.]=]
L["TACTICS"] = "Tactics on "
L["TOOLTIP_ACHIEVEBUTTON"] = "Show Achievements"
L["TOOLTIP_ACHIEVEBUTTON_2"] = [=[Leftclick: post achievements related to boss
Rightclick: post achievements related to Instance]=]
L["TOOLTIP_LOOTBUTTON"] = "Show Loot"
L["TOOLTIP_TACTICBUTTON_1"] = "Post Tactics"
L["TOOLTIP_TACTICBUTTON_2"] = [=[Leftclick: post to channel
RightClick: select Channel]=]
L["UNDONE_ACHIEVEMENTS"] = "Achievements not yet done:"
L["WHISPER"] = "whisper"
L["WHISPER_MENU"] = "Whisper"
L["WHISPER_MODE"] = "Whisper Mode"
L["WHISPER_MODE_DESC"] = "If enabled and someone types !boss in partychannel you will whisper the tactic to that person instead sending it to the party channel."
L["WHISPER_TACTIC"] = "Send tactics to ..."
L["T1836"] = "T1836 The Curator"
L["T1979"] = "T1979 Zuraal the Ascended"
L["T1903"] = "T1903 Sisters of the moon"
L["T1986"] = "T1986 The Coven of Shivarra"
L["T1982"] = "T1982 Lura"
L["T1825"] = "T1825 Maiden of Virtue"
L["T1819"] = "T1819 Odyn"
L["T1981"] = "T1981 Viceroy Nezhar"
L["T1997"] = "T1997 Antoran High Command"
L["T1904"] = "T1904 Domatrax"
L["T1980"] = "T1980 Saprish"
L["T1897"] = "T1897 Maiden of Vigilance"
L["T1826"] = "T1826 Westfall Story"
L["T1827"] = "T1827 Beautiful Beast"
L["T1830"] = "T1830 Guarm"
L["T1820"] = "T1820 Wikker"
L["T2025"] = "T2025 Eonar the Life-Binder"
L["T1867"] = "T1867 Demonic Inquisiton"
L["T2031"] = "T2031 Argus the Unmaker"
L["T1878"] = "T1878 Mephistroth"
L["T1818"] = "T1818 Mana Devourer"
L["T1984"] = "T1984 Aggramar"
L["T1983"] = "T1983 Varimatrhas"
L["T2004"] = "T2004 Kingaroth"
L["T1906"] = "T1906 Trhasbite the Scornful"
L["T2009"] = "T2009 Imonar the Soulhunter"
L["T1985"] = "T1985 Portal Keeper Hasabel"
L["T1987"] = "T1987 Felhounds of Sargeras"
L["T1992"] = "T1992 Garothi"
L["T1898"] = "T1898 Kil'jaeden"
L["T1896"] = "T1896 The Desolate Host"
L["T1873"] = "T1873 Fallen Avatar"
L["T1856"] = "T1856 Harjatan"
L["T1905"] = "T1905 Agronox"
L["T1862"] = "T1862 Goroth"
L["T1829"] = "T1829 Helya"
L["T1861"] = "T1861 Mistress Sasszine"
L["T1817"] = "T1817 Shade of Medivh"
L["T1838"] = "T1838 Viz'aduum the watcher"
L["T1835"] = "T1835 Attumen"
L["T1837"] = "T1837 Moroes"

