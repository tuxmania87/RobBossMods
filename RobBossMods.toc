## Interface: 80000
## Title: RobBossMods
## Notes: Boss tactics for heroic dungeons
## Notes-deDE: Boss Erklaerungen fuer  heroische Instanzen
## Author: Lestá
## SavedVariables: rbm_elang, rbmDB, rbm_icon
## X-Curse-Packaged-Version: v1.11a
## X-Curse-Project-Name: RobBossMods
## X-Curse-Project-ID: robbossmods1
## X-Curse-Repository-ID: wow/robbossmods1/mainline


embeds.xml

Locales\enUS.lua
Locales\deDE.lua
Locales\frFR.lua
Locales\ruRU.lua
RobBossMods.lua

